# MPAndroidChart 散点图 代码展示

以下只展示关键部分

#### 工程

**build.gradle**

```
repositories {
    maven { url 'https://maven.aliyun.com/repository/google' }
    maven { url 'https://maven.aliyun.com/repository/jcenter' }
    maven { url "https://maven.aliyun.com/repository/central" }
    maven { url "https://maven.aliyun.com/repository/gradle-plugin" }
}
```

#### gradle 依赖引用

```javascript
dependencies {
    implementation 'com.github.testpress:MPAndroidChart:v3.0.0-beta2'
}
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin13_4

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import com.github.testpress.mikephil.charting.charts.BubbleChart
import com.github.testpress.mikephil.charting.data.BubbleData
import com.github.testpress.mikephil.charting.data.BubbleDataSet
import com.github.testpress.mikephil.charting.data.BubbleEntry
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity13_4"
    }

    private var toolbar: PluginToolBar? = null
    private var bean: PluginActivityBean? = null
    private var bubbleChart : BubbleChart?= null
    private var chartContainer: FrameLayout ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        toolbar = findViewById(R.id.toolbar)
        chartContainer = findViewById(R.id.chartContainer)
        initChartView()
        initChartData()
    }

    private fun initChartView() {
        that?.let {
            bubbleChart = BubbleChart(it)
            bubbleChart?.apply {
                DensityUtils.getScreenHeight()?.let { screenHeight ->
                    val lp = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,screenHeight / 2 )
                    lp.topMargin = DensityUtils.dip2px(20f)
                    layoutParams = lp
                }
            }
            chartContainer?.addView(bubbleChart)
        }
    }

    private fun f(x:Float):Float {
        return 0.5f * x + 5f
    }

    private fun initChartData() {
        val count = 12
        val range = 10

        val values = arrayListOf<BubbleEntry>()
        for (x in 2 until count) {
            values.add(BubbleEntry(x.toFloat(), f(x.toFloat()) + (Math.random() * range).toFloat(), 10f))
        }
        val bubbleDataSet = BubbleDataSet(values, "Label")
        bubbleDataSet.setDrawValues(false)
        val bubbleData = BubbleData(bubbleDataSet)
        bubbleChart?.data = bubbleData
    }
}

```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >   

    <FrameLayout
        android:id="@+id/chartContainer"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

