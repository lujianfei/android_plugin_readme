# MPAndroidChart 条形图 代码展示

以下只展示关键部分

#### 工程

**build.gradle**

```
repositories {
    maven { url 'https://maven.aliyun.com/repository/google' }
    maven { url 'https://maven.aliyun.com/repository/jcenter' }
    maven { url "https://maven.aliyun.com/repository/central" }
    maven { url "https://maven.aliyun.com/repository/gradle-plugin" }
}
```

#### gradle 依赖引用

```javascript
dependencies {
    implementation 'com.github.testpress:MPAndroidChart:v3.0.0-beta2'
}
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin13_2

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import com.github.testpress.mikephil.charting.charts.BarChart
import com.github.testpress.mikephil.charting.charts.LineChart
import com.github.testpress.mikephil.charting.data.*
import com.github.testpress.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.testpress.mikephil.charting.interfaces.datasets.ILineDataSet
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity13_2"
    }
   
    private var barChart : BarChart?= null
    private var chartContainer: FrameLayout ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        toolbar = findViewById(R.id.toolbar)
        chartContainer = findViewById(R.id.chartContainer)
        initChartView()
        initChartData()
    }
    
    private fun initChartView() {
        that?.let {
            barChart = BarChart(it)
            barChart?.apply {
                DensityUtils.getScreenHeight()?.let { screenHeight ->
                    val lp = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,screenHeight / 2 )
                    lp.topMargin = DensityUtils.dip2px(20f)
                    layoutParams = lp
                }
            }
            chartContainer?.addView(barChart)
        }
    }

    private fun initChartData() {
        val values = arrayListOf<BarEntry>()
        values.add(BarEntry(5f, 50f))
        values.add(BarEntry(10f, 66f))
        values.add(BarEntry(15f, 120f))
        values.add(BarEntry(20f, 30f))
        values.add(BarEntry(35f, 10f))
        values.add(BarEntry(40f, 110f))
        values.add(BarEntry(45f, 30f))
        values.add(BarEntry(50f, 160f))
        values.add(BarEntry(100f, 30f))

        val mLineDataSet = BarDataSet(values, "Label")
        val dataSets = arrayListOf<IBarDataSet>()
        //添加数据集
        dataSets.add(mLineDataSet)

        val mLineData = BarData(dataSets)
        barChart?.data = mLineData
    }    
}

```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >   

    <FrameLayout
        android:id="@+id/chartContainer"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

