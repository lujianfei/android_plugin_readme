# MPAndroidChart 饼状图 代码展示

以下只展示关键部分

#### 工程

**build.gradle**

```
repositories {
    maven { url 'https://maven.aliyun.com/repository/google' }
    maven { url 'https://maven.aliyun.com/repository/jcenter' }
    maven { url "https://maven.aliyun.com/repository/central" }
    maven { url "https://maven.aliyun.com/repository/gradle-plugin" }
}
```

#### gradle 依赖引用

```javascript
dependencies {
    implementation 'com.github.testpress:MPAndroidChart:v3.0.0-beta2'
}
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin13_3

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import com.github.testpress.mikephil.charting.charts.PieChart
import com.github.testpress.mikephil.charting.data.PieData
import com.github.testpress.mikephil.charting.data.PieDataSet
import com.github.testpress.mikephil.charting.data.PieEntry
import com.github.testpress.mikephil.charting.formatter.PercentFormatter
import com.github.testpress.mikephil.charting.utils.ColorTemplate
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity13_2"
    }
   
    private var pieChart : PieChart?= null
    private var chartContainer: FrameLayout ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        toolbar = findViewById(R.id.toolbar)
        chartContainer = findViewById(R.id.chartContainer)
        initChartView()
        initChartData()
    }

    private fun initChartView() {
        that?.let {
            pieChart = PieChart(it)
            pieChart?.apply {
                DensityUtils.getScreenHeight()?.let { screenHeight ->
                    val lp = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,screenHeight / 2 )
                    lp.topMargin = DensityUtils.dip2px(20f)
                    layoutParams = lp
                }
            }
            chartContainer?.addView(pieChart)
        }
    }

    private fun initChartData() {
        val values = arrayListOf<PieEntry>()
        values.add(PieEntry(40f, "优秀"))
        values.add(PieEntry(20f, "满分"))
        values.add(PieEntry(30f, "及格"))
        values.add(PieEntry(10f, "不及格"))

        //数据和颜色
        val dataColors = arrayListOf<Int>()
        for (c in ColorTemplate.VORDIPLOM_COLORS) dataColors.add(c)
        for (c in ColorTemplate.JOYFUL_COLORS) dataColors.add(c)
        for (c in ColorTemplate.COLORFUL_COLORS) dataColors.add(c)
        for (c in ColorTemplate.LIBERTY_COLORS) dataColors.add(c)
        for (c in ColorTemplate.PASTEL_COLORS) dataColors.add(c)
        dataColors.add(ColorTemplate.getHoloBlue())
        val mPieDataSet = PieDataSet(values, "Label")
        mPieDataSet.apply {
            valueFormatter = PercentFormatter()
            colors = dataColors
            valueTextColor = Color.BLACK // 设置百分比字体颜色
            valueTextSize = 20f // 设置百分比字体大小
        }
        val mPieData = PieData(mPieDataSet)
        pieChart?.setEntryLabelColor(Color.BLACK) // 设置图表扇形文字颜色
        pieChart?.data = mPieData
    }
}

```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >   

    <FrameLayout
        android:id="@+id/chartContainer"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

