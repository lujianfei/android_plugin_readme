# Tensorflow 模型转 Android 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
    // 加入tensorflow 依赖
    implementation 'org.tensorflow:tensorflow-android:1.13.1'
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    ...

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:padding="10dp"
        android:orientation="vertical">
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="使用 Tensorflow 导出的模型计算 a x b"/>
        
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="horizontal">
            <EditText
                android:id="@+id/editA"
                android:layout_weight="1"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:inputType="number"
                android:textSize="20sp"
                android:text="3"/>
            
            <TextView
                android:layout_weight="1"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="20sp"
                android:text="X"/>
            <EditText
                android:id="@+id/editB"
                android:layout_weight="1"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:inputType="number"
                android:textSize="20sp"
                android:text="4"/>

            <TextView
                android:layout_weight="1"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:textSize="20sp"
                android:gravity="center"
                android:text="="/>

            <TextView
                android:id="@+id/txtResult"
                android:layout_weight="1"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:textSize="20sp"
                android:gravity="center"
                android:text=""/>
        </LinearLayout>
        <Button
            android:id="@+id/bt_test"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"
            android:text="基于 Tensorflow 模型计算乘法"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin12_1

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import org.tensorflow.contrib.android.TensorFlowInferenceInterface

/**
 * keras 模型转 Tensorflow
 * https://github.com/amir-abdi/keras_to_tensorflow
 */

class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
        init {
            System.loadLibrary("tensorflow_inference")
        }
        private const val MODEL_PATH = "file:///android_asset/model.pb"
        private const val INPUT_A = "a"
        private const val INPUT_B = "b"
        private const val OUTPUT_NAME = "result"
    }

    ...
    
    private var tf:TensorFlowInferenceInterface ?= null
    private var bt_test:Button ?= null
    private var editA:EditText ?= null
    private var editB:EditText ?= null
    private var txtResult:TextView ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        bt_test = findViewById(R.id.bt_test)
        editA = findViewById(R.id.editA)
        editB = findViewById(R.id.editB)
        txtResult = findViewById(R.id.txtResult)                
        tf = TensorFlowInferenceInterface(that?.assets,MODEL_PATH)
    }

    ...

    override fun initEvent() {        
        bt_test?.setOnClickListener {
            predict()
        }
    }

    private fun predict() {
        if (editA?.text?.isEmpty() == true) {
            editA?.error = "数字1不能为空"
            return
        }
        if (editB?.text?.isEmpty() == true) {
            editB?.error = "数字2不能为空"
            return
        }
        // 传入参数到模型
        tf?.feed(INPUT_A, intArrayOf(editA?.text.toString().toInt()), 1, 1) // 1x1 大小
        tf?.feed(INPUT_B, intArrayOf(editB?.text.toString().toInt()), 1, 1) // 1x1 大小
        // 运行计算输出值
        tf?.run(arrayOf(OUTPUT_NAME))
        val result = intArrayOf(0)
        // 获取输出值
        tf?.fetch(OUTPUT_NAME,result)
        txtResult?.text = "${result[0]}"
    }

    ...
}

```



### 对应的 Python 代码

模型的创建以及保存

```python
# -*- coding: utf-8 -*-
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.python.framework import graph_util


with tf.Session(graph=tf.Graph()) as sess:
    a = tf.placeholder(tf.int32, name='a') # 入参 a
    b = tf.placeholder(tf.int32, name='b') # 入参 b
    result = tf.multiply(a, b, name='result') # 出参 result
 
    sess.run(tf.global_variables_initializer())
 
    # convert_variables_to_constants 需要指定 output_node_names，list()，可以多个
    constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph_def, ['result']) # 出参 result
 
    # 测试 result
    feed_dict = {a: [5], b: [6]}
    print('result = ',sess.run(result, feed_dict))
 
    # 写入序列化的 PB 文件
    with tf.gfile.FastGFile('model.pb', mode='wb') as f:
        f.write(constant_graph.SerializeToString())

```

输出内容 

```
result =  [30]
```



模型的加载及使用

```python
# -*- coding: utf-8 -*-
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.python.platform import gfile


sess = tf.Session()
with gfile.FastGFile('model.pb', 'rb') as f:
    graph_def = tf.GraphDef()
    graph_def.ParseFromString(f.read())
    # print(graph_def)
    sess.graph.as_default()
    tf.import_graph_def(graph_def, name='') # 导入计算图
 
# 需要有一个初始化的过程    
sess.run(tf.global_variables_initializer())
# 输入
a = sess.graph.get_tensor_by_name('a:0')
b = sess.graph.get_tensor_by_name('b:0')
 
op = sess.graph.get_tensor_by_name('result:0')
 
ret = sess.run(op,  feed_dict={a: [5], b: [6]})
print('result = ',ret)
```

输出内容

```
result =  [30]
```

