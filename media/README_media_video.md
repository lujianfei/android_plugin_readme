# MediaRecorder / MediaPlayer 录像播放 代码展示

这个 Demo 使用 MediaRecorder 录像，使用 MediaPlayer 播放录制好的视频

以下只展示关键部分

##### fragment_video.xml （录像界面）

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">
    <LinearLayout
        android:id="@+id/controlPanel"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:orientation="horizontal">
        <Button
            android:id="@+id/btWavList"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="录像文件列表"/>
        <Button
            android:id="@+id/btStartRecording"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_weight="1"
            android:text="开始录像"/>
    </LinearLayout>
    <SurfaceView
        android:id="@+id/surfaceview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_above="@id/controlPanel"/>
</RelativeLayout>
```

##### VideoFragment（录像界面）

```kotlin
package com.lujianfei.plugin9_3.fragment

import android.Manifest
import android.content.DialogInterface
import android.content.pm.PackageManager
import android.util.Log
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.widget.Button
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.lujianfei.plugin9_3.R
import com.lujianfei.plugin9_3.base.BaseFragment
import com.lujianfei.plugin9_3.contract.MainContract
import com.lujianfei.plugin9_3.contract.VideoContract
import com.lujianfei.plugin9_3.dialog.CustomDialog
import com.lujianfei.plugin9_3.presenter.VideoPresenter

/**
 *@date     创建时间:2020/7/1
 *@name     作者:陆键霏
 *@describe 描述:
 */
class VideoFragment: BaseFragment(), VideoContract.View {

    companion object {
        private const val TAG = "VideoFragment"
        //申请权限
        private const val REQUEST_PERMISSION = 101
    }
    private val mPresenter by lazy { VideoPresenter(this) }
    private var btStartRecording:Button ? = null
    private var btWavList:Button ? = null
    private var surfaceview:SurfaceView ?= null
    
    private val permissions = arrayOf(
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.CAMERA)
    
    private var mainView: MainContract.View ?= null
    fun setMainView(mainView: MainContract.View): VideoFragment {
        this.mainView = mainView
        return this
    }
    
    override fun resouceId(): Int = R.layout.fragment_video
    
    override fun initView() {
        btStartRecording = findViewById(R.id.btStartRecording)
        btWavList = findViewById(R.id.btWavList)
        surfaceview = findViewById(R.id.surfaceview)
        if (ActivityCompat.checkSelfPermission(context,permissions[0]) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(context,permissions[1]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.shouldShowRequestPermissionRationale(activity,permissions[0])
            ActivityCompat.shouldShowRequestPermissionRationale(activity,permissions[1])
            requestPermissions(permissions, REQUEST_PERMISSION)    
        }
    }

    override fun initData() {
    }

    override fun initEvent() {
        btStartRecording?.isSelected = true
        btStartRecording?.setOnClickListener {
            if (btStartRecording?.isSelected == true) {
                mPresenter.startRecording()
            } else {
                mPresenter.stopRecording()
            }
        }
        btWavList?.setOnClickListener { 
            mainView?.switchToWavList()
        }
        surfaceview?.holder?.addCallback(object : SurfaceHolder.Callback{
            override fun surfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
            }

            override fun surfaceDestroyed(holder: SurfaceHolder?) {
                mPresenter.stopPreview()
            }

            override fun surfaceCreated(holder: SurfaceHolder?) {
                var permission = true
                permissions.forEach {
                    if (ContextCompat.checkSelfPermission(context,it) != PackageManager.PERMISSION_GRANTED) {
                        permission = false
                    }
                }
                if (permission) mPresenter.startPreview()
            }
        })
    }

    override fun updateTitle() {
        context?.let {context->
            val string = context.resources?.getString(R.string.app_name)
            string?.let {
                mainView?.updateTitle(string)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d(TAG,"onRequestPermissionsResult requestCode = $requestCode ")
        when(requestCode) {
            REQUEST_PERMISSION-> {
                var permission = true
                grantResults.forEach { 
                    if (it != PackageManager.PERMISSION_GRANTED) {
                        permission = false
                    }
                }
                if (permission) {
                    mPresenter.startPreview()
                } else {
                    CustomDialog.Builder(context)
                            .setTitle("提示")
                            .setMessage("【相机】和【录音】权限未开启，是否前往开启")
                            .setPositiveButton("是", DialogInterface.OnClickListener { dialog, which -> 
                                mPresenter.goSettings()
                            })
                            .setNegativeButton("否", DialogInterface.OnClickListener { dialog, which ->  })
                            .create()
                            .show()
                }
            }
        }
    }

    override fun setButton(s: String, select: Boolean) {
        btStartRecording?.text  = s
        btStartRecording?.isSelected = select
    }

    override fun getSurfaceWidth(): Int {
        return surfaceview?.width?:0
    }

    override fun getSurfaceHeight(): Int {
        return surfaceview?.height?:0
    }

    override fun getPreviewHolder(): SurfaceHolder? {
        return surfaceview?.holder
    }
}
```

##### 录像界面对应的 Presenter

```kotlin
package com.lujianfei.plugin9_3.presenter

import android.hardware.Camera
import android.media.MediaRecorder
import com.lujianfei.plugin9_3.contract.VideoContract
import com.lujianfei.plugin9_3.ext.toast
import com.lujianfei.plugin9_3.utils.FileUtil
import java.io.File

/**
 *@date     创建时间:2020/7/15
 *@name     作者:陆键霏
 *@describe 描述:
 */
class VideoPresenter(view: VideoContract.View) : VideoContract.Presenter(view) {

    private var isRecording: Boolean = false
    
    private var mediaRecorder:MediaRecorder ?= null
    
    private var outputFilePath = ""

    private var camera:Camera ?= null
    
    override fun startPreview() {
        kotlin.runCatching {
            camera = Camera.open()
            camera?.let {camera->
                val parameters = camera.parameters
                parameters.setPreviewSize(320,240)
                parameters.setPictureSize(320,240)
                camera.parameters = parameters
                camera.setDisplayOrientation(90)
                camera.setPreviewDisplay(mView?.getPreviewHolder())
                camera.startPreview()
            }
        }.onFailure {
            toast("需要开启录音和相机权限")
        }
    }

    override fun stopPreview() {
        camera?.stopPreview()
        camera?.release()
        camera = null
    }

    override fun startRecording() {
        val outputFile = File(FileUtil.getMediaFileDir(),"${System.currentTimeMillis()}.mp4")
        outputFilePath = outputFile.absolutePath
        kotlin.runCatching {
            mediaRecorder = MediaRecorder()
            mediaRecorder?.apply {
                camera?.unlock()
                setCamera(camera)
                setOrientationHint(90)
                setAudioSource(MediaRecorder.AudioSource.MIC)
                setVideoSource(MediaRecorder.VideoSource.CAMERA)
                setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
                setVideoSize(320, 240)
                setVideoFrameRate(5);
                setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                setVideoEncoder(MediaRecorder.VideoEncoder.H264);
                setOutputFile(outputFile.absolutePath)
                prepare()
                start()
            }
            isRecording = true
            mView?.setButton("停止录像",false)
        }.onFailure {
            toast("需要开启录音和相机权限")
            isRecording = false
            mView?.setButton("开始录像",true)
        }
    }

    override fun stopRecording() {
        kotlin.runCatching {
            mediaRecorder?.apply {
                stop()
                release()
            }
            toast("保存位置：$outputFilePath")
        }.onFailure { 
            
        }
        mView?.setButton("开始录像",true)
        mediaRecorder = null
    }


}
```

##### VideoPlayerFragment (播放界面)

```kotlin
package com.lujianfei.plugin9_3.fragment

import android.view.SurfaceHolder
import android.view.SurfaceView
import android.view.View
import android.widget.Button
import com.lujianfei.plugin9_3.R
import com.lujianfei.plugin9_3.base.BaseFragment
import com.lujianfei.plugin9_3.contract.MainContract
import com.lujianfei.plugin9_3.contract.VideoPlayerContract
import com.lujianfei.plugin9_3.presenter.VideoPlayerPresenter

/**
 *@date     创建时间:2020/7/15
 *@name     作者:陆键霏
 *@describe 描述:
 */
class VideoPlayerFragment: BaseFragment(), VideoPlayerContract.View {

    private val mPresenter by lazy { VideoPlayerPresenter(this) }
    private var surfaceview:SurfaceView ?= null
    private var btPlay:Button ?= null
    private var btStop:Button ?= null
    var playPath = ""

    private var mainView: MainContract.View ?= null
    fun setMainView(mainView: MainContract.View): VideoPlayerFragment {
        this.mainView = mainView
        return this
    }
    
    override fun resouceId(): Int = R.layout.fragment_video_player

    override fun initView() {
        surfaceview = findViewById(R.id.surfaceview)
        btPlay = findViewById(R.id.btPlay)
        btStop = findViewById(R.id.btStop)
    }

    override fun initData() {
    }

    override fun initEvent() {
        btPlay?.isSelected = true
        btPlay?.setOnClickListener {
            if (btPlay?.isSelected == true) {
                mPresenter.startPlay()
            } else {
                mPresenter.pausePlay()
            }
        }
        btStop?.setOnClickListener { 
            mPresenter.stopPlay()
        }
    }

    override fun updateTitle() {
        mainView?.updateTitle("视频播放")
    }

    override fun getSurface(): SurfaceHolder? {
        return surfaceview?.holder
    }

    override fun getDataSource(): String?  = playPath
    
    override fun setButton(s: String, select: Boolean) {
        btPlay?.text = s
        btPlay?.isSelected = select
    }

    fun stopPlay() {
        mPresenter.stopPlay()
    }
    
    fun resetSurfaceView() {
        surfaceview?.visibility = View.GONE
        surfaceview?.visibility = View.VISIBLE
    }
}
```

##### 播放界面对应的布局

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent" 
    android:layout_height="match_parent">

    <SurfaceView
        android:id="@+id/surfaceview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
    
    <Button
        android:id="@+id/btPlay"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:layout_centerHorizontal="true"
        android:text="开始播放"/>
</RelativeLayout>
```



##### 播放界面对应的 Presenter

```kotlin
package com.lujianfei.plugin9_3.presenter

import android.media.MediaPlayer
import com.lujianfei.plugin9_3.contract.VideoPlayerContract

/**
 *@date     创建时间:2020/7/15
 *@name     作者:陆键霏
 *@describe 描述:
 */
class VideoPlayerPresenter(view: VideoPlayerContract.View) : VideoPlayerContract.Presenter(view) {

    private var isMediaPlaying = false
    private var mediaPlayer:MediaPlayer ?= null
    
    override fun startPlay() {
        if(mediaPlayer != null) {
            mediaPlayer?.start()
            mView?.setButton("暂停", false)
            return
        }
        
        if (isMediaPlaying) return
        mediaPlayer = MediaPlayer()
        mediaPlayer?.setOnPreparedListener { 
            it.start()
        }
        mediaPlayer?.setOnCompletionListener { 
            if (isMediaPlaying) {
                stopPlay()
            }
        }
        mediaPlayer?.apply {
            setDataSource(mView?.getDataSource())
            setDisplay(mView?.getSurface())
            prepareAsync()
        }
        mView?.setButton("暂停", false)
        isMediaPlaying = true
    }

    override fun pausePlay() {
        mediaPlayer?.pause()
        mView?.setButton("播放", true)
    }
    
    override fun stopPlay() {
        mediaPlayer?.apply {
            stop()
            release()
        }
        mView?.setButton("播放", true)
        mediaPlayer = null
        isMediaPlaying = false
    }
   
}
```

