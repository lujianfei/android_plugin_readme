# MediaPlayer 录音播放 代码展示

以下只展示关键部分

##### fragment_audio.xml （录音界面）

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">

        <Button
            android:id="@+id/btWavList"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_gravity="center_horizontal"
            android:text="Wav 录音文件列表"/>
    
        <Button
            android:id="@+id/btStartRecording"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_gravity="center_horizontal"
            android:layout_marginTop="50dp"
            android:text="开始录音"/>
    
</LinearLayout>
```

##### AudioFragment （录音界面）

```kotlin
package com.lujianfei.plugin9_1.fragment

import android.Manifest
import android.util.Log
import android.widget.Button
import com.lujianfei.plugin9_1.R
import com.lujianfei.plugin9_1.base.BaseFragment
import com.lujianfei.plugin9_1.contract.MainContract
import com.lujianfei.plugin9_1.ext.toast
import com.lujianfei.plugin9_1.utils.AudioRecorder
import com.lujianfei.plugin9_1.utils.FileUtil
import java.text.SimpleDateFormat
import java.util.*

/**
 *@date     创建时间:2020/7/1
 *@name     作者:陆键霏
 *@describe 描述:
 */
class AudioFragment: BaseFragment() {

    companion object {
        private const val TAG = "AudioFragment"
        //申请录音权限
        private val GET_RECODE_AUDIO = 1
    }
    
    private var btStartRecording:Button ? = null
    private var btWavList:Button ? = null
    var fileName = ""
    
    private val permissions = arrayOf(
            Manifest.permission.RECORD_AUDIO)
    
    private var audioRecorder:AudioRecorder?= null

    private var mainView: MainContract.View ?= null
    fun setMainView(mainView: MainContract.View):AudioFragment {
        this.mainView = mainView
        return this
    }
    
    override fun resouceId(): Int = R.layout.fragment_audio
    
    override fun initView() {
        audioRecorder = AudioRecorder.INSTANCE
        btStartRecording = findViewById(R.id.btStartRecording)
        btWavList = findViewById(R.id.btWavList)
        requestPermissions(permissions, GET_RECODE_AUDIO)
    }

    override fun initData() {
    }

    override fun initEvent() {
        btStartRecording?.setOnClickListener {
            kotlin.runCatching { 
                if (audioRecorder!!.getStatus() == AudioRecorder.Status.STATUS_NO_READY) {
                    //初始化录音
                    fileName =
                            SimpleDateFormat("yyyyMMddhhmmss").format(Date())
                    audioRecorder?.createDefaultAudio(fileName)
                    audioRecorder?.startRecord(context ,null)
                    btStartRecording?.text = "停止录音"
                } else {
                    toast("已保存到 ${FileUtil.getWavFileAbsolutePath(context,fileName)}")
                    //停止录音
                    audioRecorder?.stopRecord(context)
                    btStartRecording?.text = "开始录音"
                }
            }.onFailure{e->
                toast(content = "${e.message}")
            }
        }
        btWavList?.setOnClickListener { 
            mainView?.switchToWavList()
        }
    }

    override fun updateTitle() {
        context?.let {context->
            val string = context.resources?.getString(R.string.app_name)
            string?.let {
                mainView?.updateTitle(string)
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.d(TAG,"onRequestPermissionsResult requestCode = $requestCode ")
    }
}
```

##### fragment_wav_list.xml（Wav 列表播放界面）

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">
    <ListView
        android:id="@+id/listview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        />
</LinearLayout>
```



##### WavListFragment （Wav 文件列表界面）

```kotlin
package com.lujianfei.plugin9_1.fragment

import android.app.AlertDialog
import android.content.DialogInterface
import android.media.MediaPlayer
import android.util.Log
import android.widget.ListView
import com.lujianfei.plugin9_1.R
import com.lujianfei.plugin9_1.adapter.WavListAdapter
import com.lujianfei.plugin9_1.base.BaseFragment
import com.lujianfei.plugin9_1.contract.MainContract
import com.lujianfei.plugin9_1.dialog.CustomDialog
import com.lujianfei.plugin9_1.utils.FileUtil
import java.io.File

/**
 *@date     创建时间:2020/7/1
 *@name     作者:陆键霏
 *@describe 描述:
 */
class WavListFragment: BaseFragment() {
    
    private var listview:ListView ?= null
    private val adapter by lazy { WavListAdapter() }
    
    private var mainView: MainContract.View ?= null
    fun setMainView(mainView: MainContract.View):WavListFragment {
        this.mainView = mainView
        return this
    }
    
    override fun resouceId(): Int = R.layout.fragment_wav_list

    override fun initView() {
        listview = findViewById(R.id.listview)
        listview?.adapter = adapter
    }

    override fun initData() {
        context?.let {context->
            adapter.setData(FileUtil.getWavFiles(context))
        }
        updateTitle()
    }

    override fun initEvent() {
        listview?.setOnItemClickListener { _, _, position, _ ->
            play(adapter.getItem(position))
        }
        listview?.setOnItemLongClickListener { _, _, position, _ ->
            CustomDialog.Builder(context).setTitle("提示")
                    .setMessage("确认要删除")
                    .setPositiveButton("确认",DialogInterface.OnClickListener { dialog, which ->
                        val item = adapter.getItem(position)
                        item.delete()
                        initData()
                    })
                    .setNegativeButton("取消",DialogInterface.OnClickListener { dialog, which ->  })
                    .create()
                    .show()
            true
        }
    }

    private fun play(recordedFile: File) {
        val recordedSong = MediaPlayer()
        try {
            recordedSong.setDataSource(recordedFile.absolutePath)
            recordedSong.prepareAsync()
            recordedSong.setOnPreparedListener {
                it.start()
            }
        } catch (e: Exception) {
            Log.d("AudioTest","play $e")
        }
    }
    
    override fun updateTitle() {
        context?.let {context->
            val string = context.resources?.getString(R.string.wav_list_title)
            string?.let {
                mainView?.updateTitle(string)
            }
        }
    }
}
```

##### adapter_wav_item.xml （录音列表 Adapter xml）

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:padding="10dp">

    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        />
</LinearLayout>
```

##### WavListAdapter（录音列表 Adapter）

```kotlin
package com.lujianfei.plugin9_1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.lujianfei.plugin9_1.R
import java.io.File

/**
 *@date     创建时间:2020/7/1
 *@name     作者:陆键霏
 *@describe 描述:
 */
class WavListAdapter: BaseAdapter() {
    
    private val files = arrayListOf<File>()
    
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var convertViewTmp = convertView
        var viewHolder:ViewHolder ?= null
        if (convertViewTmp == null) {
            convertViewTmp = LayoutInflater.from(parent?.context).inflate(R.layout.adapter_wav_item, parent, false)
            viewHolder = ViewHolder()
            viewHolder.title = convertViewTmp?.findViewById(R.id.title)
            convertViewTmp?.tag = viewHolder
        } else {
            viewHolder = convertViewTmp.tag as ViewHolder
        }

        val item = getItem(position)
        viewHolder.title?.text = item.name
        return convertViewTmp!!
    }

    override fun getItem(position: Int): File = files[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = files.size
    
    fun setData(files:List<File>) {
        this.files.clear()
        if (!files.isNullOrEmpty()) {
            this.files.addAll(files)
        }
        notifyDataSetChanged()
    }
    
    class ViewHolder {
        var title:TextView ?= null
    }
}
```
##### AudioPlayerFragment.kt (音频播放界面)

```kotlin
package com.lujianfei.plugin9_1.fragment

import android.widget.Button
import android.widget.TextView
import com.lujianfei.plugin9_1.R
import com.lujianfei.plugin9_1.base.BaseFragment
import com.lujianfei.plugin9_1.contract.AudioPlayerContract
import com.lujianfei.plugin9_1.contract.MainContract
import com.lujianfei.plugin9_1.presenter.AudioPlayerPresenter

/**
 *@date     创建时间:2020/7/16
 *@name     作者:陆键霏
 *@describe 描述:
 */
class AudioPlayerFragment: BaseFragment(), AudioPlayerContract.View {

    private val mPresenter by lazy { AudioPlayerPresenter(this) }
    
    private var txtInfo:TextView ?= null
    private var btPlay:Button ?= null
    private var btStop:Button ?= null
    var playSource = ""
    set(value) {
        field = value
        initData()
    }
    private var mainView: MainContract.View ?= null
    fun setMainView(mainView: MainContract.View):AudioPlayerFragment {
        this.mainView = mainView
        return this
    }
    
    override fun resouceId(): Int = R.layout.fragment_audio_player

    override fun initView() {
        txtInfo = findViewById(R.id.txtInfo)
        btPlay = findViewById(R.id.btPlay)
        btStop = findViewById(R.id.btStop)
    }

    override fun initData() {
        if (playSource.isNullOrEmpty()) return
        mPresenter.showDuration()
    }

    override fun initEvent() {
        btPlay?.isSelected = true
        btPlay?.setOnClickListener {
            if (it.isSelected) {
                mPresenter.startPlay()
            } else {
                mPresenter.pausePlay()
            }
        }
        btStop?.setOnClickListener {
            mPresenter.stopPlay()
        }
        mPresenter.onProgressCallback = {currentPos,duration->
            txtInfo?.text = "播放进度：${currentPos}/${duration}"
        }
    }

    override fun updateTitle() {
        mainView?.updateTitle("音频播放")
    }

    override fun playPath(): String? {
        return playSource
    }

    override fun setPlayButton(s: String, select: Boolean) {
        btPlay?.text = s
        btPlay?.isSelected = select
    }

    fun stopPlay() {
        mPresenter.stopPlay()
    }
}
```

##### 对应的 Presenter

```kotlin
package com.lujianfei.plugin9_1.presenter

import android.media.MediaPlayer
import android.os.Handler
import android.util.Log
import com.lujianfei.plugin9_1.contract.AudioPlayerContract
import com.lujianfei.plugin9_1.utils.FileUtil
import java.io.File

/**
 *@date     创建时间:2020/7/16
 *@name     作者:陆键霏
 *@describe 描述:
 */
class AudioPlayerPresenter(view: AudioPlayerContract.View) : AudioPlayerContract.Presenter(view) {

    companion object {
        const val TAG = "AudioPlayerPresenter"
    }

    private var isPlayerPlaying: Boolean = false
    private var mediaPlayer : MediaPlayer ?= null
    private val mHandler = Handler()
    var onProgressCallback:((Int,Int)->Unit) ?= null
    
    override fun startPlay() {
        if (mediaPlayer!=null) {
            Log.d(TAG,"mediaPlayer?.start()")
            mediaPlayer?.start()
            isPlayerPlaying = true
            mView?.setPlayButton("暂停", false)
            return
        }
        if (isPlayerPlaying) return
        mediaPlayer = MediaPlayer()
        kotlin.runCatching {
            mediaPlayer?.apply {
                setDataSource(mView?.playPath())
                setOnPreparedListener {
                    Log.d(TAG,"setOnPreparedListener")
                    it.start()
                }
                setOnCompletionListener {
                    Log.d(TAG,"setOnCompletionListener")
                    if (isPlayerPlaying) {
                        stopPlay()
                    }
                }
                prepareAsync()
                mHandler.removeCallbacks(mProgressTask)
                mHandler.post(mProgressTask)
            }
            isPlayerPlaying = true
            mView?.setPlayButton("暂停", false)
        }.onFailure { 
            Log.d(TAG,"startPlay onFailure $it")
            isPlayerPlaying = false
        }
    }

    override fun pausePlay() {
        Log.d(TAG,"pausePlay")
        mediaPlayer?.pause()
        mView?.setPlayButton("播放", true)
        isPlayerPlaying = false
    }

    override fun stopPlay() {
        mediaPlayer?.duration?.let { onProgressCallback?.invoke(0, it) }
        mediaPlayer?.apply {
            stop()
            release()
        }
        mHandler.post(mProgressTask)
        mediaPlayer = null
        mView?.setPlayButton("播放", true)
        isPlayerPlaying = false
    }

    override fun showDuration() {
        mView?.playPath()?.let {playPath->
            val file = File(playPath)
            if (!file.exists()) return
            val wavLength = FileUtil.getWavLength(file.readBytes())
            onProgressCallback?.invoke(0,wavLength.toInt())
        }
    }

    private val mProgressTask = ProgressTask()
    
    inner class ProgressTask:Runnable {
        override fun run() {
            mediaPlayer?.currentPosition?.let {currentPosition->
                mediaPlayer?.duration?.let {duration->
                    onProgressCallback?.invoke(currentPosition,duration)
                }
            }
            mHandler.postDelayed(this, 100)
        }
    }
}
```





##### 核心工具类

##### AudioRecorder

```kotlin
package com.lujianfei.plugin9_1.utils

import android.content.Context
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.text.TextUtils
import android.util.Log
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.util.*

/**
 *@date     创建时间:2020/7/1
 *@name     作者:陆键霏
 *@describe 描述:
 */
class AudioRecorder private constructor(){
    
    companion object {
        val INSTANCE =  AudioRecorder()
    }
    
    //音频输入-麦克风
    private val AUDIO_INPUT = MediaRecorder.AudioSource.MIC

    //采用频率
    //44100是目前的标准，但是某些设备仍然支持22050，16000，11025
    //采样频率一般共分为22.05KHz、44.1KHz、48KHz三个等级
    private val AUDIO_SAMPLE_RATE = 16000

    //声道 单声道
    private val AUDIO_CHANNEL = AudioFormat.CHANNEL_IN_MONO

    //编码
    private val AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT

    // 缓冲区字节大小
    private var bufferSizeInBytes = 0

    //录音对象
    private var audioRecord: AudioRecord? = null

    //录音状态
    private var status = Status.STATUS_NO_READY

    //文件名
    private var fileName: String? = ""

    //录音文件
    private val filesName: MutableList<String?> =
        ArrayList()

    /**
     * 创建录音对象
     */
    fun createAudio(
        fileName: String?,
        audioSource: Int,
        sampleRateInHz: Int,
        channelConfig: Int,
        audioFormat: Int
    ) {
        // 获得缓冲区字节大小
        bufferSizeInBytes = AudioRecord.getMinBufferSize(
            sampleRateInHz,
            channelConfig, channelConfig
        )
        audioRecord =
            AudioRecord(audioSource, sampleRateInHz, channelConfig, audioFormat, bufferSizeInBytes)
        this.fileName = fileName
    }

    /**
     * 创建默认的录音对象
     *
     * @param fileName 文件名
     */
    fun createDefaultAudio(fileName: String?) {
        // 获得缓冲区字节大小
        bufferSizeInBytes = AudioRecord.getMinBufferSize(
            AUDIO_SAMPLE_RATE,
            AUDIO_CHANNEL, AUDIO_ENCODING
        )
        audioRecord = AudioRecord(
            AUDIO_INPUT,
            AUDIO_SAMPLE_RATE,
            AUDIO_CHANNEL,
            AUDIO_ENCODING,
            bufferSizeInBytes
        )
        this.fileName = fileName
        status = Status.STATUS_READY
    }


    /**
     * 开始录音
     *
     * @param listener 音频流的监听
     */
    fun startRecord(context: Context,listener: RecordStreamListener?) {
        check(!(status == Status.STATUS_NO_READY || TextUtils.isEmpty(fileName))) { "录音尚未初始化,请检查是否禁止了录音权限~" }
        check(status != Status.STATUS_START) { "正在录音" }
        Log.d("AudioRecorder", "===startRecord===" + audioRecord!!.state)
        audioRecord!!.startRecording()
        Thread(Runnable { writeDataTOFile(context,listener) }).start()
    }

    /**
     * 暂停录音
     */
    fun pauseRecord() {
        Log.d("AudioRecorder", "===pauseRecord===")
        status = if (status != Status.STATUS_START) {
            throw IllegalStateException("没有在录音")
        } else {
            audioRecord!!.stop()
            Status.STATUS_PAUSE
        }
    }

    /**
     * 停止录音
     */
    fun stopRecord(context: Context) {
        Log.d("AudioRecorder", "===stopRecord===")
        check(!(status == Status.STATUS_NO_READY || status == Status.STATUS_READY)) { "录音尚未开始" }
        audioRecord!!.stop()
        status = Status.STATUS_STOP
        release(context)
    }

    /**
     * 释放资源
     */
    fun release(context: Context) {
        Log.d("AudioRecorder", "===release===")
        //假如有暂停录音
        try {
            if (filesName.size > 0) {
                val filePaths: MutableList<String?> =
                    ArrayList()
                for (fileName in filesName) {
                    filePaths.add(FileUtil.getPcmFileAbsolutePath(context,fileName))
                }
                //清除
                filesName.clear()
                //将多个pcm文件转化为wav文件
                mergePCMFilesToWAVFile(context,filePaths)
            } else {
                //这里由于只要录音过filesName.size都会大于0,没录音时fileName为null
                //会报空指针 NullPointerException
                // 将单个pcm文件转化为wav文件
                //Log.d("AudioRecorder", "=====makePCMFileToWAVFile======");
                //makePCMFileToWAVFile();
            }
        } catch (e: IllegalStateException) {
            throw IllegalStateException(e.message)
        }
        if (audioRecord != null) {
            audioRecord!!.release()
            audioRecord = null
        }
        status = Status.STATUS_NO_READY
    }

    /**
     * 取消录音
     */
    fun cancel() {
        filesName.clear()
        fileName = null
        if (audioRecord != null) {
            audioRecord!!.release()
            audioRecord = null
        }
        status = Status.STATUS_NO_READY
    }


    /**
     * 将音频信息写入文件
     *
     * @param listener 音频流的监听
     */
    private fun writeDataTOFile(context: Context,listener: RecordStreamListener?) {
        // new一个byte数组用来存一些字节数据，大小为缓冲区大小
        val audiodata = ByteArray(bufferSizeInBytes)
        var fos: FileOutputStream? = null
        var readsize = 0
        try {
            var currentFileName = fileName
            if (status == Status.STATUS_PAUSE) {
                //假如是暂停录音 将文件名后面加个数字,防止重名文件内容被覆盖
                currentFileName += filesName.size
            }
            filesName.add(currentFileName)
            val file = File(FileUtil.getPcmFileAbsolutePath(context,currentFileName!!))
            if (file.exists()) {
                file.delete()
            }
            fos = FileOutputStream(file) // 建立一个可存取字节的文件
        } catch (e: IllegalStateException) {
            Log.e("AudioRecorder", e.message)
            throw IllegalStateException(e.message)
        } catch (e: FileNotFoundException) {
            Log.e("AudioRecorder", e.message)
        }
        //将录音状态设置成正在录音状态
        status = Status.STATUS_START
        while (status == Status.STATUS_START) {
            readsize = audioRecord!!.read(audiodata, 0, bufferSizeInBytes)
            if (AudioRecord.ERROR_INVALID_OPERATION != readsize && fos != null) {
                try {
                    fos.write(audiodata)
                    listener?.recordOfByte(audiodata, 0, audiodata.size)
                } catch (e: IOException) {
                    Log.e("AudioRecorder", e.message)
                }
            }
        }
        try {
            fos?.close()
        } catch (e: IOException) {
            Log.e("AudioRecorder", e.message)
        }
    }

    /**
     * 将pcm合并成wav
     *
     * @param filePaths
     */
    private fun mergePCMFilesToWAVFile(context: Context,filePaths: List<String?>) {
        Thread(Runnable {
            if (PcmToWav.mergePCMFilesToWAVFile(
                    filePaths,
                    FileUtil.getWavFileAbsolutePath(context,fileName)
                )
            ) {
                //操作成功
            } else {
                //操作失败
                Log.e("AudioRecorder", "mergePCMFilesToWAVFile fail")
                throw IllegalStateException("mergePCMFilesToWAVFile fail")
            }
            fileName = null
        }).start()
    }

    /**
     * 将单个pcm文件转化为wav文件
     */
    private fun makePCMFileToWAVFile(context: Context) {
        Thread(Runnable {
            if (PcmToWav.makePCMFileToWAVFile(
                    FileUtil.getPcmFileAbsolutePath(context,fileName!!),
                    FileUtil.getWavFileAbsolutePath(context,fileName),
                    true
                )
            ) {
                //操作成功
            } else {
                //操作失败
                Log.e("AudioRecorder", "makePCMFileToWAVFile fail")
                throw IllegalStateException("makePCMFileToWAVFile fail")
            }
            fileName = null
        }).start()
    }

    /**
     * 获取录音对象的状态
     *
     * @return
     */
    fun getStatus(): Status {
        return status
    }

    /**
     * 获取本次录音文件的个数
     *
     * @return
     */
    fun getPcmFilesCount(): Int {
        return filesName.size
    }

    /**
     * 录音对象的状态
     */
    enum class Status {
        //未开始
        STATUS_NO_READY,  //预备
        STATUS_READY,  //录音
        STATUS_START,  //暂停
        STATUS_PAUSE,  //停止
        STATUS_STOP
    }
}
```

##### FileUtil

```kotlin
package com.lujianfei.plugin9_1.utils

import android.content.Context
import android.os.Environment
import android.text.TextUtils
import android.util.Log
import java.io.File
import java.util.ArrayList

/**
 *@date     创建时间:2020/7/1
 *@name     作者:陆键霏
 *@describe 描述:
 */
object FileUtil {
    
    private val TAG = "FileUtil"

    private const val rootPath = "mediaFile"

    //原始文件(不能播放)
    private const val AUDIO_PCM_BASEPATH = "/$rootPath/pcm/"

    //可播放的高质量音频文件
    private const val AUDIO_WAV_BASEPATH = "/$rootPath/wav/"

    fun getPcmFileAbsolutePath(context: Context,fileName: String?): String {
        var fileName = fileName
        if (TextUtils.isEmpty(fileName)) {
            throw NullPointerException("fileName isEmpty")
        }
        check(isSdcardExit()) { "sd card no found" }
        var mAudioRawPath = ""
        if (isSdcardExit()) {
            if (!fileName!!.endsWith(".pcm")) {
                fileName = "$fileName.pcm"
            }
            val file = File(context.filesDir,AUDIO_PCM_BASEPATH)
            val fileBasePath = file.absolutePath
            //创建目录
            if (!file.exists()) {
                file.mkdirs()
            }
            mAudioRawPath = fileBasePath + fileName
        }
        Log.d(TAG, "getPcmFileAbsolutePath mAudioRawPath = $mAudioRawPath")
        return mAudioRawPath
    }

    fun getWavFileAbsolutePath(context: Context,fileName: String?): String? {
        var fileName = fileName ?: throw NullPointerException("fileName can't be null")
        check(isSdcardExit()) { "sd card no found" }
        var mAudioWavPath = ""
        if (isSdcardExit()) {
            if (!fileName.endsWith(".wav")) {
                fileName = "$fileName.wav"
            }
            val file = File(context.filesDir,AUDIO_WAV_BASEPATH)
            val fileBasePath = file.absolutePath
            //创建目录
            if (!file.exists()) {
                file.mkdirs()
            }
            mAudioWavPath = fileBasePath + File.separator + fileName
        }
        Log.d(TAG, "getWavFileAbsolutePath mAudioWavPath = $mAudioWavPath")
        return mAudioWavPath
    }

    /**
     * 判断是否有外部存储设备sdcard
     *
     * @return true | false
     */
    private fun isSdcardExit(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    /**
     * 获取全部pcm文件列表
     * @return
     */
    fun getPcmFiles(context: Context): List<File> {
        val list: MutableList<File> = ArrayList()
        val rootFile = File(context.filesDir,AUDIO_WAV_BASEPATH)
        if (!rootFile.exists()) {
        } else {
            val files = rootFile.listFiles()
            for (file in files) {
                list.add(file)
            }
        }
        Log.d(TAG, "getPcmFiles list = $list")
        return list
    }

    /**
     * 获取全部wav文件列表
     *
     * @return
     */
    fun getWavFiles(context: Context): List<File> {
        val list: MutableList<File> = ArrayList()
        val rootFile = File(context.filesDir,AUDIO_WAV_BASEPATH)
        if (!rootFile.exists()) {
        } else {
            val files = rootFile.listFiles()
            for (file in files) {
                list.add(file)
            }
        }
        Log.d(TAG, "getWavFiles list = $list")
        return list
    }
}
```

##### PcmToWav

```kotlin
package com.lujianfei.plugin9_1.utils

import android.util.Log
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

/**
 *@date     创建时间:2020/7/1
 *@name     作者:陆键霏
 *@describe 描述:
 */
object PcmToWav {
    /**
     * 合并多个pcm文件为一个wav文件
     *
     * @param filePathList    pcm文件路径集合
     * @param destinationPath 目标wav文件路径
     * @return true|false
     */
    fun mergePCMFilesToWAVFile(
        filePathList: List<String?>,
        destinationPath: String?
    ): Boolean {
        val file = arrayOfNulls<File>(filePathList.size)
        var buffer: ByteArray? = null
        var TOTAL_SIZE = 0
        val fileNum = filePathList.size
        for (i in 0 until fileNum) {
            file[i] = File(filePathList[i])
            TOTAL_SIZE += file[i]!!.length().toInt()
        }

        // 填入参数，比特率等等。这里用的是16位单声道 8000 hz
        val header = WaveHeader()
        // 长度字段 = 内容的大小（TOTAL_SIZE) +
        // 头部字段的大小(不包括前面4字节的标识符RIFF以及fileLength本身的4字节)
        header.fileLength = TOTAL_SIZE + (44 - 8)
        header.FmtHdrLeth = 16
        header.BitsPerSample = 16
        header.Channels = 2
        header.FormatTag = 0x0001
        header.SamplesPerSec = 8000
        header.BlockAlign = (header.Channels * header.BitsPerSample / 8).toShort()
        header.AvgBytesPerSec = header.BlockAlign * header.SamplesPerSec
        header.DataHdrLeth = TOTAL_SIZE
        var h: ByteArray? = null
        h = try {
            header.getHeader()
        } catch (e1: IOException) {
            Log.e("PcmToWav", e1.message)
            return false
        }
        if (h.size != 44) // WAV标准，头部应该是44字节,如果不是44个字节则不进行转换文件
            return false

        //先删除目标文件
        val destfile = File(destinationPath)
        if (destfile.exists()) destfile.delete()

        //合成所有的pcm文件的数据，写到目标文件
        try {
            buffer = ByteArray(1024 * 4) // Length of All Files, Total Size
            var inStream: InputStream? = null
            var ouStream: OutputStream? = null
            ouStream = BufferedOutputStream(
                FileOutputStream(
                    destinationPath
                )
            )
            ouStream.write(h, 0, h.size)
            for (j in 0 until fileNum) {
                inStream = BufferedInputStream(FileInputStream(file[j]))
                var size = inStream.read(buffer)
                while (size != -1) {
                    ouStream.write(buffer)
                    size = inStream.read(buffer)
                }
                inStream.close()
            }
            ouStream.close()
        } catch (e: FileNotFoundException) {
            Log.e("PcmToWav", e.message)
            return false
        } catch (ioe: IOException) {
            Log.e("PcmToWav", ioe.message)
            return false
        }
        clearFiles(filePathList)
        Log.i(
            "PcmToWav",
            "mergePCMFilesToWAVFile  success!" + SimpleDateFormat("yyyy-MM-dd hh:mm")
                .format(Date())
        )
        return true
    }

    /**
     * 将一个pcm文件转化为wav文件
     *
     * @param pcmPath         pcm文件路径
     * @param destinationPath 目标文件路径(wav)
     * @param deletePcmFile   是否删除源文件
     * @return
     */
    fun makePCMFileToWAVFile(
        pcmPath: String?,
        destinationPath: String?,
        deletePcmFile: Boolean
    ): Boolean {
        var buffer: ByteArray? = null
        var TOTAL_SIZE = 0
        val file = File(pcmPath)
        if (!file.exists()) {
            return false
        }
        TOTAL_SIZE = file.length().toInt()
        // 填入参数，比特率等等。这里用的是16位单声道 8000 hz
        val header = WaveHeader()
        // 长度字段 = 内容的大小（TOTAL_SIZE) +
        // 头部字段的大小(不包括前面4字节的标识符RIFF以及fileLength本身的4字节)
        header.fileLength = TOTAL_SIZE + (44 - 8)
        header.FmtHdrLeth = 16
        header.BitsPerSample = 16
        header.Channels = 2
        header.FormatTag = 0x0001
        header.SamplesPerSec = 8000
        header.BlockAlign = (header.Channels * header.BitsPerSample / 8).toShort()
        header.AvgBytesPerSec = header.BlockAlign * header.SamplesPerSec
        header.DataHdrLeth = TOTAL_SIZE
        var h: ByteArray? = null
        h = try {
            header.getHeader()
        } catch (e1: IOException) {
            Log.e("PcmToWav", e1.message)
            return false
        }
        if (h.size != 44) // WAV标准，头部应该是44字节,如果不是44个字节则不进行转换文件
            return false

        //先删除目标文件
        val destfile = File(destinationPath)
        if (destfile.exists()) destfile.delete()

        //合成所有的pcm文件的数据，写到目标文件
        try {
            buffer = ByteArray(1024 * 4) // Length of All Files, Total Size
            var inStream: InputStream? = null
            var ouStream: OutputStream? = null
            ouStream = BufferedOutputStream(
                FileOutputStream(
                    destinationPath
                )
            )
            ouStream.write(h, 0, h.size)
            inStream = BufferedInputStream(FileInputStream(file))
            var size = inStream.read(buffer)
            while (size != -1) {
                ouStream.write(buffer)
                size = inStream.read(buffer)
            }
            inStream.close()
            ouStream.close()
        } catch (e: FileNotFoundException) {
            Log.e("PcmToWav", e.message)
            return false
        } catch (ioe: IOException) {
            Log.e("PcmToWav", ioe.message)
            return false
        }
        if (deletePcmFile) {
            file.delete()
        }
        Log.i(
            "PcmToWav",
            "makePCMFileToWAVFile  success!" + SimpleDateFormat("yyyy-MM-dd hh:mm")
                .format(Date())
        )
        return true
    }

    /**
     * 清除文件
     *
     * @param filePathList
     */
    private fun clearFiles(filePathList: List<String?>) {
        for (i in filePathList.indices) {
            val file = File(filePathList[i])
            if (file.exists()) {
                file.delete()
            }
        }
    }
}
```

##### RecordStreamListener

```kotlin
package com.lujianfei.plugin9_1.utils

/**
 *@date     创建时间:2020/7/1
 *@name     作者:陆键霏
 *@describe 描述:
 */
interface RecordStreamListener {
    fun recordOfByte(data: ByteArray?, begin: Int, end: Int)
}
```

##### WaveHeader

```kotlin
package com.lujianfei.plugin9_1.utils

import java.io.ByteArrayOutputStream
import java.io.IOException

/**
 *@date     创建时间:2020/7/1
 *@name     作者:陆键霏
 *@describe 描述:
 */
class WaveHeader {
    
    private val fileID = charArrayOf('R', 'I', 'F', 'F')
    var fileLength = 0
    private var wavTag = charArrayOf('W', 'A', 'V', 'E')
    private var FmtHdrID = charArrayOf('f', 'm', 't', ' ')
    var FmtHdrLeth = 0
    var FormatTag: Short = 0
    var Channels: Short = 0
    var SamplesPerSec = 0
    var AvgBytesPerSec = 0
    var BlockAlign: Short = 0
    var BitsPerSample: Short = 0
    var DataHdrID = charArrayOf('d', 'a', 't', 'a')
    var DataHdrLeth = 0

    @Throws(IOException::class)
    fun getHeader(): ByteArray {
        val bos = ByteArrayOutputStream()
        WriteChar(bos, fileID)
        WriteInt(bos, fileLength)
        WriteChar(bos, wavTag)
        WriteChar(bos, FmtHdrID)
        WriteInt(bos, FmtHdrLeth)
        WriteShort(bos, FormatTag.toInt())
        WriteShort(bos, Channels.toInt())
        WriteInt(bos, SamplesPerSec)
        WriteInt(bos, AvgBytesPerSec)
        WriteShort(bos, BlockAlign.toInt())
        WriteShort(bos, BitsPerSample.toInt())
        WriteChar(bos, DataHdrID)
        WriteInt(bos, DataHdrLeth)
        bos.flush()
        val r = bos.toByteArray()
        bos.close()
        return r
    }

    @Throws(IOException::class)
    private fun WriteShort(bos: ByteArrayOutputStream, s: Int) {
        val mybyte = ByteArray(2)
        mybyte[1] = (s shl 16 shr 24).toByte()
        mybyte[0] = (s shl 24 shr 24).toByte()
        bos.write(mybyte)
    }

    @Throws(IOException::class)
    private fun WriteInt(bos: ByteArrayOutputStream, n: Int) {
        val buf = ByteArray(4)
        buf[3] = (n shr 24).toByte()
        buf[2] = (n shl 8 shr 24).toByte()
        buf[1] = (n shl 16 shr 24).toByte()
        buf[0] = (n shl 24 shr 24).toByte()
        bos.write(buf)
    }

    private fun WriteChar(bos: ByteArrayOutputStream, id: CharArray) {
        for (i in id.indices) {
            val c = id[i]
            bos.write(c.toInt())
        }
    }
}

```

