# android 锚点定位 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
       
    <com.lujianfei.plugin2_14.widget.CustomTabLayout
        android:id="@+id/tabLayout"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        />
    
    <com.lujianfei.plugin2_14.widget.CustomScrollView
        android:id="@+id/scrollView"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:fillViewport="true"
        android:fitsSystemWindows="true"
        >
        <LinearLayout
            android:id="@+id/container"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            />
    </com.lujianfei.plugin2_14.widget.CustomScrollView>
</LinearLayout>
```


##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_14

import android.content.Intent
import android.net.Uri
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import android.widget.LinearLayout
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin2_14.widget.AnchorView
import com.lujianfei.plugin2_14.widget.CustomScrollView
import com.lujianfei.plugin2_14.widget.CustomTabLayout


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }
   
    private var tabLayout: CustomTabLayout?= null
    private var container:LinearLayout ?= null
    private var scrollView:CustomScrollView ?= null
    private val tabTextList = arrayOf("大厅", "厨房", "卧室", "厕所", "衣物间", "餐厅", "书房", "阳台", "儿童房", "钢琴房", "台球房", "麻将房")
    // 内容块 view 的集合
    private val anchorList = arrayListOf<AnchorView>()
    // 判读是否是 scrollview 主动引起的滑动: true-是 , false-否
    private var isScroll = false
    //记录上一次位置，防止在同一内容块里滑动 重复定位到tablayout
    private var lastPos = 0
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {        
        tabLayout = findViewById(R.id.tabLayout)
        container = findViewById(R.id.container)
        scrollView = findViewById(R.id.scrollView)
    }

    override fun initData() {       
        ResUtils.getThemeColor()?.let { tabLayout?.selectedTabIndicatorColor = it }
        ResUtils.getThemeColor()?.let { tabLayout?.setTabTextColors(0xff000000.toInt(), it) }
        for (tabTxt in tabTextList) {
            val anchorView = AnchorView(tabLayout?.context!!)
            anchorView.setAnchorTxt(tabTxt)
            anchorView.setContentTxt(tabTxt)
            anchorList.add(anchorView)
            container?.addView(anchorView)
            tabLayout?.addTab(tabLayout?.newTab()?.setText(tabTxt))
        }
        tabLayout?.setScrollPosition(0)
    }

    override fun initEvent() {       
        scrollView?.setOnTouchListener { v, event ->
            if (event?.action == MotionEvent.ACTION_DOWN) {
                isScroll = true
            }
            false
        }
        scrollView?.onScrollChangedListener = { left, top, _, _ ->
            if (isScroll) {
                for (i in tabTextList.size - 1 downTo 0) {
                    //根据滑动距离，对比各模块距离父布局顶部的高度判断
                    if (top > anchorList[i].top - 10) {
                        setScrollPos(i)
                        break
                    }
                }
            }
        }
        tabLayout?.onTabSelectedListener = { selectedTab ->
            isScroll = false
            val pos = selectedTab.position
            val top = anchorList[pos].top
            scrollView?.smoothScrollTo(0, top)
        }
    }   

    /**
     * tablayout对应标签的切换 
     */
    private fun setScrollPos(newPos: Int) {
        if (lastPos != newPos) {
            // 该方法不会触发 tablayout 的 onTabSelected 监听
            tabLayout?.setScrollPosition(newPos)
        }
        lastPos = newPos
    }
}

```

##### CustomScrollView.kt

```kotlin
package com.lujianfei.plugin2_14.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.ScrollView

/**
 *@date     创建时间:2020/12/8
 *@name     作者:陆键霏
 *@describe 描述: 自定义 ScrollView，用于获取 onScrollChanged 事件
 */
class CustomScrollView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ScrollView(context, attrs, defStyleAttr) {

    var onScrollChangedListener:((Int,Int,Int,Int)->Unit) ?= null
    
    override fun onScrollChanged(l: Int, t: Int, oldl: Int, oldt: Int) {
        super.onScrollChanged(l, t, oldl, oldt)
        onScrollChangedListener?.invoke(l,t,oldl,oldt)
    }
}
```

##### AnchorView.kt

```kotlin
package com.lujianfei.plugin2_14.widget

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.lujianfei.plugin2_14.R
import java.util.*

/**
 *@date     创建时间:2020/12/8
 *@name     作者:陆键霏
 *@describe 描述: 定位内容类
 */
class AnchorView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {

    private var tv_anchor: TextView? = null
    private var tv_content: TextView? = null
    
    init {
        LayoutInflater.from(context).inflate(R.layout.view_anchor, this, true)
        tv_anchor = findViewById(R.id.tv_anchor)
        tv_content = findViewById(R.id.tv_content)
        val random = Random()
        val r: Int = random.nextInt(256)
        val g: Int = random.nextInt(256)
        val b: Int = random.nextInt(256)
        tv_content?.setBackgroundColor(Color.rgb(r, g, b))
    }
    
    fun setAnchorTxt(text:String) {
        tv_anchor?.text = text
    }
    
    fun setContentTxt(text:String) {
        tv_content?.text = text
    }
}
```
##### view_anchor.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    xmlns:tools="http://schemas.android.com/tools"
    android:orientation="vertical"
    android:padding="10dp">

    <TextView
        android:id="@+id/tv_anchor"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textColor="#000000"
        android:textSize="18sp"
        tools:text="标签"/>
    
    <TextView
        android:id="@+id/tv_content"
        android:layout_width="match_parent"
        android:layout_height="400dp"
        android:layout_marginTop="5dp"
        android:gravity="center"
        android:textColor="#ffffff"
        android:textSize="20sp"
        />
    
</LinearLayout>
```



##### CustomTabLayout.kt

```kotlin
package com.lujianfei.plugin2_14.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.HorizontalScrollView
import android.widget.LinearLayout
import android.widget.TextView
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.plugin2_14.R

/**
 *@date     创建时间:2020/12/8
 *@name     作者:陆键霏
 *@describe 描述: 自定义 TabLayout
 */
class CustomTabLayout @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : HorizontalScrollView(context, attrs, defStyleAttr) {
    
    companion object {
        const val TAG = "CustomTabLayout"
    }

    private var mCustomTabs = arrayListOf<CustomTab>()
    private var layoutTabLayout:LinearLayout ?= null
    var onTabSelectedListener:((selectedTab:CustomTab)->Unit) ?= null
    
    init {
        LayoutInflater.from(context).inflate(R.layout.widget_custom_tab_layout, this, true)
        layoutTabLayout = findViewById(R.id.layoutTabLayout)
    }
    
    var selectedTabIndicatorColor:Int = 0
    var normalColor:Int = 0
    var selectedColor:Int = 0
    
    fun setTabTextColors(normalColor:Int, selectedColor:Int) {
        this.normalColor = normalColor
        this.selectedColor = selectedColor
    }
    
    fun newTab():CustomTab {
        return CustomTab(context)
    }
    
    fun addTab(tab:CustomTab?) {
        tab?.let {
            tab.setOnClickListener {
                reset()
                it.isSelected = true
                onTabSelectedListener?.invoke(tab)
            }
            tab.position = mCustomTabs.size
            layoutTabLayout?.addView(tab)
            mCustomTabs.add(tab)
        }
    }

    fun setScrollPosition(newPos: Int) {
        reset()
        val customTab = mCustomTabs[newPos]
        customTab.isSelected = true
        val loc = intArrayOf(0,0)
        customTab.getLocationInWindow(loc)
        val x = loc[0]
        // 选项位置在屏幕外，需要滑出来
        if (x < 0) { 
            smoothScrollBy(x, 0)
        } else if (x + customTab.width > width) {
            smoothScrollBy(x + customTab.width - width , 0)
        }
    }

    private fun reset() {
        for (tab in mCustomTabs) {
            tab.isSelected = false
        }
    }

    inner class CustomTab @JvmOverloads constructor(
            context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
    ) : FrameLayout(context, attrs, defStyleAttr) {

        private var txt_title: TextView? = null
        private var view_indicator: View? = null
        private var title = ""
        var position = 0
        init {
            LayoutInflater.from(context).inflate(R.layout.widget_custom_tab, this, true)
            txt_title = findViewById(R.id.txt_title)
            view_indicator = findViewById(R.id.view_indicator)
            updateUI()
        }
        
        fun setText(text:String) :CustomTab {
            title = text
            updateUI()
            return this
        }

        private fun updateUI() {
            txt_title?.text = title
        }

        override fun setSelected(selected: Boolean) {
            super.setSelected(selected)
            if (selected) {
                txt_title?.setTextColor(selectedColor)
                view_indicator?.setBackgroundColor(selectedTabIndicatorColor)
            } else {
                txt_title?.setTextColor(normalColor)
                view_indicator?.setBackgroundColor(0x0)
            }
        }
    }
}
```

##### widget_custom_tab_layout.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<HorizontalScrollView xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">

    <LinearLayout
        android:id="@+id/layoutTabLayout"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="horizontal"/>
</HorizontalScrollView>
```

##### widget_custom_tab.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">
    <TextView
        android:id="@+id/txt_title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="20sp"
        android:textColor="#000000"
        android:padding="10dp"
        android:gravity="center"/>
    <View
        android:id="@+id/view_indicator"
        android:layout_width="match_parent"
        android:layout_height="1dp"
        android:layout_gravity="bottom"
        />
</FrameLayout>
```

