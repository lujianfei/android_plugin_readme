# android PhotoView 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...
    <com.lujianfei.plugin2_2.photoview.PhotoView
        android:id="@+id/photoView"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_2

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.plugin2_2.photoview.PhotoView


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

   ...
    private var photoView: PhotoView?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        photoView = findViewById(R.id.photoView)
       ...

        photoView?.let { photoView->
            that?.let {that->
                Glide.with(that).load(
                        "https://xxx.jpg")
                        .placeholder(getPluginDrawable(R.drawable.ic_default_image))
                        .into(photoView)
            }
        }
    }

    ...
}

```

##### 以下是核心代码


PhotoView


```kotlin
/*
 Copyright 2011, 2012 Chris Banes.
 <p>
 Licensed under the Apache License, Version 2.0 (the "License")
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 <p>
 http://www.apache.org/licenses/LICENSE-2.0
 <p>
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.lujianfei.plugin2_2.photoview

import android.content.Context
import android.graphics.Matrix
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.net.Uri
import android.util.AttributeSet
import android.view.GestureDetector
import androidx.appcompat.widget.AppCompatImageView

/**
 * A zoomable ImageView. See {@link PhotoViewAttacher} for most of the details on how the zooming
 * is accomplished
 */
@SuppressWarnings("unused")
class PhotoView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr) {

    private var attacher:PhotoViewAttacher ?= null
    private var pendingScaleType:ScaleType ?= null

    init {
        attacher = PhotoViewAttacher(this)
        //We always pose as a Matrix scale type, though we can change to another scale type
        //via the attacher
        super.setScaleType(ScaleType.MATRIX)
        //apply the previously applied scale type
        if (pendingScaleType != null) {
            scaleType = pendingScaleType?:ScaleType.MATRIX
            pendingScaleType = null
        }
    }

    override fun getScaleType(): ScaleType {
        return attacher?.getScaleType()?:ScaleType.MATRIX
    }

    override fun getImageMatrix(): Matrix {
        return attacher?.getImageMatrix()?: Matrix()
    }

    override fun setOnLongClickListener(l: OnLongClickListener?) {
        l?.let {
            attacher?.setOnLongClickListener(l)
        }
    }

    override fun setOnClickListener(l: OnClickListener?) {
        l?.let {
            attacher?.setOnClickListener(l)
        }
    }

    override fun setScaleType(scaleType: ScaleType) {
        if (attacher == null) {
            pendingScaleType = scaleType
        } else {
            attacher?.setScaleType(scaleType)
        }
    }

    override fun setImageDrawable(drawable: Drawable?) {
        super.setImageDrawable(drawable)
        // setImageBitmap calls through to this method
        if (attacher != null) {
            attacher?.update()
        }
    }

    override fun setImageResource(resId: Int) {
        super.setImageResource(resId)
        if (attacher != null) {
            attacher?.update()
        }
    }

    override fun setImageURI(uri: Uri?) {
        super.setImageURI(uri)
        if (attacher != null) {
            attacher?.update()
        }
    }

    override fun setFrame(l: Int, t: Int, r: Int, b: Int): Boolean {
        val changed = super.setFrame(l, t, r, b)
        if (changed) {
            attacher?.update()
        }
        return changed
    }

    fun setRotationTo(rotationDegree:Float) {
        attacher?.setRotationTo(rotationDegree)
    }

    fun setRotationBy(rotationDegree:Float) {
        attacher?.setRotationBy(rotationDegree)
    }

    fun isZoomable():Boolean {
        return attacher?.isZoomable()?:false
    }

    fun setZoomable(zoomable:Boolean) {
        attacher?.setZoomable(zoomable)
    }

    fun getDisplayRect(): RectF {
        return attacher?.getDisplayRect()?:RectF()
    }

    fun getDisplayMatrix(matrix:Matrix) {
        attacher?.getDisplayMatrix(matrix)
    }

    @SuppressWarnings("UnusedReturnValue") 
    fun setDisplayMatrix(finalRectangle:Matrix):Boolean {
        return attacher?.setDisplayMatrix(finalRectangle)?:false
    }

    fun getSuppMatrix(matrix:Matrix) {
        attacher?.getSuppMatrix(matrix)
    }

    fun setSuppMatrix(matrix:Matrix):Boolean {
        return attacher?.setDisplayMatrix(matrix)?:false
    }

    fun getMinimumScale():Float {
        return attacher?.getMinimumScale()?:0f
    }

    fun getMediumScale():Float {
        return attacher?.getMediumScale()?:0f
    }

    fun getMaximumScale():Float {
        return attacher?.getMaximumScale()?:0f
    }

    fun getScale():Float {
        return attacher?.getScale()?:0f
    }

    fun setAllowParentInterceptOnEdge(allow:Boolean) {
        attacher?.setAllowParentInterceptOnEdge(allow)
    }

    fun setMinimumScale(minimumScale:Float) {
        attacher?.setMinimumScale(minimumScale)
    }

    fun setMediumScale(mediumScale:Float) {
        attacher?.setMediumScale(mediumScale)
    }

    fun setMaximumScale(maximumScale:Float) {
        attacher?.setMaximumScale(maximumScale)
    }

    fun setScaleLevels(minimumScale:Float, mediumScale:Float, maximumScale:Float) {
        attacher?.setScaleLevels(minimumScale, mediumScale, maximumScale)
    }

    fun setOnMatrixChangeListener(listener:OnMatrixChangedListener) {
        attacher?.setOnMatrixChangeListener(listener)
    }

    fun setOnPhotoTapListener(listener:OnPhotoTapListener) {
        attacher?.setOnPhotoTapListener(listener)
    }

    fun setOnOutsidePhotoTapListener(listener:OnOutsidePhotoTapListener) {
        attacher?.setOnOutsidePhotoTapListener(listener)
    }

    fun setOnViewTapListener(listener:OnViewTapListener) {
        attacher?.setOnViewTapListener(listener)
    }

    fun setOnViewDragListener(listener:OnViewDragListener) {
        attacher?.setOnViewDragListener(listener)
    }

    fun setScale(scale:Float) {
        attacher?.setScale(scale)
    }

    fun setScale(scale:Float, animate:Boolean) {
        attacher?.setScale(scale, animate)
    }

    fun setScale(scale:Float, focalX:Float, focalY:Float, animate:Boolean) {
        attacher?.setScale(scale, focalX, focalY, animate)
    }

    fun setZoomTransitionDuration(milliseconds:Int) {
        attacher?.setZoomTransitionDuration(milliseconds)
    }

    fun setOnDoubleTapListener(onDoubleTapListener: GestureDetector.OnDoubleTapListener) {
        attacher?.setOnDoubleTapListener(onDoubleTapListener)
    }

    fun setOnScaleChangeListener(onScaleChangedListener:OnScaleChangedListener) {
        attacher?.setOnScaleChangeListener(onScaleChangedListener)
    }

    fun setOnSingleFlingListener(onSingleFlingListener:OnSingleFlingListener) {
        attacher?.setOnSingleFlingListener(onSingleFlingListener)
    }
}

```

##### PhotoViewAttacher.kt

```kotlin
/*
 Copyright 2011, 2012 Chris Banes.
 <p>
 Licensed under the Apache License, Version 2.0 (the "License")
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 <p>
 http://www.apache.org/licenses/LICENSE-2.0
 <p>
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.lujianfei.plugin2_2.photoview

import android.content.Context
import android.graphics.Matrix
import android.graphics.Matrix.ScaleToFit
import android.graphics.RectF
import android.graphics.drawable.Drawable
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.View.OnLongClickListener
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Interpolator
import android.widget.ImageView
import android.widget.ImageView.ScaleType
import android.widget.OverScroller
import kotlin.math.pow
import kotlin.math.roundToInt
import kotlin.math.sqrt

/**
 * The component of {@link PhotoView} which does the work allowing for zooming, scaling, panning, etc.
 * It is made public in case you need to subclass something other than AppCompatImageView and still
 * gain the functionality that {@link PhotoView} offers
 */
class PhotoViewAttacher(imageView: ImageView) : View.OnTouchListener,
    View.OnLayoutChangeListener {

    companion object {
        private const val DEFAULT_MAX_SCALE = 3.0f
        private const val DEFAULT_MID_SCALE = 1.75f
        private const val DEFAULT_MIN_SCALE = 1.0f
        private const val DEFAULT_ZOOM_DURATION = 200

        private const val HORIZONTAL_EDGE_NONE = -1
        private const val HORIZONTAL_EDGE_LEFT = 0
        private const val HORIZONTAL_EDGE_RIGHT = 1
        private const val HORIZONTAL_EDGE_BOTH = 2
        private const val VERTICAL_EDGE_NONE = -1
        private const val VERTICAL_EDGE_TOP = 0
        private const val VERTICAL_EDGE_BOTTOM = 1
        private const val VERTICAL_EDGE_BOTH = 2
        private const val SINGLE_TOUCH = 1
    }

    private var mInterpolator:Interpolator = AccelerateDecelerateInterpolator()
    private var mZoomDuration = DEFAULT_ZOOM_DURATION
    private var mMinScale = DEFAULT_MIN_SCALE
    private var mMidScale = DEFAULT_MID_SCALE
    private var mMaxScale = DEFAULT_MAX_SCALE

    private var mAllowParentInterceptOnEdge = true
    private var mBlockParentIntercept = false

    private var mImageView:ImageView?= null

    // Gesture Detectors
    private var mGestureDetector:GestureDetector?=null
    private var mScaleDragDetector:CustomGestureDetector?=null

    // These are set so we don't keep allocating them on the heap
    private val mBaseMatrix = Matrix()
    private val mDrawMatrix = Matrix()
    private val mSuppMatrix = Matrix()
    private val mDisplayRect = RectF()
    private val mMatrixValues = floatArrayOf(0f,0f,0f,0f,0f,0f,0f,0f,0f)

    // Listeners
    private var mMatrixChangeListener:OnMatrixChangedListener? = null
    private var mPhotoTapListener:OnPhotoTapListener ?= null
    private var mOutsidePhotoTapListener:OnOutsidePhotoTapListener ?= null
    private var mViewTapListener:OnViewTapListener ?= null
    private var mOnClickListener: View.OnClickListener ?= null
    private var mLongClickListener:OnLongClickListener ?= null
    private var mScaleChangeListener:OnScaleChangedListener ?= null
    private var mSingleFlingListener:OnSingleFlingListener ?= null
    private var mOnViewDragListener:OnViewDragListener ?= null

    private var mCurrentFlingRunnable:FlingRunnable ?= null
    private var mHorizontalScrollEdge = HORIZONTAL_EDGE_BOTH
    private var mVerticalScrollEdge = VERTICAL_EDGE_BOTH
    private var mBaseRotation = 0f

    private var mZoomEnabled = true
    private var mScaleType = ScaleType.FIT_CENTER

    private var onGestureListener = object : OnGestureListener {

        override fun onDrag(dx: Float, dy: Float) {
            if (mScaleDragDetector?.isScaling == true) {
                return // Do not drag if we are already scaling
            }
            if (mOnViewDragListener != null) {
                mOnViewDragListener?.onDrag(dx, dy)
            }
            mSuppMatrix.postTranslate(dx, dy)
            checkAndDisplayMatrix()

            /*
             * Here we decide whether to let the ImageView's parent to start taking
             * over the touch event.
             *
             * First we check whether this function is enabled. We never want the
             * parent to take over if we're scaling. We then check the edge we're
             * on, and the direction of the scroll (i.e. if we're pulling against
             * the edge, aka 'overscrolling', let the parent take over).
             */
            val parent = mImageView?.parent
            if (mAllowParentInterceptOnEdge && (mScaleDragDetector?.isScaling == false) && !mBlockParentIntercept) {
                if (mHorizontalScrollEdge == HORIZONTAL_EDGE_BOTH
                        || (mHorizontalScrollEdge == HORIZONTAL_EDGE_LEFT && dx >= 1f)
                        || (mHorizontalScrollEdge == HORIZONTAL_EDGE_RIGHT && dx <= -1f)
                        || (mVerticalScrollEdge == VERTICAL_EDGE_TOP && dy >= 1f)
                        || (mVerticalScrollEdge == VERTICAL_EDGE_BOTTOM && dy <= -1f)) {
                    parent?.requestDisallowInterceptTouchEvent(false)
                }
            } else {
                parent?.requestDisallowInterceptTouchEvent(true)
            }
        }

        override fun onFling(startX: Float, startY: Float, velocityX: Float, velocityY: Float) {
            mCurrentFlingRunnable = mImageView?.context?.let { FlingRunnable(it) }
            mImageView?.let {mImageView->
                mCurrentFlingRunnable?.fling(getImageViewWidth(mImageView),
                        getImageViewHeight(mImageView),  velocityX.toInt(), velocityY.toInt())
            }
            mImageView?.post(mCurrentFlingRunnable)
        }

        override fun onScale(scaleFactor: Float, focusX: Float, focusY: Float) {
            if (getScale() < mMaxScale || scaleFactor < 1f) {
                if (mScaleChangeListener != null) {
                    mScaleChangeListener?.onScaleChange(scaleFactor, focusX, focusY)
                }
                mSuppMatrix.postScale(scaleFactor, scaleFactor, focusX, focusY)
                checkAndDisplayMatrix()
            }
        }
    }

    init {
        mImageView = imageView
        imageView.setOnTouchListener(this)
        imageView.addOnLayoutChangeListener(this)
        if (imageView.isInEditMode) it@{
            return@it
        }
        mBaseRotation = 0.0f
        // Create Gesture Detectors...
        mScaleDragDetector =  CustomGestureDetector(imageView.context, onGestureListener)
        mGestureDetector =  GestureDetector(imageView.context, object : GestureDetector.SimpleOnGestureListener() {

            // forward long click listener
            override fun onLongPress(e:MotionEvent) {
                if (mLongClickListener != null) {
                    mLongClickListener?.onLongClick(mImageView)
                }
            }

            override fun onFling(e1:MotionEvent, e2:MotionEvent,
                                    velocityX:Float, velocityY:Float):Boolean {
                if (mSingleFlingListener != null) {
                    if (getScale() > DEFAULT_MIN_SCALE) {
                        return false
                    }
                    if (e1.pointerCount > SINGLE_TOUCH
                        || e2.pointerCount > SINGLE_TOUCH) {
                        return false
                    }
                    return mSingleFlingListener?.onFling(e1, e2, velocityX, velocityY)?:false
                }
                return false
            }
        })
        mGestureDetector?.setOnDoubleTapListener(object : GestureDetector.OnDoubleTapListener {
            
            override fun onSingleTapConfirmed(e:MotionEvent):Boolean {
                if (mOnClickListener != null) {
                    mOnClickListener?.onClick(mImageView)
                }
                val displayRect = getDisplayRect()
                val x = e.x
                val y = e.y
                if (mViewTapListener != null) {
                    mViewTapListener?.onViewTap(mImageView, x, y)
                }
                if (displayRect.contains(x, y)) {
                    val xResult = (x - displayRect.left) / displayRect.width()
                    val yResult = (y - displayRect.top) / displayRect.height()
                    if (mPhotoTapListener != null) {
                        mPhotoTapListener?.onPhotoTap(mImageView, xResult, yResult)
                    }
                    return true
                } else {
                    if (mOutsidePhotoTapListener != null) {
                        mOutsidePhotoTapListener?.onOutsidePhotoTap(mImageView)
                    }
                }
                return false
            }

            override fun onDoubleTap(ev:MotionEvent):Boolean {
                try {
                    val scale = getScale()
                    val x = ev.x
                    val y = ev.y
                    if (scale < getMediumScale()) {
                        setScale(getMediumScale(), x, y, true)
                    } else if (scale >= getMediumScale() && scale < getMaximumScale()) {
                        setScale(getMaximumScale(), x, y, true)
                    } else {
                        setScale(getMinimumScale(), x, y, true)
                    }
                } catch (e:ArrayIndexOutOfBoundsException) {
                    // Can sometimes happen when getX() and getY() is called
                }
                return true
            }

            override fun onDoubleTapEvent(e:MotionEvent):Boolean {
                // Wait for the confirmed onDoubleTap() instead
                return false
            }
        })
    }

    fun setOnDoubleTapListener(newOnDoubleTapListener:GestureDetector.OnDoubleTapListener) {
        this.mGestureDetector?.setOnDoubleTapListener(newOnDoubleTapListener)
    }

    fun setOnScaleChangeListener(onScaleChangeListener:OnScaleChangedListener) {
        this.mScaleChangeListener = onScaleChangeListener
    }

    fun setOnSingleFlingListener(onSingleFlingListener:OnSingleFlingListener) {
        this.mSingleFlingListener = onSingleFlingListener
    }

    fun isZoomEnabled():Boolean {
        return mZoomEnabled
    }

    fun getDisplayRect():RectF {
        checkMatrixBounds()
        return getDisplayRect(getDrawMatrix())
    }

    fun setDisplayMatrix(finalMatrix:Matrix):Boolean {
        if (mImageView?.drawable == null) {
            return false
        }
        mSuppMatrix.set(finalMatrix)
        checkAndDisplayMatrix()
        return true
    }

    fun setBaseRotation(degrees:Float) {
        mBaseRotation = degrees % 360
        update()
        setRotationBy(mBaseRotation)
        checkAndDisplayMatrix()
    }

    fun setRotationTo(degrees:Float) {
        mSuppMatrix.setRotate(degrees % 360)
        checkAndDisplayMatrix()
    }

    fun setRotationBy(degrees:Float) {
        mSuppMatrix.postRotate(degrees % 360)
        checkAndDisplayMatrix()
    }

    fun getMinimumScale():Float {
        return mMinScale
    }

    fun getMediumScale():Float {
        return mMidScale
    }

    fun getMaximumScale():Float {
        return mMaxScale
    }

    fun getScale():Float {
        return  sqrt(getValue(mSuppMatrix, Matrix.MSCALE_X).toDouble().pow(2.0) +
                getValue(mSuppMatrix, Matrix.MSKEW_Y).toDouble().pow(2.0)).toFloat()
    }

    fun getScaleType():ScaleType {
        return mScaleType
    }

    override fun onLayoutChange(v: View?, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int) {
        // Update our base matrix, as the bounds have changed
        if (left != oldLeft || top != oldTop || right != oldRight || bottom != oldBottom) {
            updateBaseMatrix(mImageView?.drawable)
        }
    }

    override fun onTouch(v: View?, ev: MotionEvent?): Boolean {
        var handled = false
        if (ev == null) return false
        if (mZoomEnabled && Util.hasDrawable(v as ImageView)) {
            when (ev.action) {
                MotionEvent.ACTION_DOWN-> {
                    val parent = v.getParent()
                    // First, disable the Parent from intercepting the touch
                    // event
                    parent?.requestDisallowInterceptTouchEvent(true)
                    // If we're flinging, and the user presses down, cancel
                    // fling
                    cancelFling()
                }
                MotionEvent.ACTION_CANCEL,MotionEvent.ACTION_UP-> {
                    // If the user has zoomed less than min scale, zoom back
                    // to min scale
                    if (getScale() < mMinScale) {
                        val rect = getDisplayRect()
                        v.post(AnimatedZoomRunnable (getScale(), mMinScale,
                                rect.centerX(), rect.centerY()))
                        handled = true
                    } else if (getScale() > mMaxScale) {
                        val rect = getDisplayRect()
                        v.post(AnimatedZoomRunnable (getScale(), mMaxScale,
                                rect.centerX(), rect.centerY()))
                        handled = true
                    }
                }
            }
            // Try the Scale/Drag detector
            if (mScaleDragDetector != null) {
                val wasScaling = mScaleDragDetector?.isScaling
                val wasDragging = mScaleDragDetector?.isDragging
                handled = mScaleDragDetector?.onTouchEvent(ev)?:false
                val didntScale = (wasScaling == false) && (mScaleDragDetector?.isScaling == false)
                val didntDrag = (wasDragging == false) && (mScaleDragDetector?.isDragging == false)
                mBlockParentIntercept = didntScale && didntDrag
            }
            // Check to see if the user double tapped
            if (mGestureDetector != null && mGestureDetector?.onTouchEvent(ev) == true) {
                handled = true
            }

        }
        return handled
    }

    fun setAllowParentInterceptOnEdge(allow:Boolean) {
        mAllowParentInterceptOnEdge = allow
    }

    fun setMinimumScale(minimumScale:Float) {
        Util.checkZoomLevels(minimumScale, mMidScale, mMaxScale)
        mMinScale = minimumScale
    }

    fun setMediumScale(mediumScale:Float) {
        Util.checkZoomLevels(mMinScale, mediumScale, mMaxScale)
        mMidScale = mediumScale
    }

    fun setMaximumScale(maximumScale:Float) {
        Util.checkZoomLevels(mMinScale, mMidScale, maximumScale)
        mMaxScale = maximumScale
    }

    fun setScaleLevels(minimumScale:Float, mediumScale:Float, maximumScale:Float) {
        Util.checkZoomLevels(minimumScale, mediumScale, maximumScale)
        mMinScale = minimumScale
        mMidScale = mediumScale
        mMaxScale = maximumScale
    }

    fun setOnLongClickListener(listener:OnLongClickListener) {
        mLongClickListener = listener
    }

    fun setOnClickListener(listener:View.OnClickListener) {
        mOnClickListener = listener
    }

    fun setOnMatrixChangeListener(listener:OnMatrixChangedListener) {
        mMatrixChangeListener = listener
    }

    fun setOnPhotoTapListener(listener:OnPhotoTapListener) {
        mPhotoTapListener = listener
    }

    fun setOnOutsidePhotoTapListener(mOutsidePhotoTapListener:OnOutsidePhotoTapListener) {
        this.mOutsidePhotoTapListener = mOutsidePhotoTapListener
    }

    fun setOnViewTapListener(listener:OnViewTapListener) {
        mViewTapListener = listener
    }

    fun setOnViewDragListener(listener:OnViewDragListener) {
        mOnViewDragListener = listener
    }

    fun setScale(scale:Float) {
        setScale(scale, false)
    }

    fun setScale(scale:Float, animate:Boolean) {
        setScale(scale,
            (mImageView?.right?:0) / 2f,
            (mImageView?.bottom?:0) / 2f,
            animate)
    }

    fun setScale(scale:Float, focalX: Float, focalY:Float,
                 animate:Boolean) {
        // Check to see if the scale is within bounds
        if (scale < mMinScale || scale > mMaxScale) {
            throw IllegalArgumentException("Scale must be within the range of minScale and maxScale")
        }
        if (animate) {
            mImageView?.post(AnimatedZoomRunnable(getScale(), scale,
                focalX, focalY))
        } else {
            mSuppMatrix.setScale(scale, scale, focalX, focalY)
            checkAndDisplayMatrix()
        }
    }

    /**
     * Set the zoom interpolator
     *
     * @param interpolator the zoom interpolator
     */
    fun setZoomInterpolator(interpolator: Interpolator) {
        mInterpolator = interpolator
    }

    fun setScaleType(scaleType:ScaleType) {
        if (Util.isSupportedScaleType(scaleType) && scaleType != mScaleType) {
            mScaleType = scaleType
            update()
        }
    }

    fun isZoomable():Boolean {
        return mZoomEnabled
    }

    fun setZoomable(zoomable:Boolean) {
        mZoomEnabled = zoomable
        update()
    }

    fun update() {
        if (mZoomEnabled) {
            // Update the base matrix using the current drawable
            updateBaseMatrix(mImageView?.drawable)
        } else {
            // Reset the Matrix...
            resetMatrix()
        }
    }

    /**
     * Get the display matrix
     *
     * @param matrix target matrix to copy to
     */
    fun getDisplayMatrix(matrix:Matrix) {
        matrix.set(getDrawMatrix())
    }

    /**
     * Get the current support matrix
     */
    fun getSuppMatrix(matrix:Matrix) {
        matrix.set(mSuppMatrix)
    }

    private fun getDrawMatrix():Matrix {
        mDrawMatrix.set(mBaseMatrix)
        mDrawMatrix.postConcat(mSuppMatrix)
        return mDrawMatrix
    }

    fun getImageMatrix():Matrix {
        return mDrawMatrix
    }

    fun setZoomTransitionDuration(milliseconds:Int) {
        this.mZoomDuration = milliseconds
    }

    /**
     * Helper method that 'unpacks' a Matrix and returns the required value
     *
     * @param matrix     Matrix to unpack
     * @param whichValue Which value from Matrix.M* to return
     * @return returned value
     */
    private fun getValue(matrix:Matrix, whichValue:Int):Float {
        matrix.getValues(mMatrixValues)
        return mMatrixValues[whichValue]
    }

    /**
     * Resets the Matrix back to FIT_CENTER, and then displays its contents
     */
    private fun resetMatrix() {
        mSuppMatrix.reset()
        setRotationBy(mBaseRotation)
        setImageViewMatrix(getDrawMatrix())
        checkMatrixBounds()
    }

    private fun setImageViewMatrix(matrix:Matrix) {
        mImageView?.imageMatrix = matrix
        // Call MatrixChangedListener if needed
        if (mMatrixChangeListener != null) {
            val displayRect = getDisplayRect(matrix)
            mMatrixChangeListener?.onMatrixChanged(displayRect)
        }
    }

    /**
     * Helper method that simply checks the Matrix, and then displays the result
     */
    private fun checkAndDisplayMatrix() {
        if (checkMatrixBounds()) {
            setImageViewMatrix(getDrawMatrix())
        }
    }

    /**
     * Helper method that maps the supplied Matrix to the current Drawable
     *
     * @param matrix - Matrix to map Drawable against
     * @return RectF - Displayed Rectangle
     */
    private fun getDisplayRect(matrix:Matrix):RectF {
        val d = mImageView?.drawable
        if (d != null) {
            mDisplayRect.set(0f, 0f, d.intrinsicWidth.toFloat(),
                d.intrinsicHeight.toFloat())
            matrix.mapRect(mDisplayRect)
            return mDisplayRect
        }
        return RectF(0f,0f,0f,0f)
    }

    /**
     * Calculate Matrix for FIT_CENTER
     *
     * @param drawable - Drawable being displayed
     */
    private fun updateBaseMatrix(drawable:Drawable ?= null) {
        if (drawable == null) {
            return
        }

        val viewWidth = getImageViewWidth(mImageView)
        val viewHeight = getImageViewHeight(mImageView)
        val drawableWidth = drawable.intrinsicWidth
        val drawableHeight = drawable.intrinsicHeight
        mBaseMatrix.reset()
        val widthScale = viewWidth / drawableWidth
        val heightScale = viewHeight / drawableHeight
        when (mScaleType) {
            ScaleType.CENTER -> {
                mBaseMatrix.postTranslate((viewWidth - drawableWidth) / 2F,
                        (viewHeight - drawableHeight) / 2F)

            }
            ScaleType.CENTER_CROP -> {
                val scale = widthScale.coerceAtLeast(heightScale)
                mBaseMatrix.postScale(scale.toFloat(), scale.toFloat())
                mBaseMatrix.postTranslate((viewWidth - drawableWidth * scale) / 2F,
                        (viewHeight - drawableHeight * scale) / 2F)

            }
            ScaleType.CENTER_INSIDE -> {
                val scale = 1.0f.coerceAtMost(widthScale.coerceAtMost(heightScale).toFloat())
                mBaseMatrix.postScale(scale, scale)
                mBaseMatrix.postTranslate((viewWidth - drawableWidth * scale) / 2F,
                        (viewHeight - drawableHeight * scale) / 2F)

            }
            else -> {
                var mTempSrc =  RectF(0f, 0f, drawableWidth.toFloat(), drawableHeight.toFloat())
                val mTempDst =  RectF(0f, 0f, viewWidth.toFloat(), viewHeight.toFloat())
                if (mBaseRotation.toInt() % 180 != 0) {
                    mTempSrc = RectF(0f, 0f, drawableHeight.toFloat(), drawableWidth.toFloat())
                }
                when (mScaleType) {
                    ScaleType.FIT_CENTER->
                        mBaseMatrix.setRectToRect(mTempSrc, mTempDst, ScaleToFit.CENTER)
                    ScaleType.FIT_START->
                        mBaseMatrix.setRectToRect(mTempSrc, mTempDst, ScaleToFit.START)
                    ScaleType.FIT_END->
                        mBaseMatrix.setRectToRect(mTempSrc, mTempDst, ScaleToFit.END)
                    ScaleType.FIT_XY->
                        mBaseMatrix.setRectToRect(mTempSrc, mTempDst, ScaleToFit.FILL)
                    else ->
                        mBaseMatrix.setRectToRect(mTempSrc, mTempDst, ScaleToFit.CENTER)
                }
            }
        }
        resetMatrix()
    }

    private fun checkMatrixBounds():Boolean {
        val rect = getDisplayRect(getDrawMatrix())
        val height = rect.height()
        val width = rect.width()
        var deltaX = 0f
        var deltaY = 0f
        val viewHeight = getImageViewHeight(mImageView)
        when {
            height <= viewHeight -> {
                deltaY = when (mScaleType) {
                    ScaleType.FIT_START ->
                        -rect.top
                    ScaleType.FIT_END->
                        viewHeight - height - rect.top
                    else->
                        (viewHeight - height) / 2 - rect.top
                }
                mVerticalScrollEdge = VERTICAL_EDGE_BOTH
            }
            rect.top > 0 -> {
                mVerticalScrollEdge = VERTICAL_EDGE_TOP
                deltaY = -rect.top
            }
            rect.bottom < viewHeight -> {
                mVerticalScrollEdge = VERTICAL_EDGE_BOTTOM
                deltaY = viewHeight - rect.bottom
            }
            else -> {
                mVerticalScrollEdge = VERTICAL_EDGE_NONE
            }
        }
        val viewWidth = getImageViewWidth(mImageView)
        when {
            width <= viewWidth -> {
                deltaX = when (mScaleType) {
                    ScaleType.FIT_START->
                        -rect.left
                    ScaleType.FIT_END->
                        viewWidth - width - rect.left
                    else->
                        (viewWidth - width) / 2 - rect.left
                }
                mHorizontalScrollEdge = HORIZONTAL_EDGE_BOTH
            }
            rect.left > 0 -> {
                mHorizontalScrollEdge = HORIZONTAL_EDGE_LEFT
                deltaX = -rect.left
            }
            rect.right < viewWidth -> {
                deltaX = viewWidth - rect.right
                mHorizontalScrollEdge = HORIZONTAL_EDGE_RIGHT
            }
            else -> {
                mHorizontalScrollEdge = HORIZONTAL_EDGE_NONE
            }
        }
        // Finally actually translate the matrix
        mSuppMatrix.postTranslate(deltaX, deltaY)
        return true
    }

    private fun getImageViewWidth(imageView:ImageView?):Int {
        imageView?.let {
            return imageView.width - imageView.paddingLeft - imageView.paddingRight
        }
        return 0
    }

    private fun getImageViewHeight(imageView:ImageView?):Int {
        imageView?.let {
            return imageView.height - imageView.paddingTop - imageView.paddingBottom
        }
        return 0
    }

    private fun cancelFling() {
        if (mCurrentFlingRunnable != null) {
            mCurrentFlingRunnable?.cancelFling()
            mCurrentFlingRunnable = null
        }
    }

    inner class AnimatedZoomRunnable(currentZoom:Float,targetZoom:Float,focalX:Float,focalY:Float) : Runnable {

        private var mFocalX:Float = 0f
        private var mFocalY:Float = 0f
        private var mStartTime = 0L
        private var mZoomStart = 0f
        private var mZoomEnd = 0f

        init {
            mFocalX = focalX
            mFocalY = focalY
            mStartTime = System.currentTimeMillis()
            mZoomStart = currentZoom
            mZoomEnd = targetZoom
        }

        override fun run() {
            val t = interpolate()
            val scale = mZoomStart + t * (mZoomEnd - mZoomStart)
            val deltaScale = scale / getScale()
            onGestureListener.onScale(deltaScale, mFocalX, mFocalY)
            // We haven't hit our target scale yet, so post ourselves again
            if (t < 1f) {
                mImageView?.let {mImageView->
                    Compat.postOnAnimation(mImageView, this)
                }
            }
        }

        private fun interpolate():Float {
            var t = 1f * (System.currentTimeMillis() - mStartTime) / mZoomDuration
            t = 1f.coerceAtMost(t)
            t = mInterpolator.getInterpolation(t)
            return t
        }
    }

    inner class FlingRunnable(context: Context) : Runnable {

        private var mScroller:OverScroller ?= null
        private var mCurrentX:Int = 0
        private var mCurrentY:Int = 0

        init {
            mScroller = OverScroller(context)
        }

        fun cancelFling() {
            mScroller?.forceFinished(true)
        }

        fun fling(viewWidth:Int, viewHeight:Int, velocityX: Int,
                  velocityY: Int) {
            val rect = getDisplayRect()
            val startX = (-rect.left).roundToInt()
            val minX: Int
            val maxX: Int
            val minY: Int
            val maxY: Int
            if (viewWidth < rect.width()) {
                minX = 0
                maxX = (rect.width() - viewWidth).roundToInt()
            } else {
                minX = startX
                maxX = startX
            }
            val startY = (-rect.top).roundToInt()
            if (viewHeight < rect.height()) {
                minY = 0
                maxY = (rect.height() - viewHeight).roundToInt()
            } else {
                minY = startY
                maxY = startY
            }
            mCurrentX = startX
            mCurrentY = startY
            // If we actually can move, fling the scroller
            if (startX != maxX || startY != maxY) {
                mScroller?.fling(startX, startY, velocityX, velocityY, minX,
                    maxX, minY, maxY, 0, 0)
            }
        }

        override fun run() {
            if (mScroller?.isFinished == true) {
                return // remaining post that should not be handled
            }
            if (mScroller?.computeScrollOffset() == true) {
                val newX = mScroller?.currX
                val newY = mScroller?.currY
                newX?.let {
                    newY?.let {
                        mSuppMatrix.postTranslate((mCurrentX - newX).toFloat(), (mCurrentY - newY).toFloat())
                        checkAndDisplayMatrix()
                        mCurrentX = newX
                        mCurrentY = newY
                        // Post On animation
                        mImageView?.let {mImageView->
                            Compat.postOnAnimation(mImageView, this)
                        }
                    }
                }
            }
        }
    }
}

```

##### Util.kt

```kotlin
package com.lujianfei.plugin2_2.photoview

import android.view.MotionEvent
import android.widget.ImageView
import android.widget.ImageView.ScaleType

internal object Util {
    fun checkZoomLevels(minZoom: Float, midZoom: Float,
                        maxZoom: Float) {
        require(minZoom < midZoom) { "Minimum zoom has to be less than Medium zoom. Call setMinimumZoom() with a more appropriate value" }
        require(midZoom < maxZoom) { "Medium zoom has to be less than Maximum zoom. Call setMaximumZoom() with a more appropriate value" }
    }

    fun hasDrawable(imageView: ImageView): Boolean {
        return imageView.drawable != null
    }

    fun isSupportedScaleType(scaleType: ScaleType?): Boolean {
        if (scaleType == null) {
            return false
        }
        when (scaleType) {
            ScaleType.MATRIX -> throw IllegalStateException("Matrix scale type is not supported")
        }
        return true
    }

    fun getPointerIndex(action: Int): Int {
        return action and MotionEvent.ACTION_POINTER_INDEX_MASK shr MotionEvent.ACTION_POINTER_INDEX_SHIFT
    }
}
```

##### Compat.kt

```kotlin
/*
 Copyright 2011, 2012 Chris Banes.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.lujianfei.plugin2_2.photoview

import android.annotation.TargetApi
import android.os.Build.VERSION
import android.os.Build.VERSION_CODES
import android.view.View

internal object Compat {
    private const val SIXTY_FPS_INTERVAL = 1000 / 60
    fun postOnAnimation(view: View, runnable: Runnable) {
        if (VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN) {
            postOnAnimationJellyBean(view, runnable)
        } else {
            view.postDelayed(runnable, SIXTY_FPS_INTERVAL.toLong())
        }
    }

    @TargetApi(16)
    private fun postOnAnimationJellyBean(view: View, runnable: Runnable) {
        view.postOnAnimation(runnable)
    }
}
```



##### 以下为非核心相关类

##### CustomGestureDetector.kt

```kotlin
/*
 Copyright 2011, 2012 Chris Banes.
 <p/>
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 <p/>
 http://www.apache.org/licenses/LICENSE-2.0
 <p/>
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.lujianfei.plugin2_2.photoview

import android.content.Context
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.ScaleGestureDetector.OnScaleGestureListener
import android.view.VelocityTracker
import android.view.ViewConfiguration

/**
 * Does a whole lot of gesture detecting.
 */
internal class CustomGestureDetector(context: Context?, listener: OnGestureListener) {
    private var mActivePointerId = INVALID_POINTER_ID
    private var mActivePointerIndex = 0
    private val mDetector: ScaleGestureDetector
    private var mVelocityTracker: VelocityTracker? = null
    var isDragging = false
        private set
    private var mLastTouchX = 0f
    private var mLastTouchY = 0f
    private val mTouchSlop: Float
    private val mMinimumVelocity: Float
    private val mListener: OnGestureListener
    private fun getActiveX(ev: MotionEvent): Float {
        return try {
            ev.getX(mActivePointerIndex)
        } catch (e: Exception) {
            ev.x
        }
    }

    private fun getActiveY(ev: MotionEvent): Float {
        return try {
            ev.getY(mActivePointerIndex)
        } catch (e: Exception) {
            ev.y
        }
    }

    val isScaling: Boolean
        get() = mDetector.isInProgress

    fun onTouchEvent(ev: MotionEvent): Boolean {
        return try {
            mDetector.onTouchEvent(ev)
            processTouchEvent(ev)
        } catch (e: IllegalArgumentException) {
            // Fix for support lib bug, happening when onDestroy is called
            true
        }
    }

    private fun processTouchEvent(ev: MotionEvent): Boolean {
        val action = ev.action
        when (action and MotionEvent.ACTION_MASK) {
            MotionEvent.ACTION_DOWN -> {
                mActivePointerId = ev.getPointerId(0)
                mVelocityTracker = VelocityTracker.obtain()
                if (null != mVelocityTracker) {
                    mVelocityTracker!!.addMovement(ev)
                }
                mLastTouchX = getActiveX(ev)
                mLastTouchY = getActiveY(ev)
                isDragging = false
            }
            MotionEvent.ACTION_MOVE -> {
                val x = getActiveX(ev)
                val y = getActiveY(ev)
                val dx = x - mLastTouchX
                val dy = y - mLastTouchY
                if (!isDragging) {
                    // Use Pythagoras to see if drag length is larger than
                    // touch slop
                    isDragging = Math.sqrt(dx * dx + (dy * dy).toDouble()) >= mTouchSlop
                }
                if (isDragging) {
                    mListener.onDrag(dx, dy)
                    mLastTouchX = x
                    mLastTouchY = y
                    if (null != mVelocityTracker) {
                        mVelocityTracker!!.addMovement(ev)
                    }
                }
            }
            MotionEvent.ACTION_CANCEL -> {
                mActivePointerId = INVALID_POINTER_ID
                // Recycle Velocity Tracker
                if (null != mVelocityTracker) {
                    mVelocityTracker!!.recycle()
                    mVelocityTracker = null
                }
            }
            MotionEvent.ACTION_UP -> {
                mActivePointerId = INVALID_POINTER_ID
                if (isDragging) {
                    if (null != mVelocityTracker) {
                        mLastTouchX = getActiveX(ev)
                        mLastTouchY = getActiveY(ev)

                        // Compute velocity within the last 1000ms
                        mVelocityTracker!!.addMovement(ev)
                        mVelocityTracker!!.computeCurrentVelocity(1000)
                        val vX = mVelocityTracker!!.xVelocity
                        val vY = mVelocityTracker!!
                                .yVelocity

                        // If the velocity is greater than minVelocity, call
                        // listener
                        if (Math.max(Math.abs(vX), Math.abs(vY)) >= mMinimumVelocity) {
                            mListener.onFling(mLastTouchX, mLastTouchY, -vX,
                                    -vY)
                        }
                    }
                }

                // Recycle Velocity Tracker
                if (null != mVelocityTracker) {
                    mVelocityTracker!!.recycle()
                    mVelocityTracker = null
                }
            }
            MotionEvent.ACTION_POINTER_UP -> {
                val pointerIndex = Util.getPointerIndex(ev.action)
                val pointerId = ev.getPointerId(pointerIndex)
                if (pointerId == mActivePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    val newPointerIndex = if (pointerIndex == 0) 1 else 0
                    mActivePointerId = ev.getPointerId(newPointerIndex)
                    mLastTouchX = ev.getX(newPointerIndex)
                    mLastTouchY = ev.getY(newPointerIndex)
                }
            }
        }
        mActivePointerIndex = ev
                .findPointerIndex(if (mActivePointerId != INVALID_POINTER_ID) mActivePointerId else 0)
        return true
    }

    companion object {
        private const val INVALID_POINTER_ID = -1
    }

    init {
        val configuration = ViewConfiguration
                .get(context)
        mMinimumVelocity = configuration.scaledMinimumFlingVelocity.toFloat()
        mTouchSlop = configuration.scaledTouchSlop.toFloat()
        mListener = listener
        val mScaleListener: OnScaleGestureListener = object : OnScaleGestureListener {
            override fun onScale(detector: ScaleGestureDetector): Boolean {
                val scaleFactor = detector.scaleFactor
                if (java.lang.Float.isNaN(scaleFactor) || java.lang.Float.isInfinite(scaleFactor)) return false
                if (scaleFactor >= 0) {
                    mListener.onScale(scaleFactor,
                            detector.focusX, detector.focusY)
                }
                return true
            }

            override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
                return true
            }

            override fun onScaleEnd(detector: ScaleGestureDetector) {
                // NO-OP
            }
        }
        mDetector = ScaleGestureDetector(context, mScaleListener)
    }
}
```

##### OnGestureListenerkt

```kotlin
/*
 Copyright 2011, 2012 Chris Banes.

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package com.lujianfei.plugin2_2.photoview

internal interface OnGestureListener {
    fun onDrag(dx: Float, dy: Float)
    fun onFling(startX: Float, startY: Float, velocityX: Float,
                velocityY: Float)

    fun onScale(scaleFactor: Float, focusX: Float, focusY: Float)
}
```

##### OnMatrixChangedListener.kt

```kotlin
package com.lujianfei.plugin2_2.photoview

import android.graphics.RectF

/**
 * Interface definition for a callback to be invoked when the internal Matrix has changed for
 * this View.
 */
interface OnMatrixChangedListener {
    /**
     * Callback for when the Matrix displaying the Drawable has changed. This could be because
     * the View's bounds have changed, or the user has zoomed.
     *
     * @param rect - Rectangle displaying the Drawable's new bounds.
     */
    fun onMatrixChanged(rect: RectF?)
}
```

##### OnOutsidePhotoTapListener.kt

```kotlin
package com.lujianfei.plugin2_2.photoview

import android.widget.ImageView

/**
 * Callback when the user tapped outside of the photo
 */
interface OnOutsidePhotoTapListener {
    /**
     * The outside of the photo has been tapped
     */
    fun onOutsidePhotoTap(imageView: ImageView?)
}
```
##### OnPhotoTapListener.kt

```kotlin
package com.lujianfei.plugin2_2.photoview

import android.widget.ImageView

/**
 * A callback to be invoked when the Photo is tapped with a single
 * tap.
 */
interface OnPhotoTapListener {
    /**
     * A callback to receive where the user taps on a photo. You will only receive a callback if
     * the user taps on the actual photo, tapping on 'whitespace' will be ignored.
     *
     * @param view ImageView the user tapped.
     * @param x    where the user tapped from the of the Drawable, as percentage of the
     * Drawable width.
     * @param y    where the user tapped from the top of the Drawable, as percentage of the
     * Drawable height.
     */
    fun onPhotoTap(view: ImageView?, x: Float, y: Float)
}
```



##### OnScaleChangedListener.kt

```kotlin
package com.lujianfei.plugin2_2.photoview

/**
 * Interface definition for callback to be invoked when attached ImageView scale changes
 */
interface OnScaleChangedListener {
    /**
     * Callback for when the scale changes
     *
     * @param scaleFactor the scale factor (less than 1 for zoom out, greater than 1 for zoom in)
     * @param focusX      focal point X position
     * @param focusY      focal point Y position
     */
    fun onScaleChange(scaleFactor: Float, focusX: Float, focusY: Float)
}
```

##### OnSingleFlingListener.kt

```kotlin
package com.lujianfei.plugin2_2.photoview

import android.view.MotionEvent

/**
 * A callback to be invoked when the ImageView is flung with a single
 * touch
 */
interface OnSingleFlingListener {
    /**
     * A callback to receive where the user flings on a ImageView. You will receive a callback if
     * the user flings anywhere on the view.
     *
     * @param e1        MotionEvent the user first touch.
     * @param e2        MotionEvent the user last touch.
     * @param velocityX distance of user's horizontal fling.
     * @param velocityY distance of user's vertical fling.
     */
    fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean
}
```

##### OnViewDragListener.kt

```kotlin
package com.lujianfei.plugin2_2.photoview

/**
 * Interface definition for a callback to be invoked when the photo is experiencing a drag event
 */
interface OnViewDragListener {
    /**
     * Callback for when the photo is experiencing a drag event. This cannot be invoked when the
     * user is scaling.
     *
     * @param dx The change of the coordinates in the x-direction
     * @param dy The change of the coordinates in the y-direction
     */
    fun onDrag(dx: Float, dy: Float)
}
```

##### OnViewTapListener.kt

```kotlin
package com.lujianfei.plugin2_2.photoview

import android.view.View

interface OnViewTapListener {
    /**
     * A callback to receive where the user taps on a ImageView. You will receive a callback if
     * the user taps anywhere on the view, tapping on 'whitespace' will not be ignored.
     *
     * @param view - View the user tapped.
     * @param x    - where the user tapped from the left of the View.
     * @param y    - where the user tapped from the top of the View.
     */
    fun onViewTap(view: View?, x: Float, y: Float)
}
```

