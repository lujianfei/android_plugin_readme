# AppbarLayout 吸顶实现 代码展示

![img](https://img-blog.csdnimg.cn/c37130a6585545579b39c929192c808f.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAcXFfMzgyMzcyMjQ=,size_19,color_FFFFFF,t_70,g_se,x_16)

以下只展示关键部分

##### fragment_collapsing_toolbar_layout.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent">
    <!-- 标题栏部分 -->
    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="56dp"
        app:layout_constraintTop_toTopOf="parent" />
    <!-- 组件协调联动布局 -->
    <androidx.coordinatorlayout.widget.CoordinatorLayout
        android:layout_width="match_parent"
        android:layout_height="0dp"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintTop_toBottomOf="@id/toolbar">

        <com.google.android.material.appbar.AppBarLayout
            android:id="@+id/appBar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content">
            <!-- 可折叠区域 -->
            <com.google.android.material.appbar.CollapsingToolbarLayout
                android:id="@+id/collsping_Toolbar"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:layout_scrollFlags="scroll|exitUntilCollapsed">
                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="300dp"
                    android:background="#ff0000"
                    android:text="可折叠区域"
                    android:textColor="@color/white"
                    android:textStyle="bold"
                    android:textSize="25sp"
                    android:gravity="center"
                    />
            </com.google.android.material.appbar.CollapsingToolbarLayout>

            <!-- 不可折叠区域 -->
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:layout_collapseMode="pin">
                <TextView
                    android:layout_width="match_parent"
                    android:layout_height="56dp"
                    android:background="#dddddd"
                    android:textColor="@color/black"
                    android:gravity="center_vertical"
                    android:padding="15dp"
                    android:text="不可折叠区域 (用作吸顶标题)"/>
            </LinearLayout>
        </com.google.android.material.appbar.AppBarLayout>

        <androidx.recyclerview.widget.RecyclerView
            android:id="@+id/list"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_behavior="@string/appbar_scrolling_view_behavior" />
    </androidx.coordinatorlayout.widget.CoordinatorLayout>
    <!-- 组件协调联动布局 end -->
</androidx.constraintlayout.widget.ConstraintLayout>
```

##### CollapsingToolBarLayoutFragment.kt

```kotlin
package com.lujianfei.other.ui

import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.BaseViewHolder
import com.google.android.material.appbar.AppBarLayout
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.other.R
import com.lujianfei.other.listener.OffsetListener

/**
 * Author: mac
 * Date: 31.5.22 2:30 下午
 * Email:johnson@miaoshitech.com
 * Description: 折叠标题用法
 */
class CollapsingToolBarLayoutFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    private var toolbar: PluginToolBar?= null
    private var list: RecyclerView? = null
    private var appBar: AppBarLayout? = null
    private val mMyAdapter by lazy { MyAdapter() }

    override fun resouceId() = R.layout.fragment_collapsing_toolbar_layout

    override fun initView() {
        toolbar = view?.findViewById(R.id.toolbar)
        list = view?.findViewById(R.id.list)
        appBar = view?.findViewById(R.id.appBar)
        toolbar?.setTitle("CollapsingToolBarLayout")

        list?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mMyAdapter
        }

        appBar?.addOnOffsetChangedListener(OffsetListener())
    }

    override fun initData() {
        val datas = arrayListOf<String>()
        for (index in 0 until 20) {
            datas.add("Title $index")
        }
        mMyAdapter.setNewData(datas)
    }

    override fun initEvent() {
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }

    class MyAdapter: BaseQuickAdapter<String,BaseViewHolder>(R.layout.other_adapter_main) {
        override fun convert(helper: BaseViewHolder?, item: String?) {
            val title: TextView? = helper?.getView(R.id.title)
            val summary: TextView? = helper?.getView(R.id.summary)
            title?.text = item
        }
    }
}
```

折叠状态监听器

##### OffsetListener.kt

```kotlin
package com.lujianfei.other.listener

import com.google.android.material.appbar.AppBarLayout
import com.lujianfei.module_plugin_base.utils.LogUtils

/**
 * Author: mac
 * Date: 31.5.22 5:51 下午
 * Email:johnson@miaoshitech.com
 * Description: 折叠状态监听器
 */
class OffsetListener : AppBarLayout.OnOffsetChangedListener {

    companion object {
        const val TAG = "OffsetListener="
    }

    override fun onOffsetChanged(appBarLayout: AppBarLayout?, verticalOffset: Int) {
        if (appBarLayout == null) return
        //未发生滑动时为展开状态
        val expandStatus = (verticalOffset == 0)
        //滑动位移超过AppBar最大可滑动距离时，视为折叠状态
        val collapseStatus = (Math.abs(verticalOffset) >= appBarLayout.totalScrollRange)
        if (expandStatus) {
            LogUtils.d(TAG, "展开状态")
        } else if (collapseStatus) {
            LogUtils.d(TAG, "折叠状态")
        } else {
            val almostCollapseStatus = (Math.abs(verticalOffset) >= (appBarLayout.totalScrollRange /2))
            if(almostCollapseStatus){
                //折叠中状态
                val alpha = 255 * Math.abs(verticalOffset) / appBarLayout.totalScrollRange
                LogUtils.d(TAG, "折叠中状态 ($verticalOffset, ${appBarLayout.totalScrollRange},$alpha)")
            }else {
                LogUtils.d(TAG, "展开状态2")
            }
        }
    }
}
```