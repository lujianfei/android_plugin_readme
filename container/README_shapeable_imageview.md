# ShapeableImageView 代码展示

以下只展示关键部分

添加依赖

```
implementation 'com.google.android.material:material:1.2.0'
```



##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >   

   <ScrollView
       android:layout_width="match_parent"
       android:layout_height="wrap_content"
       android:orientation="vertical">
        <LinearLayout
            android:id="@+id/layoutButtonContainer"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:gravity="center_horizontal"
            android:orientation="vertical"/>
   </ScrollView>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_17

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.imageview.ShapeableImageView
import com.google.android.material.shape.CutCornerTreatment
import com.google.android.material.shape.RelativeCornerSize
import com.google.android.material.shape.RoundedCornerTreatment
import com.google.android.material.shape.ShapeAppearanceModel
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }   

    private var layoutButtonContainer: LinearLayout? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        layoutButtonContainer = findViewById(R.id.layoutButtonContainer)

        that?.let {
            that->
            layoutButtonContainer?.addView(createTextView(that,"原图"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that))

            layoutButtonContainer?.addView(createTextView(that,"圆角图片"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that,
                    ShapeAppearanceModel.builder()
                            .setAllCornerSizes(AbsoluteCornerSize(DensityUtils.dip2px(20f).toFloat()))
                            .build()))
            
            layoutButtonContainer?.addView(createTextView(that,"圆形图片"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that,
                    ShapeAppearanceModel.builder()
                    .setAllCornerSizes(RelativeCornerSize(0.5f))
                    .build()))

            layoutButtonContainer?.addView(createTextView(that,"切角图片"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that,
                    ShapeAppearanceModel.builder()
                            .setAllCorners(CutCornerTreatment())
                            .setAllCornerSizes(DensityUtils.dip2px(15f).toFloat())
                    .build()))

            layoutButtonContainer?.addView(createTextView(that,"菱形图片"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that,
                    ShapeAppearanceModel.builder()
                            .setAllCorners(CutCornerTreatment())
                            .setAllCornerSizes(RelativeCornerSize(0.5f))
                    .build()))

            layoutButtonContainer?.addView(createTextView(that,"左上角90度扇形图片"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that,
                    ShapeAppearanceModel.builder()
                            .setTopLeftCorner(RoundedCornerTreatment())
                            .setTopLeftCornerSize(RelativeCornerSize(1f))
                    .build()))

            layoutButtonContainer?.addView(createTextView(that,"火箭头图片"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that,
                    ShapeAppearanceModel.builder()
                            .setTopLeftCorner(RoundedCornerTreatment())
                            .setTopRightCorner(RoundedCornerTreatment())
                            .setTopLeftCornerSize(RelativeCornerSize(0.7f))
                            .setTopRightCornerSize(RelativeCornerSize(0.7f))
                    .build()))

            layoutButtonContainer?.addView(createTextView(that,"水滴"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that,
                    ShapeAppearanceModel.builder()
                            .setAllCorners(RoundedCornerTreatment())
                            .setTopLeftCornerSize(RelativeCornerSize(0.7f))
                            .setTopRightCornerSize(RelativeCornerSize(0.7f))
                            .setBottomLeftCornerSize(DensityUtils.dip2px(25f).toFloat())
                            .setBottomRightCornerSize(DensityUtils.dip2px(25f).toFloat())
                    .build()))

            layoutButtonContainer?.addView(createTextView(that,"叶子图片"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that,
                    ShapeAppearanceModel.builder()
                            .setTopLeftCorner(RoundedCornerTreatment())
                            .setBottomRightCorner(RoundedCornerTreatment())
                            .setTopLeftCornerSize(RelativeCornerSize(0.5f))
                            .setBottomRightCornerSize(RelativeCornerSize(0.5f))
                    .build()))

            layoutButtonContainer?.addView(createTextView(that,"tip图片"
            ))
            layoutButtonContainer?.addView(createShapeableImageView(that,
                    ShapeAppearanceModel.builder()
                            .setTopLeftCorner(RoundedCornerTreatment())
                            .setBottomLeftCorner(RoundedCornerTreatment())
                            .setTopRightCorner(CutCornerTreatment())
                            .setBottomRightCorner(CutCornerTreatment())
                            .setAllCornerSizes(RelativeCornerSize(0.5f))
                    .build()))
        }
    }

    private fun createTextView(that: AppCompatActivity, title:String) : TextView {
        val textView = TextView(that)
        textView.textSize = 20f
        textView.setTextColor(0xff000000.toInt())
        textView.text = title
        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT)
        textView.layoutParams = lp
        lp.topMargin = DensityUtils.dip2px(20f)
        return textView
    }

    private fun createShapeableImageView(that: AppCompatActivity, mShapeAppearanceModel: ShapeAppearanceModel? = null) : ShapeableImageView {
        val imageView = ShapeableImageView(ResUtils.context)
        imageView.scaleType = ImageView.ScaleType.FIT_XY
        Glide.with(that).load(ImageMock.urls[0]).into(imageView)
        val lp = LinearLayout.LayoutParams(DensityUtils.dip2px(200f),DensityUtils.dip2px(200f))
        imageView.layoutParams = lp
        lp.topMargin = DensityUtils.dip2px(5f)
        if (mShapeAppearanceModel != null)  imageView.shapeAppearanceModel = mShapeAppearanceModel
        return imageView
    }  
}

```



除了以上代码的写法，你还可以采用 xml 的方式做设置

```xml
<!-- 圆角图片 -->
<style name="roundImageStyle">
    <item name="cornerFamily">rounded</item>
    <item name="cornerSize">20dp</item>
</style>

<!-- 圆形图片 -->
<style name="circleImageStyle">
    <item name="cornerFamily">rounded</item>
    <item name="cornerSize">50%</item>
</style>

<!-- 切角图片 -->
<style name="cutImageStyle">
    <item name="cornerFamily">cut</item>
    <item name="cornerSize">15dp</item>
</style>

<!-- 菱形图片 -->
<style name="diamondImageStyle">
    <item name="cornerFamily">cut</item>
    <item name="cornerSize">50%</item>
</style>

<!-- 左上角90度扇形图片 -->
<style name="topLeftRoundImageStyle">
    <item name="cornerFamilyTopLeft">rounded</item>
    <item name="cornerSizeTopLeft">100%</item>
</style>

<!-- 火箭头图片 -->
<style name="rocketImageStyle">
    <item name="cornerFamilyTopLeft">rounded</item>
    <item name="cornerFamilyTopRight">rounded</item>
    <item name="cornerSizeTopLeft">70%</item>
    <item name="cornerSizeTopRight">70%</item>
</style>

<!-- 水滴 -->
<style name="waterImageStyle">
    <item name="cornerFamilyBottomLeft">rounded</item>
    <item name="cornerFamilyBottomRight">rounded</item>
    <item name="cornerFamilyTopLeft">rounded</item>
    <item name="cornerFamilyTopRight">rounded</item>
    <item name="cornerSizeBottomLeft">25dp</item>
    <item name="cornerSizeBottomRight">25dp</item>
    <item name="cornerSizeTopLeft">70%</item>
    <item name="cornerSizeTopRight">70%</item>
</style>

<!-- 叶子图片 -->
<style name="leafImageStyle">
    <item name="cornerFamilyTopLeft">rounded</item>
    <item name="cornerFamilyBottomRight">rounded</item>
    <item name="cornerSizeTopLeft">50%</item>
    <item name="cornerSizeBottomRight">50%</item>
</style>

<!-- tip图片 -->
<style name="tipImageStyle">
    <item name="cornerFamilyTopLeft">rounded</item>
    <item name="cornerSizeTopLeft">50%</item>

    <item name="cornerFamilyBottomLeft">rounded</item>
    <item name="cornerSizeBottomLeft">50%</item>

    <item name="cornerFamilyTopRight">cut</item>
    <item name="cornerSizeTopRight">50%</item>

    <item name="cornerFamilyBottomRight">cut</item>
    <item name="cornerSizeBottomRight">50%</item>
</style>
```

最终引用的方式如下

```xml
<com.google.android.material.imageview.ShapeableImageView
    android:id="@+id/image1"
    android:layout_width="100dp"
    android:layout_height="100dp"
    android:padding="1dp"
    android:src="@mipmap/ic_launcher"
    app:shapeAppearanceOverlay="@style/circleImageStyle"
     />
```

