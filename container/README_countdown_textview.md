# 验证码倒计时 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
    
    ...
    
    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="10dp"
        android:padding="5dp">
        
        <com.lujianfei.plugin2_10.VerificationCodeTextView
            android:id="@+id/verificationCodeTextView"
            android:layout_width="100dp"
            android:layout_height="50dp"
            android:layout_marginStart="5dp"
            android:layout_alignParentEnd="true"
            />

        <EditText
            android:layout_toStartOf="@id/verificationCodeTextView"
            android:layout_width="match_parent"
            android:layout_height="55dp"
            android:hint="请输入验证码"
            />
    </RelativeLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_10

import android.content.Intent
import android.net.Uri
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils


class MainActivity : BasePluginActivity() {    
    
    ...
    
    private var verificationCodeTextView:VerificationCodeTextView ?= null
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        verificationCodeTextView = findViewById(R.id.verificationCodeTextView)                
    }

    override fun onDestroy() {
        verificationCodeTextView?.release()
        super.onDestroy()
    }
}

```

核心自定义组件

##### VerificationCodeTextView.kt

```kotlin
package com.lujianfei.plugin2_10

import android.content.Context
import android.os.CountDownTimer
import android.util.AttributeSet
import android.view.Gravity

/**
 *@date     创建时间:2020/10/23
 *@name     作者:陆键霏
 *@describe 描述:
 */
class VerificationCodeTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : androidx.appcompat.widget.AppCompatTextView(context, attrs, defStyleAttr) {
    
    var mCountDownTimer:CountDownTimer ?= null
    var countDownSec = 5
    init {
        text = "获取验证码"
        setTextColor(0xff000000.toInt())
        background = context.resources.getDrawable(R.drawable.selector_verification_code)
        gravity = Gravity.CENTER
        setOnClickListener { 
            startCountDown()
        }
    }
    
    private fun startCountDown() {
        isEnabled = false
        // 倒计时处理
        mCountDownTimer = object : CountDownTimer((countDownSec + 1).toLong() * 1000,1000) {
            override fun onTick(millisUntilFinished: Long) {
                val l = millisUntilFinished / 1000
                text = "重新获取 ($l)"
                if (l == 0L) {
                    mCountDownTimer?.cancel()
                    isEnabled = true
                    text = "获取验证码"
                }
            }
            override fun onFinish() {
            }
        }
        mCountDownTimer?.start()
    }

    fun release() {
        mCountDownTimer?.cancel()
        mCountDownTimer = null
    }
}
```

