# 设置TextView渐变色 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:background="#dddddd">    

    <TextView
        android:id="@+id/txt_horizontal"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center_horizontal"
        android:textSize="50sp"
        android:padding="10dp"
        android:text="横向渐变字体"
        />

    <TextView
        android:id="@+id/txt_vertical"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center_horizontal"
        android:textSize="50sp"
        android:padding="10dp"
        android:text="纵向渐变字体"
        />
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_21

import android.content.Intent
import android.graphics.LinearGradient
import android.graphics.Shader
import android.net.Uri
import android.view.View
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import kotlin.math.ceil


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
        const val RED_COLOR = 0xFFFF0000.toInt()
        const val YELLOW_COLOR = 0xFFFFFF00.toInt()
    }
      
    private var txt_horizontal:TextView ?= null
    private var txt_vertical:TextView ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {      
        txt_horizontal = findViewById(R.id.txt_horizontal)
        txt_vertical = findViewById(R.id.txt_vertical)

        txt_horizontal?.setGradientHorizontal(startColor = RED_COLOR, endColor = YELLOW_COLOR)
        txt_vertical?.setGradientVertical(startColor = RED_COLOR, endColor = YELLOW_COLOR)
    }  
}

/**
 * 设置文字的渐变颜色
 * 渐变方向是横向的，后续可以扩展改变渐变的方向
 * @param startColor 渐变开始颜色
 * @param endColor 渐变结束颜色
 */
fun TextView.setGradientHorizontal(startColor: Int, endColor: Int) {
    val textWidth = paint.measureText(text.toString())
    val linearGradient = LinearGradient(
        0f, 0f, textWidth, 0f,
        startColor, endColor, Shader.TileMode.CLAMP
    )
    paint.shader = linearGradient
    invalidate()
}

fun TextView.setGradientVertical(startColor: Int, endColor: Int) {
    val fm = paint.fontMetrics
    val height = (ceil(fm.bottom.toDouble() - fm.top.toDouble())).toFloat()
    val linearGradient =
        LinearGradient(0f, 0f, 0f, height, startColor, endColor, Shader.TileMode.CLAMP)
    paint.shader = linearGradient
    invalidate()
}

```
