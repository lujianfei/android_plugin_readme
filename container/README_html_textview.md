# android Html textview 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
    
    ...
    
    <LinearLayout
        android:id="@+id/layoutContainer"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:showDividers="middle">
        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textSize="28sp"
            android:text="Html 源码:"/>
        <TextView
            android:id="@+id/txtHtmlSourceCode"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:layout_weight="1"
            />
        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textSize="28sp"
            android:text="Html 视图 (使用了 Html.fromHtml):"/>
        <TextView
            android:id="@+id/txtHtmlShowView"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:layout_weight="1"
            />
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_3

import android.content.Intent
import android.net.Uri
import android.text.Html
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.plugin2_3.utils.FileUtils
import com.lujianfei.plugin2_3.utils.HtmlImageGetter


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

	...
    private var txtHtmlSourceCode: TextView? = null
    private var txtHtmlShowView: TextView? = null
    private var layoutContainer: LinearLayout? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        layoutContainer = findViewById(R.id.layoutContainer)
        txtHtmlSourceCode = findViewById(R.id.txtHtmlSourceCode)
        txtHtmlShowView = findViewById(R.id.txtHtmlShowView)
        ...

        layoutContainer?.dividerDrawable = getPluginDrawable(R.drawable.shape_divider)
    }

    ...

    override fun initEvent() {
       ...
        val readHtmlSource = FileUtils.readHtmlSource()
        txtHtmlSourceCode?.text = readHtmlSource

        txtHtmlShowView?.context?.let { context ->
            txtHtmlShowView?.text = Html.fromHtml(txtHtmlSourceCode?.text.toString(), Html.FROM_HTML_MODE_LEGACY, HtmlImageGetter(context,txtHtmlShowView) {
                txtHtmlShowView?.text = Html.fromHtml(txtHtmlSourceCode?.text.toString(), Html.FROM_HTML_MODE_LEGACY, HtmlImageGetter(context,txtHtmlShowView), null)
            }, null)
        }
    }

    ...
}

```

##### 下面是核心代码

##### HtmlImageGetter.kt

```kotlin
package com.lujianfei.plugin2_3.utils

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Html
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.plugin2_3.R
import java.lang.ref.WeakReference


/**
 *@date     创建时间:2020/7/9
 *@name     作者:陆键霏
 *@describe 描述:
 */
class HtmlImageGetter(context: Context,view:View? ,callback:(()->Unit)?=null) : Html.ImageGetter {
    companion object {
        const val TAG = "HtmlImageGetter"
    }
    
    private var mContext: Context? = null
    private var callback:(()->Unit)?=null
    private var mView:View ?= null
    init {
        mView = view
        mContext = context
        this.callback = callback
    }

    override fun getDrawable(source: String?): Drawable {
        if (source.isNullOrEmpty()) return defaultDrawable()
        // 使用 Glide 下载图片
        downloadWithGlide(source)
        
        val weakDrawable = HtmlDrawableCacheManager.INSTANCE.get(source)
        return if (weakDrawable?.get() != null) { // 有缓存的情况，直接返回缓存
            val drawable = weakDrawable.get()
            drawable?.let { drawable ->
                drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
            }
            Log.d(TAG, "getDrawable $drawable")
            drawable!!
        } else {
            Log.d(TAG, "getDrawable defaultDrawable")
            defaultDrawable() // 无缓存，直接返回默认图片
        }
    }

    private fun defaultDrawable(): Drawable {
        // 从缓存获取之前下载好的图片
        val localDrawable = ResUtils.getPluginDrawable(R.drawable.ic_default_image)
        localDrawable?.setBounds(0,0,localDrawable.intrinsicWidth,localDrawable.intrinsicHeight)
        return localDrawable!!
    }

    private fun downloadWithGlide(source: String?) {
        Glide.with(mContext!!).asDrawable()
            .load(source)
            .addListener(object : RequestListener<Drawable> {
                override fun onLoadFailed(
                    e: GlideException?,
                    model: Any?,
                    target: Target<Drawable>?,
                    isFirstResource: Boolean
                ): Boolean {
                    return false
                }

                override fun onResourceReady(
                    resource: Drawable?,
                    model: Any?,
                    target: Target<Drawable>?,
                    dataSource: DataSource?,
                    isFirstResource: Boolean
                ): Boolean {
                    resource?.let {
                        source?.let {
                            HtmlDrawableCacheManager.INSTANCE.put(source, WeakReference(resource))
                            mView?.post {
                                callback?.invoke()
                            }
                        }
                    }
                    return false
                }
            }).submit()
    }
}
```

##### 用于缓存的

##### HtmlDrawableCacheManager.kt

```kotlin
package com.lujianfei.plugin2_3.utils

import android.graphics.drawable.Drawable
import java.lang.ref.WeakReference

/**
 *@date     创建时间:2020/7/9
 *@name     作者:陆键霏
 *@describe 描述:
 */
class HtmlDrawableCacheManager {
    companion object {
        val INSTANCE  = HtmlDrawableCacheManager()
    }
    
    private val map = mutableMapOf<String,WeakReference<Drawable>>()
    
    fun put(imageUrl:String, drawable:WeakReference<Drawable>) {
        map[imageUrl] = drawable
    }
    
    fun get(imageUrl: String):WeakReference<Drawable>? {
        return map[imageUrl]
    }
}
```

