# TextView自动调整字体大小 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:background="#dddddd"
    >
       

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">

        <TextView
            android:id="@+id/autoSizeTextView"
            android:layout_width="match_parent"
            android:layout_height="300dp"
            android:layout_margin="10dp"
            android:gravity="center"
            android:background="#ffffff"/>

        <com.lujianfei.module_plugin_base.widget.PluginButton
            android:id="@+id/bt_add"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"
            android:layout_marginStart="10dp"
            android:layout_marginEnd="10dp"
            android:text="添加文字"/>

        <com.lujianfei.module_plugin_base.widget.PluginButton
            android:id="@+id/bt_clear"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"
            android:layout_marginStart="10dp"
            android:layout_marginEnd="10dp"
            android:text="清空"/>

        <CheckBox
            android:id="@+id/cbx_with_return"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="带回车"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_19

import android.content.Intent
import android.net.Uri
import android.util.TypedValue
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.widget.TextViewCompat
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
        const val ADD_TEXT = "123456789abc"
        val MAX_TEXT_SIZE = DensityUtils.dip2px(40f)
        val MIN_TEXT_SIZE = DensityUtils.dip2px(1f)
    }
       
    private var autoSizeTextView: TextView?= null
    private var bt_add: Button?= null
    private var bt_clear: Button?= null
    private var cbx_with_return: CheckBox?= null
    private var sbText = StringBuilder()

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {       
        autoSizeTextView = findViewById(R.id.autoSizeTextView)
        bt_add = findViewById(R.id.bt_add)
        bt_clear = findViewById(R.id.bt_clear)
        cbx_with_return = findViewById(R.id.cbx_with_return)
        autoSizeTextView?.let {autoSizeTextView->
            // 设置关键属性, 字体最大和最小设定
            TextViewCompat.setAutoSizeTextTypeUniformWithConfiguration(autoSizeTextView,MIN_TEXT_SIZE, MAX_TEXT_SIZE,2, TypedValue.COMPLEX_UNIT_PX)
        }
    }
   
    override fun initEvent() {
        bt_add?.setOnClickListener {
            sbText.append(ADD_TEXT)
            if (cbx_with_return?.isChecked == true) {
                sbText.append("\r\n")
            }
            autoSizeTextView?.text = sbText.toString()
        }
        bt_clear?.setOnClickListener {
            sbText.clear()
            autoSizeTextView?.text = ""
        }
    }
}

```