# 自定义TextView展开更多 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
    
   <ScrollView
       android:layout_width="match_parent"
       android:layout_height="wrap_content"
       android:orientation="vertical">
        <LinearLayout
            android:id="@+id/layoutButtonContainer"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"/>
   </ScrollView>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_18

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.view.View
import android.widget.LinearLayout
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }
       
    private var layoutButtonContainer: LinearLayout? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {    
        layoutButtonContainer = findViewById(R.id.layoutButtonContainer)
    }

    override fun initData() {
        bean = that?.intent?.getParcelableExtra("data")
        bean?.let { bean ->
            toolbar?.getRightTopTextView()?.visibility = if (bean.codeUrl.isNullOrEmpty()) View.GONE else View.VISIBLE
            toolbar?.setTitle(bean.itemName)
        }
        that?.let {context->
            val textView1 = createMoreTextView(context = context,
                    text = "继承TextView，拿原始要显示的行数和设置的最大行数最对比")
            layoutButtonContainer?.addView(textView1)

            val textView2 = createMoreTextView(context = context,
                    text = "继承TextView，拿原始要显示的行数和设置的最大行数最对比。运用TextPaint计算出自定义文本的长度。运用Layout和TextUtils得出最后一行要显示的文本和前面行的文本。继承TextView，拿原始要显示的行数和设置的最大行数最对比")
            layoutButtonContainer?.addView(textView2)
        }
    }

    private fun createMoreTextView(context:Context, text:String):View {
        val moreTextView = MoreTextView(context)
        moreTextView.setBackgroundColor(0xffd0d0d0.toInt())
        moreTextView.maxLines = 2
        moreTextView.textSize = 20f
        moreTextView.text = text
        val lp = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT)
        lp.topMargin = DensityUtils.dip2px(20f)
        moreTextView.layoutParams = lp
        return moreTextView
    }  
}

```

核心组件类

##### MoreTextView.kt

```kotlin
package com.lujianfei.plugin2_18

import android.content.Context
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatTextView

class MoreTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    companion object {
        /**
         * 结尾按钮文本
         */
        private const val tailText = "…查看全文"
        /**
         * 结尾按钮颜色
         */
        private const val tailTextColor = 0xff0000ff.toInt()
        private const val LINE_BREAKER = "\n"
        /**
         * 收起按钮文本
         */
        private const val tailFoldText = " 收起"
    }

    private var showTail = true
    private var isEllipsed = false
    private var fullText = ""
    private var lastMaxLine = 0

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        //不显示小尾巴或者已经处理过的不再处理
        if (!showTail || isEllipsed) {
            val finalString = SpannableStringBuilder(fullText)
            finalString.append(tailFoldText)
            // 设置收起按钮颜色
            finalString.setSpan(ForegroundColorSpan(tailTextColor), finalString.length - tailFoldText.length, finalString.length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            // 收起按钮加入点击事件
            finalString.setSpan(FoldClickSpan(),finalString.length - tailFoldText.length, finalString.length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            movementMethod = LinkMovementMethod.getInstance()
            text = finalString
            return
        }
        // 行数没有到maxLines不处理
        if (maxLines == 0 || lineCount < maxLines || TextUtils.isEmpty(text)) ; else {
            val lineEndIndex = layout.getLineEnd(maxLines - 1) //第maxLines行的首字符offset
            val lineStartIndex = layout.getLineStart(maxLines - 1) //第(maxLines - 1)行的首字符offset
            if (lineEndIndex >= text.length) return
            val mustShowText = text.subSequence(0, lineStartIndex)
            val tailWidth = paint.measureText(tailText)
            // 最后一个字是个换行符就把这个换行符去掉，不然不能在那一行后面增加文字了
            var lastLineText = if (LINE_BREAKER == text[lineEndIndex - 1].toString()) {
                text.subSequence(lineStartIndex, lineEndIndex - 1)
            } else {
                text.subSequence(lineStartIndex, lineEndIndex)
            }
            if (lastMaxLine == 0) {
                lastMaxLine = maxLines
            }
            if (fullText.isNullOrEmpty()) {
                fullText = "" + text // 缓存完整内容
            }
            val availableWidth = measuredWidth - paddingLeft - paddingRight
            val ellipsizeLastLineText = TextUtils.ellipsize(lastLineText, paint, availableWidth - tailWidth,
                    TextUtils.TruncateAt.END)
            if (ellipsizeLastLineText.length > 2 && ellipsizeLastLineText !== lastLineText) {
                lastLineText = ellipsizeLastLineText.subSequence(0, ellipsizeLastLineText.length - 1)
            }
            val finalString = SpannableStringBuilder(mustShowText)
            finalString.append(lastLineText).append(tailText)
            // 设置更多按钮颜色
            finalString.setSpan(ForegroundColorSpan(tailTextColor), finalString.length - tailText.length, finalString.length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE)
            // 更多按钮加入点击事件
            finalString.setSpan(MoreClickSpan(),finalString.length - tailText.length, finalString.length,
                    Spannable.SPAN_INCLUSIVE_INCLUSIVE)

            movementMethod = LinkMovementMethod.getInstance()
            text = finalString
            // 重置一下这个位
            isEllipsed = false
        }

    }

    /**
     * 更多按钮点击
     */
    inner class MoreClickSpan: ClickableSpan() {

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
        }

        override fun onClick(widget: View) {
            // 恢复全部显示
            showTail = false
            this@MoreTextView.text = fullText
            this@MoreTextView.maxLines = Integer.MAX_VALUE
            this@MoreTextView.requestLayout()
        }
    }
    /**
     * 收起按钮点击
     */
    inner class FoldClickSpan: ClickableSpan() {

        override fun updateDrawState(ds: TextPaint) {
            ds.isUnderlineText = false
        }

        override fun onClick(widget: View) {
            // 恢复全部显示
            showTail = true
            this@MoreTextView.text = fullText
            this@MoreTextView.maxLines = lastMaxLine
            this@MoreTextView.requestLayout()
        }
    }
}
```
