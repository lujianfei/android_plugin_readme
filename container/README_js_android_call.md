# WebView js 和 Android 相互调用 代码展示

以下只展示关键部分

##### htmlSourceCode.html

```xml
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>html</title>
    <script>
        function jsCallAndroid(text) {
            window.injectedObject.showLog(text);
        }
        function showLog(text) {
            var logSpan = document.getElementById("logSpan")
            logSpan.innerHTML += text + "<br>";
        }
    </script>
</head>

<body>
<h1>WebView 的 html 页面</h1>
<button onclick="jsCallAndroid('Js 调用 Android 原生')">Html 按钮</button>
<br>
<span id="logSpan"></span>
</body>
</html>
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
    
   ...
    
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
        <WebView
            android:id="@+id/webview"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:layout_weight="1"/>
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="0dp"
            android:orientation="vertical"
            android:layout_weight="1"
            android:padding="10dp">
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:textSize="30sp"
                android:text="Kotlin 的原生页面"
                android:textStyle="bold"
                android:textColor="#000000"/>
            <Button
                android:id="@+id/btClick"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:text="原生按钮"/>
            <TextView
                android:id="@+id/txtInfo"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:textColor="#000000"/>
        </LinearLayout>
    </LinearLayout>
</LinearLayout>
```



##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_4

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import java.io.ByteArrayOutputStream
import java.lang.StringBuilder


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }

    ...
    private var btClick: Button? = null
    private var txtInfo: TextView? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        webview = findViewById(R.id.webview)
        btClick = findViewById(R.id.btClick)
        txtInfo = findViewById(R.id.txtInfo)
        ...
		// 开启 javascript 支持
        webview?.settings?.apply { 
            javaScriptEnabled = true
        }
        // 注册 js 回调事件
        webview?.addJavascriptInterface(MyJavascriptInterface(), "injectedObject")
        // 加载 html 页面, 内容放到 assets 目录
        webview?.loadData(loadTextFromAssets(),"text/html; charset=utf-8", "UTF-8")
    }

    override fun initEvent() {
        ...

        btClick?.setOnClickListener { 
            androidCallJs("Android 原生调用 Js")
        }
    }

    private fun androidCallJs(text: String) {
        // WebView等UI操作必须在主线程中进行
        webview?.loadUrl("javascript:showLog('$text')")
    }

    private fun loadTextFromAssets():String {
        val sb = StringBuilder()
        that?.assets?.open("htmlSourceCode.html")?.use {
            val b = ByteArray(1024)
            val byteArrayOutputStream = ByteArrayOutputStream()
            while (true) {
                val c = it.read(b)
                if (c <= 0) break
                byteArrayOutputStream.write(b,0,c)
            }
            sb.append(String(byteArrayOutputStream.toByteArray()))
        }
        return sb.toString()
    }  

    inner class MyJavascriptInterface {
        @JavascriptInterface
        fun showLog(text:String) {
            runOnUiThread {
                txtInfo?.append(text+"\r\n")
            }
        }
    }
}
```