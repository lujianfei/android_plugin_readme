# 阴影效果展示 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
      
        <TextView
            android:id="@+id/txt_radius"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="10dp"
            android:layout_marginTop="10dp"
            android:textSize="20sp"
            android:text="radius:"/>
        <SeekBar
            android:id="@+id/seekBar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"
            android:layout_marginStart="10dp"
            android:layout_marginEnd="10dp"
            />
        <TextView
            android:id="@+id/txt_dx"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="10dp"
            android:layout_marginTop="10dp"
            android:textSize="20sp"
            android:text="dx:"/>
        <SeekBar
            android:id="@+id/seekBarDx"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"
            android:layout_marginStart="10dp"
            android:layout_marginEnd="10dp"
            />
        <TextView
            android:id="@+id/txt_dy"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="10dp"
            android:layout_marginTop="10dp"
            android:textSize="20sp"
            android:text="dy:"/>
        <SeekBar
            android:id="@+id/seekBarDy"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"
            android:layout_marginStart="10dp"
            android:layout_marginEnd="10dp"
            />
    <ScrollView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content">
        <com.lujianfei.plugin2_13.ShadowView
            android:id="@+id/shadowView"
            android:layout_marginTop="10dp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"/>
    </ScrollView>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_13

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.SeekBar
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity(),SeekBar.OnSeekBarChangeListener {

    companion object {
        const val TAG = "MainActivity"
        const val MAX_PROGRESS = 100
    }

    private var txt_radius: TextView? = null
    private var seekBar: SeekBar? = null
    private var txt_dx: TextView? = null
    private var seekBarDx: SeekBar? = null
    private var txt_dy: TextView? = null
    private var seekBarDy: SeekBar? = null
    private var shadowView: ShadowView? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {        
        txt_radius = findViewById(R.id.txt_radius)
        seekBar = findViewById(R.id.seekBar)
        txt_dx = findViewById(R.id.txt_dx)
        seekBarDx = findViewById(R.id.seekBarDx)
        txt_dy = findViewById(R.id.txt_dy)
        seekBarDy = findViewById(R.id.seekBarDy)
        shadowView = findViewById(R.id.shadowView)
    }   

    override fun initEvent() {   
        seekBar?.apply {
            max = MAX_PROGRESS
            progress = MAX_PROGRESS / 2
        }
        seekBarDx?.apply {
            max = MAX_PROGRESS
            progress = MAX_PROGRESS / 2
        }
        seekBarDy?.apply {
            max = MAX_PROGRESS
            progress = MAX_PROGRESS / 2
        }
        
        seekBar?.setOnSeekBarChangeListener(this)
        seekBarDx?.setOnSeekBarChangeListener(this)
        seekBarDy?.setOnSeekBarChangeListener(this)

        updateUI()
    }   
    
    private fun updateUI() {
        val radius = seekBar?.progress
        val dx_progress = seekBarDx?.progress!! - MAX_PROGRESS / 2
        val dy_progress = seekBarDy?.progress!! - MAX_PROGRESS / 2
        shadowView?.radius = radius?.toFloat()!!
        shadowView?.dx = dx_progress.toFloat()
        shadowView?.dy = dy_progress.toFloat()

        txt_radius?.text = "radius: $radius"
        txt_dx?.text = "dx: $dx_progress"
        txt_dy?.text = "dy: $dy_progress"
    }

    override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
        updateUI()
    }

    override fun onStartTrackingTouch(seekBar: SeekBar?) {
    }

    override fun onStopTrackingTouch(seekBar: SeekBar?) {
    }
}

```

核心组件类

##### ShadowView.kt

```kotlin
package com.lujianfei.plugin2_13

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import com.lujianfei.module_plugin_base.utils.DensityUtils

/**
 *@date     创建时间:2020/11/20
 *@name     作者:陆键霏
 *@describe 描述:
 */

class ShadowView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        val TAG = "MaskFilterView"
        val padding = DensityUtils.dip2px(40f)
    }
    
    private val mPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val rect = Rect()

    init {
        setLayerType(LAYER_TYPE_SOFTWARE, null)
    }
    
    var dx = 0f
    set(value) {
        field = value
        invalidate()
    }
    
    var dy = 0f
    set(value) {
        field = value
        invalidate()
    }
    
    var radius = 0f
    set(value) {
        field = value
        invalidate()
    }
    
    init {
        mPaint.textSize = DensityUtils.dip2px(20f).toFloat()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(widthMeasureSpec, DensityUtils.dip2px(500f))
    }
    
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        drawBlur(canvas)
    }

    private fun drawBlur(canvas: Canvas?) {
        rect.set(padding, padding, width - padding, DensityUtils.dip2px(400f))
        // 绘制矩形
        mPaint.color = 0xffd4d4d4.toInt()
        mPaint.setShadowLayer(radius, dx, dy, 0xff0000ff.toInt()) 
        canvas?.drawRect(rect, mPaint)
    }
}
```