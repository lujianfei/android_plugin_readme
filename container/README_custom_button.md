# 自定义按钮 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >   

   <ScrollView
       android:layout_width="match_parent"
       android:layout_height="wrap_content"
       android:orientation="vertical">
        <LinearLayout
            android:id="@+id/layoutButtonContainer"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"/>
   </ScrollView>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_16

import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.net.Uri
import android.view.View
import android.widget.LinearLayout
import android.widget.ScrollView
import androidx.appcompat.app.AppCompatActivity
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    } 

    private var layoutButtonContainer: LinearLayout? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        toolbar = findViewById(R.id.toolbar)
        layoutButtonContainer = findViewById(R.id.layoutButtonContainer)
        toolbar?.setTitle(getPluginString(R.string.app_name))

        that?.let {
            that->
            layoutButtonContainer?.addView(createRoundButton(that))
            layoutButtonContainer?.addView(createGradientButton(that, orientation = GradientDrawable.Orientation.TL_BR, buttonText = "GradientButton TL_BR"))
            layoutButtonContainer?.addView(createGradientButton(that, orientation = GradientDrawable.Orientation.BR_TL, buttonText = "GradientButton BR_TL"))
            layoutButtonContainer?.addView(createGradientButton(that, orientation = GradientDrawable.Orientation.BL_TR, buttonText = "GradientButton BL_TR"))
            layoutButtonContainer?.addView(createGradientButton(that, orientation = GradientDrawable.Orientation.TR_BL, buttonText = "GradientButton TR_BL"))
            layoutButtonContainer?.addView(createGradientButton(that, orientation = GradientDrawable.Orientation.LEFT_RIGHT, buttonText = "GradientButton LEFT_RIGHT"))
            layoutButtonContainer?.addView(createGradientButton(that, orientation = GradientDrawable.Orientation.RIGHT_LEFT, buttonText = "GradientButton RIGHT_LEFT"))
            layoutButtonContainer?.addView(createGradientButton(that, orientation = GradientDrawable.Orientation.TOP_BOTTOM, buttonText = "GradientButton TOP_BOTTOM"))
            layoutButtonContainer?.addView(createGradientButton(that, orientation = GradientDrawable.Orientation.BOTTOM_TOP, buttonText = "GradientButton BOTTOM_TOP"))
        }
    }

    private fun createRoundButton(that: AppCompatActivity) : RoundButton {
        val roundButton = RoundButton(that)
        val gradientButtonLP = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DensityUtils.dip2px(50f))
        roundButton.apply {
            layoutParams = gradientButtonLP
            pressedColor = 0xFFFFBE0A.toInt()
            normalColor = 0xFFC54AFF.toInt()
            text = "RoundButton"
            radius = DensityUtils.dip2px(30f).toFloat()
            setTextColor(0xffffffff.toInt())
            setOnClickListener {}
        }
        gradientButtonLP.apply {
            topMargin = DensityUtils.dip2px(20f)
            marginStart = DensityUtils.dip2px(10f)
            marginEnd = DensityUtils.dip2px(10f)
        }
        return roundButton
    }

    private fun createGradientButton(that: AppCompatActivity, orientation: GradientDrawable.Orientation, buttonText:String): GradientButton {
        val gradientButton = GradientButton(that)
        val gradientButtonLP = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DensityUtils.dip2px(50f))
        gradientButton.apply {
            layoutParams = gradientButtonLP
            pressedStartColor = 0xFFFFBE0A.toInt()
            pressedEndColor = 0xFFFF9627.toInt()
            normalStartColor = 0xFFC54AFF.toInt()
            normalEndColor = 0xFF944AFF.toInt()
            buttonOrientation = orientation
            text = buttonText
            radius = DensityUtils.dip2px(30f).toFloat()
            setTextColor(0xffffffff.toInt())
            setOnClickListener {}
        }
        gradientButtonLP.apply {
            topMargin = DensityUtils.dip2px(20f)
            marginStart = DensityUtils.dip2px(10f)
            marginEnd = DensityUtils.dip2px(10f)
        }
        return gradientButton
    }
 
}

```

核心组件类

##### RoundButton.kt

```kotlin
package com.lujianfei.plugin2_16

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.util.AttributeSet
import android.view.Gravity


class RoundButton @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : androidx.appcompat.widget.AppCompatButton(context, attrs, defStyleAttr) {

    private val mNormalState = intArrayOf()
    private val mPressState = intArrayOf(android.R.attr.state_pressed, android.R.attr.state_enabled)

    var normalColor:Int = 0
    set(value) {
        field = value
        buildDrawableState()
    }

    var pressedColor:Int = 0
    set(value) {
        field = value
        buildDrawableState()
    }

    var radius = 50f
    set(value) {
        field = value
        buildDrawableState()
    }

    init {
        gravity = Gravity.CENTER
    }
    /**
     * 构建背景Drawble
     */
    private fun buildDrawableState() {
        // 创建状态管理器
        val stateListDrawable = StateListDrawable()
        /**
         * 注意StateListDrawable的构造方法我们这里使用的
         * 是第一参数它是一个float的数组保存的是圆角的半径，它是按照top-left顺时针保存的八个值
         */
        // 创建点击状态drawable
        val pressedDrawable = GradientDrawable()
        // 设置点击状态按钮背景的颜色
        pressedDrawable.apply {
            setColor(pressedColor)
            cornerRadius = radius
        }
        // 添加到状态管理里面
        stateListDrawable.addState(mPressState, pressedDrawable)

        // 创建普通状态drawable
        val normalDrawable = GradientDrawable()
        normalDrawable.apply {
            setColor(normalColor)
            cornerRadius = radius
        }
        stateListDrawable.addState(mNormalState, normalDrawable)
        // 设置我们的背景，就是xml里面的selector
        background = stateListDrawable
    }
}
```

##### GradientButton.kt

```kotlin
package com.lujianfei.plugin2_16

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.StateListDrawable
import android.util.AttributeSet
import android.view.Gravity


class GradientButton @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : androidx.appcompat.widget.AppCompatButton(context, attrs, defStyleAttr) {

    private val mNormalState = intArrayOf()
    private val mPressState = intArrayOf(android.R.attr.state_pressed, android.R.attr.state_enabled)

    var normalStartColor:Int = 0
    set(value) {
        field = value
        buildDrawableState()
    }

    var normalEndColor:Int = 0
    set(value) {
        field = value
        buildDrawableState()
    }

    var pressedStartColor:Int = 0
    set(value) {
        field = value
        buildDrawableState()
    }
    var pressedEndColor:Int = 0
    set(value) {
        field = value
        buildDrawableState()
    }

    var radius = 50f
    set(value) {
        field = value
        buildDrawableState()
    }

    var buttonOrientation = GradientDrawable.Orientation.TL_BR
    set(value) {
        field = value
        buildDrawableState()
    }

    init {
        gravity = Gravity.CENTER
    }
    /**
     * 构建背景Drawble
     */
    private fun buildDrawableState() {
        // 创建状态管理器
        val stateListDrawable = StateListDrawable()
        /**
         * 注意StateListDrawable的构造方法我们这里使用的
         * 是第一参数它是一个float的数组保存的是圆角的半径，它是按照top-left顺时针保存的八个值
         */
        // 创建点击状态drawable
        val pressedDrawable = GradientDrawable()
        // 设置点击状态按钮背景的颜色
        pressedDrawable.apply {
            gradientType = GradientDrawable.LINEAR_GRADIENT
            orientation = buttonOrientation
            colors = intArrayOf(pressedStartColor, pressedEndColor)
            cornerRadius = radius
        }
        // 添加到状态管理里面
        stateListDrawable.addState(mPressState, pressedDrawable)

        // 创建普通状态drawable
        val normalDrawable = GradientDrawable()
        normalDrawable.apply {
            gradientType = GradientDrawable.LINEAR_GRADIENT
            orientation = buttonOrientation
            colors = intArrayOf(normalStartColor, normalEndColor)
            cornerRadius = radius
        }
        stateListDrawable.addState(mNormalState, normalDrawable)
        // 设置我们的背景，就是xml里面的selector
        background = stateListDrawable
    }
}
```

