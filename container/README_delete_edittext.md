# 带清空输入框 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
    
    ...
    
    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="带清空输入框"
        android:textSize="20sp"
        android:layout_marginTop="20dp"
        android:layout_gravity="center_horizontal"
        />
    
    <com.lujianfei.module_plugin_base.widget.PluginEditText
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="30dp"
        android:layout_marginStart="30dp"
        android:layout_marginEnd="30dp"/>
</LinearLayout>
```

核心组件类

##### PluginEditText.kt

```kotlin
package com.lujianfei.module_plugin_base.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.core.content.ContextCompat
import com.lujianfei.module_plugin_base.R
import com.lujianfei.module_plugin_base.utils.ResUtils

class PluginEditText:androidx.appcompat.widget.AppCompatEditText  {

    private var rightClearDrawable: Drawable? = null
    private var drawable: Drawable? = null

    constructor(context: Context):super(context)
    constructor(context: Context,attrs: AttributeSet):super(context,attrs)

    init {
        /*获取删除按钮图片的Drawable对象*/
        ResUtils.context?.let {
            drawable = ContextCompat.getDrawable(it, R.drawable.ic_delete)
        }

        /*设置图片的范围*/
        drawable?.apply {
            setBounds(0, 0, minimumWidth, minimumHeight)
        }

        /*设置EditText和删除按钮图片的间距*/
        ResUtils.context?.let {
            compoundDrawablePadding = it.resources.getDimensionPixelSize(R.dimen.edit_delete_drawable_padding)
        }

        /*输入框内容监听*/
        addTextChangedListener(TextWatchImpl())

        /*设置是否显示删除按钮*/
        setHideRightClearDrawable(false)
    }
    private fun setHideRightClearDrawable(isVisible: Boolean) {
        /*是否显示删除按钮*/
        rightClearDrawable = if (isVisible) {
            drawable
        } else {
            null
        }

        /*给EditText左，上，右，下设置图片*/
        this.setCompoundDrawables(compoundDrawables[0], compoundDrawables[1], rightClearDrawable, compoundDrawables[3])
    }
    override fun onTouchEvent(event: MotionEvent): Boolean {

        /*判断手指按下的x坐标*/
        val x = event.x

        /*获取自定义EditText宽度*/
        val width = width.toFloat()

        /*获取EditText右Padding值, 删除图标左边缘到控件右边缘的距离 */
        val totalPaddingRight = totalPaddingRight.toFloat()

        /*判断手指按下的区域是否在删除按钮宽高范围内*/
        if (event.action == MotionEvent.ACTION_UP) {
            if (x > width - totalPaddingRight && x < width && event.y < height) {
                setText("")
            }
        }
        return super.onTouchEvent(event)
    }

    private inner class TextWatchImpl : TextWatcher {

        override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

        }

        override fun afterTextChanged(editable: Editable) {
            /*判断输入框有没有内容，设置是否显示删除按钮*/
            if ("" != text.toString().trim { it <= ' ' } && text.toString().trim { it <= ' ' }.isNotEmpty()) {
                setHideRightClearDrawable(true)
            } else {
                setHideRightClearDrawable(false)
            }
        }
    }
}
```