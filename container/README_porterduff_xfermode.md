# 图像合成效果

以下只展示关键部分

##### PorterDuffXfermodeView.kt

```kotlin
package com.lujianfei.plugin2_6

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View

/**
 *@date     创建时间:2020/10/16
 *@name     作者:陆键霏
 *@describe 描述:
 */
class PorterDuffXfermodeView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var mPaint: Paint? = null
    private var dstBmp: Bitmap? = null
    private var srcBmp:Bitmap? = null
    private var dstRect: RectF? = null
    private var srcRect:RectF? = null

    var onlySrc = false
    var onlyDst = false
    
    var mXfermode: Xfermode? = null
    
    fun setPorterDuffXfermode(mode:PorterDuff.Mode) {
        mXfermode = PorterDuffXfermode(mode)
    }
    
    init {
        mPaint = Paint(Paint.ANTI_ALIAS_FLAG or Paint.FILTER_BITMAP_FLAG)
        dstBmp = BitmapFactory.decodeResource(resources, R.drawable.dst)
        srcBmp = BitmapFactory.decodeResource(resources, R.drawable.src)
        mXfermode = PorterDuffXfermode(PorterDuff.Mode.MULTIPLY)
    }
    
    

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (dstBmp == null || dstBmp == null) return

        //背景色设为白色，方便比较效果
        canvas!!.drawColor(Color.WHITE)
        
        if (onlySrc) {
            canvas.drawBitmap(srcBmp!!, null, srcRect!!, mPaint)
            return
        }

        if (onlyDst) {
            canvas.drawBitmap(dstBmp!!, null, dstRect!!, mPaint)
            return
        }
        
        //将绘制操作保存到新的图层，因为图像合成是很耗资源的操作，将用到硬件加速，这里将图像合成的处理放到离屏缓存中进行
        val saveCount = canvas.saveLayer(srcRect, mPaint, Canvas.ALL_SAVE_FLAG)
        //绘制目标图
        canvas.drawBitmap(dstBmp!!, null, dstRect!!, mPaint)
        //设置混合模式
        mPaint!!.xfermode = mXfermode
        //绘制源图
        canvas.drawBitmap(srcBmp!!, null, srcRect!!, mPaint)
        //清除混合模式
        mPaint!!.xfermode = null
        //还原画布
        canvas.restoreToCount(saveCount)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        val width = if (w <= h) w else h
        val centerX = w / 2
        val centerY = h / 2
        val quarterWidth = width / 4
        srcRect = RectF((centerX - quarterWidth).toFloat(), (centerY - quarterWidth).toFloat(), (centerX + quarterWidth).toFloat(), (centerY + quarterWidth).toFloat())
        dstRect = RectF((centerX - quarterWidth).toFloat(), (centerY - quarterWidth).toFloat(), (centerX + quarterWidth).toFloat(), (centerY + quarterWidth).toFloat())
    }
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
    
    ...
    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content">
        <LinearLayout
            android:id="@+id/container"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            />
    </ScrollView>
</LinearLayout>
```

**MainActivity.kt**

```kotlin
package com.lujianfei.plugin2_6

import android.content.Intent
import android.graphics.PorterDuff
import android.net.Uri
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }
    
    ...
    
    private var container: LinearLayout? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        container = findViewById(R.id.container)
        toolbar = findViewById(R.id.toolbar)
        imgBack = findViewById(R.id.imgBack)
        txtTitle = findViewById(R.id.txtTitle)
        txtViewCode = findViewById(R.id.txtViewCode)
        txtTitle?.text = getPluginString(R.string.app_name)
        getPluginColor(R.color.text_color)?.let { txtTitle?.setTextColor(it) }
        getPluginColor(R.color.text_color)?.let { txtViewCode?.setTextColor(it) }
        getPluginColor(R.color.theme_color)?.let { toolbar?.setBackgroundColor(it) }
        getPluginDrawable(R.drawable.ic_back)?.let { imgBack?.setImageDrawable(it) }

        container?.let {
            that?.let { that->
                addView(that, "Src", summary = "源图", onlySrc = true)
                addView(that, "Dst", summary = "目标图",onlyDst = true)
                addView(that, "SrcOver", summary = "在目标图像上层绘制源图像",mode = PorterDuff.Mode.SRC_OVER)
                addView(that, "DstOver", summary = "与SRC_OVER相反，此模式是目标图像被绘制在源图像的上方",mode = PorterDuff.Mode.DST_OVER)
                addView(that, "SrcIn", summary = "在两者相交的地方绘制源图像，并且绘制的效果会受到目标图像对应地方透明度的影响",mode = PorterDuff.Mode.SRC_IN)
                addView(that, "DstIn", summary = "可以和SRC_IN 进行类比，在两者相交的地方绘制目标图像，并且绘制的效果会受到源图像对应地方透明度的影响",mode = PorterDuff.Mode.DST_IN)
                addView(that, "SrcOut",summary = "从字面上可以理解为在不相交的地方绘制源图像", mode = PorterDuff.Mode.SRC_OUT)
                addView(that, "DstOut",summary = "可以类比SRC_OUT , 在不相交的地方绘制目标图像，相交处根据源图像alpha进行过滤，完全不透明处则完全过滤，完全透明则不过滤", mode = PorterDuff.Mode.DST_OUT)
                addView(that, "SrcATop",summary = "源图像和目标图像相交处绘制源图像，不相交的地方绘制目标图像，并且相交处的效果会受到源图像和目标图像alpha的影响", mode = PorterDuff.Mode.SRC_ATOP)
                addView(that, "DstATop",summary = "源图像和目标图像相交处绘制目标图像，不相交的地方绘制源图像，并且相交处的效果会受到源图像和目标图像alpha的影响", mode = PorterDuff.Mode.DST_ATOP)
                addView(that, "Xor",summary = "在不相交的地方按原样绘制源图像和目标图像，相交的地方受到对应alpha和颜色值影响, 如果都完全不透明则相交处完全不绘制", mode = PorterDuff.Mode.XOR)
                addView(that, "Darken",summary = "该模式处理过后，会感觉效果变暗，即进行对应像素的比较，取较暗值，如果色值相同则进行混合", mode = PorterDuff.Mode.DARKEN)
                addView(that, "Lighten",summary = "可以和 DARKEN 对比起来看，DARKEN 的目的是变暗，LIGHTEN 的目的则是变亮，如果在均完全不透明的情况下，色值取源色值和目标色值中的较大值", mode = PorterDuff.Mode.LIGHTEN)
            }
        }
    }

    private fun addView(that: AppCompatActivity, title: String, summary:String, onlySrc: Boolean? = null, onlyDst: Boolean? = null, mode: PorterDuff.Mode? = null) {
        // 标题
        val txtTitle = TextView(that)
        txtTitle.text = title
        txtTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        txtTitle.setTextColor(0xff000000.toInt())
        var layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.leftMargin = DensityUtils.dip2px(10f)
        layoutParams.topMargin = DensityUtils.dip2px(10f)
        txtTitle.layoutParams = layoutParams
        container?.addView(txtTitle, layoutParams)

        // 简要说明
        val txtSummary = TextView(that)
        txtSummary.text = summary
        txtSummary.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15f)
        txtSummary.setTextColor(0xff000000.toInt())
        layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.topMargin = DensityUtils.dip2px(5f)
        layoutParams.leftMargin = DensityUtils.dip2px(10f)
        layoutParams.rightMargin = DensityUtils.dip2px(10f)
        txtSummary.layoutParams = layoutParams
        container?.addView(txtSummary, layoutParams)
        
        // 合成图
        val porterDuffXfermodeView = PorterDuffXfermodeView(that)
        onlySrc?.let {
            porterDuffXfermodeView.onlySrc = it
        }
        onlyDst?.let {
            porterDuffXfermodeView.onlyDst = it
        }
        mode?.let { 
            porterDuffXfermodeView.setPorterDuffXfermode(it)
        }
        layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, DensityUtils.dip2px(200f))
        layoutParams.topMargin = DensityUtils.dip2px(5f)
        porterDuffXfermodeView.layoutParams = layoutParams
        container?.addView(porterDuffXfermodeView, layoutParams)
    }   
}

```

