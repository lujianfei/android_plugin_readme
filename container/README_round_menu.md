# 圆盘按钮菜单 代码展示

以下只展示关键部分 (PluginButton可替换成你的Button)

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
   
    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/bt_click"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginStart="5dp"
        android:layout_marginEnd="5dp"
        android:layout_marginTop="5dp"
        android:text="点击这里"/>
</LinearLayout>
```



##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_11

import android.content.Intent
import android.net.Uri
import android.view.Gravity
import android.view.View
import android.widget.Button
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }
        
    private var bt_click:Button ?= null
    private var roundMenuPopupWindow:RoundMenuPopupWindow ?= null
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        bt_click = findViewById(R.id.bt_click)        
    }
    
    override fun initEvent() {       
        bt_click?.setOnClickListener { 
            roundMenuPopupWindow = RoundMenuPopupWindow(that)
            roundMenuPopupWindow?.apply { 
                showAtLocation(it, Gravity.CENTER, 0, 0)
            }
        }
    }   

    override fun onPluginDestroy() {
        roundMenuPopupWindow?.dismiss()
        super.onPluginDestroy()
    }
}

```

##### RoundMenuPopupWindow.kt

```kotlin
package com.lujianfei.plugin2_11

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.LogUtils
import kotlin.math.*

/**
 *@date     创建时间:2020/11/10
 *@name     作者:陆键霏
 *@describe 描述:
 */
class RoundMenuView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        val TAG = "RoundMenuView"
        val middleCloseContainerRadius = DensityUtils.dip2px(50f)
    }
    
    private val roundMenus = arrayListOf<RoundMenuBean>()
    private var centerX = 0f
    private var centerY = 0f
    private var rect = RectF()
    private var rectMiddle = RectF()
    private var paint = Paint()
    private var closeBitmap:Bitmap ?= null // 关闭按钮 Bitmap
    private var isDownInClose = true
    private var isDownInPicButtons = arrayListOf<Boolean>()
    
    var onCloseClickListener:(()->Unit) ?= null // 用于监听关闭按钮
    var onPicButtonsClickListener:((Int)->Unit) ?= null // 用于监听圆盘上哪个按钮被点击
    
    init {
        paint.isAntiAlias = true
        closeBitmap = getBitmapByResId(R.drawable.ic_close)  // 关闭按钮 Bitmap
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting))) // 添加各种图片按钮, 数量自己定
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
    }

    private fun getBitmapByResId(id:Int): Bitmap {
        val drawable = resources?.getDrawable(id)
        return (drawable as BitmapDrawable).bitmap
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (closeBitmap == null || canvas == null) return
        rect.left = 0f
        rect.top = 0f
        rect.right = width.toFloat()
        rect.bottom = height.toFloat()

        val totalRadius = width / 2
        centerX = totalRadius.toFloat()
        centerY = totalRadius.toFloat()
        val size = roundMenus.size
        // 画扇形
        for (idx in 0 until size) {
            drawFanShaped(canvas, idx) // 画扇形
            drawPic(canvas,idx) // 画按钮图片
        }
        // 画中间关闭的圆背景
        drawCloseContainer(canvas)
        // 绘制中间的关闭按钮
        drawClose(canvas)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                isDownInClose = isInClose(event)

                isDownInPicButtons.clear()
                for (idx in 0 until roundMenus.size) {
                    isDownInPicButtons.add(isInPicButton(event,index = idx))
                }
            }
            MotionEvent.ACTION_MOVE -> {

            }
            MotionEvent.ACTION_UP -> {
                if (isDownInClose && isInClose(event)) {
                    onCloseClickListener?.invoke()
                }
                for (idx in 0 until roundMenus.size) {
                    if (isDownInPicButtons[idx] && isInPicButton(event,index = idx)) {
                        onPicButtonsClickListener?.invoke(idx)
                    }
                }
            }
        }
        return true
    }

    private fun isInPicButton(event: MotionEvent, index:Int): Boolean {
        val degreeOfEachFan = 360f / roundMenus.size
        val distance = getDistance(event.x, event.y, centerX, centerY)
        val degree = getCurrentDegree(event)
        val matchDegree = degree > degreeOfEachFan * index && degree < degreeOfEachFan * (1 + index)
        val matchDistance = distance > middleCloseContainerRadius && distance < width / 2
        return (matchDegree && matchDistance) // 同时满足角度和距离，即可满足触发事件的条件，刚好坐标落在扇形区域
    }

    /**
     * 获取相对于圆心的角度
     */
    private fun getCurrentDegree(event: MotionEvent):Float {
        val w = event.x - centerX
        val h = event.y - centerY
        var degree = Math.toDegrees(atan(h / w).toDouble())
        if (w > 0) { // 象限右侧
            if (h < 0) { // 第一象限
                degree += 360
            }
        } else { // 象限左侧
            degree += 180
        }
        return degree.toFloat()
    }

    private fun drawClose(canvas: Canvas) {
        canvas.drawBitmap(
            closeBitmap!!,
            centerX - closeBitmap!!.width / 2,
            centerY - closeBitmap!!.height / 2,
            null
        )
    }

    private fun drawCloseContainer(canvas: Canvas) {
        paint.color = Color.GRAY
        rectMiddle.left = centerX - middleCloseContainerRadius
        rectMiddle.top = centerY - middleCloseContainerRadius
        rectMiddle.right = centerX + middleCloseContainerRadius
        rectMiddle.bottom = centerY + middleCloseContainerRadius
        canvas.drawArc(rectMiddle, 0f, 360f, false, paint)
    }

    /**
     * 画扇形里的图片
     */
    private fun drawPic(canvas: Canvas, idx: Int) {
        val totalRadius = width / 2
        val roundMenuBean = roundMenus[idx]
        val size = roundMenus.size
        val angleEach = 360f / size
        val currentAngle = idx * angleEach + angleEach /2
        // 通过 cos / sin 和 角度计算相对中心的点的 x, y 坐标 
        val picX = centerX + totalRadius * roundMenuBean.distance * cos(Math.toRadians(currentAngle.toDouble()))
        val picY = centerY + totalRadius * roundMenuBean.distance * sin(Math.toRadians(currentAngle.toDouble()))
        LogUtils.d(TAG, "center = ($centerX,$centerY) w,h = ($width, $height) x,y = ($picX,$picY)")
        roundMenuBean.centerX = picX
        roundMenuBean.centerY = picY
        canvas.drawBitmap(roundMenuBean.icon, picX.toFloat() - roundMenuBean.icon.width / 2, picY.toFloat() - roundMenuBean.icon.height / 2, null)
    }

    /**
     * 绘制扇形
     */
    private fun drawFanShaped(canvas: Canvas, idx: Int) {
        val size = roundMenus.size
        val angleEach = 360f / size
        val currentAngle = idx * angleEach
        paint.color = 0x2b000000
        var startAngle = currentAngle
        var sweepAngle = angleEach
        canvas.drawArc(rect, startAngle, sweepAngle, true, paint)

        paint.color = 0xffffffff.toInt()
        canvas.drawArc(rect, startAngle, 1f, true, paint)
    }

    /**
     * 判断点击位置是否位于关闭按钮
     */
    private fun isInClose(event: MotionEvent) :Boolean {
        val distance = getDistance(event.x, event.y, centerX, centerY)
        return (distance <= closeBitmap?.width!! / 2) 
    }

    /**
     * 计算两点距离
     */
    private fun getDistance(x1:Float, y1:Float, x2:Float, y2:Float):Float {
        val w = abs(x1 - x2)  
        val h = abs(y1 - y2)
        return sqrt(w * w + h * h)
    }

    /**
     * 用于记录图片按钮信息的实体类
     */
    data class RoundMenuBean (
        val icon:Bitmap,
        var centerX:Double = 0.0,
        var centerY:Double = 0.0,
        val distance:Float = 0.75f
    ) 
}
```



##### popupwindow_round_menu.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content">

    <com.lujianfei.plugin2_11.RoundMenuView
        android:id="@+id/roundMenuView"
        android:layout_width="300dp"
        android:layout_height="300dp" />
</RelativeLayout>
```



##### RoundMenuView.kt

```kotlin
package com.lujianfei.plugin2_11

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.LogUtils
import kotlin.math.abs
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

/**
 *@date     创建时间:2020/11/10
 *@name     作者:陆键霏
 *@describe 描述:
 */
class RoundMenuView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        val TAG = "RoundMenuView"
        val middleCloseContainerRadius = DensityUtils.dip2px(50f)
        val anglePadding = 0.1f
    }
    
    private val roundMenus = arrayListOf<RoundMenuBean>()
    private var centerX = 0f
    private var centerY = 0f
    private var rect = RectF()
    private var rectMiddle = RectF()
    private var paint = Paint()
    private var closeBitmap:Bitmap ?= null // 关闭按钮 Bitmap
    private var isDownInClose = true
    private var isDownInPicButtons = arrayListOf<Boolean>()
    
    var onCloseClickListener:(()->Unit) ?= null // 用于监听关闭按钮
    var onPicButtonsClickListener:((Int)->Unit) ?= null // 用于监听圆盘上哪个按钮被点击
    
    init {
        paint.isAntiAlias = true
        closeBitmap = getBitmapByResId(R.drawable.ic_close)  // 关闭按钮 Bitmap
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting))) // 添加各种图片按钮, 数量自己定
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
        roundMenus.add(RoundMenuBean(icon = getBitmapByResId(R.drawable.ic_setting)))
    }

    private fun getBitmapByResId(id:Int): Bitmap {
        val drawable = resources?.getDrawable(id)
        return (drawable as BitmapDrawable).bitmap
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (closeBitmap == null || canvas == null) return
        rect.left = 0f
        rect.top = 0f
        rect.right = width.toFloat()
        rect.bottom = height.toFloat()

        val totalRadius = width / 2
        centerX = totalRadius.toFloat()
        centerY = totalRadius.toFloat()
        val size = roundMenus.size
        // 画扇形
        for (idx in 0 until size) {
            drawFanShaped(canvas, idx) // 画扇形
            drawLine(canvas,idx) // 画线
            drawPic(canvas,idx) // 画按钮图片
        }
        // 画中间关闭的圆背景
        drawCloseContainer(canvas)
        // 绘制中间的关闭按钮
        drawClose(canvas)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                isDownInClose = isInClose(event)

                isDownInPicButtons.clear()
                for (idx in 0 until roundMenus.size) {
                    isDownInPicButtons.add(isInPicButton(event,index = idx))
                }
            }
            MotionEvent.ACTION_MOVE -> {

            }
            MotionEvent.ACTION_UP -> {
                if (isDownInClose && isInClose(event)) {
                    onCloseClickListener?.invoke()
                }
                for (idx in 0 until roundMenus.size) {
                    if (isDownInPicButtons[idx] && isInPicButton(event,index = idx)) {
                        onPicButtonsClickListener?.invoke(idx)
                    }
                }
            }
        }
        return true
    }

    private fun isInPicButton(event: MotionEvent, index:Int): Boolean {
        val roundMenuBean = roundMenus[index]
        val distance = getDistance(event.x, event.y, roundMenuBean.centerX.toFloat(), roundMenuBean.centerY.toFloat())
        return (distance <= roundMenuBean.icon.width / 2)
    }

    private fun drawClose(canvas: Canvas) {
        canvas.drawBitmap(
            closeBitmap!!,
            centerX - closeBitmap!!.width / 2,
            centerY - closeBitmap!!.height / 2,
            null
        )
    }

    private fun drawCloseContainer(canvas: Canvas) {
        paint.color = Color.GRAY
        rectMiddle.left = centerX - middleCloseContainerRadius
        rectMiddle.top = centerY - middleCloseContainerRadius
        rectMiddle.right = centerX + middleCloseContainerRadius
        rectMiddle.bottom = centerY + middleCloseContainerRadius
        canvas.drawArc(rectMiddle, 0f, 360f, false, paint)
    }

    /**
     * 画扇形里的图片
     */
    private fun drawPic(canvas: Canvas, idx: Int) {
        val totalRadius = width / 2
        val roundMenuBean = roundMenus[idx]
        val size = roundMenus.size
        val angleEach = 360f / size
        val currentAngle = idx * angleEach + angleEach /2
        // 通过 cos / sin 和 角度计算相对中心的点的 x, y 坐标 
        val picX = centerX + totalRadius * roundMenuBean.distance * cos(Math.toRadians(currentAngle.toDouble()))
        val picY = centerY + totalRadius * roundMenuBean.distance * sin(Math.toRadians(currentAngle.toDouble()))
        LogUtils.d(TAG, "center = ($centerX,$centerY) w,h = ($width, $height) x,y = ($picX,$picY)")
        roundMenuBean.centerX = picX
        roundMenuBean.centerY = picY
        canvas.drawBitmap(roundMenuBean.icon, picX.toFloat() - roundMenuBean.icon.width / 2, picY.toFloat() - roundMenuBean.icon.height / 2, null)
    }

    /**
     * 画扇形之间的分隔线
     */
    private fun drawLine(canvas: Canvas, idx: Int) {
        val size = roundMenus.size
        val angleEach = 360f / size
        val currentAngle = idx * angleEach
        // 画缝隙
        paint.color = 0xffffffff.toInt()
        var startAngle = anglePadding * idx + currentAngle - anglePadding * 2
        val tempAnglePadding = anglePadding * 2
        if (startAngle <= 0) {
            startAngle = 0.0f
        }
        canvas.drawArc(rect, startAngle, tempAnglePadding, true, paint)
    }

    /**
     * 绘制扇形
     */
    private fun drawFanShaped(canvas: Canvas, idx: Int) {
        val size = roundMenus.size
        val angleEach = 360f / size
        val currentAngle = idx * angleEach
        paint.color = 0x2b000000
        var startAngle = anglePadding * idx + currentAngle - anglePadding
        var sweepAngle = angleEach - anglePadding
        if (startAngle <= 0) {
            startAngle = 0f
        } else if (startAngle + sweepAngle >= 360f ) {
            sweepAngle -= anglePadding * 8f
        }
        canvas.drawArc(rect, startAngle, sweepAngle, true, paint)
    }

    /**
     * 判断点击位置是否位于关闭按钮
     */
    private fun isInClose(event: MotionEvent) :Boolean {
        val distance = getDistance(event.x, event.y, centerX, centerY)
        return (distance <= closeBitmap?.width!! / 2) 
    }

    /**
     * 计算两点距离
     */
    private fun getDistance(x1:Float, y1:Float, x2:Float, y2:Float):Float {
        val w = abs(x1 - x2)  
        val h = abs(y1 - y2)
        return sqrt(w * w + h * h)
    }

    /**
     * 用于记录图片按钮信息的实体类
     */
    data class RoundMenuBean (
        val icon:Bitmap,
        var centerX:Double = 0.0,
        var centerY:Double = 0.0,
        val distance:Float = 0.75f
    ) 
}
```

