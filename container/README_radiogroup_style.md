# RadioGroup实现下划线单选 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
    
    ...
    
    <RadioGroup
        android:id="@+id/radio_group"
        android:orientation="horizontal"
        android:layout_marginTop="20dp"
        android:layout_marginStart="10dp"
        android:layout_width="match_parent"
        android:layout_height="50dp">
    </RadioGroup>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_9

import android.content.Intent
import android.net.Uri
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }
    
    ...
    
    private var radio_group:RadioGroup ?= null
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        radio_group = findViewById(R.id.radio_group)        

        for (idx in 0 until 3) {
            val radioButton = RadioButton(that)
            // getPluginColorStateList 可替换为你项目的 getColorStateList
            getPluginColorStateList(R.color.color_radio_button)?.let { radioButton.setTextColor(it) }
            if (idx == 0) {
                radioButton.isChecked = true
            }
            radioButton.id = idx
            radioButton.layoutParams = RadioGroup.LayoutParams(DensityUtils.dip2px(0f), RadioGroup.LayoutParams.MATCH_PARENT, 1f)
            radioButton.text = "选项 $idx"
            radioButton.gravity = Gravity.CENTER
            radioButton.setTextSize(TypedValue.COMPLEX_UNIT_SP,20f)
            radioButton.buttonDrawable = null
            // getPluginDrawable 可替换为你项目的 getDrawable
            radioButton.background = getPluginDrawable(R.drawable.bg_underline)
            radio_group?.addView(radioButton)
        }
    }    
}

```

##### color_radio_button.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:color="@color/theme_color" android:state_checked="true"/>
    <item android:color="#000000"/>
</selector>
```

##### bg_underline.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<selector xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:state_checked="true">
        <layer-list>
            <!-- top: 容器高为 50dp , 这里的 top 为 49dp, 则底部线条则是 1dp -->
            <item android:top="49dp"
                android:left="25dp"
                android:right="25dp">
                <shape android:shape="rectangle">
                    <solid android:color="@color/theme_color" />
                </shape>
            </item>
        </layer-list>
    </item>
    <item android:drawable="@color/transparent" />
</selector>
```

