# 遮罩滤镜展示 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >       
        <TextView
            android:id="@+id/txt_radius"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="10dp"
            android:layout_marginTop="10dp"
            android:textSize="20sp"
            android:text="radius:"/>
        <SeekBar
            android:id="@+id/seekBar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"
            android:layout_marginStart="10dp"
            android:layout_marginEnd="10dp"
            android:progress="1"
            android:min="1"
            />
    <ScrollView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content">
        <com.lujianfei.plugin2_12.MaskFilterView
            android:id="@+id/maskFilterView"
            android:layout_marginTop="10dp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"/>
    </ScrollView>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_12

import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.View.LAYER_TYPE_SOFTWARE
import android.widget.Button
import android.widget.SeekBar
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }
    
    private var txt_radius: TextView? = null
    private var seekBar: SeekBar? = null
    private var maskFilterView: MaskFilterView? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {        
        txt_radius = findViewById(R.id.txt_radius)
        seekBar = findViewById(R.id.seekBar)
        maskFilterView = findViewById(R.id.maskFilterView)
    }

    override fun initEvent() {
        seekBar?.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    txt_radius?.text = "radius: $progress"
                    maskFilterView?.radius = progress.toFloat()
                }
    
                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }
    
                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }
            }
        )
    }
}

```

核心组件类

##### MaskFilterView.kt

```kotlin
package com.lujianfei.plugin2_12

import android.content.Context
import android.graphics.BlurMaskFilter
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.View
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.LogUtils

/**
 *@date     创建时间:2020/11/20
 *@name     作者:陆键霏
 *@describe 描述:
 */

class MaskFilterView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        val TAG = "MaskFilterView"
        val padding = DensityUtils.dip2px(20f)
        val RECT_HEIGHT = DensityUtils.dip2px(200f)
        val TEXT_HEIGHT = DensityUtils.dip2px(20f)
    }
    
    private val mPaint = Paint(Paint.ANTI_ALIAS_FLAG)
    private val rect = Rect()

    var radius = 1f
    set(value) {
        field = value
        invalidate()
    }
    
    init {
        mPaint.textSize = DensityUtils.dip2px(20f).toFloat()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(widthMeasureSpec, DensityUtils.dip2px(1100f))
    }
    
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        rect.set(padding, padding, width - padding, 0)
        drawBlur(canvas, style = BlurMaskFilter.Blur.INNER, text = "BlurMaskFilter.Blur.INNER 的效果：")
        drawBlur(canvas, style = BlurMaskFilter.Blur.OUTER, text = "BlurMaskFilter.Blur.OUTER 的效果：")
        drawBlur(canvas, style = BlurMaskFilter.Blur.SOLID, text = "BlurMaskFilter.Blur.SOLID 的效果：")
        drawBlur(canvas, style = BlurMaskFilter.Blur.NORMAL, text = "BlurMaskFilter.Blur.NORMAL 的效果：")
    }

    private fun drawBlur(canvas: Canvas?, style:BlurMaskFilter.Blur, text:String) {
        // 绘制标题
        mPaint.color = 0xff000000.toInt()
        mPaint.maskFilter = null
        rect.top = rect.top + TEXT_HEIGHT
        canvas?.drawText(text, rect.left.toFloat(), rect.top.toFloat(), mPaint)
        
        // 绘制矩形
        mPaint.color = 0xffd4d4d4.toInt()
        mPaint.maskFilter = BlurMaskFilter(radius, style)
        rect.top = rect.top + TEXT_HEIGHT
        rect.bottom = rect.top + RECT_HEIGHT
        LogUtils.d(TAG, "rect = $rect")
        canvas?.drawRect(rect, mPaint)
        
        rect.top = rect.top + RECT_HEIGHT + TEXT_HEIGHT
    }
}
```