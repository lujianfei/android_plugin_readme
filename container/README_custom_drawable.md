# 自定义Drawable 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:background="#dddddd"
    >  

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content">
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="15dp"
                android:text="圆形"
                />
            <View
                android:id="@+id/circleView"
                android:layout_width="200dp"
                android:layout_height="200dp"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="10dp"
                />

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="15dp"
                android:text="方形"
                />
            <View
                android:id="@+id/rectangleView"
                android:layout_width="200dp"
                android:layout_height="200dp"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="10dp"
                />

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="15dp"
                android:text="圆角方形"
                />
            <View
                android:id="@+id/roundView"
                android:layout_width="200dp"
                android:layout_height="200dp"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="10dp"
                />
            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="15dp"
                android:text="三角形"
                />
            <View
                android:id="@+id/triangleView"
                android:layout_width="200dp"
                android:layout_height="200dp"
                android:layout_gravity="center_horizontal"
                android:layout_marginTop="10dp"
                />
        </LinearLayout>
    </ScrollView>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_20

import android.content.Intent
import android.net.Uri
import android.util.TypedValue
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import androidx.core.widget.TextViewCompat
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin2_20.drawables.CircleDrawable
import com.lujianfei.plugin2_20.drawables.RectangleDrawable
import com.lujianfei.plugin2_20.drawables.RoundDrawable
import com.lujianfei.plugin2_20.drawables.TriangleDrawable


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }
    
    private var toolbar: PluginToolBar? = null
    private var bean: PluginActivityBean? = null

    private var circleView:View ?= null
    private var rectangleView:View ?= null
    private var roundView:View ?= null
    private var triangleView:View ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        toolbar = findViewById(R.id.toolbar)
        circleView = findViewById(R.id.circleView)
        rectangleView = findViewById(R.id.rectangleView)
        roundView = findViewById(R.id.roundView)
        triangleView = findViewById(R.id.triangleView)

        circleView?.background = CircleDrawable()
        rectangleView?.background = RectangleDrawable()
        roundView?.background = RoundDrawable(
            radiusX = DensityUtils.dip2px(15f).toFloat(),
            radiusY = DensityUtils.dip2px(15f).toFloat()
        )
        triangleView?.background = TriangleDrawable()
    }
}

```

核心组件类

##### CircleDrawable.kt

```kotlin
package com.lujianfei.plugin2_20.drawables

import android.graphics.*
import android.graphics.drawable.Drawable

/**
 * Author: mac
 * Date: 15.11.21 5:10 下午
 * Email:johnson@miaoshitech.com
 * Description: 圆形自定义Drawable
 */
class CircleDrawable: Drawable() {

    private var mPaint:Paint ?= null
    private var mPath: Path? = null
    private var mBounds: Rect? = null
    private var mAlpha: Int = 0

    init {
        mPaint = Paint().apply {
            style = Paint.Style.FILL
            color = Color.YELLOW
        }
        mPath = Path()
    }

    override fun onBoundsChange(bounds: Rect?) {
        super.onBoundsChange(bounds)
        mBounds = bounds
        mPath?.reset()
        mPath?.close()
    }

    override fun draw(canvas: Canvas) {
        mBounds?.let {
            mBounds->
            mPaint?.let {
                mPaint->
                canvas.drawCircle(mBounds.centerX().toFloat(),
                    mBounds.centerY().toFloat(),
                    mBounds.width() / 2f,
                    mPaint)
            }
        }
    }

    override fun setAlpha(alpha: Int) {
        mAlpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
    }

    override fun getOpacity(): Int {
        return mAlpha
    }
}
```

##### RectangleDrawable.kt

```kotlin
package com.lujianfei.plugin2_20.drawables

import android.graphics.*
import android.graphics.drawable.Drawable

/**
 * Author: mac
 * Date: 15.11.21 5:10 下午
 * Email:johnson@miaoshitech.com
 * Description: 矩形自定义Drawable
 */
class RectangleDrawable: Drawable() {

    private var mPaint:Paint ?= null
    private var mPath: Path? = null
    private var mBounds: Rect? = null
    private var mAlpha: Int = 0

    init {
        mPaint = Paint().apply {
            style = Paint.Style.FILL
            color = Color.BLUE
        }
        mPath = Path()
    }

    override fun onBoundsChange(bounds: Rect?) {
        super.onBoundsChange(bounds)
        mBounds = bounds
        mPath?.reset()
        mPath?.close()
    }

    override fun draw(canvas: Canvas) {
        mBounds?.let {
            mBounds->
            mPaint?.let {
                mPaint->
                canvas.drawRect(mBounds,mPaint)
            }
        }
    }

    override fun setAlpha(alpha: Int) {
        mAlpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
    }

    override fun getOpacity(): Int {
        return mAlpha
    }
}
```

##### RoundDrawable.kt

```kotlin
package com.lujianfei.plugin2_20.drawables

import android.graphics.*
import android.graphics.drawable.Drawable
import androidx.core.graphics.toRectF

/**
 * Author: mac
 * Date: 15.11.21 5:10 下午
 * Email:johnson@miaoshitech.com
 * Description: 圆角矩形自定义Drawable
 */
class RoundDrawable(var radiusX:Float, var radiusY:Float): Drawable() {

    private var mPaint:Paint ?= null
    private var mPath: Path? = null
    private var mBounds: Rect? = null
    private var mAlpha: Int = 0

    init {
        mPaint = Paint().apply {
            style = Paint.Style.FILL
            color = Color.GREEN
        }
        mPath = Path()
    }

    override fun onBoundsChange(bounds: Rect?) {
        super.onBoundsChange(bounds)
        mBounds = bounds
        mPath?.reset()
        mPath?.close()
    }

    override fun draw(canvas: Canvas) {
        mBounds?.let {
            mBounds->
            mPaint?.let {
                mPaint->
                canvas.drawRoundRect(mBounds.toRectF(),
                    radiusX,
                    radiusY,
                    mPaint)
            }
        }
    }

    override fun setAlpha(alpha: Int) {
        mAlpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
    }

    override fun getOpacity(): Int {
        return mAlpha
    }
}
```

##### TriangleDrawable.kt

```kotlin
package com.lujianfei.plugin2_20.drawables

import android.graphics.*
import android.graphics.drawable.Drawable

/**
 * Author: mac
 * Date: 15.11.21 5:10 下午
 * Email:johnson@miaoshitech.com
 * Description: 自定义三角形Drawable
 */
class TriangleDrawable: Drawable() {

    private var mPaint:Paint ?= null
    private var mPath: Path? = null
    private var mBounds: Rect? = null
    private var mAlpha: Int = 0

    init {
        mPaint = Paint().apply {
            style = Paint.Style.FILL
            color = Color.RED
        }
        mPath = Path()
    }

    override fun onBoundsChange(bounds: Rect?) {
        super.onBoundsChange(bounds)
        mBounds = bounds
        if (bounds == null) return
        mPath?.reset()
        mPath?.moveTo(0f,bounds.height().toFloat())
        mPath?.lineTo(bounds.width().toFloat(),bounds.height().toFloat())
        mPath?.lineTo(bounds.width()/2f,0f)
        mPath?.close()
        invalidateSelf()
    }

    override fun draw(canvas: Canvas) {
        if (mBounds == null || mPath == null) return
        canvas.drawPath(mPath!!,mPaint!!)
    }

    override fun setAlpha(alpha: Int) {
        mAlpha = alpha
    }

    override fun setColorFilter(colorFilter: ColorFilter?) {
    }

    override fun getOpacity(): Int {
        return mAlpha
    }
}
```

