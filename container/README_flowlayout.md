# android 流式布局 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        />

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        >
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">
            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:orientation="horizontal"
                android:paddingTop="10dp"
                android:paddingStart="5dp"
                android:paddingEnd="5dp"
                >

                <com.lujianfei.module_plugin_base.widget.PluginButton
                    android:id="@+id/bt_minus_tag"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:text="减少标签"/>

                <com.lujianfei.module_plugin_base.widget.PluginButton
                    android:id="@+id/bt_add_tag"
                    android:layout_width="0dp"
                    android:layout_height="wrap_content"
                    android:layout_weight="1"
                    android:layout_marginStart="5dp"
                    android:text="新增标签"/>
            </LinearLayout>

            <com.lujianfei.plugin2_1.FlowLayout
                android:id="@+id/flowLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginTop="10dp"/>

            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:textSize="18sp"
                android:layout_marginStart="5dp"
                android:text="超出2行折叠："
                android:layout_marginTop="20dp"/>
            <com.lujianfei.plugin2_1.CollapsableFlowLayout
                android:id="@+id/collapsableFlowLayout"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                />

        </LinearLayout>
    </ScrollView>
</LinearLayout>
```

核心组件类

##### FlowLayout.kt

```kotlin
package com.lujianfei.plugin2_1

import android.content.Context
import android.graphics.Rect
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import com.lujianfei.module_plugin_base.utils.LogUtils
import java.util.*


class FlowLayout @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ViewGroup(context, attrs, defStyleAttr) {

    companion object {
        private const val TAG = "FlowLayout"
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        //遍历去调用所有子元素的measure方法（child.getMeasuredHeight()才能获取到值，否则为0）
        measureChildren(widthMeasureSpec, heightMeasureSpec)
        var measuredWidth = 0
        var measuredHeight = 0
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val widtMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        //由于计算子view所占宽度，这里传值需要自身减去PaddingRight宽度，PaddingLeft会在接下来计算子元素位置时加上
        val compute = compute(widthSize - paddingEnd)

        //EXACTLY模式：对应于给定大小或者match_parent情况
        measuredWidth = when (widtMode) {
            MeasureSpec.EXACTLY -> {
                widthSize
                //AT_MOS模式：对应wrap-content（需要手动计算大小，否则相当于match_parent）
            }
            MeasureSpec.AT_MOST -> {
                compute.allChildWidth
            }
            else -> {
                compute.allChildWidth
            }
        }
        measuredHeight = when (heightMode) {
            MeasureSpec.EXACTLY -> {
                heightSize
            }
            MeasureSpec.AT_MOST -> {
                compute.allChildHeight
            }
            else -> {
                compute.allChildHeight
            }
        }
        //设置flow的宽高
        setMeasuredDimension(measuredWidth, measuredHeight)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            val rect = getChildAt(i).tag as Rect
            if (isRtl()) {
                val left  = width - rect.left - rect.width()
                child.layout(left, rect.top, left + rect.width(), rect.bottom)
            } else {
                child.layout(rect.left, rect.top, rect.right, rect.bottom)
            }
        }
    }

    private fun isRtl(): Boolean {
        return TextUtils.getLayoutDirectionFromLocale(Locale.getDefault()) === View.LAYOUT_DIRECTION_RTL
    }

    /**
     * 测量过程
     * @param flowWidth 该view的宽度
     * @return  返回子元素总所占宽度和高度（用于计算Flowlayout的AT_MOST模式设置宽高）
     */
    private fun compute(flowWidth: Int): FlowParams {
        //是否是单行
        var aRow = true
        var marginParams: MarginLayoutParams //子元素margin
        var rowsWidth = paddingStart //当前行已占宽度(注意需要加上paddingLeft)
        var columnHeight = paddingTop //当前行顶部已占高度(注意需要加上paddingTop)
        var rowsMaxHeight = 0 //当前行所有子元素的最大高度（用于换行累加高度）
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            //获取元素测量宽度和高度
            val measuredWidth = child.measuredWidth
            val measuredHeight = child.measuredHeight
            //获取元素的margin
            marginParams = child.layoutParams as MarginLayoutParams
            //子元素所占宽度 = MarginLeft+ child.getMeasuredWidth+MarginRight  注意此时不能child.getWidth,因为界面没有绘制完成，此时 wdith 为 0
            val childWidth = marginParams.marginStart + marginParams.marginEnd + measuredWidth
            val childHeight = marginParams.topMargin + marginParams.bottomMargin + measuredHeight
            //判断是否换行： 该行已占大小+该元素大小>父容器宽度  则换行
            rowsMaxHeight = rowsMaxHeight.coerceAtLeast(childHeight)
            //换行
            if (rowsWidth + childWidth > flowWidth) {
                //重置行宽度
                rowsWidth = paddingStart + paddingEnd
                //累加上该行子元素最大高度
                columnHeight += rowsMaxHeight
                //重置该行最大高度
                rowsMaxHeight = childHeight
                aRow = false
            }
            //累加上该行子元素宽度
            rowsWidth += childWidth
            //判断时占的宽段时加上margin计算，设置顶点位置时不包括margin位置，不然margin会不起作用，这是给View设置tag,在onlayout给子元素设置位置再遍历取出
            child.tag = Rect(rowsWidth - childWidth + marginParams.marginStart, columnHeight + marginParams.topMargin, rowsWidth - marginParams.marginEnd, columnHeight + childHeight - marginParams.bottomMargin)
        }
        //返回子元素总所占宽度和高度（用于计算Flowlayout的AT_MOST模式设置宽高）
        val flowParams = FlowParams()
        //单行
        if (aRow) {
            flowParams.allChildWidth = rowsWidth
        } else {
            //多行
            flowParams.allChildWidth = flowWidth
        }
        //FlowLayout测量高度 = 当前行顶部已占高度 +当前行内子元素最大高度+FlowLayout的PaddingBottom
        flowParams.allChildHeight = columnHeight + rowsMaxHeight + paddingBottom
        return flowParams
    }

    data class FlowParams(
        var allChildWidth: Int = 0,
        var allChildHeight: Int = 0
    )
}
```



可折叠流式布局

##### CollapsableFlowLayout

```kotlin
package com.lujianfei.plugin2_1

import android.content.Context
import android.graphics.Rect
import android.text.TextUtils
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import com.lujianfei.module_plugin_base.utils.LogUtils
import java.util.*


class CollapsableFlowLayout @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ViewGroup(context, attrs, defStyleAttr) {

    companion object {
        private const val TAG = "CollapsableFlowLayout"
    }

    private var toggleView:View ?= null
    private var totalRowCount = 0
    private var currentRowCount = 0
    private var lastMaxLines = -1
    private var showToggle = false
    var maxLines = -1
    set(value) {
        field = value
        if (value != -1) {
            lastMaxLines = value
        }
    }

    fun addToggleView() {
        val toggleImageView = ImageView(context)
        toggleImageView.setImageDrawable(context.resources.getDrawable(R.drawable.ic_down))
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(10, 10, 0, 5)
        this.toggleView = toggleImageView
        addView(toggleImageView,layoutParams)
        toggleImageView.setOnClickListener {
            it.isSelected = !it.isSelected
            if (it.isSelected) {
                this.toggleView?.rotation = 180f
                maxLines = -1
                showAllChild()
            } else {
                this.toggleView?.rotation = 0f
                maxLines = lastMaxLines
                updateChild()
            }
        }
        updateToggleView()
        // reset maxlines
        maxLines = lastMaxLines
    }

    private fun updateToggleView() {
        LogUtils.d(TAG, "currentRowCount $currentRowCount maxLines $maxLines")
        if (currentRowCount <= lastMaxLines) {
            this.toggleView?.rotation = 0f
        } else {
            this.toggleView?.rotation = 180f
        }
    }

    private fun showAllChild() {
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            child.visibility = View.VISIBLE
        }
    }
    private fun updateChild() {
        if (childCount > 0) { // 触发 onMeasure 的回调
            val child = getChildAt(0)
            child.visibility = View.GONE
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        LogUtils.d(TAG, "onMeasure $widthMeasureSpec,$heightMeasureSpec")
        //遍历去调用所有子元素的measure方法（child.getMeasuredHeight()才能获取到值，否则为0）
        measureChildren(widthMeasureSpec, heightMeasureSpec)
        var measuredWidth = 0
        var measuredHeight = 0
        val widthSize = MeasureSpec.getSize(widthMeasureSpec)
        val widtMode = MeasureSpec.getMode(widthMeasureSpec)
        val heightMode = MeasureSpec.getMode(heightMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        //由于计算子view所占宽度，这里传值需要自身减去PaddingRight宽度，PaddingLeft会在接下来计算子元素位置时加上
        val compute = compute(widthSize - paddingEnd)

        //EXACTLY模式：对应于给定大小或者match_parent情况
        measuredWidth = when (widtMode) {
            MeasureSpec.EXACTLY -> {
                widthSize
                //AT_MOS模式：对应wrap-content（需要手动计算大小，否则相当于match_parent）
            }
            MeasureSpec.AT_MOST -> {
                compute.allChildWidth
            }
            else -> {
                compute.allChildWidth
            }
        }
        measuredHeight = when (heightMode) {
            MeasureSpec.EXACTLY -> {
                heightSize
            }
            MeasureSpec.AT_MOST -> {
                compute.allChildHeight
            }
            else -> {
                compute.allChildHeight
            }
        }
        //设置flow的宽高
        setMeasuredDimension(measuredWidth, measuredHeight)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        LogUtils.d(TAG, "onLayout")
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            val tag = getChildAt(i).tag ?:continue
            val rect = tag as Rect
            if (isRtl()) {
                val left  = width - rect.left - rect.width()
                child.layout(left, rect.top, left + rect.width(), rect.bottom)
            } else {
                child.layout(rect.left, rect.top, rect.right, rect.bottom)
            }
        }
    }

    private fun isRtl(): Boolean {
        return TextUtils.getLayoutDirectionFromLocale(Locale.getDefault()) === View.LAYOUT_DIRECTION_RTL
    }

    /**
     * 测量过程
     * @param flowWidth 该view的宽度
     * @return  返回子元素总所占宽度和高度（用于计算Flowlayout的AT_MOST模式设置宽高）
     */
    private fun compute(flowWidth: Int): FlowParams {
        showToggle = false
        totalRowCount = 1
        currentRowCount = 1
        //是否是单行
        var aRow = true
        var marginParams: MarginLayoutParams //子元素margin
        var rowsWidth = paddingStart //当前行已占宽度(注意需要加上paddingLeft)
        var columnHeight = paddingTop //当前行顶部已占高度(注意需要加上paddingTop)
        var rowsMaxHeight = 0 //当前行所有子元素的最大高度（用于换行累加高度）
        var lastChild:View ?= null
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            child.visibility = View.VISIBLE
            //获取元素测量宽度和高度
            val measuredWidth = child.measuredWidth
            val measuredHeight = child.measuredHeight
            //获取元素的margin
            marginParams = child.layoutParams as MarginLayoutParams

            var childWidth = 0
            var childHeight = 0
            if (child == this.toggleView) { // 折叠按钮

            } else {
                //子元素所占宽度 = MarginStart + child.getMeasuredWidth + MarginEnd  注意此时不能child.getWidth,因为界面没有绘制完成，此时 wdith 为 0
                childWidth = marginParams.marginStart + marginParams.marginEnd + measuredWidth
                childHeight = marginParams.topMargin + marginParams.bottomMargin + measuredHeight
                //判断是否换行： 该行已占大小+该元素大小>父容器宽度  则换行
                rowsMaxHeight = rowsMaxHeight.coerceAtLeast(childHeight)
            }
            // 当前行宽度超出最大宽度，换行
            if (rowsWidth + childWidth > flowWidth) {
                if (maxLines == -1 || totalRowCount < maxLines) {
                    //重置行宽度
                    rowsWidth = paddingStart + paddingEnd
                    //累加上该行子元素最大高度
                    columnHeight += rowsMaxHeight
                    //重置该行最大高度
                    rowsMaxHeight = childHeight
                    aRow = false
                    ++currentRowCount
                } else { // 超出设定最大行数，隐藏后续的按钮，显示折叠按钮
                    child.visibility = View.GONE
                }
                ++totalRowCount
            }

            showToggle = totalRowCount > maxLines

            if (child == this.toggleView && showToggle) { // 需要显示时，显示折叠按钮
                child.visibility = View.VISIBLE
                val childWidthTmp = marginParams.marginStart + marginParams.marginEnd + measuredWidth
                if (rowsWidth + childWidthTmp > flowWidth) { // 加入折叠按钮后超出容器范围，隐藏前一个标签
                    val lastRect = lastChild?.tag as Rect
                    val toggleStart = lastRect.left + marginParams.marginStart
                    child.tag = Rect(toggleStart,lastRect.top,toggleStart + measuredWidth, lastRect.bottom)
                    lastChild.visibility = View.GONE
                } else { // 显示到最后一个显示的标签后边
                    val lastRect = lastChild?.tag as Rect
                    val toggleStart = lastRect.right + marginParams.marginStart
                    child.tag = Rect(toggleStart,lastRect.top,toggleStart + measuredWidth, lastRect.bottom)
                }
            } else if (child == this.toggleView) {
                child.visibility = View.GONE // 不需要时，隐藏折叠按钮
            }

            if (child != this.toggleView && child.visibility == View.VISIBLE) {
                lastChild = child
                //累加上该行子元素宽度
                rowsWidth += childWidth
                //判断时占的宽段时加上margin计算，设置顶点位置时不包括margin位置，不然margin会不起作用，这是给View设置tag,在onlayout给子元素设置位置再遍历取出
                child.tag = Rect(rowsWidth - childWidth + marginParams.marginStart, columnHeight + marginParams.topMargin, rowsWidth - marginParams.marginEnd, columnHeight + childHeight - marginParams.bottomMargin)
            }
        }

        //返回子元素总所占宽度和高度（用于计算Flowlayout的AT_MOST模式设置宽高）
        val flowParams = FlowParams()
        //单行
        if (aRow) {
            flowParams.allChildWidth = rowsWidth
        } else {
            //多行
            flowParams.allChildWidth = flowWidth
        }
        //FlowLayout测量高度 = 当前行顶部已占高度 +当前行内子元素最大高度+FlowLayout的PaddingBottom
        flowParams.allChildHeight = columnHeight + rowsMaxHeight + paddingBottom

        updateToggleView()
        return flowParams
    }

    data class FlowParams(
        var allChildWidth: Int = 0,
        var allChildHeight: Int = 0
    )
}
```



##### 用法示范

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_1

import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    private val tags = arrayListOf("Android", "iOS", "和平精英", "王者荣耀", "明日之后", "改变世界", "开开心心", "一切皆有定数", "人定胜天", "我命由我不由天", "爱拼才会赢", "我是野王", "码农宝", "萤火飞", "彪悍的人生")
    private val emptyTags = arrayListOf<String>()

    private var flowLayout: FlowLayout? = null
    private var collapsableFlowLayout: CollapsableFlowLayout? = null
    private var bt_minus_tag:Button ?= null
    private var bt_add_tag:Button ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        flowLayout = findViewById(R.id.flowLayout)
        collapsableFlowLayout = findViewById(R.id.collapsableFlowLayout)
        bt_minus_tag = findViewById(R.id.bt_minus_tag)
        bt_add_tag = findViewById(R.id.bt_add_tag)
        toolbar?.setTitle(getPluginString(R.string.app_name))
        flowLayout?.setBackgroundColor(0xffff0000.toInt())
        collapsableFlowLayout?.setBackgroundColor(0xff0000ff.toInt())
        collapsableFlowLayout?.maxLines = 2 // 最大行数
    }

    override fun initData() {
        emptyTags.addAll(tags.subList(0,2))
        fillData(emptyTags,flowLayout)
        fillData(emptyTags,collapsableFlowLayout)
    }

    private fun fillData(tags: ArrayList<String>, viewGroup: ViewGroup?) {
        //往容器内添加TextView数据
        val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams.setMargins(10, 10, 10, 5)
        viewGroup?.removeAllViews()
        for (i in 0 until tags.size) {
            val tv = TextView(that)
            tv.setPadding(28, 10, 28, 10)
            tv.text = tags[i]
            tv.maxEms = 10
            tv.setSingleLine()
            tv.background = getPluginDrawable(R.drawable.shape_tag)
            tv.layoutParams = layoutParams
            viewGroup?.addView(tv, layoutParams)
        }
    }

    override fun initEvent() {
        bt_add_tag?.setOnClickListener {
            emptyTags.add(tags[emptyTags.size % tags.size])
            fillData(emptyTags,flowLayout)
            fillData(emptyTags,collapsableFlowLayout)
            collapsableFlowLayout?.addToggleView()
        }
        bt_minus_tag?.setOnClickListener {
            if (emptyTags.size < 3) return@setOnClickListener
            emptyTags.removeLast()
            fillData(emptyTags,flowLayout)
            fillData(emptyTags,collapsableFlowLayout)
            collapsableFlowLayout?.addToggleView()
        }
    }
}

```

##### shape_tag.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android"
    android:shape="rectangle">
    <corners android:radius="5dp"/>
    <solid android:color="#62FF00"/>
</shape>
```

