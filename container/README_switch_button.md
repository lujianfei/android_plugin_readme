# 仿 iOS 开关 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >       

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="10dp"
        android:orientation="horizontal"
        android:gravity="center_vertical">
        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="10dp"
            android:textSize="20sp"
            android:text="初始化关闭"/>
        <com.lujianfei.plugin2_15.SwitchButton
            android:id="@+id/switchButtonClose"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="10dp"
            />
        <TextView
            android:id="@+id/txtStatus1"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textSize="20sp"
            android:layout_marginStart="10dp"
            />
    </LinearLayout>
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="10dp"
        android:orientation="horizontal"
        android:gravity="center_vertical">
        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="10dp"
            android:textSize="20sp"
            android:text="初始化开启"/>
        <com.lujianfei.plugin2_15.SwitchButton
            android:id="@+id/switchButtonOpen"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginStart="10dp"
            />
        <TextView
            android:id="@+id/txtStatus2"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textSize="20sp"
            android:layout_marginStart="10dp"
            />
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_15

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }
        
    private var switchButtonClose:SwitchButton ?= null
    private var switchButtonOpen:SwitchButton ?= null
    private var txtStatus1:TextView ?= null
    private var txtStatus2:TextView ?= null
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {       
        switchButtonClose = findViewById(R.id.switchButtonClose)
        switchButtonOpen = findViewById(R.id.switchButtonOpen)

        txtStatus1 = findViewById(R.id.txtStatus1)
        txtStatus2 = findViewById(R.id.txtStatus2)

        switchButtonOpen?.isCheck = true

        updateStatus()
    }

    private fun updateStatus() {
        txtStatus1?.text = if (switchButtonClose?.isCheck == true) "ON" else "OFF"
        txtStatus2?.text = if (switchButtonOpen?.isCheck == true) "ON" else "OFF"
    }   

    override fun initEvent() {       
        switchButtonClose?.onCheckChangeListener = {
            updateStatus()
        }
        switchButtonOpen?.onCheckChangeListener = {
            updateStatus()
        }
    }
}

```

核心组件类

##### SwitchButton.kt

```kotlin
package com.lujianfei.plugin2_15

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.animation.addListener
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.LogUtils
import kotlin.math.abs

class SwitchButton @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        private const val TAG = "SwitchButton"
        private val radius = DensityUtils.dip2px(15f).toFloat()
        private val uncheckedBg =  0xffeceaed.toInt()
        private val checkedBg =  0xff6edf82.toInt()
    }

    private var isRunningAnim = false
    private var isDown = false

    private val mPaintBg = Paint()
    private val mPaintButton = Paint()


    private var buttonCenterX = 0f
    private var buttonCenterY = 0f
    private var buttonRadius = 0f

    var onCheckChangeListener:((Boolean)->Unit) ?= null

    var isCheck = false
    set(value) {
        // 修改 按钮位置、背景颜色
        val buttonPadding = DensityUtils.dip2px(2f)
        buttonRadius = radius - buttonPadding
        if (value) {
            buttonCenterX = context.resources.getDimensionPixelOffset(R.dimen.switcher_width) - radius
            mPaintBg.color = checkedBg
        } else {
            buttonCenterX = radius
            mPaintBg.color = uncheckedBg
        }
        buttonCenterY = radius
        field = value
        invalidate()
    }

    init {
        mPaintBg.apply {
            color = 0xffeceaed.toInt()
            isAntiAlias = true
        }
        mPaintButton.apply {
            color = 0xfffffeff.toInt()
            isAntiAlias = true
        }
        // 绘制按钮
        val buttonPadding = DensityUtils.dip2px(2f)
        buttonRadius = radius - buttonPadding
        buttonCenterX = radius
        buttonCenterY = radius
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                 isDown = true
                Log.d(TAG, "ACTION_DOWN $isDown")
            }
            MotionEvent.ACTION_UP -> {
                Log.d(TAG, "ACTION_UP $isDown")
                if (isDown && !isRunningAnim) {
                    isDown = false
                    isRunningAnim = true

                    var startX = 0f
                    var endX = 0f

                    if (isCheck) {
                        endX = radius
                        startX = width - radius
                    } else {
                        startX = radius
                        endX = width - radius
                    }
                    isCheck = !isCheck
                    val animator = ValueAnimator.ofFloat(startX, endX)
                    animator.addListener(
                            onEnd = {
                                isRunningAnim = false
                                onCheckChangeListener?.invoke(isCheck)
                            }
                    )
                    animator.addUpdateListener {
                        buttonCenterX = it.animatedValue.toString().toFloat()
                        var maxColor = 0f
                        var currentProgress = 0f
                        maxColor = if (startX > endX) {
                            startX - endX
                        } else {
                            endX - startX
                        }
                        currentProgress = abs(buttonCenterX - startX)
                        if (isCheck) {
                            mPaintBg.color = changeColor(beginColor = uncheckedBg, endColor = checkedBg, progres = currentProgress, max = maxColor)
                        } else {
                            mPaintBg.color = changeColor(beginColor = checkedBg, endColor = uncheckedBg, progres = currentProgress, max = maxColor)
                        }
                        invalidate()
                    }
                    animator.start()
                }
            }
        }
        return true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(context.resources.getDimensionPixelOffset(R.dimen.switcher_width),
                context.resources.getDimensionPixelOffset(R.dimen.switcher_height))
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onDraw(canvas: Canvas?) {
        // 绘制背景
        canvas?.drawRoundRect(0f,0f,width.toFloat(),height.toFloat(),radius,radius,mPaintBg)
        // 绘制按钮
        canvas?.drawCircle(buttonCenterX, buttonCenterY, buttonRadius, mPaintButton)
    }

    private fun changeColor(beginColor:Int, endColor:Int, progres:Float, max:Float):Int {
        val beginColorObj = intToColor(beginColor)
        val endColorObj = intToColor(endColor)
        val myColor = MyColor(0,0,0)
        myColor.r = beginColorObj.r - ((beginColorObj.r - endColorObj.r).toFloat() * progres / max).toInt()
        myColor.g = beginColorObj.g - ((beginColorObj.g - endColorObj.g).toFloat() * progres / max).toInt()
        myColor.b = beginColorObj.b - ((beginColorObj.b - endColorObj.b).toFloat() * progres / max).toInt()
        return colorToInt(myColor)
    }

    private fun intToColor(color:Int) :MyColor {
        val r = color shr 16 and 0xff
        val g = color shr 8 and 0xff
        val b = color and 0xff
        return MyColor(r,g,b)
    }

    private fun colorToInt(color:MyColor ) :Int {
        return (0xff000000 + (color.r shl 16) + (color.g shl 8) + color.b).toInt()
    }

    data class MyColor (var r:Int, var g:Int, var b:Int)
}
```

##### 依赖的 dimens.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <dimen name="switcher_width">55dp</dimen>
    <dimen name="switcher_height">30dp</dimen>
</resources>
```

