# 图片倒影效果

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >
    
     ...
    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content">
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            android:background="#000000">
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:textColor="#ffffff"
                android:layout_marginTop="20dp"
                android:gravity="center"
                android:textSize="20sp"
                android:text="原图"/>
            <ImageView
                android:id="@+id/icon_origin"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                />
            
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:textColor="#ffffff"
                android:gravity="center"
                android:textSize="20sp"
                android:text="加入了倒影效果的图"/>
            <ImageView
                android:id="@+id/icon_after"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_gravity="center_horizontal"
                />
        </LinearLayout>
    </ScrollView>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin2_5

import android.content.Intent
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
    }
    
    private var icon_origin: ImageView? = null
    private var icon_after: ImageView? = null
   ...

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        icon_origin = findViewById(R.id.icon_origin)
        icon_after = findViewById(R.id.icon_after)
        ...

        val drawable = getPluginDrawable(R.drawable.horse)
        val beforeBmp = (drawable as BitmapDrawable).bitmap
        val afterBmp = createReflectedImage(beforeBmp)
        beforeBmp?.let { before->
            icon_origin?.setImageBitmap(before)
        }
        afterBmp?.let { after->
            icon_after?.setImageBitmap(after)
        }
    }

    ...

    private fun createReflectedImage(originalImage: Bitmap): Bitmap? {
        // 原图和倒影的间距 (像素)
        val reflectionGap = 4 
        val width = originalImage.width
        val height = originalImage.height
        val matrix = Matrix()
        matrix.preScale(1f, -1f)
        // 截取下半部分用作倒影
        val reflectionImage: Bitmap = Bitmap.createBitmap(originalImage, 0,
                height / 2, width, height / 2, matrix, false)
        // 加入倒影的Bitmap, 高度增加 50 %
        val bitmapWithReflection = Bitmap.createBitmap(width,
                height + height / 2, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmapWithReflection)
        // 放置原图
        canvas.drawBitmap(originalImage, 0f, 0f, null)
        // 原图和倒影的间距绘制
        val defaultPaint = Paint()
        canvas.drawRect(0f, height.toFloat(), width.toFloat(), (height + reflectionGap).toFloat(), defaultPaint)
        // 将倒影图像绘制上去
        canvas.drawBitmap(reflectionImage, 0f, (height + reflectionGap).toFloat(), null)
        val paint = Paint()
        // 线性渐变的着色器, 半透明 -> 全透明 渐变
        val shader = LinearGradient(0f,
                originalImage.height.toFloat(), 
                0f, 
                (bitmapWithReflection.height + reflectionGap).toFloat(), 
                0x70ffffff, 
                0x00ffffff,
                Shader.TileMode.MIRROR)
        paint.shader = shader
        // 求交集取Dst
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_IN) 
        
        canvas.drawRect(0f, 
                height.toFloat(), 
                width.toFloat(),
                (bitmapWithReflection.height
                + reflectionGap).toFloat(), paint)
        return bitmapWithReflection
    }    
}

```
