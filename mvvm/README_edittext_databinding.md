# android Edittext 双向绑定 操作 代码展示

以下只展示关键部分

##### Fragment_edittext_databinding.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <data>
        <variable
            name="vm"
            type="com.lujianfei.other.viewmodel.DataBindingEditTextViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <!-- 标题栏部分 -->
        <com.lujianfei.module_plugin_base.widget.PluginToolBar
            android:id="@+id/toolbar"
            android:layout_width="match_parent"
            android:layout_height="56dp"
            app:layout_constraintTop_toTopOf="parent" />


        <androidx.appcompat.widget.AppCompatTextView
            android:id="@+id/textView"
            android:layout_width="match_parent"
            android:layout_height="50dp"
            android:text="@{vm.editTextField}"
            android:textSize="25sp"
            android:gravity="center_vertical"
            android:textColor="@color/black"
            android:background="#dddddd"
            app:layout_constraintTop_toBottomOf="@id/toolbar"
            />

        <androidx.appcompat.widget.AppCompatEditText
            android:id="@+id/editText"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="@color/black"
            android:textSize="25sp"
            android:layout_marginTop="10dp"
            android:text="@{vm.editTextField}"
            android:hint="输入文本"
            app:addTextChangedListener = "@{vm.textWatcher}"
            app:layout_constraintTop_toBottomOf="@id/textView"
            >
            <requestFocus/>
        </androidx.appcompat.widget.AppCompatEditText>
        <!--
            或使用如下写法(实现双向)
            android:text="@={vm.editTextField}"
            不用写法这句监听
            app:addTextChangedListener = "@{vm.textWatcher}"
            -->
        <Button
            app:layout_constraintTop_toBottomOf="@id/editText"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:onClick="@{vm.onButtonClick}"
            android:text="获取最终文本"
            android:textColor="@color/black"
            />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
```

##### DataBindingEditTextViewModel.kt

```kotlin
package com.lujianfei.other.viewmodel

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.lujianfei.module_plugin_base.BaseApplication

/**
 * Author: mac
 * Date: 31.5.22 7:34 下午
 * Email:johnson@miaoshitech.com
 * Description: ViewModel 操作类
 */
class DataBindingEditTextViewModel: ViewModel() {

    val editTextField = ObservableField("")

    val textWatcher = object: TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            editTextField.set(s.toString())
        }
    }

    val onButtonClick = View.OnClickListener {
        Toast.makeText(BaseApplication.INSTANCE, "${editTextField.get()}", Toast.LENGTH_SHORT).show()
    }
}
```



##### DataBindingEditTextFragment.kt

```kotlin
package com.lujianfei.other.ui

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.other.R
import com.lujianfei.other.databinding.FragmentEdittextDatabindingBinding
import com.lujianfei.other.viewmodel.DataBindingEditTextViewModel

/**
 * Author: mac
 * Date: 31.5.22 6:53 下午
 * Email:johnson@miaoshitech.com
 * Description: Databinding 实现例子
 */
class DataBindingEditTextFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    private var bean: PluginActivityBean? = null
    private var binding: FragmentEdittextDatabindingBinding? = null
    private var viewModel: DataBindingEditTextViewModel? = null

    override fun resouceId() = R.layout.fragment_edittext_databinding

    override fun initView() {
        viewModel = ViewModelProvider(this).get(DataBindingEditTextViewModel::class.java)
        view?.let { view -> binding = FragmentEdittextDatabindingBinding.bind(view) }
        binding?.vm = viewModel
    }

    override fun initData() {
        bean = arguments?.getParcelable("data")
        bean?.let { bean ->
            binding?.toolbar?.setTitle(bean.itemName)
            binding?.toolbar?.getRightTopTextView()?.visibility =
                if (bean.codeUrl.isEmpty()) View.GONE else View.VISIBLE
        }
    }

    override fun initEvent() {
        binding?.toolbar?.onBackListener = {
            activity?.finish()
        }
        binding?.toolbar?.onRightTopListener = {
            openBrowser()
        }
    }

    private fun openBrowser() {
        bean?.let { bean ->
            val uri = Uri.parse(bean.codeUrl)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }
}
```

##### 
