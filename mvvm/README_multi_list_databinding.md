# android Mvvm + Databinding 多类型列表操作 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    implementation 'com.scwang.smartrefresh:SmartRefreshLayout:1.1.3'
    implementation 'com.scwang.smartrefresh:SmartRefreshHeader:1.1.3'
    implementation 'com.youth.banner:banner:2.0.10'
	...
}
```

##### fragment_mutli_list_databinding.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <data>
        <variable
            name="vm"
            type="com.lujianfei.other.viewmodel.DataBindingMultiListViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <!-- 标题栏部分 -->
        <com.lujianfei.module_plugin_base.widget.PluginToolBar
            android:id="@+id/toolbar"
            android:layout_width="match_parent"
            android:layout_height="56dp"
            app:layout_constraintTop_toTopOf="parent" />
        <com.scwang.smartrefresh.layout.SmartRefreshLayout
            android:id="@+id/refreshLayout"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            app:layout_constraintTop_toBottomOf="@id/toolbar"
            app:layout_constraintBottom_toBottomOf="parent"
            refreshing="@{vm.refreshing}"
            moreLoading="@{vm.moreLoading}"
            >
            <com.scwang.smartrefresh.layout.header.ClassicsHeader
                android:layout_width="match_parent"
                android:layout_height="wrap_content"/>
            <androidx.recyclerview.widget.RecyclerView
                android:id="@+id/list"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                multilistdata="@{vm.listdata}"
                />
            <com.scwang.smartrefresh.layout.footer.ClassicsFooter
                android:layout_width="match_parent"
                android:layout_height="wrap_content"/>
        </com.scwang.smartrefresh.layout.SmartRefreshLayout>
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
```

#### DataBindingMultiListViewModel.kt

```kotlin
package com.lujianfei.other.viewmodel

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lujianfei.other.adapter.base.BaseAdapterItem
import com.lujianfei.other.bean.*
import com.lujianfei.other.utils.CustomDiffUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Author: mac
 * Date: 31.5.22 7:34 下午
 * Email:johnson@miaoshitech.com
 * Description: ViewModel 操作类
 */
class DataBindingMultiListViewModel: ViewModel() {

    companion object {
        const val PAGE_SIZE = 20
    }

    var count = 0

    val refreshing = ObservableField(true)
    val moreLoading = ObservableField(true)

    val listdata = ObservableArrayList<BaseAdapterItem>()

    val longClickLiveData = MutableLiveData<BaseAdapterItem>()

    val itemClick:((Int,BaseAdapterItem)->Unit) = { resId, bean->

    }

    val itemLongClick:((Int,BaseAdapterItem)->Unit) = { resId, bean->
        longClickLiveData.value = bean
    }

    fun requestData() {
        viewModelScope.launch(Dispatchers.Main) {
            refreshing.set(true)
            val response = withContext(Dispatchers.IO) {
                delay(1000)
                count = 0
                val newdatalist = arrayListOf<BaseAdapterItem>()

                // Banner
                val bannerItems  = ObservableArrayList<BannerItemListItem>()
                bannerItems.add(BannerItemListItem(bgColor = 0xffff0000.toInt(), title = "Banner 1", titleColor = 0xffffffff.toInt()))
                bannerItems.add(BannerItemListItem(bgColor = 0xff00ff00.toInt(), title = "Banner 2", titleColor = 0xff000000.toInt()))
                bannerItems.add(BannerItemListItem(bgColor = 0xff0000ff.toInt(), title = "Banner 3", titleColor = 0xffffffff.toInt()))
                newdatalist.add(BannerItem(items = bannerItems))
                // Divider
                newdatalist.add(DividerItem(id = 1))

                // Grid
                val gridItems  = ObservableArrayList<GridItemListItem>()
                gridItems.add(GridItemListItem(bgColor = 0xffff0000.toInt(), title = "Grid 1", titleColor = 0xffffffff.toInt()))
                gridItems.add(GridItemListItem(bgColor = 0xff00ff00.toInt(), title = "Grid 2", titleColor = 0xff000000.toInt()))
                gridItems.add(GridItemListItem(bgColor = 0xff0000ff.toInt(), title = "Grid 3", titleColor = 0xffffffff.toInt()))
                gridItems.add(GridItemListItem(bgColor = 0xff00ffff.toInt(), title = "Grid 4", titleColor = 0xff000000.toInt()))
                newdatalist.add(GridItem(items = gridItems))
                // Divider
                newdatalist.add(DividerItem(id = 2))

                // List
                newdatalist.add(ListItem(bgColor = 0xffff0000.toInt(), title = "List Item 1", titleColor = 0xffffffff.toInt()))
                newdatalist.add(ListItem(bgColor = 0xff00ff00.toInt(), title = "List Item 2", titleColor = 0xff000000.toInt()))
                newdatalist.add(ListItem(bgColor = 0xff0000ff.toInt(), title = "List Item 3", titleColor = 0xffffffff.toInt()))
                newdatalist.add(ListItem(bgColor = 0xff00ffff.toInt(), title = "List Item 4", titleColor = 0xff000000.toInt()))
                newdatalist.add(ListItem(bgColor = 0xffff0000.toInt(), title = "List Item 5", titleColor = 0xffffffff.toInt()))
                newdatalist.add(ListItem(bgColor = 0xff00ff00.toInt(), title = "List Item 6", titleColor = 0xff000000.toInt()))
                newdatalist.add(ListItem(bgColor = 0xff0000ff.toInt(), title = "List Item 7", titleColor = 0xffffffff.toInt()))
                newdatalist.add(ListItem(bgColor = 0xff00ffff.toInt(), title = "List Item 8", titleColor = 0xff000000.toInt()))
                newdatalist.add(ListItem(bgColor = 0xffff0000.toInt(), title = "List Item 9", titleColor = 0xffffffff.toInt()))
                newdatalist.add(ListItem(bgColor = 0xff00ff00.toInt(), title = "List Item 10", titleColor = 0xff000000.toInt()))
                newdatalist.add(ListItem(bgColor = 0xff0000ff.toInt(), title = "List Item 11", titleColor = 0xffffffff.toInt()))
                newdatalist.add(ListItem(bgColor = 0xff00ffff.toInt(), title = "List Item 12", titleColor = 0xff000000.toInt()))

                newdatalist
            }
            val oldSize = listdata.size
            val newSize = response.size
            val minSize = Math.min(oldSize, newSize)
            for (idx in 0 until minSize) {
                listdata[idx] = response[idx]
            }
            if (oldSize < newSize) {
                listdata.addAll(response.subList(minSize,response.size))
            } else {
                for (idx in listdata.size - 1 downTo minSize) {
                    listdata.removeAt(idx)
                }
            }
            refreshing.set(false)
        }
    }

    fun requestMoreData() {
        viewModelScope.launch(Dispatchers.Main) {
            moreLoading.set(true)
            val response = withContext(Dispatchers.IO) {
                delay(1000)

                val newdatalist = arrayListOf<BaseAdapterItem>()
                if (count == 0) {
                    // List
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xffff0000.toInt(),
                            title = "List Item 13",
                            titleColor = 0xffffffff.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xff00ff00.toInt(),
                            title = "List Item 14",
                            titleColor = 0xff000000.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xff0000ff.toInt(),
                            title = "List Item 15",
                            titleColor = 0xffffffff.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xff00ffff.toInt(),
                            title = "List Item 16",
                            titleColor = 0xff000000.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xffff0000.toInt(),
                            title = "List Item 17",
                            titleColor = 0xffffffff.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xff00ff00.toInt(),
                            title = "List Item 18",
                            titleColor = 0xff000000.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xff0000ff.toInt(),
                            title = "List Item 19",
                            titleColor = 0xffffffff.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xff00ffff.toInt(),
                            title = "List Item 20",
                            titleColor = 0xff000000.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xffff0000.toInt(),
                            title = "List Item 21",
                            titleColor = 0xffffffff.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xff00ff00.toInt(),
                            title = "List Item 22",
                            titleColor = 0xff000000.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xff0000ff.toInt(),
                            title = "List Item 23",
                            titleColor = 0xffffffff.toInt()
                        )
                    )
                    newdatalist.add(
                        ListItem(
                            bgColor = 0xff00ffff.toInt(),
                            title = "List Item 24",
                            titleColor = 0xff000000.toInt()
                        )
                    )
                }
                ++count
                newdatalist
            }
            if (response.isNotEmpty()) {
                listdata.addAll(response)
            }
            moreLoading.set(false)
        }
    }
}
```



##### DataBindingMultiListFragment.kt

```kotlin
package com.lujianfei.other.ui

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.other.R
import com.lujianfei.other.databinding.FragmentMutliListDatabindingBinding
import com.lujianfei.other.viewmodel.DataBindingMultiListViewModel

/**
 * Author: mac
 * Date: 31.5.22 6:53 下午
 * Email:johnson@miaoshitech.com
 * Description: Databinding 实现多类型列表例子
 */
class DataBindingMultiListFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    private var bean: PluginActivityBean? = null
    private var binding: FragmentMutliListDatabindingBinding? = null
    private var viewModel: DataBindingMultiListViewModel? = null

    override fun resouceId() = R.layout.fragment_mutli_list_databinding

    override fun initView() {
        viewModel = ViewModelProvider(this).get(DataBindingMultiListViewModel::class.java)
        view?.let { view -> binding = FragmentMutliListDatabindingBinding.bind(view) }
        binding?.vm = viewModel
    }

    override fun initData() {
        bean = arguments?.getParcelable("data")
        bean?.let { bean ->
            binding?.toolbar?.setTitle(bean.itemName)
            binding?.toolbar?.getRightTopTextView()?.visibility =
                if (bean.codeUrl.isEmpty()) View.GONE else View.VISIBLE
        }
    }

    override fun initEvent() {
        viewModel?.longClickLiveData?.observe(this) { bean ->
            context?.let { context ->
            }
        }
        binding?.toolbar?.onBackListener = {
            activity?.finish()
        }
        binding?.toolbar?.onRightTopListener = {
            openBrowser()
        }
        binding?.refreshLayout?.setOnRefreshListener {
            viewModel?.requestData()
        }
        binding?.refreshLayout?.setOnLoadMoreListener {
            viewModel?.requestMoreData()
        }
        binding?.refreshLayout?.autoRefresh()
    }

    private fun openBrowser() {
        bean?.let { bean ->
            val uri = Uri.parse(bean.codeUrl)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }
}
```

##### MultiTypeBindAdapter.kt 多类型适配器

```kotlin
package com.lujianfei.other.adapter

import androidx.databinding.ObservableArrayList
import com.lujianfei.other.adapter.base.BaseAdapterItem
import com.lujianfei.other.adapter.base.BaseBindingMultiAdapter
import com.lujianfei.other.adapter.viewholder.BannerViewHolder
import com.lujianfei.other.adapter.viewholder.DividerViewHolder
import com.lujianfei.other.adapter.viewholder.GridViewHolder
import com.lujianfei.other.adapter.viewholder.ListViewHolder

class MultiTypeBindAdapter(items: ObservableArrayList<BaseAdapterItem>) :
    BaseBindingMultiAdapter<BaseAdapterItem>(items) {

    companion object {
        const val ITEM_TYPE_BANNER = 0
        const val ITEM_TYPE_GRID = 1
        const val ITEM_TYPE_LIST = 2
        const val ITEM_TYPE_DIVIDER = 3
    }

    init {
        addLayoutType(BannerViewHolder())
        addLayoutType(GridViewHolder())
        addLayoutType(ListViewHolder())
        addLayoutType(DividerViewHolder())
    }

    override fun getTypeByPosition(position: Int): Int {
        return items[position].itemType()
    }
}
```

##### BaseAdapterItem.kt 实体类基类

```kotlin
package com.lujianfei.other.adapter.base

/**
 * Author: mac
 * Date: 1.6.22 5:55 下午
 * Email:johnson@miaoshitech.com
 * Description: 实体类基类
 */
interface BaseAdapterItem {

    fun itemType():Int
}
```

##### BannerItem.kt 模型类

```kotlin
package com.lujianfei.other.bean

import androidx.databinding.ObservableArrayList
import com.lujianfei.other.adapter.MultiTypeBindAdapter
import com.lujianfei.other.adapter.base.BaseAdapterItem

/**
 * Author: mac
 * Date: 1.6.22 6:10 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */
data class BannerItem(
    val items: ObservableArrayList<BannerItemListItem>
) : BaseAdapterItem {
    override fun itemType() = MultiTypeBindAdapter.ITEM_TYPE_BANNER
}

data class BannerItemListItem(val bgColor: Int, val title: String, val titleColor:Int)
```

##### GridItem.kt 模型类

```kotlin
package com.lujianfei.other.bean

import androidx.databinding.ObservableArrayList
import com.lujianfei.other.adapter.MultiTypeBindAdapter
import com.lujianfei.other.adapter.base.BaseAdapterItem

data class GridItem(val items: ObservableArrayList<GridItemListItem>) : BaseAdapterItem {
    override fun itemType() = MultiTypeBindAdapter.ITEM_TYPE_GRID
}

data class GridItemListItem(val bgColor: Int, val title: String, val titleColor:Int)
```

##### ListItem.kt 模型类
```kotlin
package com.lujianfei.other.bean

import com.lujianfei.other.adapter.MultiTypeBindAdapter
import com.lujianfei.other.adapter.base.BaseAdapterItem

data class ListItem(val bgColor: Int, val title: String, val titleColor:Int) : BaseAdapterItem {
    override fun itemType() = MultiTypeBindAdapter.ITEM_TYPE_LIST
}
```

##### DividerItem.kt 模型类

```kotlin
package com.lujianfei.other.bean

import com.lujianfei.other.adapter.MultiTypeBindAdapter
import com.lujianfei.other.adapter.base.BaseAdapterItem

class DividerItem : BaseAdapterItem {
    override fun itemType() = MultiTypeBindAdapter.ITEM_TYPE_DIVIDER
}
```

##### BannerViewHolder.kt

Banner 类型 ViewHolder

```kotlin
package com.lujianfei.other.adapter.viewholder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.other.R
import com.lujianfei.other.adapter.MultiTypeBindAdapter
import com.lujianfei.other.adapter.base.BaseAdapterItem
import com.lujianfei.other.adapter.base.BaseBindingMultiAdapter.BaseBindingMultiViewHolder
import com.lujianfei.other.bean.BannerItem
import com.lujianfei.other.bean.BannerItemListItem
import com.lujianfei.other.databinding.OtherBannerItemBinding
import com.lujianfei.other.databinding.OtherProviderBannerItemBinding

class BannerViewHolder: BaseBindingMultiViewHolder<BaseAdapterItem>() {

    override fun layoutResId() = R.layout.other_provider_banner_item

    override fun itemType() = MultiTypeBindAdapter.ITEM_TYPE_BANNER

    override fun onBindItem(binding: ViewDataBinding?, bean: BaseAdapterItem, position: Int) {
        if (binding is OtherProviderBannerItemBinding) {
            if (bean is BannerItem) {
                binding.model = bean
            }
        }
    }

    class BannerAdapter :
        com.youth.banner.adapter.BannerAdapter<BannerItemListItem, RecyclerView.ViewHolder>(null) {

        override fun onCreateHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
            return BaseViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.other_banner_item, parent, false))
        }

        override fun onBindView(
            holder: RecyclerView.ViewHolder?,
            data: BannerItemListItem?,
            position: Int,
            size: Int
        ) {
            holder?.itemView?.let { itemView->
                val binding = OtherBannerItemBinding.bind(itemView)
                binding.model = data
                binding.executePendingBindings()
            }
        }

        class BaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        }
    }
}
```

##### GridViewHolder.kt 

Grid 类型 ViewHolder

```kotlin
package com.lujianfei.other.adapter.viewholder

import androidx.databinding.ViewDataBinding
import com.lujianfei.other.R
import com.lujianfei.other.adapter.MultiTypeBindAdapter
import com.lujianfei.other.adapter.base.BaseAdapterItem
import com.lujianfei.other.adapter.base.BaseBindingMultiAdapter.BaseBindingMultiViewHolder
import com.lujianfei.other.bean.GridItem
import com.lujianfei.other.databinding.OtherProviderGridItemBinding

class GridViewHolder: BaseBindingMultiViewHolder<BaseAdapterItem>() {

    override fun layoutResId() = R.layout.other_provider_grid_item

    override fun itemType() = MultiTypeBindAdapter.ITEM_TYPE_GRID

    override fun onBindItem(binding: ViewDataBinding?, bean: BaseAdapterItem, position: Int) {
        if (binding is OtherProviderGridItemBinding) {
            if (bean is GridItem) {
                binding.model = bean
            }
        }
    }
}
```

##### ListViewHolder.kt 

List 类型 ViewHolder

```kotlin
package com.lujianfei.other.adapter.viewholder

import androidx.databinding.ViewDataBinding
import com.lujianfei.other.R
import com.lujianfei.other.adapter.MultiTypeBindAdapter
import com.lujianfei.other.adapter.base.BaseAdapterItem
import com.lujianfei.other.adapter.base.BaseBindingMultiAdapter.BaseBindingMultiViewHolder
import com.lujianfei.other.bean.GridItem
import com.lujianfei.other.bean.ListItem
import com.lujianfei.other.databinding.OtherProviderGridItemBinding
import com.lujianfei.other.databinding.OtherProviderListItemBinding

class ListViewHolder: BaseBindingMultiViewHolder<BaseAdapterItem>() {

    override fun layoutResId() = R.layout.other_provider_list_item

    override fun itemType() = MultiTypeBindAdapter.ITEM_TYPE_LIST

    override fun onBindItem(binding: ViewDataBinding?, bean: BaseAdapterItem, position: Int) {
        if (binding is OtherProviderListItemBinding) {
            if (bean is ListItem) {
                binding.model = bean
            }
        }
    }
}
```

##### DividerViewHolder.kt 

分隔线 类型 ViewHolder

```kotlin
package com.lujianfei.other.adapter.viewholder

import androidx.databinding.ViewDataBinding
import com.lujianfei.other.R
import com.lujianfei.other.adapter.MultiTypeBindAdapter
import com.lujianfei.other.adapter.base.BaseAdapterItem
import com.lujianfei.other.adapter.base.BaseBindingMultiAdapter.BaseBindingMultiViewHolder
import com.lujianfei.other.bean.GridItem
import com.lujianfei.other.databinding.OtherProviderGridItemBinding

class DividerViewHolder: BaseBindingMultiViewHolder<BaseAdapterItem>() {

    override fun layoutResId() = R.layout.other_provider_divider_item

    override fun itemType() = MultiTypeBindAdapter.ITEM_TYPE_DIVIDER

    override fun onBindItem(binding: ViewDataBinding?, bean: BaseAdapterItem, position: Int) {
    }
}
```



##### BaseBindingMultiAdapter.kt

多类型 DataBinding 适配器基类

```kotlin
package com.lujianfei.other.adapter.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.utils.LogUtils

/**
 * 多类型 DataBinding 适配器基类
 */
abstract class BaseBindingMultiAdapter<T>(var items: ObservableArrayList<T>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    abstract class BaseBindingMultiViewHolder<T> {
        abstract fun layoutResId(): Int
        abstract fun itemType(): Int
        abstract fun onBindItem(binding: ViewDataBinding?, bean:T, position: Int)
    }

    private val typeLayoutRes = mutableMapOf<Int,BaseBindingMultiViewHolder<T>>()

    private val dataListChangedCallback = ListChangedCallback()

    fun addLayoutType(baseBindingMultiViewHolder: BaseBindingMultiViewHolder<T>) {
        typeLayoutRes[baseBindingMultiViewHolder.itemType()] = baseBindingMultiViewHolder
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (typeLayoutRes[viewType]?.layoutResId() == null)
            throw Exception("缺少 viewType = $viewType 类型?")

        return object : RecyclerView.ViewHolder(
            LayoutInflater.from(parent.context).inflate(typeLayoutRes[viewType]?.layoutResId()?:-1, parent, false)
        ) {}
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val baseBindingMultiViewHolder = typeLayoutRes[getItemViewType(position)]
        val item = items[position]
        baseBindingMultiViewHolder?.onBindItem(DataBindingUtil.bind(holder.itemView),
            bean = item,
            position = position)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        items.addOnListChangedCallback(dataListChangedCallback)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        items.removeOnListChangedCallback(dataListChangedCallback)
    }

    override fun getItemCount() = if (items.isEmpty()) 0 else items.size

    override fun getItemViewType(position: Int): Int {
        return getTypeByPosition(position)
    }

    abstract fun getTypeByPosition(position: Int): Int

    /**
     * 额外监听数据变化，这样就可以直接更新数据，
     * 而不用每次都调用notifyDataSetChanged()等方式更新数据
     *
     *
     * 我只对插入和移除做了监听处理，其他方式没有处理
     * 小伙伴可以根据自己的具体需要做额外处理
     */
    inner class ListChangedCallback :
        ObservableList.OnListChangedCallback<ObservableArrayList<*>>() {
        override fun onChanged(newItems: ObservableArrayList<*>) {
            LogUtils.i("ListChangedCallback", "onChanged:$newItems")
            notifyDataSetChanged()
        }

        override fun onItemRangeChanged(
            newItems: ObservableArrayList<*>,
            positionStart: Int,
            itemCount: Int
        ) {
            LogUtils.i(
                "ListChangedCallback",
                "onItemRangeChanged:$positionStart--$itemCount,$newItems"
            )
            notifyItemRangeChanged(positionStart, itemCount, "")
        }

        override fun onItemRangeInserted(
            newItems: ObservableArrayList<*>,
            positionStart: Int,
            itemCount: Int
        ) {
            notifyItemRangeInserted(positionStart, itemCount)
        }

        override fun onItemRangeMoved(
            newItems: ObservableArrayList<*>,
            fromPosition: Int,
            toPosition: Int,
            itemCount: Int
        ) {
            if (itemCount == 1) {
                notifyItemMoved(fromPosition, toPosition)
            } else {
                notifyDataSetChanged()
            }
        }

        override fun onItemRangeRemoved(
            newItems: ObservableArrayList<*>,
            positionStart: Int,
            itemCount: Int
        ) {
            notifyItemRangeRemoved(positionStart, itemCount)
        }
    }
}
```

##### BindAdapter.kt 定义 Databinding 依赖的自定义属性

```kotlin
package com.lujianfei.other.adapter

import android.view.View
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.other.adapter.base.BaseAdapterItem
import com.lujianfei.other.adapter.viewholder.BannerViewHolder
import com.lujianfei.other.bean.BannerItemListItem
import com.lujianfei.other.bean.DatabindingItem
import com.lujianfei.other.bean.GridItemListItem
import com.scwang.smartrefresh.layout.SmartRefreshLayout
import com.youth.banner.Banner

@BindingAdapter(value = ["listdata", "itemClick", "itemLongClick"])
fun bindDatabindingAdapter(
    recyclerView: RecyclerView,
    data: ObservableArrayList<DatabindingItem>,
    itemClick: ((resId:Int,bean:DatabindingItem)->Unit) ?= null,
    itemLongClick: ((resId:Int,bean:DatabindingItem)->Unit) ?= null
) {
    if (recyclerView.layoutManager != null && recyclerView.adapter != null) return
    val layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.layoutManager = layoutManager
    val adapter = DataBindingAdapter(data)
    adapter.itemClickListener = itemClick
    adapter.itemLongClickListener = itemLongClick
    recyclerView.adapter = adapter
}

@BindingAdapter(value = ["multilistdata"], requireAll = false)
fun bindDatabindingMultiAdapter(
    recyclerView: RecyclerView,
    data: ObservableArrayList<BaseAdapterItem>
) {
    if (recyclerView.adapter != null) return
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    val adapter = MultiTypeBindAdapter(data)
    recyclerView.adapter = adapter
}

@BindingAdapter(
    value = ["refreshing", "moreLoading", "hasMore"],
    requireAll = false
)
fun bindSmartRefreshLayout(
    smartLayout: SmartRefreshLayout,
    refreshing: Boolean,
    moreLoading: Boolean,
    hasMore: Boolean
) {//状态绑定，控制停止刷新
    if (!refreshing) smartLayout.finishRefresh()
    if (!moreLoading) smartLayout.finishLoadMore()
}

@BindingAdapter(value = ["bannerlist"])
fun bindBannerAdapter(
    banner: Banner<BannerItemListItem,*>,
    data: ObservableArrayList<BannerItemListItem>
) {
    if (banner.adapter == null) {
        val adapter = BannerViewHolder.BannerAdapter()
        banner.adapter = adapter
        banner.setDatas(data)
    }
}

@BindingAdapter(
    value = ["bindBackground"],
)
fun bindBackground(view: View,
    color:Int
) {
    view.setBackgroundColor(color)
}

@BindingAdapter(
    value = ["bindTextColor"],
)
fun bindTextColor(view: TextView,
                  color:Int
) {
    view.setTextColor(color)
}


@BindingAdapter(value = ["griddata"])
fun bindGridAdapter(
    recyclerView: RecyclerView,
    items: ObservableArrayList<GridItemListItem>,
) {
    if (recyclerView.layoutManager != null && recyclerView.adapter != null) return
    val layoutManager = GridLayoutManager(recyclerView.context, 2)
    recyclerView.layoutManager = layoutManager
    val adapter = GridAdapter(items = items)
    recyclerView.adapter = adapter
}
```