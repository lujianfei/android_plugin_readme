# android Mvvm + Databinding 列表操作 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    implementation 'com.scwang.smartrefresh:SmartRefreshLayout:1.1.3'
    implementation 'com.scwang.smartrefresh:SmartRefreshHeader:1.1.3'
	...
}
```

##### Fragment_databinding.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <data>
        <variable
            name="vm"
            type="com.lujianfei.other.viewmodel.DataBindingViewModel" />
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <!-- 标题栏部分 -->
        <com.lujianfei.module_plugin_base.widget.PluginToolBar
            android:id="@+id/toolbar"
            android:layout_width="match_parent"
            android:layout_height="56dp"
            app:layout_constraintTop_toTopOf="parent" />
        <com.scwang.smartrefresh.layout.SmartRefreshLayout
            android:id="@+id/refreshLayout"
            android:layout_width="match_parent"
            android:layout_height="0dp"
            app:layout_constraintTop_toBottomOf="@id/toolbar"
            app:layout_constraintBottom_toBottomOf="parent"
            refreshing = "@{vm.refreshing}"
            moreLoading = "@{vm.moreLoading}"
            >
            <com.scwang.smartrefresh.layout.header.ClassicsHeader
                android:layout_width="match_parent"
                android:layout_height="wrap_content"/>
            <androidx.recyclerview.widget.RecyclerView
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                listdata="@{vm.datalist}"
                itemClick="@{vm.itemClick}"
                itemLongClick="@{vm.itemLongClick}"
                />
            <com.scwang.smartrefresh.layout.footer.ClassicsFooter
                android:layout_width="match_parent"
                android:layout_height="wrap_content"/>
        </com.scwang.smartrefresh.layout.SmartRefreshLayout>
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
```

#### DataBindingViewModel.kt

```kotlin
package com.lujianfei.other.viewmodel

import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.lujianfei.other.bean.DatabindingItem
import com.lujianfei.other.utils.CustomDiffUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

/**
 * Author: mac
 * Date: 31.5.22 7:34 下午
 * Email:johnson@miaoshitech.com
 * Description: ViewModel 操作类
 */
class DataBindingViewModel : ViewModel() {

    companion object {
        const val PAGE_SIZE = 20
    }

    var count = 0

    val datalist = ObservableArrayList<DatabindingItem>()
    val refreshing = ObservableField(true)
    val moreLoading = ObservableField(true)

    val longClickLiveData = MutableLiveData<DatabindingItem>()

    val itemClick: ((Int, DatabindingItem) -> Unit) = { resId, bean ->

    }

    val itemLongClick: ((Int, DatabindingItem) -> Unit) = { resId, bean ->
        longClickLiveData.value = bean
    }

    fun requestData() {
        viewModelScope.launch(Dispatchers.Main) {
            refreshing.set(true)
            val response = withContext(Dispatchers.IO) {
                delay(1000)
                val newdatalist = arrayListOf<DatabindingItem>()
                for (index in 0 until PAGE_SIZE) {
                    newdatalist.add(DatabindingItem(title = "码农宝标题 ${count + index}"))
                }
                newdatalist
            }
            val oldSize = datalist.size
            val newSize = response.size
            val minSize = Math.min(oldSize, newSize)
            for (idx in 0 until minSize) {
                datalist[idx] = response[idx]
            }
            if (oldSize < newSize) {
                datalist.addAll(response.subList(minSize,response.size))
            } else {
                for (idx in datalist.size - 1 downTo minSize) {
                    datalist.removeAt(idx)
                }
            }
            refreshing.set(false)
            ++count
        }
    }

    fun requestMoreData() {
        viewModelScope.launch(Dispatchers.Main) {
            moreLoading.set(true)
            val response = withContext(Dispatchers.IO) {
                delay(1000)
                val startIndex = datalist.size
                val endIndex = startIndex + PAGE_SIZE
                val newdatalist = arrayListOf<DatabindingItem>()
                for (index in startIndex until endIndex) {
                    newdatalist.add(DatabindingItem(title = "码农宝标题 $index"))
                }
                newdatalist
            }
            datalist.addAll(response)
            moreLoading.set(false)
        }
    }

    fun deleteItem(bean: DatabindingItem?) {
        datalist.remove(bean)
    }
}
```



##### DataBindingFragment.kt

```kotlin
package com.lujianfei.other.ui

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.other.R
import com.lujianfei.other.databinding.FragmentDatabindingBinding
import com.lujianfei.other.viewmodel.DataBindingViewModel

/**
 * Author: mac
 * Date: 31.5.22 6:53 下午
 * Email:johnson@miaoshitech.com
 * Description: Databinding 实现例子
 */
class DataBindingFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    private var bean: PluginActivityBean? = null
    private var binding: FragmentDatabindingBinding? = null
    private var viewModel: DataBindingViewModel? = null

    override fun resouceId() = R.layout.fragment_databinding

    override fun initView() {
        viewModel = ViewModelProvider(this).get(DataBindingViewModel::class.java)
        view?.let { view -> binding = FragmentDatabindingBinding.bind(view) }
        binding?.vm = viewModel
    }

    override fun initData() {
        bean = arguments?.getParcelable("data")
        bean?.let { bean ->
            binding?.toolbar?.setTitle(bean.itemName)
            binding?.toolbar?.getRightTopTextView()?.visibility =
                if (bean.codeUrl.isEmpty()) View.GONE else View.VISIBLE
        }
    }

    override fun initEvent() {
        viewModel?.longClickLiveData?.observe(this) { bean ->
            context?.let { context ->
                AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT).setTitle("提示").setMessage("是否删除 ${bean.title} 项")
                    .setNegativeButton("取消") { _, which ->
                    }.setPositiveButton("确定") { _, which ->
                    viewModel?.deleteItem(bean)
                }
                    .create()
                    .show()
            }
        }
        binding?.toolbar?.onBackListener = {
            activity?.finish()
        }
        binding?.toolbar?.onRightTopListener = {
            openBrowser()
        }
        binding?.refreshLayout?.setOnRefreshListener {
            viewModel?.requestData()
        }
        binding?.refreshLayout?.setOnLoadMoreListener {
            viewModel?.requestMoreData()
        }
        binding?.refreshLayout?.autoRefresh()
    }

    private fun openBrowser() {
        bean?.let { bean ->
            val uri = Uri.parse(bean.codeUrl)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }
}
```

##### BaseBindingAdapter.kt 适配器基类

```kotlin
package com.lujianfei.other.adapter.base

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.utils.LogUtils

abstract class BaseBindingAdapter<M, B : ViewDataBinding>(private var items: ObservableArrayList<M>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    class BaseBindingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val dataListChangedCallback = ListChangedCallback()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding: B = DataBindingUtil.inflate(LayoutInflater.from(parent.context), getLayoutResId(), parent, false)
        return BaseBindingViewHolder(binding.root)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is BaseBindingViewHolder) {
            val bean : M = items[position]
            val binding: B? = DataBindingUtil.getBinding(holder.itemView)
            onBindItem(binding, bean, position)
            binding?.executePendingBindings()
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        items.addOnListChangedCallback(dataListChangedCallback)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)
        items.removeOnListChangedCallback(dataListChangedCallback)
    }

    override fun getItemCount() = if (items.isEmpty()) 0 else items.size

    abstract fun getLayoutResId(): Int

    abstract fun onBindItem(binding: B?, bean: M, position: Int)

    /**
     * 额外监听数据变化，这样就可以直接更新数据，
     * 而不用每次都调用notifyDataSetChanged()等方式更新数据
     *
     *
     * 我只对插入和移除做了监听处理，其他方式没有处理
     * 小伙伴可以根据自己的具体需要做额外处理
     */
    inner class ListChangedCallback : ObservableList.OnListChangedCallback<ObservableArrayList<*>>() {
        override fun onChanged(newItems: ObservableArrayList<*>) {
            LogUtils.i("ListChangedCallback","onChanged:$newItems")
            notifyDataSetChanged()
        }

        override fun onItemRangeChanged(newItems: ObservableArrayList<*>, positionStart: Int, itemCount: Int) {
            LogUtils.i("ListChangedCallback","onItemRangeChanged:$positionStart--$itemCount,$newItems")
            notifyItemRangeChanged(positionStart, itemCount ,"")
        }

        override fun onItemRangeInserted(newItems: ObservableArrayList<*>, positionStart: Int, itemCount: Int) {
            notifyItemRangeInserted(positionStart, itemCount)
        }

        override fun onItemRangeMoved(newItems: ObservableArrayList<*>, fromPosition: Int, toPosition: Int, itemCount: Int) {
            if (itemCount == 1) {
                notifyItemMoved(fromPosition, toPosition)
            } else {
                notifyDataSetChanged()
            }
        }

        override fun onItemRangeRemoved(newItems: ObservableArrayList<*>, positionStart: Int, itemCount: Int) {
            notifyItemRangeRemoved(positionStart, itemCount)
        }
    }
}
```

##### DataBindingAdapter.kt 当前适配器

```kotlin
package com.lujianfei.other.adapter

import androidx.databinding.ObservableArrayList
import com.lujianfei.other.R
import com.lujianfei.other.adapter.base.BaseBindingAdapter
import com.lujianfei.other.bean.DatabindingItem
import com.lujianfei.other.databinding.OtherAdapterDatabindingBinding

class DataBindingAdapter(items: ObservableArrayList<DatabindingItem>):
    BaseBindingAdapter<DatabindingItem, OtherAdapterDatabindingBinding>
        (items) {

    var itemClickListener: ((resId:Int,bean:DatabindingItem)->Unit) ?= null
    var itemLongClickListener: ((resId:Int,bean:DatabindingItem)->Unit) ?= null

    override fun getLayoutResId() = R.layout.other_adapter_databinding

    override fun onBindItem(
        binding: OtherAdapterDatabindingBinding?,
        bean: DatabindingItem,
        position: Int
    ) {
        binding?.root?.setOnClickListener {
            itemClickListener?.invoke(-1, bean)
        }
        binding?.root?.setOnLongClickListener {
            itemLongClickListener?.invoke(-1, bean)
            true
        }
        binding?.model = bean
    }
}
```

##### BindAdapter.kt

```kotlin
package com.lujianfei.other.adapter

import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.other.bean.DatabindingItem
import com.scwang.smartrefresh.layout.SmartRefreshLayout

@BindingAdapter(value = ["listdata", "itemClick", "itemLongClick"])
fun bindDatabindingAdapter(
    recyclerView: RecyclerView,
    data: ObservableArrayList<DatabindingItem>,
    itemClick: ((resId:Int,bean:DatabindingItem)->Unit) ?= null,
    itemLongClick: ((resId:Int,bean:DatabindingItem)->Unit) ?= null
) {
    if (recyclerView.layoutManager != null && recyclerView.adapter != null) return
    val layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.layoutManager = layoutManager
    val adapter = DataBindingAdapter(data)
    adapter.itemClickListener = itemClick
    adapter.itemLongClickListener = itemLongClick
    recyclerView.adapter = adapter
}

@BindingAdapter(
    value = ["refreshing", "moreLoading", "hasMore"],
    requireAll = false
)
fun bindSmartRefreshLayout(
    smartLayout: SmartRefreshLayout,
    refreshing: Boolean,
    moreLoading: Boolean,
    hasMore: Boolean
) {//状态绑定，控制停止刷新
    if (!refreshing) smartLayout.finishRefresh()
    if (!moreLoading) smartLayout.finishLoadMore()
}
```



##### Bean模型 DatabindingItem.kt

```kotlin
package com.lujianfei.other.bean

data class DatabindingItem(
    val title:String
)
```

##### 

