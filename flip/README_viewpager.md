# ViewPager翻页 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <androidx.viewpager.widget.ViewPager
        android:id="@+id/viewpager"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin5_1

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

   ...
    private var viewpager: ViewPager? = null
    private var adapter:MyPagerAdapter? = null
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        ...
        viewpager = findViewById(R.id.viewpager)
        ...
    }

    override fun initData() {
        ...

        val imgs = arrayListOf(R.drawable.img1,R.drawable.img2,R.drawable.img3)
        val views = arrayListOf<View>()
        for (idx in 0 until imgs.size) {
            val imageView = ImageView(that)
            imageView.setImageDrawable(getPluginDrawable(imgs[idx]))
            views.add(imageView)
        }
        adapter = MyPagerAdapter(views)
        viewpager?.adapter = adapter
    }

   ...
}

```

##### MyPagerAdapter.kt

```kotlin
package com.lujianfei.plugin5_1

import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import java.util.*


class MyPagerAdapter(private val viewLists: ArrayList<View>) : PagerAdapter() {
    override fun getCount(): Int {
        return viewLists.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        container.addView(viewLists[position])
        return viewLists[position]
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(viewLists[position])
    }
}
```

