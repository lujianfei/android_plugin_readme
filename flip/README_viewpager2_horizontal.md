# ViewPager2 水平翻页 代码展示

ViewPager2 是谷歌官方出的新组件，对 ViewPager 用 RecyclerView 进行重写，使之具备了内存回收的机制，对内存优化起到非常大的作用

以下只展示关键部分

####  build.gradle 加入以下依赖

```javascript
dependencies {
  ...
    // viewpager2
    implementation 'androidx.viewpager2:viewpager2:1.0.0'   
  ...
}
```



##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <androidx.viewpager2.widget.ViewPager2
        android:id="@+id/viewpager"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin5_2

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager2.widget.ViewPager2
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    ...
    private var viewpager: ViewPager2? = null
    private var adapter:MyPagerAdapter? = null
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
       ...
        viewpager = findViewById(R.id.viewpager)
       ...
    }

    override fun initData() {
       ...
        val imgs = arrayListOf(R.drawable.img1,R.drawable.img2,R.drawable.img3)
        adapter = MyPagerAdapter()
        adapter?.setData(imgs)
        viewpager?.adapter = adapter
    }
    ...   
}

```

##### MyPagerAdapter.kt

```kotlin
package com.lujianfei.plugin5_2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView

class MyPagerAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mData = arrayListOf<Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_page,parent,false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(data:List<Int>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var img:ImageView ?= null
        init {
           img = itemView.findViewById(R.id.img)
        }
        fun setData(resId: Int) {
            img?.setImageDrawable(itemView.resources.getDrawable(resId))
        }
    }
}
```

#### adapter_page.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <ImageView
        android:id="@+id/img"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</RelativeLayout>
```

