# android Banner 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...    
    // youth banner
    implementation  'com.youth.banner:banner:2.0.10'
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
       
    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content">
        <com.youth.banner.Banner
            android:id="@+id/myBanner"
            android:layout_width="match_parent"
            android:layout_height="match_parent"/>
        <TextView
            android:id="@+id/pageInfo"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_alignTop="@id/myBanner"
            android:layout_alignParentEnd="true"
            android:layout_marginEnd="5dp"
            android:textColor="#fff"
            android:textSize="20sp"
            android:background="#ff000000"
            />
    </RelativeLayout>
</LinearLayout>
```

##### adapter_banner.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="#000000">
</RelativeLayout>
```



##### MainActivity.kt

```kotlin
package com.lujianfei.plugin5_4

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.TextView
import com.lujianfei.module_plugin_base.Constant
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin5_4.adapter.MyBannerAdapter
import com.youth.banner.Banner
import com.youth.banner.listener.OnPageChangeListener


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
   
    var pageInfo: TextView? = null
    /**
     * Banner 组件
     */
    var myBanner: Banner<String, MyBannerAdapter>?= null
    /**
     * Banner Adapter
     */
    private val mAdapter by lazy { MyBannerAdapter() }

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {      
        pageInfo = findViewById(R.id.pageInfo)
        myBanner = findViewById(R.id.myBanner)
        myBanner?.adapter = mAdapter
    }

    override fun initData() {        
        val remoteUrls = arrayListOf(
            "${Constant.Http.baseUrl}/json/mnb/img/img1.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img2.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img3.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img4.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img5.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img6.jpeg",
            "${Constant.Http.baseUrl}/json/mnb/img/img7.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img8.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img9.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img10.jpg",
        )
        myBanner?.addOnPageChangeListener(
            object : OnPageChangeListener {
                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    LogUtils.d("MainAdapter", "onPageSelected position:$position")
                    getPageInfo(position,remoteUrls.size)
                }

                override fun onPageScrollStateChanged(state: Int) {
                }
            }
        )
        getPageInfo(position = 0, totalSize = remoteUrls.size)
        myBanner?.setDatas(remoteUrls)
    }

    fun getPageInfo(position:Int, totalSize:Int) {
        pageInfo?.text = "${position + 1}/${totalSize}"
    }    
}

```

##### MyBannerAdapter.kt

```kotlin
package com.lujianfei.plugin5_4.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.plugin5_4.R
import com.youth.banner.adapter.BannerAdapter

class MyBannerAdapter: BannerAdapter<String, MyBannerViewHolder>(null) {

    override fun onCreateHolder(parent: ViewGroup?, viewType: Int): MyBannerViewHolder {
        return MyBannerViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.adapter_banner,parent,false))
    }

    override fun onBindView(
        holder: MyBannerViewHolder?,
        data: String?,
        position: Int,
        size: Int
    ) {
        holder?.bindData(data)
    }
}

class MyBannerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var bannerImage: ImageView?= null

    init {
        bannerImage = ImageView(itemView.context)
        bannerImage?.scaleType = ImageView.ScaleType.CENTER_CROP
        // Banner 写死高度
        bannerImage?.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,DensityUtils.dip2px(300f))
        (itemView as ViewGroup).addView(bannerImage)
    }

    fun bindData(image:String?) {
        if (image == null) return
        bannerImage?.let {
            // 加载网络图片
            Glide.with(itemView.context).load(image).into(it)
        }
    }
}
```

##### MainBean.kt

```kotlin
package com.lujianfei.plugin1_12.bean

data class MainBean(
        var title: String,
        var summary: String,
        var imgList:List<String>
)
```

