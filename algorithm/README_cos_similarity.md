# 余弦相似度 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

   ...
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:text="字符串 A:"/>
        <EditText
            android:id="@+id/edit_text_a"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textColorHint="#999999"
            android:hint="例如：我是中国人"
            android:singleLine="true"
            android:background="#00000000"
            />
        <View
            android:id="@+id/underline1"
            android:layout_width="match_parent"
            android:layout_height="1dp"
            android:layout_marginTop="1dp"
            tools:background="@color/theme_color"/>
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:layout_marginTop="10dp"
            android:text="字符串 B:"/>
        <EditText
            android:id="@+id/edit_text_b"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textColorHint="#999999"
            android:singleLine="true"
            android:hint="例如：中国人"
            android:background="#00000000"
            />
        <View
            android:id="@+id/underline2"
            android:layout_width="match_parent"
            android:layout_height="1dp"
            android:layout_marginTop="1dp"
            tools:background="@color/theme_color"
            />
        <TextView
            android:id="@+id/txtSimilarity"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:layout_marginTop="5dp"
            android:text="相似度："
            />
        <Button
            android:id="@+id/btCalc"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:text="计算相似度"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin7_2

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

   ...
    private var edit_text_a: EditText? = null
    private var edit_text_b: EditText? = null
    private var underline1: View? = null
    private var underline2: View? = null
    private var txtSimilarity: TextView? = null
    private var btCalc: View? = null
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        btCalc = findViewById(R.id.btCalc)
        txtSimilarity = findViewById(R.id.txtSimilarity)
        underline1 = findViewById(R.id.underline1)
        underline2 = findViewById(R.id.underline2)
        edit_text_a = findViewById(R.id.edit_text_a)
        edit_text_b = findViewById(R.id.edit_text_b)
        ...
    }

    override fun initData() {
        bean = that?.intent?.getParcelableExtra("data")
    }

    override fun initEvent() {
       ...
        btCalc?.setOnClickListener { 
            calcClick()
        }
    }

    private fun calcClick() {
        edit_text_a?.let {edit_text_a->
            edit_text_b?.let {edit_text_b->
                if (edit_text_a.text.isEmpty()) {
                    edit_text_a.error = "字符串 A 不能为空"
                    return
                }
                if (edit_text_b.text.isEmpty()) {
                    edit_text_b.error = "字符串 B 不能为空"
                    return
                }
                val compare = MathHelper.compare(edit_text_a.text.toString(), edit_text_b.text.toString())
                txtSimilarity?.text = "相似度：$compare"
            }
        }
    }

    ...
}

```

##### MathHelper.kt

```kotlin
package com.lujianfei.plugin7_2

import kotlin.math.sqrt

/**
 *@date     创建时间:2020/6/9
 *@name     作者:陆键霏
 *@describe 描述:
 */
object MathHelper {

    /**
     * 余弦相似度比较
     */
    fun compare(source: String, target: String):Double {
        // 构建词频向量
        val buildMap = buildMap(source, target)
        // 计算相似度
        return pointMulti(buildMap) / sqrt(squaresSum(buildMap))
    }
    /**
     * 构建词频向量
     */
    private fun buildMap(source: String, target: String):Map<Char, IntArray> {
        val vectorMap = mutableMapOf<Char, IntArray>()
        // 构建 char 为 key 的 map, value 的 size 为 2 的 Int 数组，
        // value 用于存放每个 char 分别在 source 和 target 中出现的个数 
        for (sch in source.toCharArray()) {
            if (vectorMap.containsKey(sch)) {
                vectorMap[sch]?.let { it ->
                    it[0]++
                }
            } else {
                val vector = IntArray(2)
                vector[0] = 1
                vector[1] = 0
                vectorMap[sch] = vector
            }
        }
        for (tch in target.toCharArray()) {
            if (vectorMap.containsKey(tch)) {
                vectorMap[tch]?.let { it ->
                    it[1]++
                }
            } else {
                val vector = IntArray(2)
                vector[0] = 0
                vector[1] = 1
                vectorMap[tch] = vector
            }
        }
        return vectorMap
    }

    /**
     *求点积
     */
    private fun pointMulti(paramMap: Map<Char, IntArray>): Double {
        var sumXY = 0.0
        val keySet = paramMap.keys
        for (character in keySet) {
            val temp = paramMap[character]
            val xi = temp!![0]
            val yi = temp[1]
            sumXY += (xi * yi).toDouble()
        }
        return sumXY
    }
    /**
     *求平方和
     */
    private fun squaresSum(paramMap: Map<Char, IntArray>): Double {
        var sumX = 0.0 // 向量 X 的和
        var sumY = 0.0 // 向量 Y 的和
        val keySet = paramMap.keys
        for (character in keySet) {
            val temp = paramMap[character]
            val xi = temp!![0]
            val yi = temp[1]
            sumX += (xi * xi).toDouble()
            sumY += (yi * yi).toDouble()
        }
        return sumX * sumY
    }
}
```

