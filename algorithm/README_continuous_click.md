# 连续点击事件 代码展示

以下只展示关键部分 (PluginButton 可改为 Button)

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    ...

    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/bt_clickme"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="20dp"
        android:layout_marginStart="5dp"
        android:layout_marginEnd="5dp"
        android:text="连续点击4次"/>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin7_6

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.Toast
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin7_6.utils.ClickHelper


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    private var toolbar: PluginToolBar? = null
    private var bean: PluginActivityBean? = null
    private var bt_clickme:Button ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        toolbar = findViewById(R.id.toolbar)
        bt_clickme = findViewById(R.id.bt_clickme)
        toolbar?.setTitle(getPluginString(R.string.app_name))
    }  

    override fun initEvent() {       
        bt_clickme?.setOnClickListener { 
            ClickHelper.INSTANCE.continuousClick {
                Toast.makeText(that, "连续点击4次", Toast.LENGTH_SHORT).show()
            }
        }
    }   
}

```

##### 核心类 ClickHelper.kt

```kotlin
package com.lujianfei.plugin7_6.utils

/**
 *@date     创建时间:2020/11/4
 *@name     作者:陆键霏
 *@describe 描述:
 */
class ClickHelper private constructor() {

    companion object {
        private const val COUNTS = 4 // 点击次数
        private const val DURATION: Long = 1000 // 规定有效时间
        var INSTANCE = ClickHelper()
    }
    
    private var mHits = LongArray(COUNTS)

    fun continuousClick(callback: (() -> Unit)) {
        //每次点击时，数组向前移动一位
        System.arraycopy(mHits, 1, mHits, 0, mHits.size - 1)
        //为数组最后一位赋值
        mHits[mHits.size - 1] = System.currentTimeMillis()
        if (mHits[0] >= System.currentTimeMillis() - DURATION) {
            callback.invoke()
            mHits = LongArray(COUNTS) //重新初始化数组
        }
    }
}
```

