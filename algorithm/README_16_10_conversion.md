# 十六进制和十进制互转 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

   ...

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:text="十六进制:"/>
        <EditText
            android:id="@+id/edit_hex"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColorHint="#999999"
            android:hint="例如：0102a1"
            android:singleLine="true"
            />

        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:layout_marginTop="10dp"
            android:text="十进制:"/>
        <EditText
            android:id="@+id/edit_decimal"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColorHint="#333333"
            android:inputType="number"
            android:singleLine="true"
            />
        <Button
            android:id="@+id/btHex2Decimal"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="十六进制转十进制"/>
        <Button
            android:id="@+id/btDecimal2Hex"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="十进制转十六进制"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin7_1

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    ...
    private var btHex2Decimal: View? = null
    private var btDecimal2Hex: View? = null
    private var edit_hex: EditText? = null
    private var edit_decimal: EditText? = null
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        btHex2Decimal = findViewById(R.id.btHex2Decimal)
        btDecimal2Hex = findViewById(R.id.btDecimal2Hex)
        edit_hex = findViewById(R.id.edit_hex)
        edit_decimal = findViewById(R.id.edit_decimal)
       ...
    }

    ...

    override fun initEvent() {
       ...
        btHex2Decimal?.setOnClickListener {
            hex2Decimal()
        }
        btDecimal2Hex?.setOnClickListener { 
            decimal2Hex()
        }
    }

    private fun decimal2Hex() {
        edit_decimal?.let {edit->
            edit.text?.let {text->
                if (text.isEmpty()) {
                    Toast.makeText(this, "请输入十进制", Toast.LENGTH_SHORT).show()
                    return
                }
                val hex = text.toString().toInt().toString(16)
                edit_hex?.setText(hex)
            }
        }
    }

    private fun hex2Decimal() {
        edit_hex?.let {edit->
            edit.text?.let {text->
                if (text.isEmpty()) {
                    Toast.makeText(this, "请输入十六进制", Toast.LENGTH_SHORT).show()
                    return
                }
                if (text.length % 2 != 0) {
                    Toast.makeText(this, "十六进制数据格式有误", Toast.LENGTH_SHORT).show()
                    return
                }
                // 模拟实际场景，接收 bytes 数组, 如 0a => 0x0a, 0b => 0x0b, ff => 0xff
                val bytes = ConversionTools.hexStringToBytes(text.toString()) 
                // 将 bytes 数组 转 十六进制数字字符, 如 0x0a => a, 0x0b => b, 0xff => ff
                val hexString = ConversionTools.getHexString(bytes) 
                // 将十六进制字符串转十进制数字
                val decimal = ConversionTools.getDecimalByHexString(hexString) 
                edit_decimal?.setText(decimal.toString())
            }
        }
    }
	...
}

```

##### ConversionTools.kt

```kotlin
package com.lujianfei.plugin7_1

/**
 *@date     创建时间:2020/6/8
 *@name     作者:陆键霏
 *@describe 描述:
 */
object ConversionTools {

    /**
     * 十六进制字符串 转 bytes 数组, 如 0a => 0x0a, 0b => 0x0b, ff => 0xff
     */
    fun hexStringToBytes(hexString: String):ByteArray {
        val destByte = ByteArray(hexString.length / 2)
        var j = 0
        for (i in destByte.indices) {
            val high = (Character.digit(hexString[j], 16) and 0xff).toByte() // 获取字节高位
            val low = (Character.digit(hexString[j + 1], 16) and 0xff).toByte() // 获取字节低位
            destByte[i] = (high.toInt() shl 4 or low.toInt()).toByte() // 高位左移4位 和 低位 做 或 运算
            j += 2
        }
        return destByte
    }

    /**
     * bytes 数组 转十六进制字符串
     */
    fun getHexString(bytes: ByteArray): String {
        val sb = StringBuilder()
        bytes.forEach {
            sb.append((it.toInt() and 0xff).toString(16))
        }
        return sb.toString()
    }

    /**
     * 通过 十六进制 转 十进制
     */
    fun getDecimalByHexString(hexString: String):Int {
        var sum = 0
        hexString.forEach {
            sum = sum * 16 + charToDecimal(it)
        }
        return sum
    }
    /**
     * 将 十六进制的 char 转为数字，
     * 如: A => 10, B => 11 等等
     */
    private fun charToDecimal(c: Char): Int {  
        return when (c) {
            in 'A'..'F' -> {
                10 + c.toInt() - 'A'.toInt()
            }
            in 'a'..'f' -> {
                10 + c.toInt() - 'a'.toInt()
            }
            else -> c - '0'
        }
    }
}
```

