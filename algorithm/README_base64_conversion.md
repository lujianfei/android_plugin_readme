# Base64 编解码 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

   

   ...

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:text="原字符串:"/>
        <EditText
            android:id="@+id/edit_text_a"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textColorHint="#999999"
            android:hint="例如：12345"
            android:singleLine="true"
            android:background="#00000000"
            />
        <View
            android:id="@+id/underline1"
            android:layout_width="match_parent"
            android:layout_height="1dp"
            android:layout_marginTop="1dp"
            tools:background="@color/theme_color"/>
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:layout_marginTop="10dp"
            android:text="Base64 编码:"/>
        <EditText
            android:id="@+id/edit_text_b"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textColorHint="#999999"
            android:singleLine="true"
            android:hint="例如：MTIzNDU="
            android:background="#00000000"
            />
        <View
            android:id="@+id/underline2"
            android:layout_width="match_parent"
            android:layout_height="1dp"
            android:layout_marginTop="1dp"
            tools:background="@color/theme_color"
            />
        <Button
            android:id="@+id/btEncode"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:text="Base64 编码"/>

        <Button
            android:id="@+id/btDecode"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:text="Base64 解码"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin7_3

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Base64
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import java.util.*


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    ...
    private var edit_text_a: EditText? = null
    private var edit_text_b: EditText? = null
    private var underline1: View? = null
    private var underline2: View? = null
    private var btDecode: View? = null
    private var btEncode: View? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        btEncode = findViewById(R.id.btEncode)
        btDecode = findViewById(R.id.btDecode)
        underline1 = findViewById(R.id.underline1)
        underline2 = findViewById(R.id.underline2)
        edit_text_a = findViewById(R.id.edit_text_a)
        edit_text_b = findViewById(R.id.edit_text_b)
        ...
    }

   ...

    override fun initEvent() {
        ...
        btEncode?.setOnClickListener {
            encodeClick()
        }
        btDecode?.setOnClickListener {
            decodeClick()
        }
    }

    private fun encodeClick() {
        edit_text_a?.let { edittext ->
            if (edittext.text.isEmpty()) {
                edittext.error = "原字符串不能为空"
                return
            }
            edittext.text?.let {text->
                edit_text_b?.setText(MathHelper.base64Encode(text.toString()))    
            }
        }
    }

    private fun decodeClick() {
        edit_text_b?.let { edittext ->
            if (edittext.text.isEmpty()) {
                edittext.error = "Base64 编码不能为空"
                return
            }
            edittext.text?.let {text->
                kotlin.runCatching {
                    edit_text_a?.setText(MathHelper.base64Decode(text.toString()))
                }.onFailure {
                    edittext.error = "Base64 编码格式有误"    
                }
            }
        }
    }

    ...
}

```

##### MathHelper.kt

```kotlin
package com.lujianfei.plugin7_3

import android.util.Base64

/**
 *@date     创建时间:2020/6/9
 *@name     作者:陆键霏
 *@describe 描述:
 */
object MathHelper {

    /**
     * Base64 编码
     */
    fun base64Encode(str:String):String {
        val encode = Base64.encode(str.toByteArray(), Base64.DEFAULT)
        return String(encode)
    }

    /**
     * Base64 解码
     */
    fun base64Decode(str:String):String {
        val encode = Base64.decode(str.toByteArray(), Base64.DEFAULT)
        return String(encode)
    }
}
```

