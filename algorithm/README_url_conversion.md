# URL 编解码 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    ...

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:text="原字符串:"/>
        <EditText
            android:id="@+id/edit_text_a"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textColorHint="#999999"
            android:hint="例如：我是中国人"
            android:singleLine="true"
            android:background="#00000000"
            />
        <View
            android:id="@+id/underline1"
            android:layout_width="match_parent"
            android:layout_height="1dp"
            android:layout_marginTop="1dp"
            tools:background="@color/theme_color"/>
        <TextView
            android:id="@+id/txtEncode"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:layout_marginTop="10dp"
            android:text="UTF-8 编码:"/>
        <EditText
            android:id="@+id/edit_text_b"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textColorHint="#999999"
            android:singleLine="true"
            android:background="#00000000"
            />
        <View
            android:id="@+id/underline2"
            android:layout_width="match_parent"
            android:layout_height="1dp"
            android:layout_marginTop="1dp"
            tools:background="@color/theme_color"
            />
        <RadioGroup
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="#bbbbbb"
            android:orientation="horizontal">
            <RadioButton
                android:id="@+id/radioUtf8"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_weight="1"
                android:text="UTF-8"/>
            <RadioButton
                android:id="@+id/radioGb2312"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_weight="1"
                android:text="GB2312"/>
        </RadioGroup>
        <Button
            android:id="@+id/btEncode"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textAllCaps="false"
            android:text="中文转 UTF-8"/>

        <Button
            android:id="@+id/btDecode"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:textAllCaps="false"
            android:text="UTF-8 转中文"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin7_5

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

   ...
    private var edit_text_a: EditText? = null
    private var edit_text_b: EditText? = null
    private var underline1: View? = null
    private var underline2: View? = null
    private var btDecode: Button? = null
    private var btEncode: Button? = null
    private var radioUtf8: RadioButton? = null
    private var radioGb2312: RadioButton? = null
    private var txtEncode: TextView? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        txtEncode = findViewById(R.id.txtEncode)
        radioUtf8 = findViewById(R.id.radioUtf8)
        radioGb2312 = findViewById(R.id.radioGb2312)
        btEncode = findViewById(R.id.btEncode)
        btDecode = findViewById(R.id.btDecode)
        underline1 = findViewById(R.id.underline1)
        underline2 = findViewById(R.id.underline2)
        edit_text_a = findViewById(R.id.edit_text_a)
        edit_text_b = findViewById(R.id.edit_text_b)
        ...
    }

   ...

    override fun initEvent() {
       ...
        btEncode?.setOnClickListener {
            encodeClick()
        }
        btDecode?.setOnClickListener {
            decodeClick()
        }
        radioUtf8?.setOnClickListener {
            txtEncode?.text = "UTF-8 编码:"
            btDecode?.text = "UTF-8 转中文"
            btEncode?.text = "中文转 UTF-8"
        }
        radioGb2312?.setOnClickListener {
            txtEncode?.text = "GB2312 编码:"
            btDecode?.text = "GB2312 转中文"
            btEncode?.text = "中文转 GB2312"
        }
        radioUtf8?.performClick()
    }

    private fun encodeClick() {
        edit_text_a?.let { edittext ->
            if (edittext.text.isEmpty()) {
                edittext.error = "中文不能为空"
                return
            }
            edittext.text?.let {text->
                val utf8Checked = radioUtf8?.isChecked
                if (utf8Checked == true) {
                    edit_text_b?.setText(StringHelper.encodeUtf8(text.toString()))       
                } else {
                    edit_text_b?.setText(StringHelper.encodeGb2312(text.toString()))
                }
            }
        }
    }

    private fun decodeClick() {
        edit_text_b?.let { edittext ->
            if (edittext.text.isEmpty()) {
                edittext.error = "Unicode 编码不能为空"
                return
            }
            edittext.text?.let {text->
                kotlin.runCatching {
                    val utf8Checked = radioUtf8?.isChecked
                    if (utf8Checked == true) {
                        edit_text_a?.setText(StringHelper.decodeUtf8(text.toString()))
                    } else {
                        edit_text_a?.setText(StringHelper.decodeGb2312(text.toString()))
                    }
                }.onFailure {
                    val utf8Checked = radioUtf8?.isChecked
                    if (utf8Checked == true) {
                        edittext.error = "UTF-8 编码格式有误"
                    } else {
                        edittext.error = "GB2312 编码格式有误"
                    }
                }
            }
        }
    }

    ...
}

```

##### StringHelper.kt

```kotlin
package com.lujianfei.plugin7_5

import java.net.URLDecoder
import java.net.URLEncoder

/**
 *@date     创建时间:2020/6/9
 *@name     作者:陆键霏
 *@describe 描述:
 */
object StringHelper {

    /**
     * 中文转 utf8 
     */
    fun encodeUtf8(str:String):String {
        return URLEncoder.encode(str,"utf8")
    }
    /**
     * 中文转 gb2312
     */
    fun encodeGb2312(str:String):String {
        return URLEncoder.encode(str,"gb2312")
    }

    /**
     * utf8 转 中文
     */
    fun decodeUtf8(str:String):String {
        return URLDecoder.decode(str,"utf8")
    }
    /**
     * gb2312 转 中文
     */
    fun decodeGb2312(str:String):String {
        return URLDecoder.decode(str,"gb2312")
    }
}
```

