# android mnb 代码展示

#### 列表类
1. [垂直 RecyclerView](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_vertical_recyclerview.md)
2. [多列 RecyclerView](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_grid_recyclerview.md)
3. [瀑布流 RecyclerView](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_stagge_recyclerview.md)
4. [聊天界面列表](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_chat_list.md)
5. [索引列表](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_index_recyclerview.md)
6. [吸附标题列表](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_sticky_item.md)
7. [左滑删除列表](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_swipe_delete_item.md)
8. [下拉刷新列表](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_refresh_recyclerview.md)
9. [自定义下拉列表](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_my_spinner.md)
10. [Vlayout用法展示](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_vlayout.md)
11. [RecyclerView滚动修改透明度](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_onscroll_recyclerview.md)
12. [RecyclerView嵌套Banner列表](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_recyclerview_banner.md)
13. [Banner画廊效果](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_gallery_effect.md)
14. [双击定位未读](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_unread_locate_list.md)
15. [Paging3 实现分页](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_paging3.md)

16. [可拖拽 RecyclerView](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_drag_recyclerview.md)

17. [键盘切换抖动优化](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_keyboard_switch.md)
18. [仿微信下拉小程序效果](https://gitee.com/lujianfei/android_plugin_readme/blob/master/list/README_wechat_list.md)


#### 图表
- [MPAndroidChart 线图](https://gitee.com/lujianfei/android_plugin_readme/blob/master/chart/README_linechart.md) 

- [MPAndroidChart 条形图](https://gitee.com/lujianfei/android_plugin_readme/blob/master/chart/README_barchart.md) 

- [MPAndroidChart 饼状图](https://gitee.com/lujianfei/android_plugin_readme/blob/master/chart/README_piechart.md) 

- [MPAndroidChart 散点图](https://gitee.com/lujianfei/android_plugin_readme/blob/master/chart/README_bubblechart.md) 


#### 对话框类

- [自定义Dialog](https://gitee.com/lujianfei/android_plugin_readme/blob/master/dialog/README_dialog.md)

#### 按钮和容器类

- [流式布局](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_flowlayout.md)
- [可缩放 ImageView](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_photoview.md)
- [显示 html 图片的 TextView](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_html_textview.md)
- [WebView js 和 Android 相互调用](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_js_android_call.md)
- [图片倒影效果](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_reflect_imageview.md)
- [图像合成处理](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_porterduff_xfermode.md)
- [验证码输入框](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_verification_code.md)
- [带清空输入框](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_delete_edittext.md)
- [RadioGroup实现下划线单选](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_radiogroup_style.md)
- [验证码倒计时TextView](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_countdown_textview.md)
- [圆盘按钮菜单](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_round_menu.md)
- [遮罩滤镜展示](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_maskfilter.md)
- [阴影效果展示](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_shadow_layer.md)
- [锚点定位](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_anchor_location.md)
- [仿 iOS 开关](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_switch_button.md)
- [自定义按钮](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_custom_button.md)
- [ShapeableImageView用法](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_shapeable_imageview.md)

- [自定义TextView展开更多](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_more_textview.md)

- [TextView自动调整字体大小](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_autosize_textview.md)

- [自定义Drawable](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_custom_drawable.md)

- [设置TextView渐变色](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_textview_gradient.md)

- [CollapsingToolbarLayout实现吸顶折叠](https://gitee.com/lujianfei/android_plugin_readme/blob/master/container/README_collapsing_toolbar_layout.md)


#### 动画类

1. [AccelerateDecelerateInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_AccelerateDecelerateInterpolator.md)
2. [AccelerateInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_AccelerateInterpolator.md)
3. [DecelerateInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_DecelerateInterpolator.md)
4. [LinearInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_LinearInterpolator.md)
5. [AnticipateInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_AnticipateInterpolator.md)
6. [AnticipateOvershootInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_AnticipateOvershootInterpolator.md)
7. [OvershootInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_OvershootInterpolator.md)
8. [BounceInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_BounceInterpolator.md)
9. [CycleInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_CycleInterpolator.md)
10. [TimeInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_TimeInterpolator.md)
11. [PathInterpolator 插值器](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_PathInterpolator.md)
12. [TranslateAnimation 的用法](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_TranslateAnimation.md)
13. [RotateAnimation 的用法](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_RotateAnimation.md)
14. [ScaleAnimation 的用法](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_ScaleAnimation.md)
15. [AlphaAnimation 的用法](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_AlphaAnimation.md)
16. [AnimationDrawable 的用法](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_AnimationDrawable.md)
17. [属性动画的用法](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_PropertyAnimation.md)
18. [共享元素动画的用法](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_shared_element.md)
19. [SVGA动画的用法](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_svga_anim.md)
20. [MotionLayout动画](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_motionlayout.md)
21. [共享元素动画（跳转activity）](https://gitee.com/lujianfei/android_plugin_readme/blob/master/animation/README_shared_element_to_activity.md)


#### 翻页类

1. [ViewPager 翻页](https://gitee.com/lujianfei/android_plugin_readme/blob/master/flip/README_viewpager.md)
2. [ViewPager2 水平翻页](https://gitee.com/lujianfei/android_plugin_readme/blob/master/flip/README_viewpager2_horizontal.md)
3. [ViewPager2 纵向翻页](https://gitee.com/lujianfei/android_plugin_readme/blob/master/flip/README_viewpager2_vertical.md)
4. [Banner 翻页](https://gitee.com/lujianfei/android_plugin_readme/blob/master/flip/README_banner.md)


#### 进度条

- [圆形进度条](https://gitee.com/lujianfei/android_plugin_readme/blob/master/figure/README_figure.md)
- [自定义条形进度条](https://gitee.com/lujianfei/android_plugin_readme/blob/master/figure/README_progress.md)
- [自绘条形进度条(继承View实现)](https://gitee.com/lujianfei/android_plugin_readme/blob/master/figure/README_canvas_draw_progress.md)
- [自绘等级进度条(继承View实现)](https://gitee.com/lujianfei/android_plugin_readme/blob/master/figure/README_canvas_draw_level_progress.md)

#### Mvvm 和 Databinding
- [Mvvm + Databinding 实例列表操作](https://gitee.com/lujianfei/android_plugin_readme/blob/master/mvvm/README_list_databinding.md)

- [Mvvm + Databinding 多类型列表操作](https://gitee.com/lujianfei/android_plugin_readme/blob/master/mvvm/README_multi_list_databinding.md)

- [EditText 双向绑定](https://gitee.com/lujianfei/android_plugin_readme/blob/master/mvvm/README_edittext_databinding.md)


#### Compose UI
- [Compose 列表](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_list.md)

- [Compose 布局](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_layout.md)

- [Compose 文本](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_text.md)

- [Compose 图片](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_image.md)

- [Compose 动画](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_animation.md)

- [Compose 手势](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_gesture.md)

- [Compose Canvas](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_canvas.md)

- [Compose 底部Tab导航](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_bottom_tab_layout.md)

- [Compose 自定义布局](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_custom_layout.md)

- [Compose MediaPlayer](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_mediaplayer.md)

- [Compose 列表下拉刷新](https://gitee.com/lujianfei/android_plugin_readme/blob/master/compose/README_compose_list_refresh.md)



#### 编码及算法
1.[十六进制和十进制互转](https://gitee.com/lujianfei/android_plugin_readme/blob/master/algorithm/README_16_10_conversion.md)

2.[余弦相似度](https://gitee.com/lujianfei/android_plugin_readme/blob/master/algorithm/README_cos_similarity.md)

3.[Base64 编解码](https://gitee.com/lujianfei/android_plugin_readme/blob/master/algorithm/README_base64_conversion.md)

4.[UTF-8 编解码](https://gitee.com/lujianfei/android_plugin_readme/blob/master/algorithm/README_unicode_conversion.md)

5.[URL 编解码](https://gitee.com/lujianfei/android_plugin_readme/blob/master/algorithm/README_url_conversion.md)

6.[连续点击事件](https://gitee.com/lujianfei/android_plugin_readme/blob/master/algorithm/README_continuous_click.md)

#### 多媒体

1.[AudioRecord / MediaPlayer 录音播放](https://gitee.com/lujianfei/android_plugin_readme/blob/master/media/README_media_audio.md)

2.[Camera2 相机拍照](https://gitee.com/lujianfei/android_plugin_readme/blob/master/media/README_camera2.md)

3.[MediaRecorder / MediaPlayer 录像播放](https://gitee.com/lujianfei/android_plugin_readme/blob/master/media/README_media_video.md)

#### 传感器

1.[手机摇一摇](https://gitee.com/lujianfei/android_plugin_readme/blob/master/sensor/README_phone_shake.md)

2.[USB设备列表](https://gitee.com/lujianfei/android_plugin_readme/blob/master/sensor/README_phone_usb_list.md)

3.[位置信息获取](https://gitee.com/lujianfei/android_plugin_readme/blob/master/sensor/README_location.md)

4.[指纹识别](https://gitee.com/lujianfei/android_plugin_readme/blob/master/sensor/README_fingerprint.md)

#### 网络设备

1.[Wifi列表获取](https://gitee.com/lujianfei/android_plugin_readme/blob/master/network/README_wifi_list.md)

2.[蓝牙列表获取](https://gitee.com/lujianfei/android_plugin_readme/blob/master/network/README_bluetooth_list.md)

#### 人工智能

1.[Tensorflow 移植 Android](https://gitee.com/lujianfei/android_plugin_readme/blob/master/ai/README_tensorflow_to_android.md)

2.[梯度下降线性拟合](https://gitee.com/lujianfei/android_plugin_readme/blob/master/ai/README_linear_gradient_descent.md)

3.[梯度下降非线性拟合](https://gitee.com/lujianfei/android_plugin_readme/blob/master/ai/README_nonlinear_gradient_descent.md)

#### 第三方接入

1.[腾讯开屏广告](https://gitee.com/lujianfei/android_plugin_readme/blob/master/thirdparty/README_tx_splash_ad.md)

2.[腾讯激励广告](https://gitee.com/lujianfei/android_plugin_readme/blob/master/thirdparty/README_tx_encourage_ad.md)

3.[腾讯插屏广告](https://gitee.com/lujianfei/android_plugin_readme/blob/master/thirdparty/README_tx_interstitial_ad.md)

4.[腾讯 Banner 广告](https://gitee.com/lujianfei/android_plugin_readme/blob/master/thirdparty/README_tx_banner_ad.md)


#### 实用工具

1.[时间戳转换](https://gitee.com/lujianfei/android_plugin_readme/blob/master/utilities/README_timestamp_conversion.md)

2.[Json 格式化](https://gitee.com/lujianfei/android_plugin_readme/blob/master/utilities/README_json_format.md)

3.[生成二维码](https://gitee.com/lujianfei/android_plugin_readme/blob/master/utilities/README_qrcode_generation.md)

4.[发送短信](https://gitee.com/lujianfei/android_plugin_readme/blob/master/utilities/README_sms.md)

#### 日历

1.[简单样式日历](https://gitee.com/lujianfei/android_plugin_readme/blob/master/calendar/README_simple_calendar.md)

2.[范围选择日历](https://gitee.com/lujianfei/android_plugin_readme/blob/master/calendar/README_range_calendar.md)

3.[多选日历](https://gitee.com/lujianfei/android_plugin_readme/blob/master/calendar/README_multi_calendar.md)



#### 相关基类
```
package com.lujianfei.module_plugin_base.base

import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.lujianfei.module_plugin_base.BaseApplication
import com.lujianfei.module_plugin_base.utils.ResUtils

/**
 *@date     创建时间:2020/5/29
 *@name     作者:陆键霏
 *@describe 描述:
 */
abstract class BasePluginActivity: AppCompatActivity() {

    companion object {
        const val TAG = "BasePluginActivity"   
    }
    
    var that: AppCompatActivity?= null
    var from_host_activity = false
    var thatContentView:View? = null
    // 接收主应用Activity的上下文：setActivity
    fun setActivity(paramActivity: AppCompatActivity) {
        that = paramActivity
    }

    fun doOnCreate(savedInstanceState: Bundle?) {
        onCreate(savedInstanceState)
    }
    
    override fun onCreate(savedInstanceState: Bundle?) {
        if (savedInstanceState != null) {
            from_host_activity = savedInstanceState.getBoolean("KEY_START_FROM_OTHER_ACTIVITY", false)
            if (from_host_activity) {
                Log.d(TAG,"MainActivity onCreate pluginResources = ${that?.resources}")
                thatContentView = LayoutInflater.from(that).inflate(resouceId(),null, false)
                ResUtils.pluginContext = thatContentView?.context
                that?.setContentView(thatContentView)
            }
        }
        if (!from_host_activity) {
            that = this
            super.onCreate(savedInstanceState)
            thatContentView = LayoutInflater.from(that).inflate(resouceId(),null, false)
            ResUtils.pluginContext = thatContentView?.context
            setContentView(thatContentView)
        }
    
        
        initView()
        initData()
        initEvent()
    }

    abstract fun resouceId():Int
    abstract fun initView()
    abstract fun initData()
    abstract fun initEvent()


    override fun startActivity(intent: Intent?) {
        if (from_host_activity) {
            when(intent?.action) {
                Intent.ACTION_VIEW-> {
                    (BaseApplication.INSTANCE as BaseApplication).openWebView(title = "", url = intent.data?.toString()?:"")
                }
                else-> {
                    that?.startActivity(intent)
                }
            }
        } else {
            super.startActivity(intent)
        }
    }

    override fun <T : View?> findViewById(id: Int): T {
        val findViewById = thatContentView?.findViewById<T>(id)
        findViewById?.let { 
            return it
        }
        return super.findViewById<T>(id)
    }

    override fun getWindow(): Window {
        if (from_host_activity) {
            that?.window?.let {
                return it
            }
        }
        return super.getWindow()
    }

    fun getPluginString(resId:Int):String? {
        return thatContentView?.context?.getString(resId)
    }

    fun getPluginColor(resId:Int):Int? {
        return thatContentView?.context?.resources?.getColor(resId)
    }

    fun getPluginDrawable(resId:Int):Drawable? {
        return thatContentView?.context?.resources?.getDrawable(resId)
    }

    fun getPluginColorStateList(resId:Int): ColorStateList? {
        return thatContentView?.context?.resources?.getColorStateList(resId)
    }

    override fun finish() {
        if (from_host_activity) {
            that?.finish()
        } else {
            super.finish()
        }
    }

    override fun onBackPressed() {
        that?.finish()
    }

    open fun onPluginResume() {
    }

    open fun onPluginPause() {
    }

    open fun onPluginDestroy() {
    }

    open fun onPluginNewIntent(intent: Intent?) {
        
    }
}
```