# android 获取蓝牙列表 代码展示

以下只展示关键部分

#### 必要的权限

```xml
 <!--蓝牙依赖权限-->
<uses-permission android:name="android.permission.BLUETOOTH"/>
<uses-permission android:name="android.permission.BLUETOOTH_ADMIN"/>
<!--也会用到的权限-->
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
```



#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
	...
}
```

##### fragment_main.xml （Wifi 列表界面）

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">
    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/bt_scan"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_margin="10dp"
        android:text="开始扫描"/>
    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_below="@id/bt_scan"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        />
    <com.lujianfei.plugin11_2.widget.EmptyView
        android:id="@+id/emptyview"
        android:layout_below="@id/bt_scan"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_centerInParent="true"
        android:visibility="gone"/>
</RelativeLayout>
```

##### MainFragment.xml （Wifi 列表界面）

```kotlin
package com.lujianfei.plugin11_2.fragment

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.provider.Settings
import android.view.Gravity
import android.view.View
import android.widget.Button
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.plugin11_2.adapter.BluetoothListAdapter
import com.lujianfei.plugin11_2.base.BaseFragment
import com.lujianfei.plugin11_2.bean.BluetoothInfoBean
import com.lujianfei.plugin11_2.contract.MainContract
import com.lujianfei.plugin11_2.popupwindow.DetailPopupWindow
import com.lujianfei.plugin11_2.presenter.MainPresenter
import com.lujianfei.plugin11_2.widget.EmptyView
import com.lujianfei.plugin11_2.R

/**
 *@date     创建时间:2020/7/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class MainFragment: BaseFragment(), MainContract.View {
    
    companion object {
        const val TAG = "MainFragment"
        const val PERMISSION1 = Manifest.permission.ACCESS_COARSE_LOCATION
        const val PERMISSION2 = Manifest.permission.BLUETOOTH_ADMIN
        const val REQUEST = 101
        const val REQUEST_BLUETOOTH = 102    
    }

    private val mPresenter by lazy { MainPresenter(this) }
    private val mAdapter by lazy { BluetoothListAdapter() }
    private var recyclerview:RecyclerView ?= null
    private var emptyview: EmptyView?= null
    private var bt_scan: Button?= null
    
    
    override fun resourceId(): Int = R.layout.fragment_main

    override fun initView() {
        bt_scan = findViewById(R.id.bt_scan)
        emptyview = findViewById(R.id.emptyview)
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply { 
            adapter = mAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun initData() {
        if (!mPresenter.isBluetoothEnable()) {
            emptyview?.apply {
                visibility = View.VISIBLE
                setText("蓝牙未开启")
                setButtonText("去开启")
                showButton()
                onButtonClickListener = {
                    startActivityForResult(Intent(Settings.ACTION_BLUETOOTH_SETTINGS), REQUEST_BLUETOOTH)
                }
            }
            return
        }
        if (isPermissionGranted()) {
            mPresenter.loadData()
        } else {
            requestPermission()
        }
    }
    
    override fun initEvent() {
        mAdapter.onItemClickListener = {scanResult ->
            context?.let {
                val mWifiDetailPopupWindow = DetailPopupWindow(it)
                mWifiDetailPopupWindow.setBean(scanResult)
                mWifiDetailPopupWindow.showAtLocation(recyclerview, Gravity.CENTER, 0,0)
            }
        }
        bt_scan?.setOnClickListener {
            mPresenter.startScan()
        }
    }

    private fun requestPermission() {
        requestPermissions(arrayOf(PERMISSION1,PERMISSION2),REQUEST)
    }

    private fun isPermissionGranted():Boolean {
        return (ActivityCompat.checkSelfPermission(context,PERMISSION1) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context,PERMISSION2) == PackageManager.PERMISSION_GRANTED)
    }
    
    override fun updateTitle(title: String?) {
    }

    override fun onTitleRightClick(that: Activity?, view: View?) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            REQUEST -> {
                if (isPermissionGranted()) {
                    mPresenter.startScan()
                }
            }
            REQUEST_BLUETOOTH -> {
                if (!mPresenter.isBluetoothEnable()) return

                if (isPermissionGranted()) {
                    mPresenter.startScan()
                } else {
                    requestPermission()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            REQUEST -> {
                var granted = true
                for (result in grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        granted = false
                    }
                }
                if (granted) {
                    mPresenter.loadData()
                    emptyview?.apply {
                        visibility = View.GONE
                        hideButton()
                    }
                } else {
                    emptyview?.apply {
                        visibility = View.VISIBLE
                        setText("蓝牙 权限被取消（获取蓝牙列表依赖该权限）")
                        setButtonText("去开启")
                        showButton()
                        onButtonClickListener = {
                            if (isPermissionGranted()) { // 权限被设置为以后不弹出
                                mPresenter.goSettings()
                            } else { // 权限被禁用，但未设置为以后不弹出
                                requestPermission()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        mPresenter.onDestroy()
        super.onDestroy()
    }

    override fun setData(scanResults: List<BluetoothInfoBean>?) {
        mAdapter.setData(scanResults)

        scanResults?.let { 
            if (it.isNotEmpty()) {
                hideEmptyView()
                showRecyclerview()
            } else {
                hideRecyclerview()
                emptyview?.apply {
                    visibility = View.VISIBLE
                    setText("蓝牙未开启")
                    setButtonText("去开启")
                    showButton()
                    onButtonClickListener = {
                        startActivityForResult(Intent(Settings.ACTION_BLUETOOTH_SETTINGS), REQUEST_BLUETOOTH)
                    }
                }
            }
        }
    }

    override fun hideEmptyView() {
        emptyview?.visibility = View.GONE
    }

    override fun showEmptyView() {
        emptyview?.visibility = View.VISIBLE
    }

    override fun setEmptyText(s: String) {
        emptyview?.setText(s)
    }

    override fun hideEmptyViewButton() {
        emptyview?.hideButton()
    }

    override fun hideRecyclerview() {
        recyclerview?.visibility = View.GONE
    }

    override fun showRecyclerview() {
        recyclerview?.visibility = View.VISIBLE
    }

    override fun setButtonStatus(text: String, enabled: Boolean) {
        bt_scan?.isEnabled = enabled
        bt_scan?.text = text
    }
}
```

#### 用到的实体类

```kotlin
package com.lujianfei.plugin11_2.bean

import android.bluetooth.BluetoothDevice


/**
 *@date     创建时间:2020/9/5
 *@name     作者:陆键霏
 *@describe 描述:
 */
data class BluetoothInfoBean(
        var name: String,
        var scanResult: BluetoothDevice
) 
```



#### 对应的Adapter

```kotlin
package com.lujianfei.plugin11_2.adapter

import android.bluetooth.BluetoothDevice
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.plugin11_2.R
import com.lujianfei.plugin11_2.bean.BluetoothInfoBean

/**
 *@date     创建时间:2020/7/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class BluetoothListAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    
    private val mData = arrayListOf<BluetoothInfoBean>()
    var onItemClickListener:((BluetoothInfoBean)->Unit) ?= null
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_bluetooth_item, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val scanResult = mData[holder.adapterPosition]
        if (holder is ViewHolder) {
            holder.bindData(scanResult)
        }
        holder.itemView.setOnClickListener {
            onItemClickListener?.invoke(scanResult)
        }
    }
    
    fun setData(data:List<BluetoothInfoBean>?) {
        mData.clear()
        data?.let {
            mData.addAll(it)
        }
        notifyDataSetChanged()
    }
    
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        
        var icon:ImageView ?= null
        var title:TextView ?= null
        var status:TextView ?= null
        
        init {
            icon = itemView.findViewById(R.id.icon)
            title = itemView.findViewById(R.id.title)
            status = itemView.findViewById(R.id.status)
        }
        
        fun bindData(bean: BluetoothInfoBean) {
            icon?.setImageDrawable(ResUtils.getPluginDrawable(R.drawable.ic_bluetooth))
            title?.text = bean.name
            when(bean.scanResult.bondState) {
                BluetoothDevice.BOND_NONE -> {
                    status?.text = "未配对"
                    status?.visibility = View.VISIBLE
                }
                BluetoothDevice.BOND_BONDING -> {
                    status?.text = "配对中..."
                    status?.visibility = View.VISIBLE
                    
                }
                BluetoothDevice.BOND_BONDED -> {
                    status?.text = "已配对"
                    status?.visibility = View.VISIBLE
                }
            }
        }
    }
}
```

#### Adapter 的布局

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    xmlns:tools="http://schemas.android.com/tools"
    android:gravity="center_vertical"
    android:orientation="horizontal"
    android:padding="10dp">

    <ImageView
        android:id="@+id/icon"
        android:layout_width="30dp"
        android:layout_height="30dp"
        tools:src="@drawable/ic_bluetooth"/>
    
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">
        <TextView
            android:id="@+id/title"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textSize="25sp"
            android:layout_marginStart="10dp"
            android:text="lujianfei"
            />

        <TextView
            android:id="@+id/status"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textSize="20sp"
            android:layout_marginStart="10dp"
            android:visibility="gone"
            android:text="已连接"
            />
    </LinearLayout>
</LinearLayout>
```



#### 对应的 Presenter

```kotlin
package com.lujianfei.plugin11_2.presenter

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.provider.Settings
import android.util.Log
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.plugin11_2.bean.BluetoothInfoBean
import com.lujianfei.plugin11_2.contract.MainContract
import com.lujianfei.plugin11_2.fragment.MainFragment

/**
 *@date     创建时间:2020/7/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class MainPresenter(view: MainContract.View) : MainContract.Presenter(view) {

    companion object {
        const val TAG = "MainPresenter"   
    }

    private var hasRegister: Boolean = false
    private val receiver = BluetoothChangedReceiver()
    private var scanResults = arrayListOf<BluetoothInfoBean>()
    private val mBluetoothAdapter by lazy { BluetoothAdapter.getDefaultAdapter() }
    
    override fun startScan() {
        if (!hasRegister) {
            val filter = IntentFilter()
            filter.addAction(BluetoothDevice.ACTION_FOUND)
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
            filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)
            ResUtils.pluginContext?.registerReceiver(receiver, filter)
            hasRegister = true
        }
        LogUtils.d(TAG, "startScan")
        mBluetoothAdapter.startDiscovery()
    }

    override fun onDestroy() {
        kotlin.runCatching {
            ResUtils.pluginContext?.unregisterReceiver(receiver)
        }.onFailure { 
            Log.e(TAG, "onFailure $it")
        }
    }

    override fun loadData() {
        scanResults.clear()
        for (device in mBluetoothAdapter.bondedDevices) {
            scanResults.add(BluetoothInfoBean(name = device.name,scanResult = device))
        }
        mView?.setData(scanResults)
    }

    override fun isBluetoothEnable(): Boolean {
        return mBluetoothAdapter != null
    }

    override fun goSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", ResUtils.pluginContext?.packageName, null)
        intent.data = uri
        try {
            mView?.startActivityForResult(intent, MainFragment.REQUEST)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
 
    inner class BluetoothChangedReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context?, intent: Intent?) {
            LogUtils.d(TAG, "BluetoothChangedReceiver onReceive")
            when(intent?.action) {
                BluetoothDevice.ACTION_FOUND -> {
                    LogUtils.d(TAG, "BluetoothChangedReceiver onReceive ACTION_FOUND")
                    val foundDevice = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE) as BluetoothDevice
                    val find = scanResults.find { it.name == foundDevice.name }
                    find?.let {
                        it.scanResult = foundDevice
                        mView?.setData(scanResults)
                    }
                }
                BluetoothAdapter.ACTION_DISCOVERY_STARTED -> {
                    LogUtils.d(TAG, "BluetoothChangedReceiver onReceive ACTION_DISCOVERY_STARTED")
                    mView?.setButtonStatus("正在扫描...", enabled = false)
                }
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                    LogUtils.d(TAG, "BluetoothChangedReceiver onReceive ACTION_DISCOVERY_FINISHED")
                    mView?.setButtonStatus("开始扫描", enabled = true)
                }
            }
        }
    }
}
```

##### 弹出详情对话框

##### DetailPopupWindow.kt

```kotlin
package com.lujianfei.plugin11_2.popupwindow

import android.bluetooth.BluetoothDevice
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.PopupWindow
import android.widget.TextView
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.DrawableHelper
import com.lujianfei.plugin11_2.R
import com.lujianfei.plugin11_2.bean.BluetoothInfoBean
import java.lang.StringBuilder

/**
 *@date     创建时间:2020/11/25
 *@name     作者:陆键霏
 *@describe 描述:
 */
class DetailPopupWindow(context: Context?) : PopupWindow(context) {

    companion object {
        const val TAG = "WifiDetailPopupWindow"
    }
    
    private var txt_title: TextView? = null
    private var bt_iknow: Button? = null
    private var txt_content: TextView? = null
    private var scanResult: BluetoothInfoBean?= null
    
    init {
        contentView = LayoutInflater.from(context).inflate(R.layout.popupwindow_detail, null)
        isOutsideTouchable =false
        setBackgroundDrawable(DrawableHelper.createDrawable(fillColor = 0))
        DensityUtils.getScreenHeight()?.let {getScreenHeight->
            height = getScreenHeight / 2
        }
        initView()
        initEvent()
        initData()
    }

    private fun initData() {
        txt_title?.text = "${scanResult?.name} 的详细信息"
        val bluetoothDevice = scanResult?.scanResult
        val info = StringBuilder()
        info.append("address: ").append(bluetoothDevice?.address).append("\r\n").append("\r\n")
        info.append("type: ").append(getBluetoothType(bluetoothDevice)).append("\r\n").append("\r\n")
        info.append("bondState: ").append(getBluetootBondState(bluetoothDevice))
        txt_content?.text = "$info"
    }

    private fun getBluetootUUIDS(bluetoothDevice: BluetoothDevice?): String {
        val sb = StringBuilder()
        bluetoothDevice?.uuids?.let {
            for (uuid in it) {
                sb.append("$uuid").append("/")
            }
        }
        if (sb.isNotEmpty())
            sb.deleteCharAt(sb.length - 1)
        return sb.toString() 
    }

    private fun getBluetootBondState(bluetoothDevice: BluetoothDevice?): String {
        return when(bluetoothDevice?.bondState) {
            BluetoothDevice.BOND_BONDED -> {
                "${bluetoothDevice.bondState} (已配对)"
            }
            BluetoothDevice.BOND_BONDING -> {
                "${bluetoothDevice.bondState} (配对中...)"
            }
            else-> {
                "${bluetoothDevice?.bondState} 未知状态"
            }
        }
    }

    private fun getBluetoothType(bluetoothDevice: BluetoothDevice?): String {
        if (Build.VERSION.SDK_INT >= 18) {
            return when (bluetoothDevice?.type) {
                BluetoothDevice.DEVICE_TYPE_CLASSIC -> {
                    "${bluetoothDevice.type} (Classic - BR/EDR devices)"
                }
                BluetoothDevice.DEVICE_TYPE_LE -> {
                    "${bluetoothDevice.type} (Low Energy - LE-only)"
                }
                BluetoothDevice.DEVICE_TYPE_DUAL -> {
                    "${bluetoothDevice.type} (Dual Mode - BR/EDR/LE)"
                }
                else ->
                    "${bluetoothDevice?.type} 未知类型"
            }
        } else {
            return ""
        }
    }

    private fun initEvent() {
        bt_iknow?.setOnClickListener { 
            dismiss()
        }
    }

    private fun initView() {
        txt_title = findViewById(R.id.txt_title)
        bt_iknow = findViewById(R.id.bt_iknow)
        txt_content = findViewById(R.id.txt_content)
        contentView?.background = DrawableHelper.createDrawable(fillColor = 0xa1000000.toInt(), roundRadius = DensityUtils.dip2px(10f).toFloat())
    }
    
    private fun <T:View> findViewById(resId:Int) :T {
        return contentView?.findViewById(resId) as T
    }

    fun setBean(scanResult: BluetoothInfoBean) {
        this.scanResult = scanResult
        initData()
    }
}
```

##### 详情对话框布局

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:padding="10dp">

    <TextView
        android:id="@+id/txt_title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:gravity="center"
        android:text="提示"
        android:textStyle="bold"
        android:textSize="20sp"
        android:textColor="#ffffff"
        android:padding="10dp"
        />

    <Button
        android:id="@+id/bt_iknow"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_centerHorizontal="true"
        android:layout_alignParentBottom="true"
        android:text="知道了"
        />
    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_below="@id/txt_title"
        android:layout_marginTop="10dp"
        android:layout_above="@id/bt_iknow">
        <TextView
            android:id="@+id/txt_content"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:textIsSelectable="true"
            android:textColor="#ffffff"
            />
    </ScrollView>
</RelativeLayout>
```
