# android 获取wifi列表 代码展示

以下只展示关键部分

#### 必要的权限

```xml
<!--获取 Wifi 列表-->
<uses-permission android:name="android.permission.CHANGE_WIFI_STATE" />
<uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
```



#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
	...
}
```

##### fragment_main.xml （Wifi 列表界面）

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">
    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        />
    <com.lujianfei.plugin11_1.widget.EmptyView
        android:id="@+id/emptyview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:layout_centerInParent="true"
        android:visibility="gone"/>
</RelativeLayout>
```

##### MainFragment.xml （Wifi 列表界面）

```kotlin
package com.lujianfei.plugin11_1.fragment

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.wifi.WifiManager
import android.provider.Settings
import android.util.Log
import android.view.Gravity
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.plugin11_1.R
import com.lujianfei.plugin11_1.adapter.WifiAdapter
import com.lujianfei.plugin11_1.base.BaseFragment
import com.lujianfei.plugin11_1.bean.WifiInfoBean
import com.lujianfei.plugin11_1.contract.MainContract
import com.lujianfei.plugin11_1.popupwindow.WifiDetailPopupWindow
import com.lujianfei.plugin11_1.presenter.MainPresenter
import com.lujianfei.plugin11_1.widget.EmptyView

/**
 *@date     创建时间:2020/7/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class MainFragment: BaseFragment(), MainContract.View {
    
    companion object {
        const val TAG = "MainFragment"
        const val PERMISSION1 = Manifest.permission.ACCESS_COARSE_LOCATION
        const val PERMISSION2 = Manifest.permission.CHANGE_WIFI_STATE
        const val REQUEST = 101
        const val REQUEST_WIFI = 102    
    }

    private val mPresenter by lazy { MainPresenter(this) }
    private val mAdapter by lazy { WifiAdapter() }
    private var recyclerview:RecyclerView ?= null
    private var emptyview:EmptyView ?= null
    
    override fun resourceId(): Int = R.layout.fragment_main

    override fun initView() {
        emptyview = findViewById(R.id.emptyview)
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply { 
            adapter = mAdapter
            layoutManager = LinearLayoutManager(context)
        }
    }

    override fun initData() {
        if (!isWifiEnable()) {
            emptyview?.apply {
                visibility = View.VISIBLE
                setText("Wifi 未开启")
                setButtonText("去开启")
                showButton()
                onButtonClickListener = {
                    startActivityForResult(Intent(Settings.ACTION_WIFI_SETTINGS),REQUEST_WIFI)
                }
            }
            return
        }
        
        if (isPermissionGranted()) {
            mPresenter.startScan()    
        } else {
            requestPermission()
        }
    }
    
    private fun isPermissionGranted():Boolean {
        return (ActivityCompat.checkSelfPermission(context,PERMISSION1) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context,PERMISSION2) == PackageManager.PERMISSION_GRANTED
                )
    }
    
    private fun requestPermission() {
        requestPermissions(arrayOf(PERMISSION1,PERMISSION2),REQUEST)
    }

    override fun initEvent() {
        mAdapter.onItemClickListener = {scanResult ->
            context?.let {
                val mWifiDetailPopupWindow = WifiDetailPopupWindow(it)
                mWifiDetailPopupWindow.setBean(scanResult)
                mWifiDetailPopupWindow.showAtLocation(recyclerview, Gravity.CENTER, 0,0)
            }
        }
    }
    
    private fun isWifiEnable():Boolean {
        val wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val wifiEnabled = wifiManager.isWifiEnabled
        Log.d(TAG, "isWifiEnable $wifiEnabled")
        return wifiEnabled
    }

    override fun updateTitle(title: String?) {
    }

    override fun onTitleRightClick(that: Activity?, view: View?) {
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(requestCode) {
            REQUEST -> {
                if (isPermissionGranted()) {
                    mPresenter.startScan()
                }
            }
            REQUEST_WIFI -> {
                if (!isWifiEnable()) return

                if (isPermissionGranted()) {
                    mPresenter.startScan()
                } else {
                    requestPermission()
                }
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode) {
            REQUEST-> {
                var granted = true
                for (result in grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        granted = false
                    }
                }
                if (granted) {
                    mPresenter.startScan()
                    emptyview?.apply {
                        visibility = View.GONE
                        hideButton()
                    }
                } else {
                    emptyview?.apply {
                        visibility = View.VISIBLE
                        setText("Wifi 权限被取消（获取Wifi列表依赖该权限）")
                        setButtonText("去开启")
                        showButton()
                        onButtonClickListener = {
                            if (isPermissionGranted()) { // 权限被设置为以后不弹出
                                mPresenter.goSettings()
                            } else { // 权限被禁用，但未设置为以后不弹出
                                requestPermission()
                            }
                        }
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        mPresenter.onDestroy()
        super.onDestroy()
    }

    override fun setData(scanResults: List<WifiInfoBean>?) {
        mAdapter.setData(scanResults)

        scanResults?.let { 
            if (it.isNotEmpty()) {
                hideEmptyView()
                showRecyclerview()
            } else {
                hideRecyclerview()
                emptyview?.apply {
                    visibility = View.VISIBLE
                    setText("Wifi 未开启")
                    setButtonText("去开启")
                    showButton()
                    onButtonClickListener = {
                        startActivityForResult(Intent(Settings.ACTION_WIFI_SETTINGS),REQUEST_WIFI)
                    }
                }
            }
        }
    }

    override fun hideEmptyView() {
        emptyview?.visibility = View.GONE
    }

    override fun showEmptyView() {
        emptyview?.visibility = View.VISIBLE
    }

    override fun setEmptyText(s: String) {
        emptyview?.setText(s)
    }

    override fun hideEmptyViewButton() {
        emptyview?.hideButton()
    }

    override fun hideRecyclerview() {
        recyclerview?.visibility = View.GONE
    }

    override fun showRecyclerview() {
        recyclerview?.visibility = View.VISIBLE
    }
}
```

#### 用到的实体类

```kotlin
package com.lujianfei.plugin11_1.bean

import android.net.wifi.ScanResult


/**
 *@date     创建时间:2020/9/5
 *@name     作者:陆键霏
 *@describe 描述:
 */
data class WifiInfoBean(
        val name: String,
        var scanResult: ScanResult,
        var status: Int = StatusDisconnected // 0 未连接，1 已连接, 2 连接中， 3 断开中
) {
    companion object {
        const val StatusDisconnected = 0
        const val StatusConnected = 1
        const val StatusConnecting = 2
        const val StatusDisconnecting = 3
    }
}
```



#### 对应的Adapter

```kotlin
package com.lujianfei.plugin11_1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.plugin11_1.R
import com.lujianfei.plugin11_1.bean.WifiInfoBean

/**
 *@date     创建时间:2020/7/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class WifiAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    
    private val mData = arrayListOf<WifiInfoBean>()
    var onItemClickListener:((WifiInfoBean)->Unit) ?= null
    
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_wifi_item, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val scanResult = mData[holder.adapterPosition]
        if (holder is ViewHolder) {
            holder.bindData(scanResult)
        }
        holder.itemView.setOnClickListener {
            onItemClickListener?.invoke(scanResult)
        }
    }
    
    fun setData(data:List<WifiInfoBean>?) {
        mData.clear()
        data?.let {
            mData.addAll(it)
        }
        notifyDataSetChanged()
    }
    
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        
        var icon:ImageView ?= null
        var title:TextView ?= null
        var status:TextView ?= null
        
        init {
            icon = itemView.findViewById(R.id.icon)
            title = itemView.findViewById(R.id.title)
            status = itemView.findViewById(R.id.status)
        }
        
        fun bindData(bean: WifiInfoBean) {
            icon?.setImageDrawable(ResUtils.getPluginDrawable(R.drawable.ic_wifi))
            title?.text = bean.name
            status?.visibility = if (bean.status == WifiInfoBean.StatusDisconnected) View.GONE else View.VISIBLE
        }
    }
}
```

#### Adapter 的布局

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    xmlns:tools="http://schemas.android.com/tools"
    android:gravity="center_vertical"
    android:orientation="horizontal"
    android:padding="10dp">

    <ImageView
        android:id="@+id/icon"
        android:layout_width="30dp"
        android:layout_height="30dp"
        tools:src="@drawable/ic_wifi"/>
    
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">
        <TextView
            android:id="@+id/title"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textSize="25sp"
            android:layout_marginStart="10dp"
            android:text="lujianfei"
            />
        <TextView
            android:id="@+id/status"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textSize="20sp"
            android:layout_marginStart="10dp"
            android:visibility="gone"
            android:text="已连接"
            />
    </LinearLayout>
</LinearLayout>
```



#### 对应的 Presenter

```kotlin
package com.lujianfei.plugin11_1.presenter

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri
import android.net.wifi.WifiManager
import android.os.Handler
import android.provider.Settings
import android.util.Log
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.plugin11_1.bean.WifiInfoBean
import com.lujianfei.plugin11_1.callback.NetworkReceiverCallBackManager
import com.lujianfei.plugin11_1.contract.MainContract
import com.lujianfei.plugin11_1.fragment.MainFragment
import com.lujianfei.plugin11_1.receiver.NetworkConnectChangedReceiver
import com.lujianfei.plugin11_1.utils.WifiManagerUtils
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext


/**
 *@date     创建时间:2020/7/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class MainPresenter(view: MainContract.View) : MainContract.Presenter(view), NetworkReceiverCallBackManager.OnNetworkReceiverListener {

    companion object {
        const val TAG = "MainPresenter"   
    }

    private var hasRegister: Boolean = false
    private val receiver = NetworkConnectChangedReceiver()
    private var mWifiManagerUtils: WifiManagerUtils?= null

    private var scanResults :List<WifiInfoBean> ?= null
    private var lastSSID :String ?= ""
    private var lastStatus = WifiInfoBean.StatusDisconnected
    
    override fun startScan() {
        if (!hasRegister) {
            val filter = IntentFilter()
            filter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION)
            filter.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION)
            filter.addAction(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)
            filter.addAction(ConnectivityManager.CONNECTIVITY_ACTION)
            ResUtils.pluginContext?.registerReceiver(receiver, filter)
            hasRegister = true
        }

        if (mWifiManagerUtils == null) {
            ResUtils.pluginContext?.let { context ->
                mWifiManagerUtils = WifiManagerUtils(context)
            }
        }
        NetworkReceiverCallBackManager.INSTANCE.removeListener(this)
        NetworkReceiverCallBackManager.INSTANCE.addListener(this)
        mWifiManagerUtils?.startScan()
    }

    override fun onDestroy() {
        NetworkReceiverCallBackManager.INSTANCE.removeListener(this)
        kotlin.runCatching {
            ResUtils.pluginContext?.unregisterReceiver(receiver)
        }.onFailure { 
            Log.e(TAG, "onFailure $it")
        }
    }

    override fun goSettings() {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri = Uri.fromParts("package", ResUtils.pluginContext?.packageName, null)
        intent.data = uri
        try {
            mView?.startActivityForResult(intent, MainFragment.REQUEST)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun loadData() {
        launchOnUI {
            scanResults = withContext(Dispatchers.IO) {
                val resultBefore = mWifiManagerUtils?.getScanResults()
                resultBefore?.find { it.name == lastSSID }?.status = lastStatus
                resultBefore
            }
            mView?.setData(scanResults)
            scanResults?.let {
                if (it.isNotEmpty()) {
                    mView?.hideEmptyView()
                    mView?.showRecyclerview()
                }
            }
        }
    }

    override fun onScanResultRecieve(result: Boolean) {
    }

    override fun onWifiStateEnable(enabled: Boolean) {
        loadData()
    }

    override fun onNetworkStateChange(state: NetworkInfo.State) {
        val map = mapOf(Pair(NetworkInfo.State.DISCONNECTED, WifiInfoBean.StatusDisconnected),
                Pair(NetworkInfo.State.DISCONNECTING,WifiInfoBean.StatusDisconnecting),
                Pair(NetworkInfo.State.CONNECTING,WifiInfoBean.StatusConnecting),
                Pair(NetworkInfo.State.CONNECTED,WifiInfoBean.StatusConnected)
        )
        lastSSID = mWifiManagerUtils?.getSSID()?.replace("\"","")
        lastStatus = map[state] ?: WifiInfoBean.StatusDisconnected
        when (lastStatus) {
            WifiInfoBean.StatusDisconnected,WifiInfoBean.StatusDisconnecting -> {
                scanResults?.forEach { it.status = WifiInfoBean.StatusDisconnected }
            }
            WifiInfoBean.StatusConnected -> {
                scanResults?.find { it.name == lastSSID }?.status = lastStatus
            }
        }
        mView?.setData(scanResults)
    }
}
```

##### 相关的 Receiver

```kotlin
package com.lujianfei.plugin11_1.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.util.Log
import com.lujianfei.plugin11_1.callback.NetworkReceiverCallBackManager

/**
 *@date     创建时间:2020/7/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class NetworkConnectChangedReceiver : BroadcastReceiver() {

    companion object {
        const val TAG  = "NetworkConnectChangedReceiver"
    }
    
    override fun onReceive(context: Context?, intent: Intent?) {
        when (intent?.action) {
            WifiManager.WIFI_STATE_CHANGED_ACTION -> { // wifi开关变化广播
                Log.d(TAG, "WIFI_STATE_CHANGED_ACTION")
                processWifiStateChange(context, intent)
            }
            WifiManager.SCAN_RESULTS_AVAILABLE_ACTION -> { // 热点扫描结果通知广播
                Log.d(TAG, "SCAN_RESULTS_AVAILABLE_ACTION")
                processWifiScanResultAvailable(context, intent)
            }
            WifiManager.NETWORK_STATE_CHANGED_ACTION -> { // 网络状态变化广播
                Log.d(TAG, "NETWORK_STATE_CHANGED_ACTION")
                processNetworkStateChange(context, intent)
            }
        }
    }

    private fun processWifiScanResultAvailable(context: Context?, intent: Intent) {
        val isScanned = intent.getBooleanExtra(WifiManager.EXTRA_RESULTS_UPDATED, true)
        Log.d(TAG, "processWifiScanResultAvailable isScanned = $isScanned")
        if (isScanned) {
            
        }
    }

    private fun processNetworkStateChange(context: Context?, intent: Intent) {
        val networkInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO) as NetworkInfo
        NetworkReceiverCallBackManager.INSTANCE.notifyNetworkStateChange(state = networkInfo.state)
        when(networkInfo.state) {
            NetworkInfo.State.DISCONNECTED-> {
                Log.d(TAG, "processNetworkStateChange disconnected")
            }
            NetworkInfo.State.CONNECTED-> {
                Log.d(TAG, "processNetworkStateChange connected networkInfo = ${networkInfo.extraInfo}")
            }
            NetworkInfo.State.CONNECTING-> {
                Log.d(TAG, "processNetworkStateChange connecting")
            }
        }
    }

    private fun processWifiStateChange(context: Context?, intent: Intent) {
        val wifistate = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE,
                WifiManager.WIFI_STATE_DISABLED)
        when(wifistate) {
            WifiManager.WIFI_STATE_DISABLED -> {
                Log.d(TAG, "WIFI_STATE_DISABLED")
                NetworkReceiverCallBackManager.INSTANCE.notifyWifiStateEnable(false)
            }
            WifiManager.WIFI_STATE_ENABLED -> {
                Log.d(TAG, "WIFI_STATE_ENABLED")
                NetworkReceiverCallBackManager.INSTANCE.notifyWifiStateEnable(true)
            }
            WifiManager.WIFI_STATE_ENABLING -> {
                Log.d(TAG, "WIFI_STATE_ENABLING")
            }
            WifiManager.WIFI_STATE_DISABLING -> {
                Log.d(TAG, "WIFI_STATE_DISABLING")
            }
            WifiManager.WIFI_STATE_UNKNOWN -> {
                Log.d(TAG, "WIFI_STATE_UNKNOWN")
            }
        }
    }
}
```

##### 弹出详情对话框

##### WifiDetailPopupWindow.kt

```kotlin
package com.lujianfei.plugin11_1.popupwindow

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.PopupWindow
import android.widget.TextView
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.DrawableHelper
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.plugin11_1.R
import com.lujianfei.plugin11_1.bean.WifiInfoBean

/**
 *@date     创建时间:2020/11/25
 *@name     作者:陆键霏
 *@describe 描述:
 */
class WifiDetailPopupWindow(context: Context?) : PopupWindow(context) {

    companion object {
        const val TAG = "WifiDetailPopupWindow"
    }
    
    private var txt_title: TextView? = null
    private var bt_iknow: Button? = null
    private var txt_content: TextView? = null
    private var scanResult: WifiInfoBean?= null
    
    init {
        contentView = LayoutInflater.from(context).inflate(R.layout.popupwindow_detail, null)
        isOutsideTouchable =false
        setBackgroundDrawable(DrawableHelper.createDrawable(fillColor = 0))
        DensityUtils.getScreenHeight()?.let {getScreenHeight->
            height = getScreenHeight / 2
        }
        initView()
        initEvent()
        initData()
    }

    private fun initData() {
        txt_title?.text = "${scanResult?.name} 的详细信息"
        val toString = scanResult?.scanResult.toString().replace(",","\r\n")
        txt_content?.text = "$toString"
    }

    private fun initEvent() {
        bt_iknow?.setOnClickListener { 
            dismiss()
        }
    }

    private fun initView() {
        txt_title = findViewById(R.id.txt_title)
        bt_iknow = findViewById(R.id.bt_iknow)
        txt_content = findViewById(R.id.txt_content)
        contentView?.background = DrawableHelper.createDrawable(fillColor = 0xa1000000.toInt(), roundRadius = DensityUtils.dip2px(10f).toFloat())
    }
    
    private fun <T:View> findViewById(resId:Int) :T {
        return contentView?.findViewById(resId) as T
    }

    fun setBean(scanResult: WifiInfoBean) {
        this.scanResult = scanResult
        initData()
    }
}
```

##### 详情对话框布局

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:padding="10dp">

    <TextView
        android:id="@+id/txt_title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:gravity="center"
        android:text="提示"
        android:textSize="20sp"
        android:textColor="#ffffff"
        android:padding="10dp"
        />

    <Button
        android:id="@+id/bt_iknow"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_centerHorizontal="true"
        android:layout_alignParentBottom="true"
        android:text="知道了"
        />
    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_below="@id/txt_title"
        android:layout_above="@id/bt_iknow">
        <TextView
            android:id="@+id/txt_content"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="18sp"
            android:textColor="#ffffff"
            />
    </ScrollView>
</RelativeLayout>
```



##### 相关的工具类

```kotlin
package com.lujianfei.plugin11_1.utils

import android.content.Context
import android.net.wifi.WifiManager
import android.util.Log
import com.lujianfei.plugin11_1.bean.WifiInfoBean

/**
 *@date     创建时间:2020/7/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class WifiManagerUtils(context: Context) {
    
    companion object {
        const val TAG = "WifiManagerUtils"   
    }
    private var wifiManager: WifiManager?= null

    init {
        wifiManager = context.getSystemService(Context.WIFI_SERVICE) as WifiManager?
    }
    
    fun startScan() {
        kotlin.runCatching {
            wifiManager?.startScan()
        }.onFailure { 
            Log.d(TAG, "$it")
        }
    }

    fun getScanResults(): List<WifiInfoBean>? {
        val list = arrayListOf<WifiInfoBean>()
        wifiManager?.scanResults?.let {scanResults->
            for (scanResult in scanResults) {
                if (scanResult.SSID.isNullOrEmpty()) continue
                list.add(WifiInfoBean(name = scanResult.SSID, status = 0))
            }
        }
        return list
    }
    
    fun getSSID():String? {
        return wifiManager?.connectionInfo?.ssid
    }
}
```

##### 相关的回调事件管理类

```kotlin
package com.lujianfei.plugin11_1.callback

/**
 *@date     创建时间:2020/7/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class NetworkReceiverCallBackManager private constructor() {
    
    companion object {
        val INSTANCE = NetworkReceiverCallBackManager()
    }
    
    private val listeners = arrayListOf<OnNetworkReceiverListener>()
    
    fun addListener(listener: OnNetworkReceiverListener) {
        synchronized(listeners) {
            listeners.add(listener)
        }
    }

    fun removeListener(listener: OnNetworkReceiverListener) {
        synchronized(listeners) {
            val iterator = listeners.iterator()
            while (iterator.hasNext()) {
                if (iterator.next() == listener) {
                    iterator.remove()
                }
            }
        }
    }
    
    fun notifyScanResult(result:Boolean) {
        for (idx in 0 until listeners.size) {
            listeners[idx].onScanResultRecieve(result)
        }
    }
    
    fun notifyWifiStateEnable(result:Boolean) {
        for (idx in 0 until listeners.size) {
            listeners[idx].onWifiStateEnable(result)
        }
    }
    
    interface OnNetworkReceiverListener {
        fun onScanResultRecieve(result:Boolean)
        fun onWifiStateEnable(enabled:Boolean)
    }
}
```

