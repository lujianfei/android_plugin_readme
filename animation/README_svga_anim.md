# SVGA 动画 代码展示

以下只展示关键部分

加入以下依赖 build.gradle

```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

app 的 build.gradle

```
implementation 'com.github.svga:SVGAPlayer-Android:2.5.8'
```



##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >       
    <FrameLayout
        android:id="@+id/container"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin4_19

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.FrameLayout
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.opensource.svgaplayer.SVGADrawable
import com.opensource.svgaplayer.SVGAImageView
import com.opensource.svgaplayer.SVGAParser
import com.opensource.svgaplayer.SVGAVideoEntity


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    private var container: FrameLayout? = null
    private var mSVGAImageView: SVGAImageView?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        container = findViewById(R.id.container)
        that?.let {
            context->
            SVGAParser.shareParser().init(context) // svga 初始化
            mSVGAImageView = SVGAImageView(context) // SVGAImageView 对象初始化
            val mSVGAImageViewLP = FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,FrameLayout.LayoutParams.MATCH_PARENT)
            mSVGAImageView?.apply {
                layoutParams = mSVGAImageViewLP
            }
            container?.addView(mSVGAImageView)
        }
    }

    override fun initData() {
        // 播放 svga 动画，资源存放于 assets 目录
        val mSVGAParser = SVGAParser(that)
        mSVGAParser.decodeFromAssets("rose.svga", object:SVGAParser.ParseCompletion{
            override fun onComplete(videoItem: SVGAVideoEntity) {
                val drawable = SVGADrawable(videoItem)
                mSVGAImageView?.setImageDrawable(drawable)
                mSVGAImageView?.startAnimation()
            }

            override fun onError() {
            }
        })
    }

    override fun onPluginDestroy() {
        mSVGAImageView?.stopAnimation()
        super.onPluginDestroy()
    }
}
```

