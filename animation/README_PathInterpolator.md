# PathInterpolator 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...
    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <ImageView
            android:id="@+id/imgBall"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            tools:src="@drawable/ic_ball"/>
        
       ...
        <Button
            android:id="@+id/btClick"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:layout_alignParentEnd="true"
            android:text="开始动画"/>
    </RelativeLayout>
</LinearLayout>
```

##### 

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin4_11

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.animation.PathInterpolator
import android.view.inputmethod.EditorInfo
import android.widget.CheckBox
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
        const val DURATION = 2000L
    }
    ...
    private var lastDuration = DURATION
    private var lastX1 = 0.11f
    private var lastY1 = 0.95f
    private var lastX2 = 0.92f
    private var lastY2 = 0.12f
    private var imgBall: ImageView? = null    
    private var btClick: View? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        ...
        btClick = findViewById(R.id.btClick)
        imgBall = findViewById(R.id.imgBall)
        ...
    }

    ...

    override fun initEvent() {
        ...
        btClick?.setOnClickListener {
            imgBall?.translationY = 0f
            var pathInterpolator:PathInterpolator? = null
            cbx_control_point?.let {cbx_control_point->
                pathInterpolator = if (cbx_control_point.isChecked) { // 两个控制点 或 一个控制点
                    PathInterpolator(lastX1,lastY1,lastX2,lastY2)
                } else {
                    PathInterpolator(lastX1,lastY1)
                }
            }
            val animate = imgBall?.animate()
            animate?.apply {
                interpolator = pathInterpolator
                duration = lastDuration
                DensityUtils.getScreenHeight()?.toFloat()?.let { ScreenHeight -> translationY(ScreenHeight * 0.7f) }
                start()
            }
        }
        ...
    }

    ...
}

```

