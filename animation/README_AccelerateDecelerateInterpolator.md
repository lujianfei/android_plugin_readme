# AccelerateDecelerateInterpolator 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...
    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <ImageView
            android:id="@+id/imgBall"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            tools:src="@drawable/ic_ball"/>
        
        ...
        <Button
            android:id="@+id/btClick"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textColor="#333333"
            android:layout_alignParentEnd="true"
            android:text="开始动画"/>
    </RelativeLayout>
</LinearLayout>
```

##### 

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin4_1

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
        const val DURATION = 2000L
    }

    private var lastDuration = DURATION
    private var imgBall: ImageView? = null
    ...    
    private var btClick: View? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
       ...
        btClick = findViewById(R.id.btClick)
        imgBall = findViewById(R.id.imgBall)
       ...
    }

   ...

    override fun initEvent() {
       ...
        btClick?.setOnClickListener {
            imgBall?.translationY = 0f
            val animate = imgBall?.animate()
            animate?.apply {
                interpolator = AccelerateDecelerateInterpolator()
                duration = lastDuration
                DensityUtils.getScreenHeight()?.toFloat()?.let { ScreenHeight -> translationY(ScreenHeight * 0.7f) }
                start()
            }
        }
       ...
    }

   ...
}

```

