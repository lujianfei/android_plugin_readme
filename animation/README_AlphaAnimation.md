# AlphaAnimation 代码展示

##### 动画的分类如下所示，AlphaAnimation 属于补间动画 (Tween Animation)

![img](https://images.gitee.com/uploads/images/2020/0925/121330_32efcd7e_411238.png)

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...
    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
       
        <ImageView
            android:id="@+id/icon"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_centerHorizontal="true"
            android:layout_marginTop="50dp"
            />
        
        <Button
            android:id="@+id/bt_clickme"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_below="@id/icon"
            android:layout_marginTop="50dp"
            android:layout_centerHorizontal="true"
            android:text="点我看效果"/>
    </RelativeLayout>
</LinearLayout>
```

##### 

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin4_14

import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.animation.*
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    ...
    private var icon: ImageView? = null
    private var bt_clickme: Button? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        icon = findViewById(R.id.icon)
        bt_clickme = findViewById(R.id.bt_clickme)
        
       ...
    }

   ...

    override fun initEvent() {
        ...
        bt_clickme?.setOnClickListener { 
             startIconAnimation()
        }
    }

    private fun startIconAnimation() {
        // 透明度动画 [1,0.5]
        val alphaAnimation = AlphaAnimation(
                1f,
                0.5f
        )
        alphaAnimation.duration = 1000
        icon?.startAnimation(alphaAnimation)
    }

    ...
}

```

