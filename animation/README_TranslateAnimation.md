# TranslateAnimation 代码展示

##### 动画的分类如下所示，TranslateAnimation 属于补间动画 (Tween Animation)

![img](https://images.gitee.com/uploads/images/2020/0925/121330_32efcd7e_411238.png)

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...
    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <TextView
            android:id="@+id/txt_input"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_marginTop="20dp"
            android:layout_marginStart="20dp"
            android:textSize="20sp"
            android:text="输入："/>
       <EditText
           android:id="@+id/edit_input"
           android:layout_width="match_parent"
           android:layout_height="wrap_content"
           android:layout_marginEnd="20dp"
           android:layout_alignBaseline="@id/txt_input"
           android:layout_toEndOf="@id/txt_input"
           android:hint="输入框"/>
        
        <Button
            android:id="@+id/bt_clickme"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_below="@id/edit_input"
            android:layout_marginTop="10dp"
            android:layout_alignEnd="@id/edit_input"
            android:text="点我看效果"/>
    </RelativeLayout>
</LinearLayout>
```

##### 

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin4_12

import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.animation.Animation
import android.view.animation.CycleInterpolator
import android.view.animation.TranslateAnimation
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
        const val DURATION = 2000L
    }

    ...
    private var edit_input: EditText? = null
    private var bt_clickme: Button? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        edit_input = findViewById(R.id.edit_input)
        bt_clickme = findViewById(R.id.bt_clickme)
        
        ...
    }

    ...

    override fun initEvent() {
        ...
        bt_clickme?.setOnClickListener { 
            startEditAnimation()
        }
    }

    private fun startEditAnimation() {
        val translateAnimation = TranslateAnimation(0f, 5f, 0f, 5f)
        translateAnimation.apply {
            interpolator = CycleInterpolator(3f) // 抖动次数控制
            duration = 500
        }
        edit_input?.startAnimation(translateAnimation)
    }

    ...
}

```

