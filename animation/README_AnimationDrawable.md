# AnimationDrawable 用法代码展示

##### 动画的分类如下所示，AnimationDrawable 属于帧动画 (Frame Animation)

![img](https://images.gitee.com/uploads/images/2020/0925/121330_32efcd7e_411238.png)

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...
    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
       <TextView
           android:id="@+id/txtlbl1"
           android:layout_width="wrap_content"
           android:layout_height="wrap_content"
           android:textSize="20sp"
           android:layout_centerHorizontal="true"
           android:layout_marginTop="10dp"
           android:text="多张图片的实现"/>
        <ImageView
            android:id="@+id/icon1"
            android:layout_width="141dp"
            android:layout_height="113dp"
            android:layout_below="@id/txtlbl1"
            android:layout_centerHorizontal="true"
            android:layout_marginTop="30dp"
            />
        
        <Button
            android:id="@+id/bt_clickme1"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_below="@id/icon1"
            android:layout_marginTop="30dp"
            android:layout_centerHorizontal="true"
            android:text="开始动画"/>

        <TextView
            android:id="@+id/txtlbl2"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:textSize="20sp"
            android:layout_below="@id/bt_clickme1"
            android:layout_centerHorizontal="true"
            android:layout_marginTop="20dp"
            android:text="单张图片的实现"/>

        <ImageView
            android:id="@+id/icon2"
            android:layout_width="141dp"
            android:layout_height="113dp"
            android:layout_below="@id/txtlbl2"
            android:layout_centerHorizontal="true"
            android:layout_marginTop="30dp"
            />

        <Button
            android:id="@+id/bt_clickme2"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_below="@id/icon2"
            android:layout_marginTop="30dp"
            android:layout_centerHorizontal="true"
            android:text="开始动画"/>
    </RelativeLayout>
</LinearLayout>
```

##### 

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin4_16

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PixelFormat.OPAQUE
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    ...
    
    private var icon1: ImageView? = null
    private var icon2: ImageView? = null
    private var bt_clickme1: Button? = null
    private var bt_clickme2: Button? = null
    private var animationDrawable1:AnimationDrawable? = null
    private var animationDrawable2:AnimationDrawable? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        icon1 = findViewById(R.id.icon1)
        bt_clickme1 = findViewById(R.id.bt_clickme1)
        icon2 = findViewById(R.id.icon2)
        bt_clickme2 = findViewById(R.id.bt_clickme2)
        
        ...
    }

    override fun initData() {        
        initIcon1Animation()
        initIcon2Animation()
    }

    override fun initEvent() {        
        bt_clickme1?.setOnClickListener { 
            startIconAnimation(animationDrawable1,bt_clickme1)
        }
        bt_clickme2?.setOnClickListener { 
            startIconAnimation(animationDrawable2,bt_clickme2)
        }
    }

    private fun startIconAnimation(animationDrawable: AnimationDrawable?, button: Button?) {
        if (animationDrawable?.isRunning == true) {
            animationDrawable.stop()
            button?.text = "开始动画"
        } else {
            animationDrawable?.start()
            button?.text = "停止动画"
        }
    }

    /**
     * 将多张图片加入 AnimationDrawable，为实现帧动画轮播做准备
     */
    private fun initIcon1Animation() {
        val animationPics = arrayListOf(
                R.drawable.ic_pic1,
                R.drawable.ic_pic2,
                R.drawable.ic_pic3,
                R.drawable.ic_pic4,
                R.drawable.ic_pic5,
                R.drawable.ic_pic6,
                R.drawable.ic_pic7,
                R.drawable.ic_pic8
        )
        animationDrawable1 = AnimationDrawable()
        animationPics.forEach {
            val drawable = that?.getDrawable(it)
            drawable?.let { drawable->
                animationDrawable1?.addFrame(drawable, 100)
            }
        }
        animationDrawable1?.isOneShot = false
        icon1?.background = animationDrawable1
    }

    /**
     * 将一张完整动画进行图片均分，并加入 AnimationDrawable, 为实现帧动画轮播做准备
     */
    private fun initIcon2Animation() {
        val birdFlyDrawable = that?.getDrawable(R.drawable.ic_bird_fly)
        birdFlyDrawable?.let {
            val bitmapOrigin = drawableToBitmap(birdFlyDrawable)
            val x: Int = bitmapOrigin.width / 4
            val y: Int = bitmapOrigin.height / 2
            val bitmapDrawable1 = BitmapDrawable(Bitmap.createBitmap(bitmapOrigin, 0, 0, x, y)) 
            val bitmapDrawable2 = BitmapDrawable(Bitmap.createBitmap(bitmapOrigin, x, 0, x, y))
            val bitmapDrawable3 = BitmapDrawable(Bitmap.createBitmap(bitmapOrigin, 2 * x, 0, x, y))
            val bitmapDrawable4 = BitmapDrawable(Bitmap.createBitmap(bitmapOrigin, 3 * x, 0, x, y))
            val bitmapDrawable5 = BitmapDrawable(Bitmap.createBitmap(bitmapOrigin, 0, y, x, y))
            val bitmapDrawable6 = BitmapDrawable(Bitmap.createBitmap(bitmapOrigin, x, y, x, y))
            val bitmapDrawable7 = BitmapDrawable(Bitmap.createBitmap(bitmapOrigin, 2 * x, y, x, y))
            val bitmapDrawable8 = BitmapDrawable(Bitmap.createBitmap(bitmapOrigin, 3 * x, y, x, y))
            animationDrawable2 = AnimationDrawable()
            animationDrawable2?.addFrame(bitmapDrawable1, 100)
            animationDrawable2?.addFrame(bitmapDrawable2, 100)
            animationDrawable2?.addFrame(bitmapDrawable3, 100)
            animationDrawable2?.addFrame(bitmapDrawable4, 100)
            animationDrawable2?.addFrame(bitmapDrawable5, 100)
            animationDrawable2?.addFrame(bitmapDrawable6, 100)
            animationDrawable2?.addFrame(bitmapDrawable7, 100)
            animationDrawable2?.addFrame(bitmapDrawable8, 100)
        }
        animationDrawable2?.isOneShot = false
        icon2?.background = animationDrawable2
    }
   
    /**
     * Drawable转换成一个Bitmap
     * @param drawable drawable对象
     * @return
     */
    private fun drawableToBitmap(drawable: Drawable): Bitmap {
        val bitmap = Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight,
                if (drawable.opacity !== OPAQUE) Bitmap.Config.ARGB_8888 else Bitmap.Config.RGB_565)
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, drawable.intrinsicWidth, drawable.intrinsicHeight)
        drawable.draw(canvas)
        return bitmap
    }    
}

```

素材展示：

ic_pic1

![img](https://images.gitee.com/uploads/images/2020/0927/105613_bc3aabcd_411238.jpeg)

ic_pic2

![img](https://images.gitee.com/uploads/images/2020/0927/105654_2361571a_411238.jpeg)

ic_pic3

![img](https://images.gitee.com/uploads/images/2020/0927/105726_4e540542_411238.jpeg)

ic_pic4

![img](https://images.gitee.com/uploads/images/2020/0927/105745_5ed07b40_411238.jpeg)

ic_pic5

![img](https://images.gitee.com/uploads/images/2020/0927/105800_15baf71e_411238.jpeg)

ic_pic6

![img](https://images.gitee.com/uploads/images/2020/0927/105812_4af6c4a5_411238.jpeg)

ic_pic7

![img](https://images.gitee.com/uploads/images/2020/0927/105829_49073673_411238.jpeg)

ic_pic8

![img](https://images.gitee.com/uploads/images/2020/0927/105850_2b46b4a8_411238.jpeg)

ic_bird_fly

![img](https://images.gitee.com/uploads/images/2020/0927/105920_6cd2093c_411238.jpeg)