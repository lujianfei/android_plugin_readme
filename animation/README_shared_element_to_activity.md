# 共享元素代码（跳转Activity）展示

以下只展示关键部分
#### gradle 依赖引用

```javascript
dependencies {
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'    
}
```

##### share_element_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### 

##### MainActivity.kt

```kotlin
package com.lujianfei.shareelement

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.base.BaseActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.shareelement.adapter.MainAdapter
import com.lujianfei.shareelement.bean.ListBean
import com.lujianfei.shareelement.ui.DetailActivity
import kotlinx.android.synthetic.main.share_element_main_item.*

class MainActivity : BaseActivity() {
  
    private var recyclerview:RecyclerView ?= null
    private val mAdapter by lazy { MainAdapter() }

    override fun resouceId() = R.layout.share_element_main

    override fun initView(savedInstanceState: Bundle?) {
        toolbar = findViewById(R.id.toolbar)
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = mAdapter
        }
    }

    override fun initData() {       
        val dataList = arrayListOf<ListBean>()
        for (i in 0 until 20) {
            dataList.add(ListBean(title = "码农宝$i",
                    summary = "一款可以观看Demo并学习代码的App$i",
                    icon = R.drawable.ic_ball))
        }

        mAdapter.setList(dataList)
    }

    override fun initEvent() {       
        mAdapter.onItemClickListener = {
            view,bean->
            // 关键之处
            ActivityOptionsCompat.makeSceneTransitionAnimation(this@MainActivity,
                    Pair(view.findViewById(R.id.icon),"icon"),
                    Pair(view.findViewById(R.id.title),"title"),
                    Pair(view.findViewById(R.id.summary),"summary")
            ).toBundle()?.let {
                DetailActivity.start(this@MainActivity,
                        bean,it)
            }
        }
    }
    
}
```

##### share_element_main_item.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    xmlns:tools="http://schemas.android.com/tools"
    android:padding="10dp">

    <androidx.appcompat.widget.AppCompatImageView
        android:id="@+id/icon"
        android:layout_width="50dp"
        android:layout_height="50dp"
        android:background="#dddddd"
        android:src="@drawable/ic_ball"
        />

    <androidx.appcompat.widget.AppCompatTextView
        android:id="@+id/title"
        android:layout_toEndOf="@id/icon"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginStart="5dp"
        android:textSize="20sp"
        tools:text="title"
        />
    <androidx.appcompat.widget.AppCompatTextView
        android:id="@+id/summary"
        android:layout_below="@id/title"
        android:layout_alignStart="@id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginStart="5dp"
        android:textSize="15sp"
        tools:text="Summary"
        />
</RelativeLayout>
```

##### MainAdapter.kt

```kotlin
package com.lujianfei.shareelement.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.shareelement.R
import com.lujianfei.shareelement.bean.ListBean

class MainAdapter:RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mList = arrayListOf<ListBean>()
    var onItemClickListener:((View,ListBean)->Unit) ?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.share_element_main_item,parent,false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val bean = mList[position]
        if (holder is MyViewHolder) {
            holder.bindData(bean)
        }
        holder.itemView.setOnClickListener {
            onItemClickListener?.invoke(it,bean)
        }
    }

    override fun getItemCount() = mList.size

    fun setList(list:List<ListBean>) {
        mList.clear()
        mList.addAll(list)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var icon:ImageView ?= null
        var title:TextView ?= null
        var summary:TextView ?= null

        init {
            icon = itemView.findViewById(R.id.icon)
            title = itemView.findViewById(R.id.title)
            summary = itemView.findViewById(R.id.summary)
        }

        fun bindData(bean: ListBean) {
            icon?.setImageResource(bean.icon)
            title?.text = bean.title
            summary?.text = bean.summary
        }
    }
}
```

##### share_element_detail.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>

    <androidx.appcompat.widget.AppCompatImageView
        android:id="@+id/icon"
        android:layout_width="match_parent"
        android:layout_height="300dp"
        android:background="#dddddd"
        />

    <androidx.appcompat.widget.AppCompatTextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="30sp"
        android:padding="10dp"
        tools:text = "Title"
        />
    <androidx.appcompat.widget.AppCompatTextView
        android:id="@+id/summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="20sp"
        android:padding="10dp"
        tools:text = "Summary"
        />
</LinearLayout>
```

##### MainAdapter.kt

```kotlin
package com.lujianfei.shareelement.ui

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import com.lujianfei.module_plugin_base.base.BaseActivity
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.shareelement.R
import com.lujianfei.shareelement.bean.ListBean

class DetailActivity:BaseActivity() {

    companion object {
        fun start(context:Activity, bean: ListBean, bundle: Bundle) {
            context.startActivity(
                    Intent(context,DetailActivity::class.java).putExtra("data",bean),
                    bundle)
        }
    }

    private var toolbar: PluginToolBar?= null
    private var icon:ImageView ?= null
    private var title: TextView?= null
    private var summary: TextView?= null
    private var mListBean:ListBean ?= null

    override fun resouceId() = R.layout.share_element_detail

    override fun initView(savedInstanceState: Bundle?) {
        toolbar = findViewById(R.id.toolbar)
        icon = findViewById(R.id.icon)
        title = findViewById(R.id.title)
        summary = findViewById(R.id.summary)

        // 关键之处
        icon?.let {
            ViewCompat.setTransitionName(it,"icon")
        }
        title?.let {
            ViewCompat.setTransitionName(it,"title")
        }
        summary?.let {
            ViewCompat.setTransitionName(it,"summary")
        }
    }

    override fun initData() {
        mListBean = intent?.getParcelableExtra("data")
        toolbar?.setTitle("详情")

        icon?.let {
            it.setImageResource(mListBean?.icon?:R.drawable.ic_ball)
        }
        title?.let {
            it.text = mListBean?.title
        }
        summary?.let {
            it.text = mListBean?.summary
        }
    }

    override fun initEvent() {
        toolbar?.onBackListener = {
            onBackPressed()
        }
    }
}
```

##### ListBean.kt

用到的实体类

```kotlin
package com.lujianfei.shareelement.bean

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ListBean(val title: String,
                    val summary: String,
                    val icon: Int) : Parcelable
```


