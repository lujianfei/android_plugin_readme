# 共享元素代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >        

    <FrameLayout
        android:id="@+id/container"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### 写一个用于回调 MainActivity 的 Contract

```kotlin
package com.lujianfei.plugin4_18.contract

import com.lujianfei.module_plugin_base.base.BaseView
import com.lujianfei.plugin4_18.bean.MainBean

/**
 *@date     创建时间:2020/11/5
 *@name     作者:陆键霏
 *@describe 描述:
 */
interface MainActivityContract {
    interface View: BaseView {
        fun onItemClickListener(bean: MainBean, view: android.view.View)

    }
}
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin4_18

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.transition.*
import android.util.AttributeSet
import android.view.View
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentTransaction
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin4_18.base.BaseFragment
import com.lujianfei.plugin4_18.bean.MainBean
import com.lujianfei.plugin4_18.contract.MainActivityContract
import com.lujianfei.plugin4_18.fragment.DetailFragment
import com.lujianfei.plugin4_18.fragment.MainFragment


class MainActivity : BasePluginActivity(), MainActivityContract.View {
    companion object {
        const val TAG = "MainActivity"
    }   

    private val mMainFragment by lazy { MainFragment().setMainActivityView(this) }

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        that?.supportFragmentManager
                ?.beginTransaction()
                ?.add(R.id.container, mMainFragment)
                ?.commitAllowingStateLoss()
    }    

    /**
     * 从 MainFragment 的 RecyclerView 回调的点击事件
     */
    override fun onItemClickListener(bean: MainBean, view: View) {
        val mDetailFragment = DetailFragment()
        val bundle = Bundle()
        bundle.putParcelable(DetailFragment.DATA_KEY, bean)
        mDetailFragment.arguments = bundle
        
        // 核心部分, 加入各种转场动画
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mMainFragment.enterTransition = Fade()
            mDetailFragment.enterTransition = Fade()
            mDetailFragment.sharedElementEnterTransition = DetailTransition(that, null)
            mDetailFragment.sharedElementReturnTransition = DetailTransition(that, null)
        }
        val icon = view.findViewById(R.id.icon) as View
        val title = view.findViewById(R.id.txt_title) as View
        val summary = view.findViewById(R.id.txt_summary) as View
        // 加入需要绑定的共享元素，需要和详情布局里的 transitionName 名称保持一致
        that?.supportFragmentManager?.beginTransaction()
                ?.addSharedElement(icon, "transition_name_icon")
                ?.addSharedElement(title, "transition_name_title")
                ?.addSharedElement(summary, "transition_name_summary")
                ?.replace(R.id.container, mDetailFragment)
                ?.addToBackStack(null)
                ?.commitAllowingStateLoss()
    }

    override fun onBackPressed() {
        val supportFragmentManager = that?.supportFragmentManager ?: return
        if (supportFragmentManager.backStackEntryCount > 0){
            supportFragmentManager.popBackStackImmediate()
        } else {
            super.onBackPressed()
        }
    }

    class DetailTransition(context: Context?, attrs: AttributeSet?) : TransitionSet(context, attrs) {
        init {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ordering = ORDERING_TOGETHER
                addTransition(ChangeBounds()) //视图的位置和大小
                addTransition(ChangeTransform()) //视图的比例(scale)
                addTransition(ChangeImageTransform()) //图像的比例
            }
        }
    }
}
```

##### fragment_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</RelativeLayout>
```

##### MainFragment.kt

```kotlin
package com.lujianfei.plugin4_18.fragment

import android.app.Activity
import android.os.Build
import android.view.View
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.plugin4_18.R
import com.lujianfei.plugin4_18.adapter.MainAdapter
import com.lujianfei.plugin4_18.base.BaseFragment
import com.lujianfei.plugin4_18.bean.MainBean
import com.lujianfei.plugin4_18.contract.MainActivityContract

class MainFragment: BaseFragment() {

    companion object {
        const val TAG = "MainFragment"
    }
    private var recyclerview:RecyclerView ?= null
    private val mAdapter by lazy { MainAdapter() }
    private var mainActivityView: MainActivityContract.View ?= null
    fun setMainActivityView(mainActivityView: MainActivityContract.View? = null): MainFragment {
        this.mainActivityView = mainActivityView
        return this
    }
    
    override fun resourceId() = R.layout.fragment_main

    override fun initView() {
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = mAdapter
        }
    }

    override fun initData() {
        val mData = arrayListOf<MainBean>()
        for (idx in 0 until 30) {
            mData.add(MainBean(imageUrl = "http://iwanted.sinaapp.com/json/mnb/img/car.jpg",
                        title = "码农宝标题 $idx",
                        summary = "码农宝简介：主要功能：\n" +
                                "- 快速查看安卓设备信息 （手机屏幕分辨率，手机型号，设备id, 可用内存等等）\n" +
                                "- 各类开发过程中常用代码及效果 Demo, 分别有\n" +
                                "- 列表, 容器，对话框，动画，翻页，图表，编码及算法，多媒体，传感器，实用工具等 $idx"))
        }
        mAdapter.setData(mData)
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun initEvent() {
        mAdapter.onItemClickListener = { bean,view->
            mainActivityView?.onItemClickListener(bean,view)
        }
    }
}
```

##### adapter_main_item.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    xmlns:tools="http://schemas.android.com/tools"
    android:padding="5dp">

    <ImageView
        android:id="@+id/icon"
        android:layout_width="80dp"
        android:layout_height="80dp"
        tools:src="@mipmap/ic_launcher"
        android:background="#dddddd"/>

    <TextView
        android:id="@+id/txt_title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginStart="5dp"
        android:layout_toEndOf="@id/icon"
        android:layout_alignTop="@id/icon"
        android:ellipsize="end"
        android:maxLines="1"
        android:text="title"/>

    <TextView
        android:id="@+id/txt_summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="summary"
        android:ellipsize="end"
        android:maxLines="3"
        android:layout_marginTop="5dp"
        android:layout_alignStart="@id/txt_title"
        android:layout_below="@id/txt_title"
        />
</RelativeLayout>
```

##### MainAdapter.kt

```kotlin
package com.lujianfei.plugin4_18.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lujianfei.plugin4_18.R
import com.lujianfei.plugin4_18.bean.MainBean

class MainAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val mData = arrayListOf<MainBean>()
    var onItemClickListener:((MainBean,View)->Unit) ?= null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_main_item, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mainBean = mData[holder.adapterPosition]
        if (holder is MyViewHolder) {
            holder.bindData(mainBean, holder.adapterPosition)
        }
        holder.itemView.setOnClickListener {
            onItemClickListener?.invoke(mainBean, it)
        }
    }

    fun setData(data:List<MainBean>) {
        mData.clear()
        mData.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemCount() = mData.size

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private var icon: ImageView? = null
        private var txt_title: TextView? = null
        private var txt_summary: TextView? = null

        init {
            icon = itemView.findViewById(R.id.icon)
            txt_title = itemView.findViewById(R.id.txt_title)
            txt_summary = itemView.findViewById(R.id.txt_summary)
        }

        fun bindData(mainBean: MainBean, position: Int) {
            if (icon == null) return

            // 把每个图片视图设置不同的Transition名称, 防止在一个视图内有多个相同的名称, 在变换的时候造成混乱
            // Fragment支持多个View进行变换, 使用适配器时, 需要加以区分
            ViewCompat.setTransitionName(icon!!, "transition_name_icon_$position")
            ViewCompat.setTransitionName(txt_title!!, "transition_name_title_$position")
            ViewCompat.setTransitionName(txt_summary!!, "transition_name_summary_$position")
            
            icon?.let {
                Glide.with(itemView.context)
                        .load(mainBean.imageUrl)
                        .placeholder(itemView.context.resources.getDrawable(R.mipmap.ic_launcher))
                        .into(it)
            }
            txt_title?.text = mainBean.title
            txt_summary?.text = mainBean.summary
        }
    }
}
```

##### MainBean.kt

用到的实体类

```kotlin
package com.lujianfei.plugin4_18.bean

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MainBean(
        val imageUrl:String,
        val title:String,
        val summary:String) : Parcelable
```



##### DetailFragment.kt

详情类 Fragment

```kotlin
package com.lujianfei.plugin4_18.fragment

import android.app.Activity
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.plugin4_18.R
import com.lujianfei.plugin4_18.base.BaseFragment
import com.lujianfei.plugin4_18.bean.MainBean

class DetailFragment: BaseFragment() {

    companion object {
        private const val TAG = "DetailFragment"
        const val DATA_KEY = "data_key"
    }
    private var icon: ImageView? = null
    private var txt_title: TextView? = null
    private var txt_summary: TextView? = null
    private var bean: MainBean ?= null
    
    override fun resourceId() = R.layout.fragment_detail

    override fun initView() {
        icon = findViewById(R.id.icon)
        txt_title = findViewById(R.id.txt_title)
        txt_summary = findViewById(R.id.txt_summary)
    }

    override fun initData() {
        bean = arguments?.getParcelable(DATA_KEY)
        updateUI()
    }

    override fun initEvent() {
    }

    override fun updateTitle(title: String?) {
    }

    override fun onTitleRightClick(that: Activity?, view: View?) {
    }

    override fun release() {
    }

    private fun updateUI() {
        if (icon == null || bean == null) return
        context?.let {
            Glide.with(it)
                    .load(bean!!.imageUrl)
                    .placeholder(it.resources?.getDrawable(R.mipmap.ic_launcher))
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                            return false
                        }

                        override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                            updateIcon(resource)
                            return true
                        }
                    }).submit()
        }
        txt_title?.text = bean?.title
        txt_summary?.text = bean?.summary
    }

    private fun updateIcon(resource: Drawable?) {
        if (resource == null) return
        val screenW = DensityUtils.getScreenWidth() ?: return
        val widthScale = screenW / resource.intrinsicWidth
        val height = resource.intrinsicHeight * widthScale
        icon?.post {
            if (icon?.layoutParams == null ) {
                icon?.layoutParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, height)
            } else {
                icon?.layoutParams?.height = height
                icon?.requestLayout()
            }
            icon?.setImageDrawable(resource)
        }
    }
}
```
##### fragment_detail.xml

这个是详情页的布局，需要注意的就是和元素 transitionName 的命名需要和 列表的元素相对应

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <ImageView
        android:id="@+id/icon"
        android:layout_width="match_parent"
        android:layout_height="300dp"
        android:scaleType="fitXY"
        android:transitionName="transition_name_icon"
        />

    <TextView
        android:id="@+id/txt_title"
        android:layout_below="@id/icon"
        android:layout_marginTop="10dp"
        android:textSize="20sp"
        android:padding="5dp"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:transitionName="transition_name_title"
        />

    <TextView
        android:id="@+id/txt_summary"
        android:layout_below="@id/txt_title"
        android:layout_marginTop="10dp"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:padding="5dp"
        android:transitionName="transition_name_summary"
        />
</RelativeLayout>
```

