# MotionLayout 动画代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">   

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content">
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical">
            <!--平移动画-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text="平移"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutLine"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_line_scene">
                <ImageView
                    android:id="@+id/ivAnimObj"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_line"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />

            <!--平移加透明度变化动画-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text="平移加透明"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutLineAlpha"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_line_alpha_scene">
                <ImageView
                    android:id="@+id/ivAnimObjAlpha"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_line_alpha"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />

            <!--曲线动画-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线\nkeyPositionType='pathRelative'\npercentY='1'\nframePosition='50'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_curve_scene">
                <ImageView
                    android:id="@+id/ivAnimObjCurve"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_curve"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />

            <!--曲线动画2-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线2\nkeyPositionType='pathRelative'\npercentY='0.5'\nframePosition='50'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve2"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_curve_scene2">
                <ImageView
                    android:id="@+id/ivAnimObjCurve2"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_curve2"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />

            <!--曲线动画3-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线3\nkeyPositionType='parentRelative'\npercentY='1'\nframePosition='50'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve3"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                app:showPaths = "true"
                android:background="#dddddd"
                app:layoutDescription="@xml/motion_layout_curve_scene3">
                <ImageView
                    android:id="@+id/ivAnimObjCurve3"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_curve3"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />
            <!--曲线动画4-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线4\nkeyPositionType='parentRelative'\npercentY='0.5'\nframePosition='50'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve4"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_curve_scene4">
                <ImageView
                    android:id="@+id/ivAnimObjCurve4"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_curve4"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />

            <!--曲线动画5-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线5\npathMotionArc='startVertical'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve5"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_curve_scene5">
                <ImageView
                    android:id="@+id/ivAnimObjCurve5"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_curve5"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />

            <!--曲线动画6-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线6\npathMotionArc='startHorizontal'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve6"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_curve_scene6">
                <ImageView
                    android:id="@+id/ivAnimObjCurve6"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_curve6"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />

            <!--曲线动画7-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线7\nTransition->pathMotionArc='startHorizontal'\nKeyPosition->pathMotionArc='startHorizontal'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve7"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_curve_scene7">
                <ImageView
                    android:id="@+id/ivAnimObjCurve7"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_curve7"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />
            <!--曲线动画8-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线8\nTransition->pathMotionArc='startHorizontal'\nKeyPosition->pathMotionArc='flip'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve8"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_curve_scene8">
                <ImageView
                    android:id="@+id/ivAnimObjCurve8"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_curve8"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />

            <!--曲线动画9-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线9\nTransition->pathMotionArc='startHorizontal'\nKeyPosition->pathMotionArc='startVertical'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve9"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_curve_scene9">
                <ImageView
                    android:id="@+id/ivAnimObjCurve9"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_curve9"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />
            <!--曲线动画10-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线10\nKeyCycle->waveShape='sin'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutKeyCycle10"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_keycycle_scene10">
                <ImageView
                    android:id="@+id/ivAnimObjKeyCycle10"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_keycycle10"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />

            <!--曲线动画11-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线11\nKeyCycle->waveShape='square'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutKeyCycle11"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_keycycle_scene11">
                <ImageView
                    android:id="@+id/ivAnimObjKeyCycle11"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_keycycle11"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />
            <!--曲线动画12-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线12\nKeyCycle->waveShape='triangle'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutKeyCycle12"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_keycycle_scene12">
                <ImageView
                    android:id="@+id/ivAnimObjKeyCycle12"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_keycycle12"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />
            <!--曲线动画13-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线13\nKeyCycle->waveShape='sawtooth'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutKeyCycle13"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_keycycle_scene13">
                <ImageView
                    android:id="@+id/ivAnimObjKeyCycle13"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_keycycle13"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />
            <!--曲线动画14-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线14\nKeyCycle->waveShape='reverseSawtooth'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutKeyCycle14"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_keycycle_scene14">
                <ImageView
                    android:id="@+id/ivAnimObjKeyCycle14"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_keycycle14"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />
            <!--曲线动画15-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线15\nKeyCycle->waveShape='cos'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutCurve15"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_keycycle_scene15">
                <ImageView
                    android:id="@+id/ivAnimObjKeyCycle15"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_keycycle15"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />
            <!--曲线动画16-->
            <TextView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:gravity="center"
                android:textSize="18sp"
                android:layout_marginTop="10dp"
                android:text = "曲线16\nwaveShape='bounce'"/>

            <androidx.constraintlayout.motion.widget.MotionLayout
                android:id="@+id/motionLayoutKeyCycle16"
                android:layout_width="match_parent"
                android:layout_height="200dp"
                android:background="#dddddd"
                app:showPaths = "true"
                app:layoutDescription="@xml/motion_layout_keycycle_scene16">
                <ImageView
                    android:id="@+id/ivAnimObjKeyCycle16"
                    android:layout_width="50dp"
                    android:layout_height="50dp"
                    android:src="@drawable/ic_ball"
                    app:layout_constraintStart_toStartOf="parent"
                    app:layout_constraintTop_toTopOf="parent"
                    />
            </androidx.constraintlayout.motion.widget.MotionLayout>
            <com.lujianfei.module_plugin_base.widget.PluginButton
                android:id="@+id/bt_anim_keycycle16"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_marginTop="5dp"
                android:layout_gravity="center_horizontal"
                android:text="执行动画"
                />
        </LinearLayout>
    </ScrollView>
</LinearLayout>
```



##### MainActivity.kt

```kotlin
package com.lujianfei.motionlayout

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.constraintlayout.motion.widget.MotionLayout
import com.lujianfei.module_plugin_base.base.BaseActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar

class MainActivity : BaseActivity() {
   
    private var bt_anim_line: View?= null
    private var bt_anim_line_alpha: View?= null
    private var bt_anim_curve: View?= null
    private var bt_anim_curve2: View?= null
    private var bt_anim_curve3: View?= null
    private var bt_anim_curve4: View?= null
    private var bt_anim_curve5: View?= null
    private var bt_anim_curve6: View?= null
    private var bt_anim_curve7: View?= null
    private var bt_anim_curve8: View?= null
    private var bt_anim_curve9: View?= null
    private var bt_anim_keycycle10: View?= null
    private var bt_anim_keycycle11: View?= null
    private var bt_anim_keycycle12: View?= null
    private var bt_anim_keycycle13: View?= null
    private var bt_anim_keycycle14: View?= null
    private var bt_anim_keycycle15: View?= null
    private var bt_anim_keycycle16: View?= null
    private var motionLayoutLine: MotionLayout?= null
    private var motionLayoutLineAlpha: MotionLayout?= null
    private var motionLayoutCurve: MotionLayout?= null
    private var motionLayoutCurve2: MotionLayout?= null
    private var motionLayoutCurve3: MotionLayout?= null
    private var motionLayoutCurve4: MotionLayout?= null
    private var motionLayoutCurve5: MotionLayout?= null
    private var motionLayoutCurve6: MotionLayout?= null
    private var motionLayoutCurve7: MotionLayout?= null
    private var motionLayoutCurve8: MotionLayout?= null
    private var motionLayoutCurve9: MotionLayout?= null
    private var motionLayoutKeyCycle10: MotionLayout?= null
    private var motionLayoutKeyCycle11: MotionLayout?= null
    private var motionLayoutKeyCycle12: MotionLayout?= null
    private var motionLayoutKeyCycle13: MotionLayout?= null
    private var motionLayoutKeyCycle14: MotionLayout?= null
    private var motionLayoutKeyCycle15: MotionLayout?= null
    private var motionLayoutKeyCycle16: MotionLayout?= null
    private var constraintLayoutAnim17: ConstraintLayout?= null
    private var constraintLayoutAnim18: ConstraintLayout?= null
    private var motionLayoutAnim19: MotionLayout?= null

    override fun resouceId() = R.layout.motion_layout_main

    override fun initView(savedInstanceState: Bundle?) {
        toolbar = findViewById(R.id.toolbar)
        bt_anim_line = findViewById(R.id.bt_anim_line)
        bt_anim_line_alpha = findViewById(R.id.bt_anim_line_alpha)
        bt_anim_curve = findViewById(R.id.bt_anim_curve)
        bt_anim_curve2 = findViewById(R.id.bt_anim_curve2)
        bt_anim_curve3 = findViewById(R.id.bt_anim_curve3)
        bt_anim_curve4 = findViewById(R.id.bt_anim_curve4)
        bt_anim_curve5 = findViewById(R.id.bt_anim_curve5)
        bt_anim_curve6 = findViewById(R.id.bt_anim_curve6)
        bt_anim_curve7 = findViewById(R.id.bt_anim_curve7)
        bt_anim_curve8 = findViewById(R.id.bt_anim_curve8)
        bt_anim_curve9 = findViewById(R.id.bt_anim_curve9)
        bt_anim_keycycle10 = findViewById(R.id.bt_anim_keycycle10)
        bt_anim_keycycle11 = findViewById(R.id.bt_anim_keycycle11)
        bt_anim_keycycle12 = findViewById(R.id.bt_anim_keycycle12)
        bt_anim_keycycle13 = findViewById(R.id.bt_anim_keycycle13)
        bt_anim_keycycle14 = findViewById(R.id.bt_anim_keycycle14)
        bt_anim_keycycle15 = findViewById(R.id.bt_anim_keycycle15)
        bt_anim_keycycle16 = findViewById(R.id.bt_anim_keycycle16)
        bt_anim_keycycle17 = findViewById(R.id.bt_anim_constraint17)
        bt_anim_keycycle18 = findViewById(R.id.bt_anim_constraint18)
        bt_anim_motionlayout19 = findViewById(R.id.bt_anim_motionlayout19)
        motionLayoutLine = findViewById(R.id.motionLayoutLine)
        motionLayoutLineAlpha = findViewById(R.id.motionLayoutLineAlpha)
        motionLayoutCurve = findViewById(R.id.motionLayoutCurve)
        motionLayoutCurve2 = findViewById(R.id.motionLayoutCurve2)
        motionLayoutCurve3 = findViewById(R.id.motionLayoutCurve3)
        motionLayoutCurve4 = findViewById(R.id.motionLayoutCurve4)
        motionLayoutCurve5 = findViewById(R.id.motionLayoutCurve5)
        motionLayoutCurve6 = findViewById(R.id.motionLayoutCurve6)
        motionLayoutCurve7 = findViewById(R.id.motionLayoutCurve7)
        motionLayoutCurve8 = findViewById(R.id.motionLayoutCurve8)
        motionLayoutCurve9 = findViewById(R.id.motionLayoutCurve9)
        motionLayoutKeyCycle10 = findViewById(R.id.motionLayoutKeyCycle10)
        motionLayoutKeyCycle11 = findViewById(R.id.motionLayoutKeyCycle11)
        motionLayoutKeyCycle12 = findViewById(R.id.motionLayoutKeyCycle12)
        motionLayoutKeyCycle13 = findViewById(R.id.motionLayoutKeyCycle13)
        motionLayoutKeyCycle14 = findViewById(R.id.motionLayoutKeyCycle14)
        motionLayoutKeyCycle15 = findViewById(R.id.motionLayoutCurve15)
        motionLayoutKeyCycle16 = findViewById(R.id.motionLayoutKeyCycle16)
		constraintLayoutAnim17 = findViewById(R.id.constraintLayoutAnim17)
        constraintLayoutAnim18 = findViewById(R.id.constraintLayoutAnim18)
        motionLayoutAnim19 = findViewById(R.id.motionLayoutAnim19)
        
        val lp = motionLayoutCurve?.layoutParams
        if (lp != null) {
            lp.height = DensityUtils.getScreenWidth()?:0
            motionLayoutCurve?.layoutParams = lp
        }
        val lp2 = motionLayoutCurve2?.layoutParams
        if (lp2 != null) {
            lp2.height = DensityUtils.getScreenWidth()?:0
            motionLayoutCurve2?.layoutParams = lp
        }
    }


    override fun initEvent() {        
        setClickListener(bt_anim_line, motionLayoutLine)
        setClickListener(bt_anim_line_alpha, motionLayoutLineAlpha)
        setClickListener(bt_anim_curve, motionLayoutCurve)
        setClickListener(bt_anim_curve2, motionLayoutCurve2)
        setClickListener(bt_anim_curve3, motionLayoutCurve3)
        setClickListener(bt_anim_curve4, motionLayoutCurve4)
        setClickListener(bt_anim_curve5, motionLayoutCurve5)
        setClickListener(bt_anim_curve6, motionLayoutCurve6)
        setClickListener(bt_anim_curve7, motionLayoutCurve7)
        setClickListener(bt_anim_curve8, motionLayoutCurve8)
        setClickListener(bt_anim_curve9, motionLayoutCurve9)
        setClickListener(bt_anim_keycycle10, motionLayoutKeyCycle10)
        setClickListener(bt_anim_keycycle11, motionLayoutKeyCycle11)
        setClickListener(bt_anim_keycycle12, motionLayoutKeyCycle12)
        setClickListener(bt_anim_keycycle13, motionLayoutKeyCycle13)
        setClickListener(bt_anim_keycycle14, motionLayoutKeyCycle14)
        setClickListener(bt_anim_keycycle15, motionLayoutKeyCycle15)
        setClickListener(bt_anim_keycycle16, motionLayoutKeyCycle16)
        onAnimation17Click()
        onAnimation18Click()
        
        val constraintSetEnd = motionLayoutAnim19?.getConstraintSet(R.id.end)
        // 使用代码修改结束状态时的属性
        constraintSetEnd?.constrainWidth(R.id.ivAnimObj19, 0)
        constraintSetEnd?.constrainHeight(R.id.ivAnimObj19, 0)
        setClickListener(bt_anim_motionlayout19, motionLayoutAnim19)
    }

    private fun setClickListener(view:View?, motionLayout:MotionLayout?) {
        if (view == null || motionLayout == null) {
            return
        }
        view.setOnClickListener {
            if (motionLayout.progress == 0f) {
                motionLayout.transitionToEnd()
            } else{
                motionLayout.transitionToStart()
            }
        }
    }  
    
    
    private fun onAnimation17Click() {
        bt_anim_keycycle17?.setOnClickListener {
            it.isSelected = !it.isSelected
            val constraintSet = ConstraintSet()
            constraintSet.clone(this@MainActivity, R.layout.param_constraintlayout)
            constraintLayoutAnim17?.let { constraintLayoutKeyCycle17 ->
                if (animation17Running) {
                    return@setOnClickListener
                }
                val mTransitionSet = AutoTransition()
                mTransitionSet.duration = 1000
                mTransitionSet.addListener(object :
                    androidx.transition.Transition.TransitionListener {
                    override fun onTransitionStart(transition: androidx.transition.Transition) {
                        animation17Running = true
                    }

                    override fun onTransitionEnd(transition: androidx.transition.Transition) {
                        animation17Running = false
                    }

                    override fun onTransitionCancel(transition: androidx.transition.Transition) {
                        animation17Running = false
                    }

                    override fun onTransitionPause(transition: androidx.transition.Transition) {
                    }

                    override fun onTransitionResume(transition: androidx.transition.Transition) {
                    }
                })
                TransitionManager.beginDelayedTransition(constraintLayoutKeyCycle17, mTransitionSet)
            }
            if (it.isSelected) {
                constraintSet.constrainWidth(R.id.ivAnimObjKeyCycle17, DensityUtils.dip2px(150f))
                constraintSet.constrainHeight(R.id.ivAnimObjKeyCycle17, DensityUtils.dip2px(150f))
            } else {
                constraintSet.constrainWidth(R.id.ivAnimObjKeyCycle17, DensityUtils.dip2px(50f))
                constraintSet.constrainHeight(R.id.ivAnimObjKeyCycle17, DensityUtils.dip2px(50f))
            }
            constraintSet.applyTo(constraintLayoutAnim17)
            animation17Running = true
        }
    }

    private fun onAnimation18Click() {
        bt_anim_keycycle18?.setOnClickListener {
            if (animation18Running) {
                return@setOnClickListener
            }
            it.isSelected = !it.isSelected
            val constraintSet = ConstraintSet()
            constraintLayoutAnim18?.let { constraintLayoutKeyCycle18 ->
                val mTransitionSet = AutoTransition()
                mTransitionSet.duration = 1000
                mTransitionSet.interpolator = BounceInterpolator()
                mTransitionSet.addListener(object :
                    androidx.transition.Transition.TransitionListener {
                    override fun onTransitionStart(transition: androidx.transition.Transition) {
                        animation18Running = true
                    }

                    override fun onTransitionEnd(transition: androidx.transition.Transition) {
                        animation18Running = false
                    }

                    override fun onTransitionCancel(transition: androidx.transition.Transition) {
                        animation18Running = false
                    }

                    override fun onTransitionPause(transition: androidx.transition.Transition) {
                    }

                    override fun onTransitionResume(transition: androidx.transition.Transition) {
                    }
                })
                TransitionManager.beginDelayedTransition(constraintLayoutKeyCycle18, mTransitionSet)
            }
            if (it.isSelected) {
                constraintSet.clone(this@MainActivity, R.layout.param_constraintlayout_end)
            } else {
                constraintSet.clone(this@MainActivity, R.layout.param_constraintlayout_start)
            }
            constraintSet.applyTo(constraintLayoutAnim18)
        }
    }
}
```

## 动画核心部分

##### motion_layout_curve_scene.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@+id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000">
        <KeyFrameSet>
            <KeyPosition
                app:keyPositionType="pathRelative"
                app:percentY="1"
                app:framePosition="50"
                app:motionTarget="@id/ivAnimObjCurve"/>
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjCurve"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjCurve"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>
</MotionScene>
```



##### motion_layout_curve_scene2.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@+id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000">
        <KeyFrameSet>
            <KeyPosition
                app:keyPositionType="pathRelative"
                app:percentY="0.5"
                app:framePosition="50"
                app:motionTarget="@id/ivAnimObjCurve2"/>
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjCurve2"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjCurve2"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_curve_scene3.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@+id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000">
        <KeyFrameSet>
            <KeyPosition
                app:keyPositionType="parentRelative"
                app:percentY="1"
                app:framePosition="50"
                app:motionTarget="@id/ivAnimObjCurve3"/>
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjCurve3"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjCurve3"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_curve_scene4.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@+id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000">
        <KeyFrameSet>
            <KeyPosition
                app:keyPositionType="parentRelative"
                app:percentY="0.5"
                app:framePosition="50"
                app:motionTarget="@id/ivAnimObjCurve4"/>
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjCurve4"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjCurve4"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_curve_scene5.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000"
        app:pathMotionArc="startVertical">
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjCurve5"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjCurve5"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_curve_scene6.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000"
        app:pathMotionArc="startHorizontal">
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjCurve6"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjCurve6"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_curve_scene7.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000"
        app:pathMotionArc="startHorizontal"
        >
        <KeyFrameSet>
            <KeyPosition
                app:motionTarget="@id/ivAnimObjCurve7"
                app:framePosition="50"
                app:percentY="0.5"
                app:keyPositionType="parentRelative"
                app:pathMotionArc="startHorizontal"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjCurve7"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjCurve7"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_curve_scene8.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000"
        app:pathMotionArc="startHorizontal"
        >
        <KeyFrameSet>
            <KeyPosition
                app:motionTarget="@id/ivAnimObjCurve8"
                app:framePosition="50"
                app:percentY="0.5"
                app:keyPositionType="parentRelative"
                app:pathMotionArc="flip"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjCurve8"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjCurve8"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_curve_scene9.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000"
        app:pathMotionArc="startHorizontal"
        >
        <KeyFrameSet>
            <KeyPosition
                app:motionTarget="@id/ivAnimObjCurve9"
                app:framePosition="50"
                app:percentY="0.5"
                app:keyPositionType="parentRelative"
                app:pathMotionArc="startVertical"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjCurve9"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjCurve9"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_keycycle_scene10.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="2000"
        app:pathMotionArc="startHorizontal"
        >
        <KeyFrameSet>
            <KeyCycle
                android:rotation="45"
                app:motionTarget="@id/ivAnimObjKeyCycle10"
                app:framePosition="50"
                app:waveOffset="0"
                app:wavePeriod="1"
                app:waveShape="sin"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle10"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle10"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```



##### motion_layout_keycycle_scene11.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="2000"
        app:pathMotionArc="startHorizontal"
        >
        <KeyFrameSet>
            <KeyCycle
                android:rotation="45"
                app:motionTarget="@id/ivAnimObjKeyCycle11"
                app:framePosition="50"
                app:waveOffset="0"
                app:wavePeriod="1"
                app:waveShape="square"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle11"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle11"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```



##### motion_layout_keycycle_scene12.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="2000"
        app:pathMotionArc="startHorizontal"
        >
        <KeyFrameSet>
            <KeyCycle
                android:rotation="45"
                app:motionTarget="@id/ivAnimObjKeyCycle12"
                app:framePosition="50"
                app:waveOffset="0"
                app:wavePeriod="1"
                app:waveShape="triangle"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle12"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle12"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```



##### motion_layout_keycycle_scene13.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="2000"
        app:pathMotionArc="startHorizontal"
        >
        <KeyFrameSet>
            <KeyCycle
                android:rotation="45"
                app:motionTarget="@id/ivAnimObjKeyCycle13"
                app:framePosition="50"
                app:waveOffset="0"
                app:wavePeriod="1"
                app:waveShape="sawtooth"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle13"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle13"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```



##### motion_layout_keycycle_scene14.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="2000"
        app:pathMotionArc="startHorizontal"
        >
        <KeyFrameSet>
            <KeyCycle
                android:rotation="45"
                app:motionTarget="@id/ivAnimObjKeyCycle14"
                app:framePosition="50"
                app:waveOffset="0"
                app:wavePeriod="1"
                app:waveShape="reverseSawtooth"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle14"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle14"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_keycycle_scene15.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="2000"
        >
        <KeyFrameSet>
            <KeyCycle
                android:rotation="45"
                app:motionTarget="@id/ivAnimObjKeyCycle15"
                app:framePosition="50"
                app:waveOffset="0"
                app:wavePeriod="1"
                app:waveShape="cos"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle15"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent"
            />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle15"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_keycycle_scene16.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="2000"
        >
        <KeyFrameSet>
            <KeyCycle
                android:rotation="45"
                app:motionTarget="@id/ivAnimObjKeyCycle16"
                app:framePosition="50"
                app:waveOffset="0"
                app:wavePeriod="1"
                app:waveShape="bounce"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle16"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent"
            />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjKeyCycle16"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_line_alpha_scene.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@+id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000">
        <KeyFrameSet>
            <KeyAttribute
                android:alpha="0"
                app:framePosition="100"
                app:motionTarget="@id/ivAnimObjAlpha"
                />
        </KeyFrameSet>
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObjAlpha"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObjAlpha"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </ConstraintSet>
</MotionScene>
```

##### motion_layout_line_scene.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene 
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@+id/end"
        app:constraintSetStart="@id/start"
        app:duration="1000">
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObj"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
             />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObj"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            />
    </ConstraintSet>
</MotionScene>
```

##### param_constraintlayout.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="200dp"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:background="#dddddd">
        <ImageView
            android:id="@+id/ivAnimObjKeyCycle17"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent"
            />
</androidx.constraintlayout.widget.ConstraintLayout>
```

##### param_constraintlayout_start.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="200dp"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:background="#dddddd">
        <ImageView
            android:id="@+id/ivAnimObj18"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent"
            />
</androidx.constraintlayout.widget.ConstraintLayout>
```

##### param_constraintlayout_end.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="200dp"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    android:background="#dddddd">
        <ImageView
            android:id="@+id/ivAnimObj18"
            android:layout_width="0dp"
            android:layout_height="0dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent"
            />
</androidx.constraintlayout.widget.ConstraintLayout>
```

##### motion_layout_scene19.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<MotionScene xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto">

    <Transition
        app:constraintSetEnd="@id/end"
        app:constraintSetStart="@id/start"
        app:duration="2000"
        >
    </Transition>

    <ConstraintSet android:id="@+id/start">
        <Constraint
            android:id="@+id/ivAnimObj19"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent"
            />
    </ConstraintSet>

    <ConstraintSet android:id="@+id/end">
        <Constraint
            android:id="@+id/ivAnimObj19"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:src="@drawable/ic_ball"
            android:alpha="0.1"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintBottom_toBottomOf="parent" />
    </ConstraintSet>
</MotionScene>
```

