# 属性动画 代码展示

##### 动画的分类如下所示，这里主要展示 ObjectAnimator 的用法， 属于属性动画 (PropertyAnimation)

![img](https://images.gitee.com/uploads/images/2020/0925/121330_32efcd7e_411238.png)

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <ScrollView
        android:layout_width="match_parent"
        android:layout_height="wrap_content">

        <RelativeLayout
            android:layout_width="match_parent"
            android:layout_height="match_parent">

            <Button
                android:id="@+id/bt_clickme1"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/icon1"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="20dp"
                android:text="开始动画" />

            <Button
                android:id="@+id/bt_clickme2"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/icon2"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="20dp"
                android:text="开始动画" />

            <Button
                android:id="@+id/bt_clickme3"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/icon3"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="20dp"
                android:text="开始动画" />

            <Button
                android:id="@+id/bt_clickme4"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/icon4"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="20dp"
                android:text="开始动画" />

            <ImageView
                android:id="@+id/icon1"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/txtlbl1"
                android:layout_marginTop="20dp" />

            <ImageView
                android:id="@+id/icon3"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/txtlbl3"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="20dp" />

            <ImageView
                android:id="@+id/icon4"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/txtlbl4"
                android:layout_marginTop="20dp" />

            <ImageView
                android:id="@+id/icon2"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/txtlbl2"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="20dp" />

            <TextView
                android:id="@+id/txtlbl3"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/bt_clickme2"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="10dp"
                android:text="旋转: "
                android:textSize="20sp" />

            <TextView
                android:id="@+id/txtlbl1"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="10dp"
                android:text="平移: "
                android:textSize="20sp" />

            <TextView
                android:id="@+id/txtlbl2"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/bt_clickme1"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="10dp"
                android:text="缩放: "
                android:textSize="20sp" />

            <TextView
                android:id="@+id/txtlbl4"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_below="@id/bt_clickme3"
                android:layout_centerHorizontal="true"
                android:layout_marginTop="10dp"
                android:text="平移 + 旋转: "
                android:textSize="20sp" />

        </RelativeLayout>
    </ScrollView>
</LinearLayout>
```

##### 

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin4_17

import android.animation.Animator
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.DisplayMetrics
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
   
    private var txtTitle: TextView? = null
    private var txtViewCode: TextView? = null
    private var icon1: ImageView? = null
    private var icon2: ImageView? = null
    private var icon3: ImageView? = null
    private var icon4: ImageView? = null
    private var bt_clickme1: Button? = null
    private var bt_clickme2: Button ?= null
    private var bt_clickme3: Button ?= null
    private var bt_clickme4: Button ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        icon1 = findViewById(R.id.icon1)
        icon2 = findViewById(R.id.icon2)
        icon3 = findViewById(R.id.icon3)
        icon4 = findViewById(R.id.icon4)
        bt_clickme1 = findViewById(R.id.bt_clickme1)
        bt_clickme2 = findViewById(R.id.bt_clickme2)
        bt_clickme3 = findViewById(R.id.bt_clickme3)
        bt_clickme4 = findViewById(R.id.bt_clickme4)                
    }
    

    override fun initEvent() {       
        bt_clickme1?.setOnClickListener {
            translateAnimation()
        }
        bt_clickme2?.setOnClickListener {
            scaleAnimation()
        }
        bt_clickme3?.setOnClickListener { 
            rotateAnimation()
        }
        bt_clickme4?.setOnClickListener {
            translateAndRotateAnimation()
        }
    }

    private fun translateAndRotateAnimation() {
        val animatorSet = AnimatorSet()
        // 位移 0 -> screenWidth 
        val animatorRight = ObjectAnimator.ofFloat(icon4, "translationX", 0f, getScreenWidth().toFloat())
        animatorRight?.duration = 1000
        // 位移 screenWidth -> 0
        val animatorLeft = ObjectAnimator.ofFloat(icon4, "translationX", getScreenWidth().toFloat(), 0f)
        animatorLeft?.duration = 1000
        
        // 顺时针旋转
        val animatorRotationRight = ObjectAnimator.ofFloat(icon4, "rotation", 0f, 360f)
        animatorRotationRight?.duration = 1000
        // 逆时针旋转
        val animatorRotationLeft = ObjectAnimator.ofFloat(icon4, "rotation", 0f, -360f)
        animatorRotationLeft?.duration = 1000

        // 右位移的同时，顺时针旋转
        val animatorSetMoveRightAndRotate = AnimatorSet()
        animatorSetMoveRightAndRotate.playTogether(animatorRight,animatorRotationRight)
        // 左位移的同时，顺时针旋转
        val animatorSetMoveLeftAndRotate = AnimatorSet()
        animatorSetMoveLeftAndRotate.playTogether(animatorLeft,animatorRotationLeft)
        
        
        animatorSet.playSequentially(animatorSetMoveRightAndRotate,animatorSetMoveLeftAndRotate)
        animatorSet.addListener(object : Animator.AnimatorListener{
            override fun onAnimationStart(animation: Animator?) {
                bt_clickme4?.isEnabled = false
            }

            override fun onAnimationEnd(animation: Animator?) {
                bt_clickme4?.isEnabled = true
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationRepeat(animation: Animator?) {
            }
        })
        animatorSet.start()
    }

    private fun rotateAnimation() {
        val animatorRotation = ObjectAnimator.ofFloat(icon3, "rotation", 0f, 360f)
        animatorRotation?.duration = 2000
        animatorRotation?.addListener(object :Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator?) {
                bt_clickme3?.isEnabled = false
            }

            override fun onAnimationEnd(animation: Animator?) {
                bt_clickme3?.isEnabled = true
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationRepeat(animation: Animator?) {
            }
        })
        animatorRotation?.start()
    }

    private fun scaleAnimation() {
        val animatorSet = AnimatorSet()
        // 缩放比例 1 -> 1.5
        val animatorBigX = ObjectAnimator.ofFloat(icon2, "scaleX", 1f, 1.5f)
        animatorBigX?.duration = 1000
        val animatorBigY = ObjectAnimator.ofFloat(icon2, "scaleY", 1f, 1.5f)
        animatorBigY?.duration = 1000
        val animatorBigSet = AnimatorSet()
        animatorBigSet.playTogether(animatorBigX,animatorBigY)
        // 缩放比例 1.5 -> 1
        val animatorSmallX = ObjectAnimator.ofFloat(icon2, "scaleX", 1.5f, 1f)
        animatorSmallX?.duration = 1000
        val animatorSmallY = ObjectAnimator.ofFloat(icon2, "scaleY", 1.5f, 1f)
        animatorSmallY?.duration = 1000
        val animatorSmallSet = AnimatorSet()
        animatorSmallSet.playTogether(animatorSmallX,animatorSmallY)
        // 先放大后缩小
        animatorSet.playSequentially(animatorBigSet,animatorSmallSet)
        animatorSet.addListener(object : Animator.AnimatorListener{
            override fun onAnimationStart(animation: Animator?) {
                bt_clickme2?.isEnabled = false
            }

            override fun onAnimationEnd(animation: Animator?) {
                bt_clickme2?.isEnabled = true
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationRepeat(animation: Animator?) {
            }
        })
        animatorSet.start()
    }

    private fun translateAnimation() {
        val animatorSet = AnimatorSet()
        // 位移 0 -> screenWidth 
        val animatorRight = ObjectAnimator.ofFloat(icon1, "translationX", 0f, getScreenWidth().toFloat())
        animatorRight?.duration = 1000
        // 位移 screenWidth -> 0
        val animatorLeft = ObjectAnimator.ofFloat(icon1, "translationX", getScreenWidth().toFloat(), 0f)
        animatorLeft?.duration = 1000
        
        animatorSet.playSequentially(animatorRight,animatorLeft)
        animatorSet.addListener(object : Animator.AnimatorListener{
            override fun onAnimationStart(animation: Animator?) {
                bt_clickme1?.isEnabled = false
            }

            override fun onAnimationEnd(animation: Animator?) {
                bt_clickme1?.isEnabled = true
            }

            override fun onAnimationCancel(animation: Animator?) {
            }

            override fun onAnimationRepeat(animation: Animator?) {
            }
        })
        animatorSet.start()
    }    

    private fun getScreenWidth():Int {
        val windowManager = that?.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay?.getRealMetrics(displayMetrics)
        return displayMetrics.widthPixels
    }
}

```

