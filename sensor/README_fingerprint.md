# android 指纹识别 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">   

    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/bt_click"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_margin="10dp"
        android:text="点击验证指纹"/>

</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin10_5

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.hardware.fingerprint.FingerprintManager
import android.net.Uri
import android.os.Build
import android.os.CancellationSignal
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {

    companion object {
        const val TAG = "MainActivity"
        const val PERMISSION = Manifest.permission.USE_FINGERPRINT
        const val REQUEST_PERMISSION = 100
    }
   
    private var bt_click:Button ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {        
        bt_click = findViewById(R.id.bt_click)
    }    

    override fun initEvent() {      
        bt_click?.setOnClickListener { 
           showFingerPringDialog()
        }
    }

    private fun showFingerPringDialog() {
        if (Build.VERSION.SDK_INT <= 23) {
            Toast.makeText(that, "仅支持 android 6.0 以上", Toast.LENGTH_SHORT).show()
            return
        }
        that?.let {
            if (ActivityCompat.checkSelfPermission(it,PERMISSION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(it, arrayOf(PERMISSION), REQUEST_PERMISSION)
                return
            }
            val fingerPrintDialog = FingerPrintDialog(it)
            fingerPrintDialog.onSuccessCallback = {
                Toast.makeText(it, "识别成功", Toast.LENGTH_SHORT).show()
            }
            fingerPrintDialog.show()
        }
    }

    private fun openBrowser() {
        bean?.let { bean ->
            val uri = Uri.parse(bean.codeUrl)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }    
}

```

核心类 - 指纹识别对话框

##### FingerPrintDialog.kt

```kotlin
package com.lujianfei.plugin10_5

import android.app.Dialog
import android.content.Context
import android.hardware.fingerprint.FingerprintManager
import android.os.Build
import android.os.Bundle
import android.os.CancellationSignal
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import com.lujianfei.module_plugin_base.utils.DensityUtils

/**
 *@date     创建时间:2020/12/9
 *@name     作者:陆键霏
 *@describe 描述:
 */
@RequiresApi(Build.VERSION_CODES.M)
class FingerPrintDialog(context: Context) : Dialog(context) {

    private var image_icon: ImageView? = null
    private var error_msg: TextView? = null
    private var cancel: TextView? = null
    private var mFingerprintManager: FingerprintManager?= null
    private val mCancellationSignal = CancellationSignal()
    var onSuccessCallback:(()->Unit) ?= null
    
    init {
        setCancelable(false)
        initView(context)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mFingerprintManager = context.getSystemService(FingerprintManager::class.java)
        initEvent()
    }

    private fun initEvent() {
        cancel?.setOnClickListener { 
            dismiss()
        }
        setOnDismissListener {
            mCancellationSignal.cancel()
        }
        if (Build.VERSION.SDK_INT >= 23) {
            mFingerprintManager?.authenticate(null,mCancellationSignal,0, mAuthenticationCallback, null)
        }
    }

    private fun initView(context: Context) {
        val inflate = LayoutInflater.from(context).inflate(R.layout.fingerprint_dialog, null)
        addContentView(inflate, LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT))
        image_icon = inflate?.findViewById(R.id.image_icon)
        error_msg = inflate?.findViewById(R.id.error_msg)
        cancel = inflate?.findViewById(R.id.cancel)
        image_icon?.setImageDrawable(context.resources.getDrawable(R.drawable.ic_fp_40px))
        val attributes = window?.attributes
        attributes?.apply { 
            width = DensityUtils.dip2px(300f)
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private val mAuthenticationCallback = object : FingerprintManager.AuthenticationCallback() {
        /**
         * 尝试次数过多，请稍后重试
         */
        override fun onAuthenticationError(errorCode: Int, errString: CharSequence?) {
            super.onAuthenticationError(errorCode, errString)
            error_msg?.text = errString
        }

        /**
         * 指纹验证失败，可再验，可能手指过脏，或者移动过快等原因。
         */
        override fun onAuthenticationHelp(helpCode: Int, helpString: CharSequence?) {
            super.onAuthenticationHelp(helpCode, helpString)
            error_msg?.text = helpString
        }

        /**
         * 指纹密码验证成功
         */
        override fun onAuthenticationSucceeded(result: FingerprintManager.AuthenticationResult?) {
            super.onAuthenticationSucceeded(result)
            onSuccessCallback?.invoke()
            dismiss()
        }

        /**
         * 指纹验证失败，指纹识别失败，可再验，错误原因为：该指纹不是系统录入的指纹。
         */
        override fun onAuthenticationFailed() {
            super.onAuthenticationFailed()
            error_msg?.text = "指纹识别失败"
        }
    }
}
```

##### fingerprint_dialog.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <ImageView
        android:id="@+id/image_icon"
        android:layout_width="60dp"
        android:layout_height="60dp"
        android:layout_marginTop="30dp"
        android:layout_gravity="center_horizontal"
        tools:src="@drawable/ic_fp_40px"
        />

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center"
        android:layout_marginTop="20dp"
        android:text="请验证指纹解锁"
        android:textColor="#000"
        android:textSize="16sp"
        />

    <TextView
        android:id="@+id/error_msg"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center"
        android:layout_marginTop="5dp"
        android:maxLines="1"
        android:textSize="12sp"
        android:textColor="#f45"
        />

    <View
        android:layout_width="match_parent"
        android:layout_height="0.5dp"
        android:layout_marginTop="10dp"
        android:background="#ccc"
        />

    <TextView
        android:id="@+id/cancel"
        android:layout_width="match_parent"
        android:layout_height="50dp"
        android:gravity="center"
        android:text="取消"
        android:textColor="#5d7883"
        android:textSize="16sp"
        />

</LinearLayout>
```



