# 手机摇一摇 代码展示

以下只展示关键部分

这里用到的是【加速度传感器】

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    ...

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        android:padding="10dp">
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="这里运用了【加速度传感器】, 你可以开始尝试摇一摇"/>
        
       <TextView
           android:id="@+id/sensorInfo"
           android:layout_width="match_parent"
           android:layout_height="wrap_content"
           android:layout_marginTop="10dp"/>
        
        <TextView
            android:id="@+id/debugInfo"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_marginTop="10dp"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin10_1

import android.content.Context
import android.content.Intent
import android.hardware.SensorEventListener
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.plugin10_1.utils.ShakeHelper


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    ...
    private var sensorInfo: TextView? = null
    private var debugInfo: TextView? = null

    private var shakeHelper:ShakeHelper? = null
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        debugInfo = findViewById(R.id.debugInfo)
        sensorInfo = findViewById(R.id.sensorInfo)
        ...

        that?.let {that->
            shakeHelper = ShakeHelper(that)
        }
        shakeHelper?.registerSensor()
    }

    ...

    override fun initEvent() {
        ...
        shakeHelper?.sensorCallback = {x,y,z,speed->
            val afterX = String.format("%.2f",x)
            val afterY = String.format("%.2f",y)
            val afterZ = String.format("%.2f",z)
            val afterSpeed = String.format("%.2f",speed)
            sensorInfo?.text = "( x, y, z ) = ( $afterX, $afterY, $afterZ )\r\n\r\nspeed = $afterSpeed"
        }
        shakeHelper?.shakeCallback = {
            debugInfo?.append("你触发了摇一摇\r\n")
        }
    }

    ...

    override fun onBackPressed() {
        shakeHelper?.unRegisterSensor()
        super.onBackPressed()
    }
}

```

##### 下面是核心工具类

##### ShakeHelper

```kotlin
package com.lujianfei.plugin10_1.utils

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Vibrator
import kotlin.math.sqrt

/**
 *@date     创建时间:2020/7/2
 *@name     作者:陆键霏
 *@describe 描述:
 */
class ShakeHelper(context: Context) : SensorEventListener  {

    // 两次检测的时间间隔
    private val UPTATE_INTERVAL_TIME = 100

    // 加速度变化阈值，当摇晃速度达到这值后产生作用
    private val SPEED_THRESHOLD = 1000

    private var mSensorManager: SensorManager? = null
    private var lastUpdateTime: Long = 0
    private var lastX = 0f
    private var lastY = 0f
    private var lastZ = 0f
    
    var sensorCallback:((Float,Float,Float,Float)->Unit) ?= null
    var shakeCallback:(()->Unit) ?= null
    
    init {
        mSensorManager = context.getSystemService(Context.SENSOR_SERVICE) as SensorManager
    }
    
    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val currentUpdateTime = System.currentTimeMillis()

        val timeInterval = currentUpdateTime - lastUpdateTime

        if (timeInterval < UPTATE_INTERVAL_TIME) {
            return
        }

        lastUpdateTime = currentUpdateTime
        val values = event?.values ?: return

        // 获得x,y,z加速度
        val x = values[0]
        val y = values[1]
        val z = values[2]

        // 获得x,y,z加速度的变化值
        val deltaX = x - lastX
        val deltaY = y - lastY
        val deltaZ = z - lastZ

        // 将现在的坐标变成last坐标
        lastX = x
        lastY = y
        lastZ = z

        // speed = sqrt ( x^2 + y^2 + z^2 )
        val speed = sqrt(deltaX * deltaX +
                deltaY * deltaY + deltaZ
                * deltaZ).toDouble() / timeInterval * 10000

        sensorCallback?.invoke(values[0],values[1],values[2],speed.toFloat())

        if (speed > SPEED_THRESHOLD) {
            //在这里可以提供一个回调
            shakeCallback?.invoke()
        }
    }

    fun registerSensor() {
        val sensor = mSensorManager
                ?.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        if (null != sensor) mSensorManager?.registerListener(this, sensor,
                SensorManager.SENSOR_DELAY_NORMAL)
    }

    fun unRegisterSensor() {
        lastX = 0f
        lastY = 0f
        lastZ = 0f
        mSensorManager?.unregisterListener(this)
    }
}
```

