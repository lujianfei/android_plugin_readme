# android 获取 usb设备 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

   ...
    <ListView
        android:id="@+id/listview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin10_3

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.ListView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.plugin10_3.utils.UsbDeviceHelper
import com.lujianfei.plugin10_3.adapter.UsbAdapter


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

   ...
    
    private var listview:ListView ?= null
    private val adapter by lazy { UsbAdapter() }
    private var usbHelper: UsbDeviceHelper?= null
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        listview = findViewById(R.id.listview)
       ...
        
        listview?.adapter = adapter
    }

    override fun initData() {
        ...
        that?.let {that->
            usbHelper = UsbDeviceHelper(that)
        }
        adapter.setData(usbHelper?.getAllUsbDevices())
    }

   ...
}

```

##### UsbAdapter.kt

```kotlin
package com.lujianfei.plugin10_3.adapter

import android.hardware.usb.UsbDevice
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.lujianfei.plugin10_3.R

/**
 *@date     创建时间:2020/7/2
 *@name     作者:陆键霏
 *@describe 描述:
 */
class UsbAdapter: BaseAdapter() {
    
    private val usblist = arrayListOf<UsbDevice>()
    
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var convertViewTmp = convertView
        var viewHolder: ViewHolder?= null
        if (convertViewTmp == null) {
            viewHolder = ViewHolder()
            convertViewTmp = LayoutInflater.from(parent?.context).inflate(R.layout.adapter_usb_info, parent , false)
            viewHolder.name = convertViewTmp.findViewById(R.id.name)
            viewHolder.deviceId = convertViewTmp.findViewById(R.id.deviceId)
            viewHolder.vendorId = convertViewTmp.findViewById(R.id.vendorId)
            viewHolder.manufacturer = convertViewTmp.findViewById(R.id.manufacturer)
            viewHolder.productId = convertViewTmp.findViewById(R.id.productId)
            viewHolder.product = convertViewTmp.findViewById(R.id.product)
            viewHolder.version = convertViewTmp.findViewById(R.id.version)
            viewHolder.serialNumber = convertViewTmp.findViewById(R.id.serialNumber)
            convertViewTmp.tag = viewHolder
        } else {
            viewHolder = convertViewTmp.tag as ViewHolder
        }
        val item = getItem(position)
        viewHolder.name?.text = item.deviceName 
        viewHolder.deviceId?.text = "设备 Id：${item.deviceId}"
        viewHolder.vendorId?.text = "供应商 Id：${item.vendorId}"
        viewHolder.manufacturer?.text = "厂商：${item.manufacturerName}"
        viewHolder.productId?.text = "产品 Id：${item.productId}"
        viewHolder.product?.text = "产品：${item.productName}"
        viewHolder.version?.text = "版本：${item.version} " 
        viewHolder.serialNumber?.text = "序列号：${item.serialNumber} " 
        return convertViewTmp!!
    }

    override fun getItem(position: Int): UsbDevice = usblist[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = usblist.size
    
    fun setData(sensors:List<UsbDevice>?) {
        this.usblist.clear()
        sensors?.let { 
            this.usblist.addAll(it)
        }
        notifyDataSetChanged()
    }
    
    fun getData() = usblist
    
    class ViewHolder {
        var name:TextView ?= null
        var deviceId:TextView ?= null
        var vendorId:TextView ?= null
        var manufacturer:TextView ?= null
        var productId:TextView ?= null
        var product:TextView ?= null
        var version:TextView ?= null
        var serialNumber:TextView ?= null
    }
}
```

##### adapter_usb_info.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:padding="5dp">

    <TextView
        android:id="@+id/name"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="18sp"/>
    <TextView
        android:id="@+id/deviceId"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="15sp"/>
    <TextView
        android:id="@+id/vendorId"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="15sp"/>
    <TextView
        android:id="@+id/manufacturer"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="15sp"/>
    <TextView
        android:id="@+id/productId"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="15sp"/>
    <TextView
        android:id="@+id/product"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="15sp"/>
    <TextView
        android:id="@+id/version"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="15sp"/>
    <TextView
        android:id="@+id/serialNumber"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="15sp"/>
</LinearLayout>
```



##### 下面是核心工具类

##### UsbDeviceHelper

```kotlin
package com.lujianfei.plugin10_3.utils

import android.content.Context
import android.hardware.usb.UsbDevice
import android.hardware.usb.UsbManager

/**
 *@date     创建时间:2020/7/2
 *@name     作者:陆键霏
 *@describe 描述:
 */
class UsbDeviceHelper(context: Context) {

    private var mSensorManager: UsbManager? = null
    
    init {
        mSensorManager = context.getSystemService(Context.USB_SERVICE) as UsbManager
    }
    
    fun getAllUsbDevices():List<UsbDevice>? {
        val sensorList = mSensorManager?.deviceList
        return sensorList?.values?.toList()
    }
}
```

