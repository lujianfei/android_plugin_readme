# 生成二维码 代码展示

以下只展示关键部分

##### 新增引用

build.gradle 

```javascript
// 二维码相关
implementation 'com.google.zxing:core:3.3.0'
```



##### fragment_input.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:padding="10dp">

    <TextView
        android:id="@+id/txt_qrcode_lbl"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:textSize="18sp"
        android:text="输入二维码内容"/>

    <EditText
        android:id="@+id/edit_qrcode_content"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_below="@id/txt_qrcode_lbl"
        android:layout_marginTop="10dp"
        />

    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/bt_generate_qrcode"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_below="@id/edit_qrcode_content"
        android:layout_marginTop="10dp"
        android:layout_marginStart="10dp"
        android:layout_marginEnd="10dp"
        android:text="生成二维码"/>
    
</RelativeLayout>
```

##### InputFragment.kt

```kotlin
package com.lujianfei.plugin8_14.fragment

import android.widget.Button
import android.widget.EditText
import com.lujianfei.plugin8_14.QRCodeDialog
import com.lujianfei.plugin8_14.R
import com.lujianfei.plugin8_14.base.BaseFragment

/**
 *@date     创建时间:2020/10/20
 *@name     作者:陆键霏
 *@describe 描述:
 */
class InputFragment: BaseFragment() {

    private var edit_qrcode_content: EditText? = null
    private var bt_generate_qrcode: Button? = null

    override fun resouceId(): Int = R.layout.fragment_input

    override fun initView() {
        edit_qrcode_content = findViewById(R.id.edit_qrcode_content)
        bt_generate_qrcode = findViewById(R.id.bt_generate_qrcode)
    }

    override fun initData() {
    }

    override fun initEvent() {
        bt_generate_qrcode?.setOnClickListener {
            startGenerateQRCode()
        }
    }

    private fun startGenerateQRCode() {
        if (edit_qrcode_content?.text?.isEmpty() == true) {
            edit_qrcode_content?.error = "内容不能为空"
            return
        }
        context?.let {
            QRCodeDialog(it, edit_qrcode_content?.text.toString()).show()
        }
    }

    override fun updateTitle() {
    }
}
```

弹出二维码对话框

##### QRCodeDialog.kt

```kotlin
package com.lujianfei.plugin8_14

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.plugin8_14.utils.QRCodeHelper
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream

class QRCodeDialog(context: Context, content: String): Dialog(context) {

    companion object {
        private const val TAG = "QRCodeDialog"
    }
    
    private var img_qrcode:ImageView ?= null
    private var bt_save:Button ?= null
    private var content:String ?= ""
    private var createQRCodeBitmap:Bitmap ?= null

    init {
        this.content = content
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        // instantiate the dialog with the custom Theme
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        val layout = inflater.inflate(R.layout.dialog_qrcode, null)
        addContentView(layout, LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT))
        initView(layout)
        initData()
        initEvent()
    }

    private fun initEvent() {
        bt_save?.setOnClickListener {
            saveBitmapAndUpdateAlbum()
        }
    }

    private fun saveBitmapAndUpdateAlbum() {
        val dir = ResUtils.pluginContext?.getExternalFilesDir(Environment.DIRECTORY_DCIM)
        val outputFile = File(dir, "mnb_${System.currentTimeMillis()}.jpg")
        LogUtils.d(TAG, "saveBitmapAndUpdateAlbum $outputFile")
        // 写文件
        FileOutputStream(outputFile).use {
            createQRCodeBitmap?.compress(Bitmap.CompressFormat.JPEG, 80, it)
            it.flush()
        }
        // 其次把文件插入到系统图库
        kotlin.runCatching {
            MediaStore.Images.Media.insertImage(context.contentResolver,
                    outputFile.absolutePath, outputFile.name, null)
        }.onFailure {
            LogUtils.e(TAG, "saveBitmapAndUpdateAlbum onFailure $it")    
        }
        // 通知系统更新图库
        context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(File(outputFile.path))))
        ResUtils.pluginContext?.let {
            Toast.makeText(it, "保存成功", Toast.LENGTH_SHORT).show()
        }
    }

    private fun initData() {
        val scale = 0.9f
        val dialogWindow = window
        val lp = dialogWindow?.attributes
        val displayMetrics = context.resources.displayMetrics // 获取屏幕宽、高用
        createQRCodeBitmap = QRCodeHelper.createQRCodeBitmap(content,
                width = (displayMetrics.widthPixels * scale).toInt(),
                height = (displayMetrics.widthPixels * scale).toInt())

        img_qrcode?.setImageBitmap(createQRCodeBitmap)
    }

    private fun initView(layout: View) {
        img_qrcode = layout.findViewById(R.id.img_qrcode)
        bt_save = layout.findViewById(R.id.bt_save)
    }
}
```

生成二维码工具类

##### QRCodeHelper.kt

```kotlin
package com.lujianfei.plugin8_14.utils

import android.graphics.Bitmap
import android.graphics.Color
import android.text.TextUtils
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.WriterException
import com.google.zxing.qrcode.QRCodeWriter
import java.util.*

object QRCodeHelper {
    /**
     * 生成简单二维码
     *
     * @param content                字符串内容
     * @param width                  二维码宽度
     * @param height                 二维码高度
     * @param character_set          编码方式（一般使用UTF-8）
     * @param error_correction_level 容错率 L：7% M：15% Q：25% H：35%
     * @param margin                 空白边距（二维码与边框的空白区域）
     * @param color_black            黑色色块
     * @param color_white            白色色块
     * @return BitMap
     */
    fun createQRCodeBitmap(
            content: String?,
            width: Int,
            height: Int,
            character_set: String? = "UTF-8",
            error_correction_level: String? = "",
            margin: String? = "1",
            color_black: Int = Color.BLACK,
            color_white: Int = Color.WHITE
    ): Bitmap? {
        // 字符串内容判空
        if (TextUtils.isEmpty(content)) {
            return null
        }
        // 宽和高>=0
        return if (width < 0 || height < 0) {
            null
        } else try {
            /** 1.设置二维码相关配置  */
            val hints: Hashtable<EncodeHintType, String?> = Hashtable()
            // 字符转码格式设置
            if (!TextUtils.isEmpty(character_set)) {
                hints[EncodeHintType.CHARACTER_SET] = character_set
            }
            // 容错率设置
            if (!TextUtils.isEmpty(error_correction_level)) {
                hints[EncodeHintType.ERROR_CORRECTION] = error_correction_level
            }
            // 空白边距设置
            if (!TextUtils.isEmpty(margin)) {
                hints[EncodeHintType.MARGIN] = margin
            }
            /** 2.将配置参数传入到QRCodeWriter的encode方法生成BitMatrix(位矩阵)对象  */
            val bitMatrix =
                    QRCodeWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints)

            /** 3.创建像素数组,并根据BitMatrix(位矩阵)对象为数组元素赋颜色值  */
            val pixels = IntArray(width * height)
            for (y in 0 until height) {
                for (x in 0 until width) {
                    //bitMatrix.get(x,y)方法返回true是黑色色块，false是白色色块
                    if (bitMatrix[x, y]) {
                        pixels[y * width + x] = color_black //黑色色块像素设置
                    } else {
                        pixels[y * width + x] = color_white // 白色色块像素设置
                    }
                }
            }
            /** 4.创建Bitmap对象,根据像素数组设置Bitmap每个像素点的颜色值,并返回Bitmap对象  */
            val bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
            bitmap.setPixels(pixels, 0, width, 0, 0, width, height)
            bitmap
        } catch (e: WriterException) {
            null
        }
    }
}
```

