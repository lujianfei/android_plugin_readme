# android 发送短信 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    >

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="5dp"
        android:layout_marginTop="5dp"
        android:textSize="20sp"
        android:text="发送号码:"/>

    <EditText
        android:id="@+id/editNumber"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="20sp"
        android:hint="输入发送号码"
        android:inputType="number"
        android:singleLine="true"
        />
    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="5dp"
        android:layout_marginTop="5dp"
        android:textSize="20sp"
        android:text="发送内容:"/>
    <EditText
        android:id="@+id/editContent"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="20sp"
        android:hint="输入发送内容"
        />
    
    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/btSend"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="20sp"
        android:layout_marginStart="5dp"
        android:layout_marginEnd="5dp"
        android:text="调用发送短信"/>

</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin8_16

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar

/**
 * https://github.com/magiclen/JavaChineseCalendar
 */
class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity8_16"
    }

    private var editNumber: EditText? = null
    private var editContent: EditText? = null
    private var btSend: com.lujianfei.module_plugin_base.widget.PluginButton? = null
    
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {       
        editNumber = findViewById(R.id.editNumber)
        editContent = findViewById(R.id.editContent)
        btSend = findViewById(R.id.btSend)
    }

    override fun initEvent() {        
        btSend?.setOnClickListener {
            sendMessage()
        }
    }

    private fun sendMessage() {
        if (editNumber?.text?.isEmpty() == true) {
            editNumber?.error = "请输入发送号码"
            return
        }
        if (editContent?.text?.isEmpty() == true) {
            editContent?.error = "请输入发送内容"
            return
        }
        val smsToUri = Uri.parse("smsto:${editNumber?.text.toString()}")
        val sendIntent = Intent(Intent.ACTION_SENDTO, smsToUri)
        sendIntent.putExtra("sms_body", editContent?.text.toString())
        kotlin.runCatching {
            startActivity(sendIntent)
        }.onFailure {
            Toast.makeText(that, "$it", Toast.LENGTH_SHORT).show()
        }
    }
}

```
