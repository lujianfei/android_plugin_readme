# Json 格式化 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    ...

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:textSize="25sp"
            android:text="json 字符串 :"/>
        <EditText
            android:id="@+id/editJson"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:hint="请输入 json 字符串"
            />
        <Button
            android:id="@+id/btPretty"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="json 格式化"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin8_2

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import org.json.JSONObject

class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    ...
    private var editJson: EditText? = null
    private var btPretty: View? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        btPretty = findViewById(R.id.btPretty)
        editJson = findViewById(R.id.editJson)
        ...
    }

    override fun initData() {
        bean = that?.intent?.getParcelableExtra("data")
    }

    override fun initEvent() {
        ...
        btPretty?.setOnClickListener {
            editJson?.text.let { 
                if (it.isNullOrEmpty()) {
                    editJson?.error = "请输入 json 字符串"
                    return@setOnClickListener
                }
                kotlin.runCatching {
                    val jsonObject = JSONObject(it.toString())
                    editJson?.setText(jsonObject.toString(4))
                    editJson?.error = null
                }.onFailure {
                    editJson?.error = "json 格式有误"
                }
            }
        }
    }

    ...
}

```
