# Timestamp conversion 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">

    ...

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="日期时间 (yyyy-MM-dd HH:mm:ss) :"/>
        <EditText
            android:id="@+id/editDate"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:focusable="false"
            android:hint="点此选择日期"
            />
        <EditText
            android:id="@+id/editTime"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:focusable="false"
            android:hint="点此选择时间"
            />
        <TextView
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="时间戳 (ms) :"/>
        <EditText
            android:id="@+id/editTimestamp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:hint="输入时间戳"
            android:inputType="number"
            />
        
        <Button
            android:id="@+id/btDateToTimestamp"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="日期 --> 时间戳"/>

        <Button
            android:id="@+id/btTimestampToDate"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:text="时间戳 --> 日期"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin8_1

import android.content.Context
import android.content.Intent
import android.icu.util.Calendar
import android.net.Uri
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.plugin8_1.widget.DatePickerPopupWindow
import com.lujianfei.plugin8_1.widget.TimePickerPopupWindow
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    ...
    private var editDate: EditText? = null
    private var editTime: EditText? = null
    private var btDateToTimestamp: View? = null
    private var btTimestampToDate: View? = null
    private var mYear = 0
    private var mMonth = 0
    private var mDay = 0
    private var mHour = 0
    private var mMinute = 0
    private var mSecond = 0

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        btDateToTimestamp = findViewById(R.id.btDateToTimestamp)
        btTimestampToDate = findViewById(R.id.btTimestampToDate)
        editDate = findViewById(R.id.editDate)
        editTime = findViewById(R.id.editTime)
        ...
    }
		...

    override fun initEvent() {
        ...
        editDate?.setOnClickListener {
            if (mYear == 0) {
                val calendar = Calendar.getInstance()
                mYear = calendar.get(Calendar.YEAR)
                mMonth = calendar.get(Calendar.MONTH)
                mDay = calendar.get(Calendar.DATE)
            }
            
            val datePickerPopupWindow = DatePickerPopupWindow(that,
                    mYear,
                    mMonth,
                    mDay)
            datePickerPopupWindow.onDateCallback = { year, month, day ->
                mYear = year
                mMonth = month
                mDay = day

                editDate?.setText(String.format("%04d-%02d-%02d", mYear, mMonth + 1, mDay))
            }
            datePickerPopupWindow.showAsDropDown(it)
        }

        editTime?.setOnClickListener {
            if (mHour == 0) {
                val calendar = Calendar.getInstance()
                mHour = calendar.get(Calendar.HOUR_OF_DAY)
                mMinute = calendar.get(Calendar.MINUTE)
            }
            val timePickerPopupWindow = TimePickerPopupWindow(that,mHour,mMinute)
            timePickerPopupWindow.onTimeCallback = { hour,minute->
                mHour = hour
                mMinute = minute
                mSecond = 0
                editTime?.setText(String.format("%02d:%02d:%02d", mHour, mMinute, mSecond))
            }
            timePickerPopupWindow.showAsDropDown(it)
        }

        btDateToTimestamp?.setOnClickListener {
            
            editDate?.text.let { 
                if (it.isNullOrEmpty()) {
                    editDate?.error = "日期不能为空"
                    return@setOnClickListener
                }
            }

            editTime?.text.let {
                if (it.isNullOrEmpty()) {
                    editTime?.error = "时间不能为空"
                    return@setOnClickListener
                }
            }

            editDate?.error = null
            editTime?.error = null
            
            editTimestamp?.setText("${DateTimeHelper.getTimeStampBy(mYear,mMonth,mDay,mHour,mMinute,mSecond)}")
        }
        
        btTimestampToDate?.setOnClickListener {
            editTimestamp?.text.let {text->
                if (text.isNullOrEmpty()) {
                    editTimestamp?.error = "请输入时间戳"
                    return@setOnClickListener
                }

                val instance = Calendar.getInstance()
                instance.timeInMillis = text.toString().toLong()
                mYear = instance.get(Calendar.YEAR)
                mMonth = instance.get(Calendar.MONTH)
                mDay = instance.get(Calendar.DATE)
                mHour = instance.get(Calendar.HOUR_OF_DAY)
                mMinute = instance.get(Calendar.MINUTE)
                mSecond = instance.get(Calendar.SECOND)
                
                editDate?.setText("${DateTimeHelper.getDateStringByDate(instance.time)}")
                editTime?.setText("${DateTimeHelper.getTimeStringByDate(instance.time)}")
            }
        }
    }

   ...
}

```

##### DateTimeHelper.kt

```kotlin
package com.lujianfei.plugin8_1

import android.icu.util.Calendar
import java.text.SimpleDateFormat
import java.util.*

/**
 *@date     创建时间:2020/6/12
 *@name     作者:陆键霏
 *@describe 描述:
 */
object DateTimeHelper {
    
    fun getTimeStampBy(year: Int, month: Int, date: Int, hour: Int, min: Int, second: Int): Long {
        val instance = Calendar.getInstance()
        instance.set(Calendar.YEAR, year)
        instance.set(Calendar.MONTH, month)
        instance.set(Calendar.DATE, date)
        instance.set(Calendar.HOUR_OF_DAY, hour)
        instance.set(Calendar.MINUTE, min)
        instance.set(Calendar.SECOND, second)
        instance.set(Calendar.MILLISECOND, 0)
        return instance.timeInMillis
    }

    fun getDateStringByDate(date:Date): String {
        return SimpleDateFormat("yyyy-MM-dd").format(date)
    }

    fun getTimeStringByDate(date:Date): String {
        return SimpleDateFormat("HH:mm:ss").format(date)
    }
}
```

DatePickerPopupWindow.kt

```kotlin
package com.lujianfei.plugin8_1.widget

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.DatePicker
import android.widget.PopupWindow
import com.lujianfei.plugin8_1.R


/**
 *@date     创建时间:2020/6/12
 *@name     作者:陆键霏
 *@describe 描述:
 */
class DatePickerPopupWindow(context: Context?,year:Int,month:Int,day:Int) : PopupWindow(context) {

    private var datepicker: DatePicker? = null
    private var mYear = 0
    private var mMonth = 0
    private var mDay = 0
    
    var onDateCallback:((Int,Int,Int)->Unit) ?= null
    
    init {
        contentView = LayoutInflater.from(context).inflate(R.layout.widget_datepicker, null, false)
        isOutsideTouchable = true
        val cd = ColorDrawable(0xdd000000.toInt())
        setBackgroundDrawable(cd)

        mYear = year
        mMonth = month
        mDay = day
        initView()
        initEvent()
    }

    private fun initView() {
        datepicker = findViewById(R.id.datepicker)
    }

    private fun initEvent() {
        datepicker?.init(mYear,
                mMonth,
                mDay) { view, year, monthOfYear, dayOfMonth ->
//            Toast.makeText(contentView?.context, "$year\t$monthOfYear\t$dayOfMonth", Toast.LENGTH_SHORT).show()
            onDateCallback?.invoke(year,monthOfYear,dayOfMonth)
            dismiss()
        }
    }
    
    private fun <T:View> findViewById(id:Int):T? {
        return contentView?.findViewById(id)
    }
}
```



widget_datepicker.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content">

    <DatePicker
        android:id="@+id/datepicker"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="#ffffff"/>
</RelativeLayout>
```



TimePickerPopupWindow.kt

```kotlin
package com.lujianfei.plugin8_1.widget

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.PopupWindow
import android.widget.TimePicker
import com.lujianfei.plugin8_1.R


/**
 *@date     创建时间:2020/6/12
 *@name     作者:陆键霏
 *@describe 描述:
 */
class TimePickerPopupWindow(context: Context?, hour:Int, minute:Int) : PopupWindow(context) {

    private var timepicker: TimePicker? = null
    private var btConfirm: Button? = null
    private var mHour = 0
    private var mMinute = 0
    
    var onTimeCallback:((Int,Int)->Unit) ?= null
    
    init {
        contentView = LayoutInflater.from(context).inflate(R.layout.widget_timepicker, null, false)
        isOutsideTouchable = true
        val cd = ColorDrawable(0xdd000000.toInt())
        setBackgroundDrawable(cd)

        mHour = hour
        mMinute = minute
        initView()
        initEvent()
    }

    private fun initView() {
        timepicker = findViewById(R.id.timepicker)
        btConfirm = findViewById(R.id.btConfirm)
    }

    private fun initEvent() {
        timepicker?.hour = mHour
        timepicker?.minute = mMinute
        timepicker?.setOnTimeChangedListener { view, hourOfDay, minute ->
            mHour = hourOfDay
            mMinute = minute
        }
        btConfirm?.setOnClickListener {
            onTimeCallback?.invoke(mHour,mMinute)
            dismiss()
        }
    }
    
    private fun <T:View> findViewById(id:Int):T? {
        return contentView?.findViewById(id)
    }
}
```



widget_timepicker.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content">

    <TimePicker
        android:id="@+id/timepicker"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="#ffffff"/>
    <Button
        android:id="@+id/btConfirm"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentEnd="true"
        android:text="确认"/>
</RelativeLayout>
```

