# 自定义Dialog 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <Button
        android:id="@+id/btClick"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textColor="#333333"
        android:text="弹出对话框"/>
</LinearLayout>
```

##### dialog_normal_layout.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:tools="http://schemas.android.com/tools"
    android:orientation="vertical">

    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="40dp"
        tools:background="@color/theme_color">
        <TextView
            android:id="@+id/title"
            android:layout_width="wrap_content"
            android:layout_height="match_parent"
            tools:textColor="@color/text_color"
            android:layout_marginStart="10dp"
            android:gravity="center"
            android:textSize="18sp"
            tools:text =  "提示" />

    </RelativeLayout>

    <LinearLayout
        android:id="@+id/content"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">
        <TextView
            android:id="@+id/message"
            android:layout_width="match_parent"
            android:layout_height="100dp"
            android:gravity="center"
            tools:text="Content"
            android:textSize="20sp"
            />
    </LinearLayout>
    <LinearLayout
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="end"
        android:orientation="horizontal"
        android:paddingEnd="10dp">
        <TextView
            android:id="@+id/txtPositive"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="center"
            android:textSize="18sp"
            android:padding="10dp"
            tools:text="确认"
            />
        <TextView
            android:id="@+id/txtNegative"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="center"
            android:layout_marginStart="10dp"
            android:textSize="18sp"
            android:padding="10dp"
            tools:text="取消"
            />
    </LinearLayout>
</LinearLayout>
```

##### CustomDialog.kt

```kotlin
package com.lujianfei.plugin3_1

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import com.lujianfei.module_plugin_base.utils.ResUtils


class CustomDialog private constructor(context: Context) : Dialog(context) {

    class Builder(private val context: Context) {
        private var title: String? = null
        private var message: String? = null
        private var positiveButtonText: String? = null
        private var negativeButtonText: String? = null
        private var contentView: View? = null
        private var positiveButtonClickListener: DialogInterface.OnClickListener? = null
        private var negativeButtonClickListener: DialogInterface.OnClickListener? = null
        fun setMessage(message: String?): Builder {
            this.message = message
            return this
        }

        /**
         * Set the Dialog message from resource
         *
         * @param
         * @return
         */
        fun setMessage(message: Int): Builder {
            this.message = context.getText(message) as String
            return this
        }

        /**
         * Set the Dialog title from resource
         *
         * @param title
         * @return
         */
        fun setTitle(title: Int): Builder {
            this.title = context.getText(title) as String
            return this
        }

        /**
         * Set the Dialog title from String
         *
         * @param title
         * @return
         */
        fun setTitle(title: String?): Builder {
            this.title = title
            return this
        }

        fun setContentView(v: View?): Builder {
            contentView = v
            return this
        }

        /**
         * Set the positive button resource and it's listener
         *
         * @param positiveButtonText
         * @return
         */
        fun setPositiveButton(positiveButtonText: Int,
                              listener: DialogInterface.OnClickListener?): Builder {
            this.positiveButtonText = context
                    .getText(positiveButtonText) as String
            positiveButtonClickListener = listener
            return this
        }

        fun setPositiveButton(positiveButtonText: String?,
                              listener: DialogInterface.OnClickListener?): Builder {
            this.positiveButtonText = positiveButtonText
            positiveButtonClickListener = listener
            return this
        }

        fun setNegativeButton(negativeButtonText: Int,
                              listener: DialogInterface.OnClickListener?): Builder {
            this.negativeButtonText = context
                    .getText(negativeButtonText) as String
            negativeButtonClickListener = listener
            return this
        }

        fun setNegativeButton(negativeButtonText: String?,
                              listener: DialogInterface.OnClickListener?): Builder {
            this.negativeButtonText = negativeButtonText
            negativeButtonClickListener = listener
            return this
        }

        fun create(): CustomDialog {
            val inflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            // instantiate the dialog with the custom Theme
            val dialog = CustomDialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            val layout: View = inflater.inflate(R.layout.dialog_normal_layout, null)
            dialog.addContentView(layout, LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT))
            // set the dialog title
            val txtTitle = layout.findViewById(R.id.title) as TextView
            ResUtils.getPluginColor(R.color.theme_color)?.let {
                (txtTitle.parent as View).setBackgroundColor(it)
            }
            ResUtils.getPluginColor(R.color.text_color)?.let {
                txtTitle.setTextColor(it)
            }
            txtTitle.text = title
            // set the confirm button
            if (positiveButtonText != null) {
                val textView = layout.findViewById(R.id.txtPositive) as TextView
                textView.text = positiveButtonText
                ResUtils.getPluginColor(R.color.theme_color)?.let {
                    textView.setTextColor(it)
                }
                if (positiveButtonClickListener != null) {
                    textView
                            .setOnClickListener {
                                positiveButtonClickListener?.onClick(dialog,
                                        DialogInterface.BUTTON_POSITIVE)
                            }
                }
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById<View>(R.id.txtPositive).visibility = View.GONE
            }
            // set the cancel button
            if (negativeButtonText != null) {
                val textView = layout.findViewById(R.id.txtNegative) as TextView
                textView.text = negativeButtonText
                if (negativeButtonClickListener != null) {
                    textView
                            .setOnClickListener {
                                    negativeButtonClickListener?.onClick(dialog,
                                            DialogInterface.BUTTON_NEGATIVE)
                                }
                }
            } else {
                // if no confirm button just set the visibility to GONE
                layout.findViewById<View>(R.id.txtNegative).visibility = View.GONE
            }
            // set the content message
            if (message != null) {
                (layout.findViewById(R.id.message) as TextView).text = message
            } else if (contentView != null) {
                // if no message set
                // add the contentView to the dialog body
                (layout.findViewById(R.id.content) as LinearLayout)
                        .removeAllViews()
                (layout.findViewById(R.id.content) as LinearLayout)
                        .addView(contentView, LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT))
            }
            dialog.setContentView(layout)

            val dialogWindow = dialog.window
            val lp = dialogWindow?.attributes
            val d = context.resources.displayMetrics // 获取屏幕宽、高用
            lp?.width = (d.widthPixels * 0.8).toInt() // 宽度设置为屏幕的0.8
            dialogWindow?.attributes = lp
            return dialog
        }

    }
}
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin3_1

import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

   ...
    private var btClick: View? = null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
       ...
        btClick = findViewById(R.id.btClick)
       ...
    }

   ...
    override fun initEvent() {
       ...
        btClick?.setOnClickListener {
            that?.let { that ->
                CustomDialog.Builder(that).setTitle("提示")
                        .setMessage("你想变得牛逼么")
                        .setPositiveButton("是的", DialogInterface.OnClickListener { dialog, _ ->
                            dialog.dismiss()
                        })
                        .setNegativeButton("不想", DialogInterface.OnClickListener { dialog, _ ->
                            dialog.dismiss()
                        })
                        .create()
                        .show()
            }
        }
    }

   ...
}

```
