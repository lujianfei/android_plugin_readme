# RecyclerView 下拉刷新 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    // SwipeRefreshLayout
    implementation "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <androidx.swiperefreshlayout.widget.SwipeRefreshLayout
        android:id="@+id/swipeRefreshLayout"
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <androidx.recyclerview.widget.RecyclerView
            android:id="@+id/recyclerview"
            android:layout_width="match_parent"
            android:layout_height="match_parent"/>
    </androidx.swiperefreshlayout.widget.SwipeRefreshLayout>
</LinearLayout>
```

##### adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    android:padding="5dp">
    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title"
        android:textSize="20sp"
        />

    <TextView
        android:id="@+id/summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="10dp"
        android:text="summary"
        android:ellipsize="end"
        android:maxLines="2"
        android:textSize="18sp"
        />
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_8

import android.content.Intent
import android.net.Uri
import android.os.Handler
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin1_8.adapter.MainAdapter
import com.lujianfei.plugin1_8.bean.MainBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    private var recyclerview:RecyclerView ?= null
    private var swipeRefreshLayout:SwipeRefreshLayout ?= null
    private var mHandler = Handler()
    private val mAdapter by lazy { MainAdapter() }

    override fun resouceId(): Int = R.layout.activity_main
    
    override fun initView() {        
        recyclerview = findViewById(R.id.recyclerview)
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout)
        recyclerview?.apply {
            layoutManager = LinearLayoutManager(that)   
            adapter = mAdapter
            addItemDecoration(DividerItemDecoration(that, DividerItemDecoration.VERTICAL))
        }
        toolbar?.setTitle(getPluginString(R.string.app_name))
    }

    override fun initData() {        
        val mData = arrayListOf<MainBean>()
        for (idx in 0 until 20) {
            mData.add(MainBean(title = "码农宝标题 $idx", summary = "码农宝简介：主要功能：\n" +
                    "- 快速查看安卓设备信息 （手机屏幕分辨率，手机型号，设备id, 可用内存等等）\n" +
                    "- 各类开发过程中常用代码及效果 Demo, 分别有\n" +
                    "- 列表, 容器，对话框，动画，翻页，图表，编码及算法，多媒体，传感器，实用工具等 $idx"))
        }
        mAdapter.setData(mData)
    }

    override fun initEvent() {       
        swipeRefreshLayout?.setOnRefreshListener {
            refresh()
        }
    }

    private fun refresh() {
        mHandler.postDelayed(runnableTask, 1000)
    }

    private val runnableTask = {
        val mData = arrayListOf<MainBean>()
        for (idx in 0 until 20) {
            mData.add(MainBean(title = "码农宝标题 $idx - ${(1..9).random()}", summary = "码农宝简介：主要功能：\n" +
                    "- 快速查看安卓设备信息 （手机屏幕分辨率，手机型号，设备id, 可用内存等等）\n" +
                    "- 各类开发过程中常用代码及效果 Demo, 分别有\n" +
                    "- 列表, 容器，对话框，动画，翻页，图表，编码及算法，多媒体，传感器，实用工具等"))
        }
        mAdapter.setData(mData)
        Toast.makeText(that, "刷新成功", Toast.LENGTH_SHORT).show()
        swipeRefreshLayout?.isRefreshing = false
    }

    override fun onPluginDestroy() {
        mHandler.removeCallbacks(runnableTask)
        super.onPluginDestroy()
    }
}

```

##### MainAdapter.kt

```kotlin
package com.lujianfei.plugin1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class MainAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mData = arrayListOf<MainBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_main, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(mData: ArrayList<MainBean>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView? = null
        var summary: TextView? = null

        init {
            title = itemView.findViewById(R.id.title)
            summary = itemView.findViewById(R.id.summary)
        }

        fun setData(mainBean: MainBean) {
            title?.text = mainBean.title
            summary?.text = mainBean.summary
        }
    }
}
```

##### MainBean.kt

```kotlin
package com.lujianfei.plugin1

data class MainBean(
        var title: String,
        var summary: String
)
```

