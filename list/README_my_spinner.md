# 自定义下拉列表 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <com.lujianfei.plugin1_9.MySpinner
        android:id="@+id/mySpinner"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center_horizontal"
        android:layout_marginTop="20dp"
        />
</LinearLayout>
```

##### MainActivity 主界面代码

```kotlin
package com.lujianfei.plugin1_9

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    ...
    
    private var mySpinner:MySpinner ?= null

    override fun resouceId(): Int = R.layout.activity_main
    
    override fun initView() {
        ...
        mySpinner = findViewById(R.id.mySpinner)
	    ...
    }

    override fun initData() {
        ...
        mySpinner?.items = arrayListOf("深圳市", "广州市" , "北京市" ,"上海市")
    }

    ...
}

```



##### 核心组件类 MySpinner

```kotlin
package com.lujianfei.plugin1_9

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.PopupWindow
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.ResUtils

/**
 *@date     创建时间:2020/9/15
 *@name     作者:陆键霏
 *@describe 描述:
 */
class MySpinner @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    private var spinner_item:TextView ?= null
    private var ic_dropdown:ImageView ?= null
    
    var items:List<String> ?= null
    set(value) {
        field = value
        spinner_item?.text = value!![0]
    }
    
    init {
        LayoutInflater.from(context).inflate(R.layout.widget_my_spinner,this)
        ic_dropdown = findViewById(R.id.ic_dropdown)
        spinner_item = findViewById(R.id.spinner_item)
        context.getDrawable(R.drawable.ic_dropdown)?.let {
            ic_dropdown?.setImageDrawable(it)
        }
        context.getDrawable(R.drawable.layer_spinner)?.let{
            (ic_dropdown?.parent as View).background = it    
        }
        setOnClickListener {
            val mySpinnerPopupWindow = MySpinnerPopupWindow(context, this.items!!)
            mySpinnerPopupWindow.onItemClickListener = {
                spinner_item?.text = it
                mySpinnerPopupWindow.dismiss()
            }
            mySpinnerPopupWindow.setOnDismissListener {
                ic_dropdown?.rotation = 0f // 方向标还原
            }
            ic_dropdown?.rotation = 180f // 方向标翻转180
            // 加入下拉列表位置偏移调整
            mySpinnerPopupWindow.showAsDropDown(spinner_item, DensityUtils.dip2px(-15f), DensityUtils.dip2px(10f))
        }
    }
    
    class MySpinnerPopupWindow(context: Context?,items:List<String>) : PopupWindow(context) {
        
        var recyclerview:RecyclerView ?= null
        var mAdapter = MySpinnerAdapter()
        var items:List<String> ?= null
        var onItemClickListener:((String)->Unit) ?= null
        init {
            this.items = items
            val popupView = LayoutInflater.from(context).inflate(R.layout.popup_myspinner, null)
            recyclerview = popupView.findViewById(R.id.recyclerview)
            (recyclerview?.parent as View).background = context?.getDrawable(R.drawable.layer_dropdown)
            contentView = popupView
            setBackgroundDrawable(ColorDrawable(0x00000000))
            isOutsideTouchable = true
            isFocusable = true

            recyclerview?.apply { 
                layoutManager = LinearLayoutManager(context)
                adapter = mAdapter
            }
            mAdapter.onItemClickListener = {
                onItemClickListener?.invoke(it)
            }
            mAdapter.setData(items)
        }
    }
    
    class MySpinnerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        var mData = arrayListOf<String>()
        var onItemClickListener:((String)->Unit) ?= null

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.simple_spinner_dropdown_item, parent, false))
        }

        override fun getItemCount(): Int = mData.size

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            val str = mData[position]
            if (holder is MyViewHolder) {
                holder.setData(str)
            }
            holder.itemView.setOnClickListener {
                onItemClickListener?.invoke(str)
            }
        }

        fun setData(mData: List<String>) {
            this.mData.clear()
            this.mData.addAll(mData)
            notifyDataSetChanged()
        }

        class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            private var title: TextView? = null

            init {
                title = itemView.findViewById(android.R.id.text1)
            }

            fun setData(str: String) {
                title?.text = str
            }
        }
    }
}
```

##### layout 下widget_my_spinner.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    xmlns:tools="http://schemas.android.com/tools">

    <TextView
        android:id="@+id/spinner_item"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:minWidth="40dp"
        android:textSize="20sp"
        android:layout_centerVertical="true"
        android:textColor="#272727"
        android:text="男"
        />

    <ImageView
        android:id="@+id/ic_dropdown"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_centerVertical="true"
        android:layout_marginEnd="5dp"
        android:layout_toEndOf="@id/spinner_item"
        tools:src="@drawable/ic_dropdown"
        />
</RelativeLayout>
```

##### drawable 下的 layer_spinner.xml 用于控制选中后的 Spinner 样式

```xml
<?xml version="1.0" encoding="utf-8"?>
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">

    <item>
        <shape
            android:shape="rectangle">
            <solid android:color="#ffffff"/>
            <corners android:radius="10dp"/>
            <padding android:left="15dp" android:top="5dp" android:bottom="5dp" android:right="5dp"/>
            <stroke
                android:width="1dp"
                android:color="#dddddd"/>
        </shape>
    </item>
</layer-list>
```

##### layout 下的 popup_myspinner.xml，用于显示 Spinner 下拉列表样式

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    >
    <FrameLayout
        android:id="@+id/listContainer"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:paddingStart="15dp"
        android:paddingTop="5dp"
        android:paddingEnd="5dp"
        android:paddingBottom="5dp">
        <androidx.recyclerview.widget.RecyclerView
            android:id="@+id/recyclerview"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            />
    </FrameLayout>
</RelativeLayout>
```

##### layout 下的 simple_spinner_dropdown_item.xml，用于控制下拉列表 item 的样式

```xml
<?xml version="1.0" encoding="utf-8"?>
<!--
/* //device/apps/common/assets/res/any/layout/simple_spinner_item.xml
**
** Copyright 2008, The Android Open Source Project
**
** Licensed under the Apache License, Version 2.0 (the "License")
** you may not use this file except in compliance with the License.
** You may obtain a copy of the License at
**
**     http://www.apache.org/licenses/LICENSE-2.0
**
** Unless required by applicable law or agreed to in writing, software
** distributed under the License is distributed on an "AS IS" BASIS,
** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
** See the License for the specific language governing permissions and
** limitations under the License.
*/
-->
<CheckedTextView xmlns:android="http://schemas.android.com/apk/res/android"
    android:id="@android:id/text1"
    android:singleLine="true"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:minWidth="100dp"
    android:minHeight="30dp"
    android:textColor="#272727"
    android:textSize="20sp"
    android:gravity="center_vertical"
    />

```

