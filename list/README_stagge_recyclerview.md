# android 瀑布流 RecyclerView 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
apply plugin: 'kotlin-kapt'  // 顶上加上这个插件引用
...

dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    // glide
    implementation 'com.github.bumptech.glide:glide:4.9.0'
    kapt 'com.github.bumptech.glide:compiler:4.9.0'
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    android:padding="5dp">
    <ImageView
        android:id="@+id/icon"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>
    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="5dp"
        android:text="Title"
        android:ellipsize="end"
        android:maxLines="1"
        android:textSize="20sp"
        />

    <TextView
        android:id="@+id/summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="summary"
        android:ellipsize="end"
        android:maxLines="1"
        android:textSize="18sp"
        />
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_3

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
  
    private var recyclerview: RecyclerView? = null
    private val adapter by lazy { MainAdapter() }

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {        
        recyclerview = findViewById(R.id.recyclerview)
        val staggeredGridLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
        staggeredGridLayoutManager.gapStrategy = StaggeredGridLayoutManager.GAP_HANDLING_NONE
        recyclerview?.layoutManager = staggeredGridLayoutManager
        toolbar?.setTitle(getPluginString(R.string.app_name))
    }

    override fun initData() {        
        recyclerview?.adapter = adapter
        val mData = arrayListOf<MainBean>()
        for (idx in 0 until ImageMock.urls.size) {
            mData.add(MainBean(title = "码农宝标题 $idx", summary = "码农宝简介：主要功能：\n" +
                    "- 快速查看安卓设备信息 （手机屏幕分辨率，手机型号，设备id, 可用内存等等）\n" +
                    "- 各类开发过程中常用代码及效果 Demo, 分别有\n" +
                    "- 列表, 容器，对话框，动画，翻页，图表，编码及算法，多媒体，传感器，实用工具等 $idx",
                    icon = ImageMock.urls[idx]))
        }
        adapter.setData(mData)
    }
}

```

##### MainAdapter.kt

```kotlin
package com.lujianfei.plugin1_3

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lujianfei.module_plugin_base.utils.ResUtils
import java.util.*

class MainAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mData = arrayListOf<MainBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_main, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(mData: ArrayList<MainBean>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView? = null
        var summary: TextView? = null
        var icon: ImageView? = null

        init {
            title = itemView.findViewById(R.id.title)
            summary = itemView.findViewById(R.id.summary)
            icon = itemView.findViewById(R.id.icon)
        }

        fun setData(mainBean: MainBean) {
            icon?.let {icon->
                Glide.with(icon.context)
                        .load(mainBean.icon)
                        .placeholder(ResUtils.getPluginDrawable(R.drawable.ic_default_image))
                        .error(ResUtils.getPluginDrawable(R.drawable.ic_default_image))
                        .into(icon)
            }
            title?.text = mainBean.title
            summary?.text = mainBean.summary
        }
    }
}
```

##### MainBean.kt

```kotlin
package com.lujianfei.plugin3

data class MainBean(
        var title: String,
        var summary: String,
        var icon:String
)
```

##### ImageMock.kt

```kotlin
package com.lujianfei.plugin3

object ImageMock {
    val urls = arrayListOf(
            "https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=3344832883,2462232837&fm=26&gp=0.jpg",
            "https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3931439360,1525397146&fm=26&gp=0.jpg",
            "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2655406508,1191616699&fm=26&gp=0.jpg",
            "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=2829616367,3321707906&fm=26&gp=0.jpg",
            "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1913474091,1373885953&fm=26&gp=0.jpg",
            "https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=1619362632,3737554689&fm=26&gp=0.jpg",
            "https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2565689134,788370536&fm=26&gp=0.jpg"
            )
}
```

