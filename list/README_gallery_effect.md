# android 画廊效果 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    // youth banner
    implementation  'com.youth.banner:banner:2.0.10'
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:id="@+id/rootView"
    tools:context=".MainActivity">
    
    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        />
</LinearLayout>
```

##### adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity"
    android:orientation="vertical"
    >
    <ImageView
        android:id="@+id/icon"
        android:layout_centerHorizontal="true"
        android:layout_width="300dp"
        android:layout_height="300dp"
        tools:src="@mipmap/ic_launcher"
        />
</RelativeLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_13

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.Constant
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin1_13.adapter.MainAdapter
import com.lujianfei.plugin1_13.bean.MainBean
import com.youth.banner.Banner


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    private var rootView: LinearLayout?= null
    private var banner:Banner<*,*> ?= null
    private val mAdapter by lazy { MainAdapter() }

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        that?.let {
            banner = Banner<MainBean,MainAdapter>(it)
        }
        rootView = findViewById(R.id.rootView)
        rootView?.let {
            it.addView(banner)
        }
        banner?.apply {
            isAutoLoop(false)
            // 设置画廊效果
            setBannerGalleryEffect(40,50,0.5f)
            adapter = mAdapter
        }
    }

    override fun initData() {
        val mData = arrayListOf<MainBean>()
        for (i in 0 until 10) {
            mData.add(MainBean(resId = R.mipmap.ic_launcher))
        }
        mAdapter.setDatas(mData)
    }
}

```



##### MainAdapter.kt

```kotlin
package com.lujianfei.plugin1_13.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.plugin1_13.R
import com.lujianfei.plugin1_13.bean.MainBean
import com.youth.banner.adapter.BannerAdapter

class MainAdapter : BannerAdapter<MainBean,RecyclerView.ViewHolder>(null) {

    override fun onCreateHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.adapter_main, parent, false))
    }

    override fun onBindView(
        holder: RecyclerView.ViewHolder?,
        data: MainBean?,
        position: Int,
        size: Int
    ) {
        if (holder is MyViewHolder) {
            holder.setData(data)
        }
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var icon: ImageView? = null
        init {
            icon = itemView.findViewById(R.id.icon)
        }

        fun setData(mainBean: MainBean?) {
            if (mainBean == null) return
            icon?.setImageDrawable(ResUtils.getPluginDrawable(mainBean.resId))
        }
    }
}
```

