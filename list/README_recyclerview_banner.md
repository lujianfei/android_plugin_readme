# android RecyclerView + Banner 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    // youth banner
    implementation  'com.youth.banner:banner:2.0.10'
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    >
    <TextView
        android:id="@+id/title"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Title"
        android:textColor="#000"
        android:layout_marginStart="5dp"
        android:textSize="20sp"
        />

    <com.youth.banner.Banner
        android:id="@+id/myBanner"
        android:layout_below="@id/title"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>

    <TextView
        android:id="@+id/pageInfo"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignTop="@id/myBanner"
        android:layout_alignParentEnd="true"
        android:layout_marginEnd="5dp"
        android:textColor="#fff"
        android:textSize="20sp"
        android:background="#ff000000"
        />
</RelativeLayout>
```

##### adapter_banner.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="#000000">
</RelativeLayout>
```



##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_12

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.Constant
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin1_12.adapter.MainAdapter
import com.lujianfei.plugin1_12.bean.MainBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    private var recyclerview:RecyclerView ?= null
    private val mAdapter by lazy { MainAdapter() }

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply {
            layoutManager = LinearLayoutManager(that)
            addItemDecoration(DividerItemDecoration(that, DividerItemDecoration.VERTICAL))
            adapter = mAdapter
        }
    }

    override fun initData() {
        val mData = arrayListOf<MainBean>()
        val remoteUrls = arrayListOf(
            "${Constant.Http.baseUrl}/json/mnb/img/img1.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img2.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img3.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img3.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img4.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img5.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img6.jpeg",
            "${Constant.Http.baseUrl}/json/mnb/img/img7.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img8.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img9.jpg",
            "${Constant.Http.baseUrl}/json/mnb/img/img10.jpg",
        )
        for (idx in 0 until 3) {
            val newIdx = (idx * 3) % remoteUrls.size
            if (newIdx + 3 < remoteUrls.size ) {
                val imgList = remoteUrls.subList(newIdx, newIdx + 3)
                mData.add(
                    MainBean(
                        title = "码农宝 Banner ${idx+1}",
                        summary = "",
                        imgList = imgList
                    )
                )
            }
        }
        mAdapter.setData(mData)
    }
}

```

##### MainAdapter.kt

```kotlin
package com.lujianfei.plugin1_12.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.plugin1_12.R
import com.lujianfei.plugin1_12.bean.MainBean
import com.youth.banner.Banner
import com.youth.banner.adapter.BannerAdapter
import com.youth.banner.listener.OnPageChangeListener
import java.util.*

class MainAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mData = arrayListOf<MainBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_main, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(mData: ArrayList<MainBean>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView? = null
        var pageInfo: TextView? = null
        /**
         * Banner 组件
         */
        var myBanner:Banner<String,MyBannerAdapter> ?= null
        /**
         * Banner Adapter
         */
        private val mAdapter by lazy { MyBannerAdapter() }
        init {
            title = itemView.findViewById(R.id.title)
            pageInfo = itemView.findViewById(R.id.pageInfo)
            myBanner = itemView.findViewById(R.id.myBanner)
            myBanner?.isAutoLoop(false)
            myBanner?.adapter = mAdapter
        }

        fun setData(mainBean: MainBean) {
            title?.text = mainBean.title
            myBanner?.addOnPageChangeListener(
                    object : OnPageChangeListener{
                        override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                            LogUtils.d("MainAdapter", "onPageScrolled position:$position")
                            getPageInfo(position,mainBean.imgList.size)
                        }

                        override fun onPageSelected(position: Int) {
                            LogUtils.d("MainAdapter", "onPageSelected position:$position")
                            getPageInfo(position,mainBean.imgList.size)
                        }

                        override fun onPageScrollStateChanged(state: Int) {
                        }
                    }
            )
            myBanner?.setDatas(mainBean.imgList)
        }

        fun getPageInfo(position:Int, totalSize:Int) {
            pageInfo?.text = "${position + 1}/${totalSize}"
        }
    }

    class MyBannerAdapter: BannerAdapter<String, MyBannerViewHolder>(null) {

        override fun onCreateHolder(parent: ViewGroup?, viewType: Int): MyBannerViewHolder {
            return MyBannerViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.adapter_banner,parent,false))
        }

        override fun onBindView(
            holder: MyBannerViewHolder?,
            data: String?,
            position: Int,
            size: Int
        ) {
            holder?.bindData(data)
        }
    }

    class MyBannerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var bannerImage: ImageView?= null

        init {
            bannerImage = ImageView(itemView.context)
            // Banner 写死高度
            bannerImage?.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,DensityUtils.dip2px(600f))
            (itemView as ViewGroup).addView(bannerImage)
        }

        fun bindData(image:String?) {
            if (image == null) return
            bannerImage?.let {
                // 加载网络图片
                Glide.with(itemView.context).load(image).into(it)
            }
        }
    }
}
```

##### MainBean.kt

```kotlin
package com.lujianfei.plugin1_12.bean

data class MainBean(
        var title: String,
        var summary: String,
        var imgList:List<String>
)
```

