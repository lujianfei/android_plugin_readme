# 仿微信下拉小程序效果 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    // glide
    implementation 'com.github.bumptech.glide:glide:4.9.0'
    kapt 'com.github.bumptech.glide:compiler:4.9.0'
    // Gson
    implementation 'com.google.code.gson:gson:2.8.6'
}
```

#### fragment_wechat.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android">
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
        <androidx.viewpager2.widget.ViewPager2
            android:id="@+id/viewpager"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:overScrollMode="never"
            android:clipToPadding="false"
            android:orientation="vertical"
            />
    </LinearLayout>
</layout>
```



```kotlin
package com.lujianfei.other.ui.wechat

import android.animation.ArgbEvaluator
import android.util.Log
import androidx.viewpager2.widget.ViewPager2
import com.gyf.immersionbar.ImmersionBar
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.other.R
import com.lujianfei.other.databinding.FragmentWechatBinding
import com.lujianfei.other.ui.wechat.adapter.TwoLevelAdapter
import com.lujianfei.other.ui.wechat.fragment.BottomFragment
import com.lujianfei.other.ui.wechat.fragment.TopFragment
import com.lujianfei.other.ui.wechat.transformer.TwoLevelTransformer

/**
 * Author: mac
 * Date: 8.7.22 11:56 上午
 * Email:johnson@miaoshitech.com
 * Description: 高仿微信小程序交互
 */
class WechatFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    companion object {
        val START_COLOR = ResUtils.getThemeColor()!!
        val END_COLOR = 0xff000000.toInt()
    }

    var binding: FragmentWechatBinding? = null

    /**
     * 用作标题栏颜色渐变
     */
    private val evaluator = ArgbEvaluator()

    override fun initView() {
        binding = view?.let { FragmentWechatBinding.bind(it) }
        val mAdapter = TwoLevelAdapter(this@WechatFragment)
        val bottomFragment = BottomFragment()
        bottomFragment.arguments = arguments
        mAdapter.addFragment(TopFragment())
        mAdapter.addFragment(bottomFragment)
        binding?.viewpager?.apply {
            adapter = mAdapter
            setPageTransformer(TwoLevelTransformer(this))
            setCurrentItem(1, false)
            registerOnPageChangeCallback(object: ViewPager2.OnPageChangeCallback() {
                override fun onPageScrolled(
                    position: Int,
                    positionOffset: Float,
                    positionOffsetPixels: Int
                ) {
                    super.onPageScrolled(position, positionOffset, positionOffsetPixels)
                    if (position == 0) {
                        val screenH = DensityUtils.getScreenHeight()?:0
                        val statusBarColor = if (positionOffsetPixels < 0) {
                            END_COLOR
                        } else if (positionOffsetPixels > screenH) {
                            START_COLOR
                        } else {
                            evaluator.evaluate(positionOffsetPixels.toFloat() / screenH.toFloat(), END_COLOR, START_COLOR).toString()
                                .toInt()
                        }
                        // 动态控制状态栏背景色
                        activity?.window?.statusBarColor = statusBarColor
                    }
                    Log.d("WechatFragment=", "onPageScrolled position:$position,positionOffsetPixels:$positionOffsetPixels,positionOffset:$positionOffset")
                }
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)
                    when (position) {
                        0-> {
                            // 下拉到小程序页时，禁用 消息列表页的事件处理
                            bottomFragment.disableRecyclerViewTouch = true
                        }
                        1-> {
                            // 上拉到消息列表页时，开放 消息列表页的事件处理
                            bottomFragment.disableRecyclerViewTouch = false
                        }
                    }
                }
            })
        }
    }

    override fun initData() {
    }

    override fun initEvent() {
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }

    override fun resouceId() = R.layout.fragment_wechat

}
```



#### **TopFragment.kt** 小程序列表

```kotlin
package com.lujianfei.other.ui.wechat.fragment

import androidx.lifecycle.ViewModelProvider
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.other.R
import com.lujianfei.other.databinding.FragmentTopBinding
import com.lujianfei.other.ui.wechat.viewmodel.TopFragmentViewModel

/**
 * Author: mac
 * Date: 8.7.22 2:46 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */
class TopFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    private var binding: FragmentTopBinding? = null
    private var mTopFragmentViewModel: TopFragmentViewModel?= null

    override fun initView() {
        mTopFragmentViewModel = ViewModelProvider(this).get(TopFragmentViewModel::class.java)
        binding = view?.let { FragmentTopBinding.bind(it) }
        binding?.vm = mTopFragmentViewModel
    }

    override fun initData() {
        mTopFragmentViewModel?.requestData()
    }

    override fun initEvent() {
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }

    override fun resouceId() = R.layout.fragment_top
}
```

#### **fragment_top.xml** 小程序列表布局

```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android">
    <data>
        <variable
            name="vm"
            type="com.lujianfei.other.ui.wechat.viewmodel.TopFragmentViewModel" />
    </data>
    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <com.lujianfei.other.ui.wechat.widget.CustomGridRecyclerView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:background="#000000"
            listdata="@{vm.list}"
            itemClick="@{vm.itemClick}"
            />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
```

#### **TopFragmentViewModel.kt**

```kotlin
package com.lujianfei.other.ui.wechat.viewmodel

import androidx.databinding.ObservableArrayList
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lujianfei.other.ui.wechat.model.BottomListModal
import com.lujianfei.other.ui.wechat.model.TopListModal

/**
 * Author: mac
 * Date: 11.7.22 6:33 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */
class TopFragmentViewModel : ViewModel() {

    val list = ObservableArrayList<TopListModal>()

    val itemClickLiveData = MutableLiveData<TopListModal>()

    val itemClick: ((resId: Int, bean: TopListModal) -> Unit) = { resId, bean ->
        itemClickLiveData.value = bean
    }

    fun requestData() {
        val colors =
            arrayOf(0xffff0000.toInt(), 0xff00ff00.toInt(), 0xff0000ff.toInt(), 0xff00ffff.toInt())
        for (i in 0 until 50) {
            list.add(
             TopListModal(
                    iconColor = colors[i % colors.size],
                    title = "小程序 ${i + 1}",
                    summary = "描述 ${i + 1}"
                )
            )
        }
    }
}
```



#### **BottomFragment.kt** 消息列表

```kotlin
package com.lujianfei.other.ui.wechat.fragment

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.other.R
import com.lujianfei.other.databinding.FragmentBottomBinding
import com.lujianfei.other.databinding.FragmentTopBinding
import com.lujianfei.other.ui.wechat.viewmodel.BottomFragmentViewModel

/**
 * Author: mac
 * Date: 8.7.22 2:46 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */
class BottomFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    var binding: FragmentBottomBinding? = null

    var disableRecyclerViewTouch = false
    set(value) {
        binding?.recyclerview?.disableRecyclerViewTouch = value
        field = value
    }

    private var bean: PluginActivityBean?= null

    private var mBottomFragmentViewModel: BottomFragmentViewModel ?= null

    override fun initView() {
        mBottomFragmentViewModel = ViewModelProvider(this).get(BottomFragmentViewModel::class.java)
        binding = view?.let { FragmentBottomBinding.bind(it) }
        binding?.vm = mBottomFragmentViewModel
    }

    override fun initData() {
        bean = arguments?.getParcelable("data")
        bean?.let { bean->
            binding?.toolbar?.setTitle(bean.itemName)
            binding?.toolbar?.getRightTopTextView()?.visibility = if (bean.codeUrl.isEmpty()) View.GONE else View.VISIBLE
        }
        mBottomFragmentViewModel?.requestData()
    }

    override fun initEvent() {
        mBottomFragmentViewModel?.itemClickLiveData?.observe(this) {
            Toast.makeText(context, "${it.title} click", Toast.LENGTH_SHORT).show()
        }
        binding?.toolbar?.onBackListener = {
            activity?.finish()
        }
        binding?.toolbar?.onRightTopListener = {
            openBrowser()
        }
    }

    private fun openBrowser() {
        bean?.let { bean ->
            val uri = Uri.parse(bean.codeUrl)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }

    override fun resouceId() = R.layout.fragment_bottom
}
```

#### **fragment_bottom.xml** 消息列表布局

```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools">
    <data>
        <variable
            name="vm"
            type="com.lujianfei.other.ui.wechat.viewmodel.BottomFragmentViewModel" />
    </data>
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">

        <com.lujianfei.module_plugin_base.widget.PluginToolBar
            android:id="@+id/toolbar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"/>
        <com.lujianfei.other.ui.wechat.widget.CustomListRecyclerView
            android:id="@+id/recyclerview"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            listdata="@{vm.list}"
            itemClick="@{vm.itemClick}"
            />
    </LinearLayout>
</layout>
```



#### **BottomFragmentViewModel.kt**

```kotlin
package com.lujianfei.other.ui.wechat.viewmodel

import androidx.databinding.ObservableArrayList
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.lujianfei.other.ui.wechat.model.BottomListModal

/**
 * Author: mac
 * Date: 8.7.22 4:19 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */
class BottomFragmentViewModel: ViewModel() {

    val list = ObservableArrayList<BottomListModal>()

    val itemClickLiveData = MutableLiveData<BottomListModal>()

    val itemClick: ((resId:Int,bean: BottomListModal)->Unit) = { resId,bean->
        itemClickLiveData.value = bean
    }

    fun requestData() {
        val colors = arrayOf(0xffff0000.toInt(), 0xff00ff00.toInt(), 0xff0000ff.toInt(), 0xff00ffff.toInt())
        for (i in 0 until 50) {
            list.add(BottomListModal(iconColor = colors[i % colors.size], title = "仿微信标题 ${i + 1}", summary = "描述 ${i+1}"))
        }
    }
}
```



#### **TwoLevelTransformer.kt** 控制滑动特效

```kotlin
package com.lujianfei.other.ui.wechat.transformer

import android.util.Log
import android.view.View
import androidx.core.view.ViewCompat
import androidx.viewpager2.widget.ViewPager2


/**
 * Author: mac
 * Date: 8.7.22 2:11 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */

class TwoLevelTransformer(private val viewPager: ViewPager2) : ViewPager2.PageTransformer {

    companion object {
        private var sCurrentPage = 1
        private var sFirstLoad = true
    }

    override fun transformPage(page: View, position: Float) {
        val offscreenPageLimit = viewPager.offscreenPageLimit.toFloat()
        if (position < 0.005 && position >= 0.0) {
            sCurrentPage = 1
        } else if (position > 0.995 && position <= 1.0) {
            sCurrentPage = 0
        }
        if (sFirstLoad) {
            sCurrentPage = 0
            sFirstLoad = false
        }

        //当下拉在一定范围内会自动回弹，否则自动下拉到底（暂时不用）
        /*if (sCurrentPage == 0) {
                  if (position >= 0.65 && position <= 1.0) {
                      mViewPager.setCurrentItem(0);
                  } else if (position < 0.65 && position >= 0.0) {
                      mViewPager.setCurrentItem(1);
                  }
              }*/
        if (sCurrentPage == 0) {
            if (position >= 0) {
                val translationY: Float = page.height * position
                page.translationY = -translationY
                val scaleFactor = Math.min(1f - position * 0.2f, 1f)
                page.scaleX = scaleFactor
                page.scaleY = scaleFactor
            }
        } else {
            page.translationY = 0f
            page.scaleX = 1f
            page.scaleY = 1f
        }
        if (position > -1 && position < 0) {
            page.alpha = position * position * position + 1
        } else if (position > offscreenPageLimit - 1) {
            page.alpha = (1 - position + Math.floor(position.toDouble())).toFloat()
        } else {
            page.alpha = 1f
        }
        ViewCompat.setElevation(page, (offscreenPageLimit - position) * 2)
    }
}
```



#### **BindAdapter.kt** 数据绑定

```kotlin
@BindingAdapter(value = ["listdata", "itemClick"])
fun bindWechatListAdapter(
    recyclerView: RecyclerView,
    data: ObservableArrayList<BottomListModal>,
    itemClick: ((resId:Int,bean: BottomListModal)->Unit) ?= null
) {
    if (recyclerView.adapter != null) return
    val layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.layoutManager = layoutManager
    val adapter = BottomFragmentAdapter(data)
    adapter.itemClick = itemClick
    recyclerView.adapter = adapter
}
@BindingAdapter(value = ["listdata", "itemClick"])
fun bindMiniProgramListAdapter(
    recyclerView: RecyclerView,
    data: ObservableArrayList<TopListModal>,
    itemClick: ((resId:Int,bean: TopListModal)->Unit) ?= null
) {
    if (recyclerView.adapter != null) return
    val layoutManager = GridLayoutManager(recyclerView.context,3)
    recyclerView.layoutManager = layoutManager
    val adapter = TopFragmentAdapter(data)
    adapter.itemClick = itemClick
    recyclerView.adapter = adapter
}
```



#### 实体类

```kotlin
package com.lujianfei.other.ui.wechat.model

/**
 * Author: mac
 * Date: 8.7.22 3:14 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */
data class TopListModal(
    val iconColor: Int,
    val title: String,
    val summary: String
)

```



```kotlin
package com.lujianfei.other.ui.wechat.model

/**
 * Author: mac
 * Date: 8.7.22 3:14 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */
data class BottomListModal(
    val iconColor: Int,
    val title: String,
    val summary: String
)

```



#### 自定义列表组件

##### **CustomGridRecyclerView.kt**

```kotlin
package com.lujianfei.other.ui.wechat.widget

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Author: mac
 * Date: 8.7.22 4:58 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */
class CustomGridRecyclerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : RecyclerView(context, attrs) {

    private var startX = 0
    private var startY = 0

    /**
     * 禁用列表事件, 开放父容器事件
     */
    var disableRecyclerViewTouch = false

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = ev.x.toInt()
                startY = ev.y.toInt()
                parent.requestDisallowInterceptTouchEvent(true) //告诉 父 viewgroup 不要去拦截我
            }
            MotionEvent.ACTION_MOVE -> {
                val endX = ev.x
                val endY = ev.y
                val disX = endX - startX
                val disY = endY - startY
                val lm = layoutManager as GridLayoutManager?
                val lastCompleteVisiblePos = lm?.findLastCompletelyVisibleItemPosition()
                Log.d("CustomGridRecyclerView=", "lastCompleteVisiblePos:$lastCompleteVisiblePos, disY:$disY, lm?.itemCount:${(lm?.itemCount?:0) -1}")
                if (disableRecyclerViewTouch) {
                    parent.requestDisallowInterceptTouchEvent(false)
                } else {
                    if (lastCompleteVisiblePos == (lm?.itemCount?:0) - 1 && disY < 0) { // 滑动到消息列表顶部，且向下滑动时，告诉 父 viewgroup 去拦截我， viewpager2 可以翻页
                        Log.d("CustomGridRecyclerView=", "requestDisallowInterceptTouchEvent(false)")
                        parent.requestDisallowInterceptTouchEvent(false)
                    } else {
                        Log.d("CustomGridRecyclerView=", "requestDisallowInterceptTouchEvent(true)")
                        parent.requestDisallowInterceptTouchEvent(true) //告诉 父 viewgroup 不要去拦截我, recyclerview 可以处理自己的事件
                    }
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                parent.requestDisallowInterceptTouchEvent(true) //告诉 父 viewgroup 不要去拦截我
            }
        }
        return super.dispatchTouchEvent(ev)
    }
}
```



##### **CustomListRecyclerView.kt**

```kotlin
package com.lujianfei.other.ui.wechat.widget

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 * Author: mac
 * Date: 8.7.22 4:58 下午
 * Email:johnson@miaoshitech.com
 * Description:
 */
class CustomListRecyclerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : RecyclerView(context, attrs) {

    private var startX = 0
    private var startY = 0

    /**
     * 禁用列表事件, 开放父容器事件
     */
    var disableRecyclerViewTouch = false

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = ev.x.toInt()
                startY = ev.y.toInt()
                parent.requestDisallowInterceptTouchEvent(true) //告诉 父 viewgroup 不要去拦截我
            }
            MotionEvent.ACTION_MOVE -> {
                val endX = ev.x
                val endY = ev.y
                val disX = endX - startX
                val disY = endY - startY
                val lm = layoutManager as LinearLayoutManager?
                val firstVisiblePosition = lm?.findFirstCompletelyVisibleItemPosition()
                if (disableRecyclerViewTouch) {
                    parent.requestDisallowInterceptTouchEvent(false)
                } else {
                    if (firstVisiblePosition == 0 && disY > 0) { // 滑动到消息列表顶部，且向下滑动时，告诉 父 viewgroup 去拦截我， viewpager2 可以翻页
                        parent.requestDisallowInterceptTouchEvent(false)
                    } else {
                        parent.requestDisallowInterceptTouchEvent(true) //告诉 父 viewgroup 不要去拦截我, recyclerview 可以处理自己的事件
                    }
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                parent.requestDisallowInterceptTouchEvent(true) //告诉 父 viewgroup 不要去拦截我
            }
        }
        return super.dispatchTouchEvent(ev)
    }
}
```

