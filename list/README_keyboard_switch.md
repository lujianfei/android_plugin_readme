# android 键盘切换抖动优化 代码展示

思路简介：

​       首先 Activity 键盘模式设置为 adjustResize

1. 整体使用线性布局，Content 内容高度使用 weight="1" 撑开
2. 键盘弹起时，会把 Content 的高度挤压，同时通过 ImmersionBar 获取键盘高度, 并给到底部 inputLayout 的 **功能面板** 同样的高度
3. 键盘收起时，Content 设置属性 weight = "0", layoutParams.height = content.height 锁定当前高度, 在父控件（LinearLayout）的高度变化时它的高度也不再会变化，显示 inputLayout 的功能面板, Content 设置延迟  200 ms 设置属性 layout_weight="1",  解锁 Content 高度



以下只展示关键部分

布局文件

other_softinput_switch.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    >
    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        />

    <TextView
        android:id="@+id/tvContent"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:gravity="center"
        android:textSize="25sp"
        android:textColor="@color/black"
        android:text="Content"/>

    <com.lujianfei.other.ui.widget.InputLayout
        android:id="@+id/inputLayout"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textColor="@color/black"
        />
</LinearLayout>

```

##### 

##### MainActivity.kt

```kotlin
package com.lujianfei.other.ui

import android.content.Intent
import android.net.Uri
import android.view.View
import android.view.WindowManager
import androidx.activity.OnBackPressedCallback
import com.gyf.immersionbar.ImmersionBar
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.other.R
import com.lujianfei.other.ui.widget.EmotionInputDetector
import com.lujianfei.other.ui.widget.InputLayout

/**
 * 输入法
 *
 */
class SoftInputSwitchFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    private var toolbar: PluginToolBar?= null
    private var tvContent: View?= null
    private var inputLayout: InputLayout?= null
    private var bean: PluginActivityBean?= null

    private var mDetector: EmotionInputDetector? = null

    override fun resouceId() = R.layout.other_softinput_switch

    override fun initView() {
        LogUtils.d("SoftInputSwitchFragment", "initView")
        toolbar = view?.findViewById(R.id.toolbar)
        inputLayout = view?.findViewById(R.id.inputLayout)
        tvContent = view?.findViewById(R.id.tvContent)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        ImmersionBar.with(this)
            .statusBarDarkFont(false)
            .titleBarMarginTop(toolbar)
            .statusBarColor(com.lujianfei.module_plugin_base.R.color.theme_color)
            .keyboardEnable(true)
            .setOnKeyboardListener { isPopup, keyboardHeight ->
                mDetector?.performSoftKeyboardVisible(isPopup, keyboardHeight)
            }
            .init()

        inputLayout?.mFragmentManager = childFragmentManager
        mDetector = EmotionInputDetector.with(activity!!)
            .setEmotionView(inputLayout?.getFunctionContainer())
            .setOnHideSoftCallback {
                mDetector?.callFromEmotionInputDetector = true
            }
            .bindToContent(tvContent)
            .bindToEditText(inputLayout?.getEditText())
            .bindToEmotionButton(inputLayout?.getFunction1()!!, onClickCallback = {
                inputLayout?.handleFunction1Click()
            })
            .bindToEmotionButton(inputLayout?.getFunction2()!!) {
                inputLayout?.handleFunction2Click()
            }
            .build()

        mDetector?.softButtonsBarHeight = ImmersionBar.getNavigationBarHeight(activity!!)
    }



    override fun initData() {
        bean = arguments?.getParcelable("data")
        bean?.let {bean->
            toolbar?.setTitle(bean.itemName)
            toolbar?.getRightTopTextView()?.visibility = if (bean.codeUrl.isNullOrEmpty()) View.GONE else View.VISIBLE
        }
    }


    override fun initEvent() {
        toolbar?.onBackListener = {
            backAction(needFinish = true)
        }
        toolbar?.onRightTopListener = {
            openBrowser()
        }
        requireActivity().onBackPressedDispatcher.addCallback(this,object : OnBackPressedCallback(false){
            override fun handleOnBackPressed() {
                LogUtils.d("SoftInputSwitchFragment", "handleOnBackPressed")
                backAction(needFinish = false)
            }
        })
    }

    private fun openBrowser() {
        bean?.let { bean ->
            val uri = Uri.parse(bean.codeUrl)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    fun backAction(needFinish:Boolean = false) {
        if (mDetector?.isSoftInputShown == true || inputLayout?.getFunctionContainer()?.isShown == true) {
            inputLayout?.getFunction1()?.reset()
            mDetector?.hideEmotionLayout(false)
            mDetector?.hideSoftInput()
        } else {
            if (needFinish) {
                activity?.finish()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        mDetector?.onPause()
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }
}
```

##### 核心控制类

```kotlin
package com.lujianfei.other.ui.widget

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.LinearLayout
import androidx.fragment.app.FragmentActivity
import com.lujianfei.other.ui.widget.base.BaseFuntionButton

/**
 * 核心代码
 */
class EmotionInputDetector private constructor() {

    companion object {
        private const val SHARE_PREFERENCE_NAME = "com.dss886.emotioninputdetector"
        private const val SHARE_PREFERENCE_TAG = "soft_input_height"
        fun with(activity: FragmentActivity): EmotionInputDetector {
            val emotionInputDetector = EmotionInputDetector()
            emotionInputDetector.mActivity = activity
            emotionInputDetector.mInputManager =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            emotionInputDetector.sp =
                activity.getSharedPreferences(SHARE_PREFERENCE_NAME, Context.MODE_PRIVATE)
            return emotionInputDetector
        }
    }

    private var mActivity: Activity? = null
    private var mInputManager: InputMethodManager? = null
    private var sp: SharedPreferences? = null
    private var mEmotionLayout: View? = null
    private var mEditText: EditText? = null
    private var mContentView: View? = null
    var onHideSoftCallback:(()->Unit) ?= null

    fun bindToContent(contentView: View?): EmotionInputDetector {
        mContentView = contentView
        return this
    }

    fun bindToEditText(editText: EditText?): EmotionInputDetector {
        mEditText = editText
        mEditText?.requestFocus()
        mEditText?.setOnTouchListener { v: View?, event: MotionEvent ->
            if (event.action == MotionEvent.ACTION_UP && mEmotionLayout?.isShown == true) {
                lockContentHeight()
                hideEmotionLayout(true)
                mEditText?.postDelayed(Runnable { unlockContentHeightDelayed() }, 200L)
            }
            false
        }
        return this
    }

    fun bindToEmotionButton(emotionButton: BaseFuntionButton, onClickCallback:()->Unit): EmotionInputDetector {
        emotionButton.onClickListener { v: View? ->
            onClickCallback.invoke()
            if (mEmotionLayout?.isShown == true) {
                lockContentHeight()
                if (emotionButton.needHideBroad()) {
                    hideEmotionLayout(showSoftInput = emotionButton.needShowKeyboard())
                } else {
                    if (emotionButton.needShowKeyboard()) {
                        showSoftInput()
                    } else {
                        hideSoftInput()
                    }
                }
                unlockContentHeightDelayed()
            } else {
                if (isSoftInputShown) {
                    lockContentHeight()
                    showEmotionLayout()
                    unlockContentHeightDelayed()
                } else {
                    showEmotionLayout()
                }
            }
        }
        return this
    }

    fun setEmotionView(emotionView: View?): EmotionInputDetector {
        mEmotionLayout = emotionView
        return this
    }

    fun setOnHideSoftCallback(onHideSoftCallback:()->Unit) :EmotionInputDetector {
        this.onHideSoftCallback = onHideSoftCallback
        return this
    }

    fun build(): EmotionInputDetector {
        mActivity?.window?.setSoftInputMode(
            WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN or
                    WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
        )
        hideSoftInput()
        return this
    }

    fun interceptBackPress(): Boolean {
        // TODO: 15/11/2 change this method's name
        if (mEmotionLayout?.isShown == true) {
            hideEmotionLayout(false)
            return true
        }
        return false
    }

    private fun showEmotionLayout() {
        var softInputHeight = supportSoftInputHeight
        if (softInputHeight == 0) {
            softInputHeight = sp?.getInt(SHARE_PREFERENCE_TAG, 400)?:400
        }
        hideSoftInput()
        mEmotionLayout?.layoutParams?.height = softInputHeight
        mEmotionLayout?.visibility = View.VISIBLE
        mEmotionLayout?.requestLayout()
    }

    fun hideEmotionLayout(showSoftInput: Boolean) {
        if (mEmotionLayout?.isShown == true) {
            mEmotionLayout?.visibility = View.GONE
            if (showSoftInput) {
                showSoftInput()
            }
        }
    }

    private fun lockContentHeight() {
        val params = mContentView?.layoutParams as LinearLayout.LayoutParams
        params.height = mContentView?.height?:0
        params.weight = 0.0f
    }

    private fun unlockContentHeightDelayed() {
        mEditText?.postDelayed({
            (mContentView?.layoutParams as LinearLayout.LayoutParams).weight = 1.0f
        }, 200L)
    }

    private fun showSoftInput() {
        mEditText?.requestFocus()
        mEditText?.post { mInputManager!!.showSoftInput(mEditText, 0) }
    }

    fun hideSoftInput() {
        onHideSoftCallback?.invoke()
        mInputManager?.hideSoftInputFromWindow(mEditText?.windowToken, 0)
    }

    fun onPause() {
        interceptBackPress()
        mEditText?.postDelayed({
            val lp = mContentView?.layoutParams as LinearLayout.LayoutParams
            lp.weight = 1.0f
            mContentView?.layoutParams = lp
        }, 200L)
    }

    fun performSoftKeyboardVisible(isPopup: Boolean, keyboardHeight: Int) {
        if (!isPopup) {
            if (!callFromEmotionInputDetector) {
                hideEmotionLayout(false)
            }
            supportSoftInputHeight = 0
        } else {
            supportSoftInputHeight = keyboardHeight
        }
        callFromEmotionInputDetector = false
    }

    val isSoftInputShown: Boolean
    get() = supportSoftInputHeight != 0

    // When SDK Level >= 20 (Android L), the softInputHeight will contain the height of softButtonsBar (if has)
    var supportSoftInputHeight: Int = 0
    set(value) {
        field = value
        if (value > 0) {
            sp?.edit()?.putInt(SHARE_PREFERENCE_TAG, value)?.apply()
        }
    }

    var softButtonsBarHeight: Int = 0
    set(value) {
        field = value
    }

    var callFromEmotionInputDetector: Boolean = false
}
```
