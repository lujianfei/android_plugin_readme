# android 多列 RecyclerView 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### adapter_adapter.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    android:padding="5dp">
    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title"
        android:textSize="20sp"
        />

    <TextView
        android:id="@+id/summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="10dp"
        android:text="summary"
        android:ellipsize="end"
        android:maxLines="2"
        android:textSize="18sp"
        />
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_2

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    private var recyclerview:RecyclerView ?= null
    private val mAdapter by lazy { MainAdapter() }

    override fun resouceId(): Int = R.layout.activity_main
    
    override fun initView() {
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply {
            layoutManager = GridLayoutManager(that, 2)
            adapter = mAdapter
            addItemDecoration(RecycleGridDivider(color = 0xff333333.toInt(), space = 1))
        }
        toolbar?.setTitle(getPluginString(R.string.app_name))
    }

    override fun initData() {
        val mData = arrayListOf<MainBean>()
        for (idx in 0 until 40) {
            mData.add(MainBean(title = "码农宝标题 $idx", summary = "码农宝简介：主要功能：\n" +
                    "- 快速查看安卓设备信息 （手机屏幕分辨率，手机型号，设备id, 可用内存等等）\n" +
                    "- 各类开发过程中常用代码及效果 Demo, 分别有\n" +
                    "- 列表, 容器，对话框，动画，翻页，图表，编码及算法，多媒体，传感器，实用工具等 $idx"))
        }
        mAdapter.setData(mData)
    }   
}

```

##### MainAdapter.kt

```kotlin
package com.lujianfei.plugin1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class MainAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mData = arrayListOf<MainBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_main, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(mData: ArrayList<MainBean>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView? = null
        var summary: TextView? = null

        init {
            title = itemView.findViewById(R.id.title)
            summary = itemView.findViewById(R.id.summary)
        }

        fun setData(mainBean: MainBean) {
            title?.text = mainBean.title
            summary?.text = mainBean.summary
        }
    }
}
```

##### MainBean.kt

```kotlin
package com.lujianfei.plugin1

data class MainBean(
        var title: String,
        var summary: String
)
```



对GridLayout自定义分隔线有兴趣的可以看看这个类

##### RecycleGridDivider.kt

```kotlin
package com.lujianfei.plugin1_2

import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView


/**
 *@date     创建时间:2020/11/9
 *@name     作者:陆键霏
 *@describe 描述:
 */
class RecycleGridDivider(color: Int = 0, space: Int = 1): RecyclerView.ItemDecoration() {
    
    private var space = 0
    private var color = 0
    private var mPaint: Paint? = null
    private val rectHorizontal = Rect()
    private val rectVertical = Rect()
    
    init {
        this.color = color
        this.space = space
        mPaint = Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint?.apply {
            setColor(color)
            style = Paint.Style.FILL
            strokeWidth = space.toFloat()
        }
    }

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        val layoutManager = parent.layoutManager
        if (layoutManager !is GridLayoutManager) return
        val manager = layoutManager as GridLayoutManager?
        val childSize = parent.childCount
        val span = manager!!.spanCount
        //为了Item大小均匀，将设定分割线平均分给左右两边Item各一半
        val offset = space / 2
        //得到View的位置
        val childPosition = parent.getChildAdapterPosition(view)
        //第一排，顶部不画
        if (childPosition < span) {
            //最左边的，左边不画
            when {
                childPosition % span == 0 -> {
                    outRect[0, 0, offset] = 0
                    //最右边，右边不画
                }
                childPosition % span == span - 1 -> {
                    outRect[offset, 0, 0] = 0
                }
                else -> {
                    outRect[offset, 0, offset] = 0
                }
            }
        } else {
            //上下的分割线，就从第二排开始，每个区域的顶部直接添加设定大小，不用再均分了
            when {
                childPosition % span == 0 -> {
                    outRect[0, space, offset] = 0
                }
                childPosition % span == span - 1 -> {
                    outRect[offset, space, 0] = 0
                }
                else -> {
                    outRect[offset, space, offset] = 0
                }
            }
        }
    }

    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (mPaint == null) return
        if (parent.layoutManager !is GridLayoutManager) return
        val gridLayoutManager = parent.layoutManager as GridLayoutManager?
        val spanCount = gridLayoutManager?.spanCount ?: return
        for (i in 0 until parent.childCount) {
            val view = parent.getChildAt(i)
            val col = i % spanCount
            // 得到 Rect
            parent.getDecoratedBoundsWithMargins(view, rectHorizontal)
            // 减去 Item 的高度, 获取横分隔线
            rectHorizontal.bottom = rectHorizontal.bottom - view.height
            // 获取垂直分隔线
            rectVertical.left = view.left
            rectVertical.top = view.top
            rectVertical.right = rectVertical.left + space
            rectVertical.bottom = rectVertical.top + view.height
            // 画它
            c.drawRect(rectHorizontal, mPaint!!)
            if (col > 0) { // 首列不画垂线
                c.drawRect(rectVertical, mPaint!!)
            }
        }
    }
}
```

