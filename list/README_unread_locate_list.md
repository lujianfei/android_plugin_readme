# android 双击定位未读 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'    
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    android:id="@+id/rootView"
    tools:context=".MainActivity">
    
    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        />

    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/bt_double_click"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginStart="5dp"
        android:layout_marginEnd="5dp"
        android:layout_alignParentBottom="true"
        android:layout_marginBottom="10dp"
        android:text="双击定位"/>

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_below="@id/toolbar"
        android:layout_above="@id/bt_double_click"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>

</RelativeLayout>
```

##### adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    android:padding="5dp"
    >

    <ImageView
        android:id="@+id/icon"
        android:layout_width="50dp"
        android:layout_height="50dp"
        tools:src="@mipmap/ic_launcher"
        />

    <TextView
        android:id="@+id/tv_name"
        android:layout_toEndOf="@id/icon"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textSize="18sp"
        android:text="Name"/>

    <TextView
        android:id="@+id/tv_summary"
        android:layout_alignStart="@id/tv_name"
        android:layout_below="@id/tv_name"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Name"/>

    <TextView
        android:id="@+id/tv_status"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentEnd="true"
        android:layout_marginEnd="10dp"
        android:text="未读"/>
</RelativeLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_14

import android.content.Intent
import android.net.Uri
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin1_14.adapter.MainAdapter
import com.lujianfei.plugin1_14.bean.MainBean
import kotlinx.android.synthetic.main.adapter_main.*


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    
    private var recyclerview:RecyclerView ?= null
    private var bt_double_click:Button ?= null
    private var detector:GestureDetector? = null
    private val mAdapter by lazy { MainAdapter() }

    private var lastUnreadIndex = -1
    private val unreadIndexs = arrayListOf<Int>()

    // 单个item高度
    private var itemHeight: Int = 0

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {       
        bt_double_click = findViewById(R.id.bt_double_click)
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply {
            layoutManager = LinearLayoutManager(that)
            adapter = mAdapter
        }
    }

    override fun initData() {       
        val mData = arrayListOf<MainBean>()
        for (i in 0 until 50) {
            mData.add(MainBean(icon = R.mipmap.ic_launcher,
            title = "码农宝$i",
            summary = "消息内容$i"))
            if (i == 5 || i == 10 || i == 20) {
                mData[i].unread = true
            }
        }
        mAdapter.setData(mData)
    }

    override fun initEvent() {
        that?.let {
            detector =  GestureDetector(it, object : GestureDetector.SimpleOnGestureListener() {
                override fun onDoubleTap(e: MotionEvent?): Boolean {
                    scrollToUnreadMessage()
                    return super.onDoubleTap(e)
                }
            })
        }       
        bt_double_click?.setOnTouchListener { v, event ->
            detector?.onTouchEvent(event)
            true
        }
    }

    private fun scrollToUnreadMessage() {
        val mLayoutManager = recyclerview?.layoutManager?:return

        val mLinearLayoutManager = mLayoutManager as LinearLayoutManager
        unreadIndexs.clear()

        for (i in 0 until mAdapter.itemCount) {
            val bean = mAdapter.getItem(i)
            if (bean.unread) {
                unreadIndexs.add(i)
            }
        }

        var unreadPos = 0
        if (unreadIndexs.size > 0) {
            lastUnreadIndex = ++lastUnreadIndex % unreadIndexs.size
            unreadPos = unreadIndexs[lastUnreadIndex]
        }
        // 所有的移动距离都以 firstVisibleItemPosition 来做参照
        val firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition()
    // 获取到当前参照view的偏移量
        val firstView = mLinearLayoutManager.findViewByPosition(firstVisibleItemPosition) ?: return
        if (firstView.height != 0) {
            itemHeight = firstView.height
        }
        val top = firstView.top
        // 移动目标位置大于参照view postion
        // （这里item为等高）如果包含headview移动距离必须考虑到Headview的高度
        when {
            unreadPos > firstVisibleItemPosition -> {
                val transPostion = unreadPos - firstVisibleItemPosition
                recyclerview?.smoothScrollBy(0, transPostion * itemHeight + top)
                // 移动目标位置小于参照view postion
            }
            unreadPos < firstVisibleItemPosition -> {
                val transPostion = firstVisibleItemPosition - unreadPos
                recyclerview?.smoothScrollBy(0, -(transPostion * itemHeight - top))
            }
            else -> {
                // 移动目标位置等于参照view postion
                recyclerview?.smoothScrollBy(0, top)
            }
        }
    }   
}

```

