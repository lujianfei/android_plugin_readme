# android Vlayout 代码展示

Vlayout 是阿里开源的强大工具集，专门用来处理一个 RecyclerView 里多种类型布局的实现， 可以用多个 Adapter 来管理多个布局类型，起到最大限度的解耦

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
		// RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.0.0'
    implementation 'androidx.viewpager2:viewpager2:1.0.0'
    // vlayout
    implementation('com.alibaba.android:vlayout:1.2.8@aar') {
        transitive = true
    }
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```


##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_10

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.alibaba.android.vlayout.DelegateAdapter
import com.alibaba.android.vlayout.VirtualLayoutManager
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin1_10.adapter.MainBannerAdapter
import com.lujianfei.plugin1_10.adapter.MainGridAdapter
import com.lujianfei.plugin1_10.adapter.MainListAdapter
import com.lujianfei.plugin1_10.bean.MainBanner
import com.lujianfei.plugin1_10.bean.MainBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    private var recyclerview:RecyclerView ?= null
    private val mMainBannerAdapter by lazy { MainBannerAdapter() }
    private val mMainGridAdapter by lazy { MainGridAdapter() }
    private val mMainListAdapter by lazy { MainListAdapter() }

    override fun resouceId(): Int = R.layout.activity_main
    
    override fun initView() {
        recyclerview = findViewById(R.id.recyclerview)
        val mVirtualLayoutManager = VirtualLayoutManager(this)
        val mDelegateAdapter = DelegateAdapter(mVirtualLayoutManager)
        // 添加 banner
        mDelegateAdapter.addAdapter(0,mMainBannerAdapter)
        // 添加 网格
        mDelegateAdapter.addAdapter(1,mMainGridAdapter)
        // 添加 列表
        mDelegateAdapter.addAdapter(2,mMainListAdapter)

        recyclerview?.layoutManager = mVirtualLayoutManager
        recyclerview?.adapter = mDelegateAdapter
    }

    override fun initData() {
        // 初始化 banner
        val mBannerList = arrayListOf<MainBanner>()
        val mBannerColors = arrayListOf(0xffff0000.toInt(),0xff9d9d9d.toInt(),0xff0000ff.toInt(),0xff28004D.toInt())
        for (idx in 0 until mBannerColors.size) {
            mBannerList.add(MainBanner(title = "码农宝 Banner $idx", color = mBannerColors[idx]))
        }
        mMainBannerAdapter.setData(mBannerList)

        // 初始化 网格
        val mGridData = arrayListOf<MainBanner>()
        for (idx in 0 until mBannerColors.size) {
            mGridData.add(MainBanner(title = "码农宝 Grid $idx", color = mBannerColors[idx]))
        }
        mMainGridAdapter.setData(mGridData)

        // 初始化列表
        val mListData = arrayListOf<MainBean>()
        for (idx in 0 until 10) {
            mListData.add(MainBean(title = "码农宝标题 $idx", summary = "码农宝简介：主要功能：\n" +
                    "- 快速查看安卓设备信息 （手机屏幕分辨率，手机型号，设备id, 可用内存等等）\n" +
                    "- 各类开发过程中常用代码及效果 Demo, 分别有\n" +
                    "- 列表, 容器，对话框，动画，翻页，图表，编码及算法，多媒体，传感器，实用工具等 $idx"))
        }
        mMainListAdapter.setData(mListData)
    }
}

```

##### MainBannerAdapter.kt

```kotlin
package com.lujianfei.plugin1_10.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import androidx.viewpager2.widget.ViewPager2
import com.alibaba.android.vlayout.DelegateAdapter
import com.alibaba.android.vlayout.layout.LinearLayoutHelper
import com.lujianfei.plugin1_10.R
import com.lujianfei.plugin1_10.bean.MainBanner
import com.lujianfei.plugin1_10.widget.Banner
import java.util.*

/**
 * Author: mac
 * Date: 3/9/21 3:08 PM
 * Description: Banner专用Adapter
 */
class MainBannerAdapter : DelegateAdapter.Adapter<RecyclerView.ViewHolder>() {

    private var mData = arrayListOf<MainBanner>()
    private val viewPagerAdapter = ViewPagerAdapter()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_banner, parent, false))
    }

    override fun getItemCount(): Int = 1

    override fun onCreateLayoutHelper() = LinearLayoutHelper()

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.updateUI()
        }
    }

    fun setData(mData: ArrayList<MainBanner>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var banner: Banner? = null

        init {
            banner = itemView.findViewById(R.id.banner)
        }

        fun updateUI() {
            banner?.setAdapter(viewPagerAdapter)
        }
    }

    inner class ViewPagerAdapter:RecyclerView.Adapter<MyBannerViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyBannerViewHolder {
            return MyBannerViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_banner_item, parent, false))
        }

        override fun getItemCount() = mData.size

        override fun onBindViewHolder(holder: MyBannerViewHolder, position: Int) {
            holder.bindData(mData[position])
        }
    }

    inner class MyBannerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvTitle :TextView ?= null
        init {
            tvTitle = itemView.findViewById(R.id.tvTitle)
        }
        fun bindData(mainBanner: MainBanner) {
            tvTitle?.setBackgroundColor(mainBanner.color)
            tvTitle?.text = mainBanner.title
            tvTitle?.setTextColor(0xffffffff.toInt())
        }
    }
}
```

##### MainGridAdapter.kt

```kotlin
package com.lujianfei.plugin1_10.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.alibaba.android.vlayout.DelegateAdapter
import com.alibaba.android.vlayout.layout.GridLayoutHelper
import com.lujianfei.plugin1_10.R
import com.lujianfei.plugin1_10.bean.MainBanner
import java.util.*
/**
 * Author: mac
 * Date: 3/9/21 3:08 PM
 * Description: Grid 网格专用Adapter
 */

class MainGridAdapter : DelegateAdapter.Adapter<RecyclerView.ViewHolder>() {

    private var mData = arrayListOf<MainBanner>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_banner_item, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onCreateLayoutHelper():GridLayoutHelper {
        val layoutHelper = GridLayoutHelper(2)
        layoutHelper.setMargin(10,10,10,10) // 设置外边距
        layoutHelper.setGap(10) // 设置内部边距
        return layoutHelper
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(mData: ArrayList<MainBanner>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvTitle: TextView? = null

        init {
            tvTitle = itemView.findViewById(R.id.tvTitle)
        }

        fun setData(mainBean: MainBanner) {
            tvTitle?.text = mainBean.title
            tvTitle?.setTextColor(0xffffffff.toInt())
            itemView.setBackgroundColor(mainBean.color)
        }
    }
}
```
##### MainListAdapter.kt

```kotlin
package com.lujianfei.plugin1_10.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.alibaba.android.vlayout.DelegateAdapter
import com.alibaba.android.vlayout.layout.LinearLayoutHelper
import com.lujianfei.plugin1_10.bean.MainBean
import com.lujianfei.plugin1_10.R
import java.util.ArrayList

/**
 * Author: mac
 * Date: 3/9/21 3:08 PM
 * Description: List 专用Adapter
 */

class MainListAdapter : DelegateAdapter.Adapter<RecyclerView.ViewHolder>() {

    private var mData = arrayListOf<MainBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_list, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onCreateLayoutHelper():LinearLayoutHelper {
        val layoutHelper = LinearLayoutHelper()
        layoutHelper.setMargin(10,0,10,0) // 设置外边距
        layoutHelper.setDividerHeight(1) // 设置 item 上下间距
        return layoutHelper
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(mData: ArrayList<MainBean>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView? = null
        var summary: TextView? = null

        init {
            title = itemView.findViewById(R.id.title)
            summary = itemView.findViewById(R.id.summary)
        }

        fun setData(mainBean: MainBean) {
            title?.text = mainBean.title
            summary?.text = mainBean.summary
            title?.setTextColor(0xffffffff.toInt())
            summary?.setTextColor(0xffffffff.toInt())
            itemView.setBackgroundColor(0xff28004D.toInt())
        }
    }
}
```

##### Banner.kt

```kotlin
package com.lujianfei.plugin1_10.widget

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewConfiguration
import android.widget.FrameLayout
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.MarginPageTransformer
import androidx.viewpager2.widget.ViewPager2
import java.lang.Math.abs

/**
 * Description: 一个简单的Banner，只实现左右滑动
 * Date : 2020/9/26 3:47 PM
 */
class Banner(context: Context, attributeSet: AttributeSet? = null) :
        FrameLayout(context, attributeSet) {

    private val viewPager2: ViewPager2 = ViewPager2(context)

    // 滑动最小距离
    private val touchSlop = ViewConfiguration.get(context).scaledTouchSlop / 2
    // 处理滑动冲突，记录屏幕按下位置
    private var startX = 0f
    private var startY = 0f

    // 各个Item之间的间隔
    private var pageMargin: Int = 0

    // 用于显示多个Item（左右padding）
    private var bannerPadding: Int = 0;

    init {
        viewPager2.layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        attachViewToParent(viewPager2, 0, viewPager2.layoutParams)
    }


    /**
     * 设置数据源
     */
    fun <T : RecyclerView.ViewHolder> setAdapter(adapter: RecyclerView.Adapter<T>) {
        viewPager2.adapter = adapter
    }

    fun setPageMargin(pageMargin: Int) {
        this.pageMargin = pageMargin
        // 利用Transformer来设置Item的间隔
        viewPager2.setPageTransformer(
                MarginPageTransformer(pageMargin)
        )
    }

    fun setBannerPadding(bannerPadding: Int) {
        this.bannerPadding = bannerPadding
        // 只处理左右padding
        with(viewPager2 as RecyclerView) {
            setPadding(
                    bannerPadding,
                    0,
                    bannerPadding, 0
            )
            // 需要显示多个Item
            clipToPadding = false
        }
    }


    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                startX = event.x
                startY = event.y
                // 先禁止父view拦截事件
                parent.requestDisallowInterceptTouchEvent(true)
            }
            MotionEvent.ACTION_MOVE -> {
                val endX = event.x
                val endY = event.x
                val distanceX = abs(endX - startX)
                val distanceY = abs(endY - startY)
                // 水平滑动
                if (viewPager2.orientation == RecyclerView.HORIZONTAL && (distanceX > touchSlop && distanceX > distanceY)) {
                    // 让vp2处理
                } else {
                    // 竖直方向，让父类处理
                    parent.requestDisallowInterceptTouchEvent(false)
                }

            }

            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                // 状态复原
                parent.requestDisallowInterceptTouchEvent(false)
            }
        }
        return super.onInterceptTouchEvent(event)
    }

}

```



##### MainBean.kt

```kotlin
package com.lujianfei.plugin1

data class MainBean(
        var title: String,
        var summary: String
)
```

##### MainBanner.kt

```kotlin
data class MainBanner(var title:String = "",
                      var color:Int = 0)
```



##### adapter_banner.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="wrap_content">
    <com.lujianfei.plugin1_10.widget.Banner
        android:id="@+id/banner"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</RelativeLayout>
```

##### adapter_banner_item.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <TextView
        android:id="@+id/tvTitle"
        android:layout_width="match_parent"
        android:layout_height="100dp"
        android:gravity="center"
        android:textSize="20sp"
        />
</RelativeLayout>
```

##### adapter_list.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    android:padding="5dp">
    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title"
        android:textSize="20sp"
        />

    <TextView
        android:id="@+id/summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="summary"
        android:textSize="18sp"
        android:ellipsize="end"
        android:maxLines="2"
        />
</LinearLayout>
```

