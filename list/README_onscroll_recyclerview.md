# RecyclerView滚动修改透明度 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">
    
    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>

    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        />
</RelativeLayout>
```

##### adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    android:padding="5dp">
    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title"
        android:textSize="20sp"
        />

    <TextView
        android:id="@+id/summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="10dp"
        android:text="summary"
        android:textSize="18sp"
        android:ellipsize="end"
        android:maxLines="2"
        />
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_11

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.plugin1_11.adapter.MainAdapter
import com.lujianfei.plugin1_11.bean.MainBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    private var toolbar:PluginToolBar ?= null
    private var recyclerview:RecyclerView ?= null
    private val mAdapter by lazy { MainAdapter() }
    private var mTitleOnScrollListener:TitleOnScrollListener ?= null

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        toolbar = findViewById(R.id.toolbar)
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply {
            layoutManager = LinearLayoutManager(that)
            addItemDecoration(DividerItemDecoration(that, DividerItemDecoration.VERTICAL))
            adapter = mAdapter
        }
        if (mTitleOnScrollListener == null) {
            mTitleOnScrollListener = TitleOnScrollListener(toolbar!!)
        }
        recyclerview?.addOnScrollListener(mTitleOnScrollListener!!)
    }

    override fun initData() {     
        val mData = arrayListOf<MainBean>()
        for (idx in 0 until 20) {
            mData.add(MainBean(title = "码农宝标题 $idx", summary = "码农宝简介：主要功能：\n" +
                    "- 快速查看安卓设备信息 （手机屏幕分辨率，手机型号，设备id, 可用内存等等）\n" +
                    "- 各类开发过程中常用代码及效果 Demo, 分别有\n" +
                    "- 列表, 容器，对话框，动画，翻页，图表，编码及算法，多媒体，传感器，实用工具等 $idx"))
        }
        mData.add(0,MainBean(title = "向上滑动", summary = "向上滑动即可看到标题背景色的变化过程"))
        mAdapter.setData(mData)
    }
}

```

##### MainAdapter.kt

```kotlin
package com.lujianfei.plugin1_1

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import java.util.ArrayList

class MainAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var mData = arrayListOf<MainBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_main, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(mData: ArrayList<MainBean>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyDataSetChanged()
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView? = null
        var summary: TextView? = null

        init {
            title = itemView.findViewById(R.id.title)
            summary = itemView.findViewById(R.id.summary)
        }

        fun setData(mainBean: MainBean) {
            title?.text = mainBean.title
            summary?.text = mainBean.summary
        }
    }
}
```

##### MainBean.kt

```kotlin
package com.lujianfei.plugin1

data class MainBean(
        var title: String,
        var summary: String
)
```

##### 核心工具类

```kotlin

import android.animation.ArgbEvaluator
import android.graphics.Color
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.module_plugin_base.utils.ResUtils

class TitleOnScrollListener(titleView:View): RecyclerView.OnScrollListener() {

    companion object {
        val START_COLOR = 0 // 这里是放透明的颜色
        val END_COLOR = ResUtils.getThemeColor() // 这里使用不透明的颜色
    }

    private val height = DensityUtils.dip2px(56f) // 滑动开始变色的高,真实项目中此高度是由广告轮播或其他首页view高度决定
    private var overallXScroll = 0f
    private val evaluator = ArgbEvaluator()
    private var titleView:View ?= null

    init {
        this.titleView = titleView
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        overallXScroll += dy;// 累加y值 解决滑动一半y值为0
        // 滚动的总距离相对 0 - duration 之间有一个百分比，头部的透明度也是从初始值变动到不透明，通过距离的百分比，得到透明度对应的值
        // 如果小于0那么透明度为初始值，如果大于 duration 为不透明状态
        val bgColor: Int = when {
            overallXScroll < 0 -> {
                START_COLOR
            }
            overallXScroll > height -> {
                END_COLOR ?:0
            }
            else -> {
                evaluator.evaluate((overallXScroll / height), START_COLOR, END_COLOR).toString().toInt()
            }
        }
        LogUtils.d("TitleOnScrollListener","bgColor:$bgColor")
        titleView?.setBackgroundColor(bgColor)
    }
}
```

