# android 吸附标题 RecyclerView 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    >    
    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title"
        android:padding="10dp"
        android:textSize="20sp"
        />
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_6

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.plugin1_6.adapter.MainAdapter
import com.lujianfei.plugin1_6.bean.MainBean
import com.lujianfei.plugin1_6.decoration.StickyItemDecoration


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    ...
    private var recyclerview:RecyclerView ?= null
    private val adapter by lazy { MainAdapter() }

    override fun resouceId(): Int = R.layout.activity_main
    
    override fun initView() {
        ...
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply {
            layoutManager = LinearLayoutManager(that)
            addItemDecoration(object : StickyItemDecoration() {
         override fun isIndexItem(position: Int) = if (list.size <= 0 || position < 0) false else list[position].isHeader

        override fun getIndexTitle(position: Int) = if (list.size <= 0 || position < 0) "" else list[position].showHeader?:""

        override fun isSameIndexTitle(position1: Int, position2: Int) = if (list.size <= 0 || position1 < 0 || position2 < 0) true else     list[position1].showHeader ==  list[position2].showHeader
        })
            
        override fun showIndex() = true
        }
        ...
    }

    override fun initData() {
        ...
        recyclerview?.adapter = adapter
        val mData = arrayListOf<MainBean>()
        
        for (idx in 0 until 100) {
            if (idx % 10 == 0) {
                mData.add(MainBean(title = "Type $idx", isHeader = true, showHeader = "Type $idx"))
            } else {
                mData.add(MainBean(title = "title $idx", isHeader = false, showHeader = ""))
            }
        }
        adapter.setData(mData)
    }

    ...
}

```

##### MainAdapter.kt

```kotlin
package com.lujianfei.plugin1_6.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.plugin1_6.R
import com.lujianfei.plugin1_6.bean.MainBean
import java.util.ArrayList

class MainAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

     
    var mData = arrayListOf<MainBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.adapter_main, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(mData: ArrayList<MainBean>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyDataSetChanged()
    }


    override fun getItemViewType(position: Int): Int {
        return this.mData[position].type
    }
    
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView? = null

        init {
            title = itemView.findViewById(R.id.title)
        }

        fun setData(mainBean: MainBean) {
            title?.text = mainBean.title           
        }
    }
}
```

##### MainBean.kt

```kotlin
package com.lujianfei.plugin1_6.bean

data class MainBean(
        var title: String,
        var showLetterHeader: String? = null,
        var isLetterHeader: Boolean = false,
)
```

##### 实现吸附效果的核心代码

```kotlin
package com.lujianfei.plugin1_6.decoration

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.purui.mobile.R
import com.purui.mobile.base.dip2sp
import com.purui.mobile.utils.ResUtils

abstract class StickyItemDecoration: RecyclerView.ItemDecoration() {

    companion object {
        // 边距
        private val indexPaddingHorizontal = ResUtils.getDimenF(R.dimen.letter_padding_start)
        // 索引文字颜色
        private val indexFontColor = ResUtils.getColor(R.color.font_color_dark)
        // 索引条背景颜色
        private val indexBgColor = ResUtils.getColor(R.color.divider_color)
        // 索引条高度
        private val indexHeight = ResUtils.getDimen(R.dimen.letter_height)
        // 文字大小
        private val indexTextSize:Float = dip2sp(16f)
    }

    private val mIndexBgPaint by lazy { Paint() }

    private val mTextPaint by lazy { Paint() }

    init {
        mTextPaint.let {
            it.textSize = indexTextSize
            it.isAntiAlias = true
            it.color = indexFontColor
        }

        mIndexBgPaint.let {
            it.isAntiAlias = true
            it.color = indexBgColor
        }
    }

    /**
     * recyclerView 绘制 onDraw -> item.onDraw -> onDrawOver
     */
    override fun onDraw(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (!showIndex()) {
            return
        }
        for (index in 0 until parent.childCount) {
            val childView = parent.getChildAt(index)
            childView?.let {childViewNotNull->
                // 绘制每个索引条
                val indexRect = Rect()
                val position = parent.getChildAdapterPosition(childViewNotNull)
                if (isIndexItem(position) && position >= 0) {
                    // 控制索引条的绘制位置
                    indexRect.apply {
                        top = childViewNotNull.top - indexHeight
                        bottom = childViewNotNull.top
                        left = parent.paddingLeft
                        right = parent.width - parent.paddingRight
                    }
                    // 绘制索引条背景
                    c.drawRect(indexRect, mIndexBgPaint)
                    // 绘制索引条文字
                    c.drawText(getIndexTitle(position),
                        indexPaddingHorizontal,
                        getBaseLineY(paint = mTextPaint, centerY = indexRect.centerY()),
                        mTextPaint)
                }
            }
        }
    }

    private fun getBaseLineY(paint:Paint,centerY:Int):Float {
        return centerY - ( paint.fontMetricsInt.bottom + paint.fontMetricsInt.top ) / 2f
    }

    /**
     * recyclerView 绘制 onDraw -> item.onDraw -> onDrawOver
     * 绘制覆盖在 item 上面的索引条
     */
    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (!showIndex()) {
            return
        }
        val firstView = parent.getChildAt(0)
        var nextView:View? = null;
        if (1 < parent.childCount) {
            nextView = parent.getChildAt(1) // 下一个 item
        }
        firstView?.let { firstViewNotNull->
            val floatIndexRect = Rect()
            val position = parent.getChildAdapterPosition(firstViewNotNull)
            val firstLetterWillMeetSecondLetter = firstViewNotNull.bottom - indexHeight < parent.paddingTop
            val secondItemViewIsTimeItem = if (nextView == null) false else isIndexItem(parent.getChildAdapterPosition(nextView))
            val currentTitleNotEqualsToNextTitle = if (nextView == null) false else !isSameIndexTitle(parent.getChildAdapterPosition(nextView),position)
            // 如果下一个 view 不为空，第一个 索引 快要接触到下一个 索引，下一个 item 需要显示 索引, 下一个 item 和 当前 item 索引文字不同
            if (firstLetterWillMeetSecondLetter
                && secondItemViewIsTimeItem
                && currentTitleNotEqualsToNextTitle) {
                // 跟随最后一个 item 向上推
                floatIndexRect.top = firstViewNotNull.bottom - indexHeight // 第一个 item 的底部为悬浮提示的索引的底部 (显示的索引推出列表外)
                floatIndexRect.bottom = firstViewNotNull.bottom
            } else {
                // 顶部固定悬浮
                floatIndexRect.top = parent.paddingTop
                floatIndexRect.bottom = floatIndexRect.top + indexHeight
            }
            floatIndexRect.left = parent.paddingLeft
            floatIndexRect.right = parent.width - parent.paddingRight

            // 绘制索引条背景
            c.drawRect(floatIndexRect, mIndexBgPaint)
            // 绘制索引条文字
            c.drawText(getIndexTitle(position),
                       indexPaddingHorizontal,
                       getBaseLineY(paint = mTextPaint, centerY = floatIndexRect.centerY()),
                       mTextPaint)
        }
    }

    /**
     * 用于设置item周围的偏移量的，类似于设置padding喝margin效果，
     * 空出的效果用于绘制分割线
     */
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        if (!showIndex()) {
            return
        }
        val position:Int = parent.getChildAdapterPosition(view)
        if (position >=0 && isIndexItem(position)) {
            outRect.top = indexHeight
        } else{
            outRect.top = 0
        }
    }

    /**
     * 外部决定是否索引 item
     */
    abstract fun isIndexItem(position: Int):Boolean

    /**
     * 索引标题
     */
    abstract fun getIndexTitle(position: Int):String

    /**
     * 相同索引标题
     */
    abstract fun isSameIndexTitle(position1: Int, position2: Int):Boolean
    
    /**
     * 显示索引 (当你有搜索模式不想显示的索引的时候使用)
     */
    abstract fun showIndex():Boolean
}
```

