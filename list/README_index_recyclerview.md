# android 索引列表 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
	...
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent">
        <androidx.recyclerview.widget.RecyclerView
            android:id="@+id/recyclerview"
            android:layout_width="match_parent"
            android:layout_height="match_parent"/>
        <TextView
            android:id="@+id/middleLetter"
            android:layout_width="50dp"
            android:layout_height="50dp"
            android:gravity="center"
            android:textSize="30sp"
            android:background="#FF888888"
            android:textColor="#ffffff"
            android:visibility="gone"
            android:layout_centerInParent="true"/>
        <com.lujianfei.plugin1_5.widget.IndexBar
            android:id="@+id/indexBar"
            android:layout_width="35dp"
            android:layout_height="match_parent"
            android:layout_alignParentEnd="true"/>
    </RelativeLayout>
</LinearLayout>
```

##### adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    >

    <TextView
        android:id="@+id/index"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="A"
        android:textSize="20sp"
        android:paddingStart="10dp"
        android:background="#dddddd"
        />

    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title"
        android:textSize="20sp"
        android:padding="10dp"
        />

    <TextView
        android:id="@+id/summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="summary"
        android:textSize="15sp"
        android:padding="10dp"
        />
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_5

import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.plugin1_5.utils.AssetHelper
import com.lujianfei.plugin1_5.widget.IndexBar


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
    ...
    
    
    private var recyclerview:RecyclerView ?= null
    private var middleLetter:TextView ?= null
    private var indexBar:IndexBar ?= null
    private val adapter by lazy { MainAdapter() }
    

    override fun resouceId(): Int = R.layout.activity_main
    
    override fun initView() {
        ...

        middleLetter = findViewById(R.id.middleLetter)
        indexBar = findViewById(R.id.indexBar)
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.layoutManager = LinearLayoutManager(that)
        ...
    }

    override fun initData() {
        ...
        recyclerview?.adapter = adapter
        val mData = AssetHelper.getData()
        adapter.setData(mData)
        
        indexBar?.sourceDatas = mData.map { it.bfirstletter }
    }

    
    override fun initEvent() {
        ...
        indexBar?.mOnIndexPressListener = object : IndexBar.OnIndexPressListener {
            override fun onIndexChange(index: Int, text: String?) {
                text?.let {
                    middleLetter?.visibility = View.VISIBLE
                    middleLetter?.text = text
                    (recyclerview?.layoutManager as LinearLayoutManager).scrollToPositionWithOffset(adapter.getIndexByFirstLetter(text),0)
                }
            }

            override fun onMotionEventEnd() {
                middleLetter?.visibility = View.GONE
            }
        }
    }

    ...
}

```

##### MainAdapter.kt

```kotlin
package com.lujianfei.plugin1_5

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.plugin1_5.bean.BrandBean

class MainAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mData = arrayListOf<BrandBean>()

    private var letterPositionMap = mutableMapOf<String, Int>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.adapter_main, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            val mainBean = mData[position]
            if (letterPositionMap.containsKey(mainBean.bfirstletter)) {
                holder.setData(mainBean, letterPositionMap[mainBean.bfirstletter] == position)
            } else {
                holder.setData(mainBean, false)
            }
        }
    }

    fun setData(mData: List<BrandBean>) {
        this.mData.clear()
        this.mData.addAll(mData)
        buildLetterIndex()
        notifyDataSetChanged()
    }

    private fun buildLetterIndex() {
        letterPositionMap.clear()
        for (index in mData.indices) {
            val bean = mData[index]
            if (!letterPositionMap.containsKey(bean.bfirstletter)) {
                letterPositionMap[bean.bfirstletter] = index
            }
        }
    }

    fun getIndexByFirstLetter(firstLetter: String): Int {
        return letterPositionMap[firstLetter] ?: 0
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView? = null
        var summary: TextView? = null
        var index: TextView? = null

        init {
            title = itemView.findViewById(R.id.title)
            summary = itemView.findViewById(R.id.summary)
            index = itemView.findViewById(R.id.index)
        }

        fun setData(mainBean: BrandBean, showIndex :Boolean) {
            title?.text = mainBean.name
            summary?.text = "来自 ${mainBean.country}"
            index?.text = mainBean.bfirstletter
            index?.visibility = if (showIndex) View.VISIBLE else View.GONE
        }
    }
}
```

##### BrandBean.kt

```kotlin
package com.lujianfei.plugin1_5.bean

data class BrandBean(val id: Int,
                     val name: String,
                     val bfirstletter: String,
                     val logo: String,
                     val country: String,
                     val countryid: Int)
```



##### 核心组件 IndexBar.kt

```kotlin
package com.lujianfei.plugin1_5.widget

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import android.os.Looper
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import com.lujianfei.module_plugin_base.utils.DensityUtils
import com.lujianfei.plugin1_5.sort.IDataHelper
import com.lujianfei.plugin1_5.sort.IndexDataHelper


/**
 *@date     创建时间:2020/7/23
 *@name     作者:陆键霏
 *@describe 描述:
 */
class IndexBar @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private var mContext: Context? = null

    /**
     * 默认索引
     */
    private val DEFAULT_INDEX = arrayOf("A", "B", "C", "D", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
            "W", "X", "Y", "Z", "#")

    /**
     * 每个index的高度
     */
    private var indexHeght = 0

    /**
     * view宽度
     */
    private var mWidth = 0

    /**
     * view高度
     */
    private var mHeight = 0

    /**
     * 画笔
     */
    private val mPaint = Paint()

    /**
     * 按下时的背景颜色
     */
    private val mPressBackground = android.graphics.Color.GRAY

    /**
     * 文字颜色
     */
    private val mTextColor = android.graphics.Color.GRAY

    /**
     * 按下时文字的颜色
     */
    private val mPressTextColor = android.graphics.Color.WHITE

    /**
     * 文字选中的颜色
     */
    private val mSelectTextColor = android.graphics.Color.BLUE

    /**
     * 字体大小
     */
    private val letterTextSize = DensityUtils.dip2px(18f)
    /**
     * 松手时的背景颜色 
     */
    private val mUnpressBgcolor = android.graphics.Color.TRANSPARENT

    private var indexDatas = DEFAULT_INDEX.toMutableList()

    /**
     * 原始数据
     */
    var sourceDatas: List<String>? = null
        set(value) {
            field = value
            initIndexDatas()
            invalidateMySelft()
        }

    private var isPress = false

    private var currentIndex = 0

    var mOnIndexPressListener: OnIndexPressListener? = null

    private var mIndexDataHelper: IDataHelper? = null

    //存放每个绘制的index的Rect区域
    private val indexBounds = Rect()

    init {
        mContext = context
        initPaint()
    }

    private fun initPaint() {
        mPaint.apply {
            mPaint.textSize = letterTextSize.toFloat()
            mPaint.isAntiAlias = true
        }
    }

    /**
     * 初始原始数据 并提取索引
     */
    private fun initIndexDatas() {
        if (sourceDatas.isNullOrEmpty() || indexDatas.isNullOrEmpty()) return
        if (mIndexDataHelper == null) mIndexDataHelper = IndexDataHelper()
        mIndexDataHelper?.getIndex(sourceDatas, indexDatas)
        computeIndexHeight()
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event?.action) {
            MotionEvent.ACTION_DOWN -> {
                isPress = true
                setBackgroundColor(mPressBackground)
                computePressIndexLocation(event.y)
            }
            MotionEvent.ACTION_MOVE -> {
                computePressIndexLocation(event.y)
            }
            else -> {
                isPress = false
                //手指抬起时背景恢复透明
                setBackgroundColor(mUnpressBgcolor)
                //重置当前位置
                currentIndex = -1
                if (mOnIndexPressListener != null) {
                    mOnIndexPressListener?.onMotionEventEnd()
                }
            }
        }
        return true
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        mWidth = w
        mHeight = h
        computeIndexHeight()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        //取出宽高的MeasureSpec  Mode 和Size
        val wMode = MeasureSpec.getMode(widthMeasureSpec)
        val wSize = MeasureSpec.getSize(widthMeasureSpec)
        val hMode = MeasureSpec.getMode(heightMeasureSpec)
        val hSize = MeasureSpec.getSize(heightMeasureSpec)
        //最终测量出来的宽高
        var measureWidth = 0
        var measureHeight = 0
        //得到合适宽度：
        var index: String //每个要绘制的index内容

        for (i in indexDatas.indices) {
            index = indexDatas[i]
            //测量计算文字所在矩形，可以得到宽高
            mPaint.getTextBounds(index, 0, index.length, indexBounds)
            //循环结束后，得到index的最大宽度
            measureWidth = Math.max(indexBounds.width() + paddingLeft + paddingRight, measureWidth)
            //循环结束后，得到index的最大高度，然后*size
            measureHeight = Math.max(indexBounds.height(), measureHeight)
        }
        measureHeight *= indexDatas.size
        if (wMode == MeasureSpec.EXACTLY) {
            measureWidth = wSize
        } else if (wMode == MeasureSpec.AT_MOST) {
            //wSize此时是父控件能给子View分配的最大空间
            measureWidth = Math.min(measureWidth, wSize)
        }
        //得到合适的高度：
        if (hMode == MeasureSpec.EXACTLY) {
            measureHeight = hSize
        } else if (hMode == MeasureSpec.AT_MOST) {
            //wSize此时是父控件能给子View分配的最大空间
            measureHeight = Math.min(measureHeight, hSize)
        }
        setMeasuredDimension(measureWidth, measureHeight)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        for (i in indexDatas.indices) {
            val index = indexDatas[i]
            val metrics = mPaint.fontMetrics
            //计算 baseline
            val baseLine = ((indexHeght - metrics.bottom - metrics.top) / 2).toInt()
            if (currentIndex == i) {
                mPaint.color = mSelectTextColor
            } else {
                mPaint.color = if (isPress) mPressTextColor else mTextColor
            }
            //绘制文字
            canvas?.drawText(index, mWidth / 2 - mPaint.measureText(index) / 2,
                    (paddingTop + baseLine + indexHeght * i).toFloat(), mPaint)
        }
    }

    /**
     * 计算单个index高度
     */
    private fun computeIndexHeight() {
        if (indexDatas.isNullOrEmpty()) return
        indexHeght = (mHeight - paddingTop - paddingBottom) / 26
    }


    /**
     * 计算按下的位置
     */
    private fun computePressIndexLocation(y: Float) {
        if (indexDatas.isNullOrEmpty()) return
        // 计算按下的区域位置
        currentIndex = ((y - paddingTop) / indexHeght).toInt()
        if (currentIndex < 0) {
            currentIndex = 0
        } else if (currentIndex >= indexDatas.size) {
            currentIndex = indexDatas.size - 1
        }
        invalidateMySelft()
        mOnIndexPressListener?.onIndexChange(currentIndex, indexDatas[currentIndex])
    }

    private fun invalidateMySelft() {
        if (isMainThread()) {
            invalidate()
        } else {
            postInvalidate()
        }
    }

    private fun isMainThread(): Boolean {
        return Thread.currentThread() === Looper.getMainLooper().thread
    }


    interface OnIndexPressListener {
        /**
         * @param index 当前选中的位置
         * @param text  选中的文字
         */
        fun onIndexChange(index: Int, text: String?)

        /**
         * 事件结束时回调
         */
        fun onMotionEventEnd()
    }
}
```
##### 相关工具类

##### DensityUtils.kt

```kotlin
package com.lujianfei.plugin1_5.utils

import com.lujianfei.module_plugin_base.utils.ResUtils

/**
 *@date     创建时间:2020/7/23
 *@name     作者:陆键霏
 *@describe 描述:
 */
object DensityUtils {
    /**
     * 根据手机的分辨率从 dp 的单位 转成为 px(像素)
     */
    fun dip2px(dpValue: Float): Int {
        ResUtils.context?.let {context->
            val density = context.resources?.displayMetrics?.density
            return (dpValue * density!! + 0.5f).toInt()
        }
        return dpValue.toInt()
    }

    /**
     * 根据手机的分辨率从 px(像素) 的单位 转成为 dp
     */
    fun px2dip(pxValue: Float): Int {
        ResUtils.context?.let {context->
            val density = context.resources?.displayMetrics?.density
            return (pxValue * density!! + 0.5f).toInt()
        }
        return pxValue.toInt()
    }
}
```


IDataHelper.kt
```kotlin
package com.lujianfei.plugin1_5.sort

/**
 *@date     创建时间:2020/7/23
 *@name     作者:陆键霏
 *@describe 描述:
 */
interface IDataHelper {
    /**
     * 获取索引
     *
     * @param datas
     * @param indexDatas
     */
    fun getIndex(datas:List<String>?, indexDatas:MutableList<String>?)
}
```

IndexDataHelper.kt
```kotlin
package com.lujianfei.plugin1_5.sort

/**
 *@date     创建时间:2020/7/23
 *@name     作者:陆键霏
 *@describe 描述:
 */
class IndexDataHelper:IDataHelper {
    
    override fun getIndex(datas: List<String>?, indexDatas: MutableList<String>?) {
        indexDatas?.clear()
        if (datas == null) return
        if (indexDatas == null) return
        for (data in datas) {
            if (!indexDatas.contains(data)) {
                if (data.matches("[A-Z]".toRegex())) {
                    indexDatas.add(data)
                } else {
                    indexDatas.add("#")
                }
            }
        }
    }
}
```




##### AssetHelper.kt

```kotlin
package com.lujianfei.plugin1_5.utils

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.lujianfei.module_plugin_base.utils.ResUtils
import com.lujianfei.plugin1_5.bean.BrandBean
import java.io.InputStreamReader
import java.lang.StringBuilder

/**
 *@date     创建时间:2020/7/23
 *@name     作者:陆键霏
 *@describe 描述:
 */
object AssetHelper {
    
    fun getData():List<BrandBean> {
        val type = object : TypeToken<List<BrandBean>>(){}.type
        val gson = Gson()
        return gson.fromJson(jsonFromAsset(), type)
    }

    private fun jsonFromAsset():String {
        val sb = StringBuilder()
        InputStreamReader(ResUtils.pluginContext?.assets?.open("BrandData.json"),Charsets.UTF_8).use {
            sb.append(it.readText())
        }
        return sb.toString()
    }
}
```