# 聊天界面列表 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    // glide
    implementation 'com.github.bumptech.glide:glide:4.9.0'
    kapt 'com.github.bumptech.glide:compiler:4.9.0'
    // Gson
    implementation 'com.google.code.gson:gson:2.8.6'
}
```

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<com.lujianfei.plugin1_4.widget.SoftInputRelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:id="@+id/root"
    android:fitsSystemWindows="true"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    <include
        android:id="@+id/toolbar"
        layout="@layout/toobar_layout"/>
    <RelativeLayout
        android:layout_below="@id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:orientation="vertical">

        <RelativeLayout
            android:id="@+id/controlPanel"
            android:layout_width="match_parent"
            android:layout_height="70dp"
            android:layout_alignParentBottom="true">

            <EditText
                android:id="@+id/editText"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:layout_toStartOf="@id/txtSend" />

            <TextView
                android:id="@+id/txtSend"
                android:layout_width="80dp"
                android:layout_height="match_parent"
                android:layout_alignParentEnd="true"
                android:gravity="center"
                android:text="发送"
                android:textColor="#000000"
                android:textSize="20sp" />
        </RelativeLayout>

        <com.lujianfei.plugin1_4.widget.ChatRecyclerView
            android:id="@+id/recyclerview"
            android:layout_above="@id/controlPanel"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:background="#000000" />
    </RelativeLayout>
</com.lujianfei.plugin1_4.widget.SoftInputRelativeLayout>
```

##### adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    tools:context=".MainActivity"
    android:orientation="vertical"
    android:padding="5dp">
    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title"
        android:textSize="20sp"
        />

    <TextView
        android:id="@+id/summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="summary"
        android:textSize="18sp"
        />
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin1_4

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.core.view.get
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.plugin1_4.bean.AdapterItemBean
import com.lujianfei.plugin1_4.contract.ChatContract
import com.lujianfei.plugin1_4.presenter.ChatPresenter
import com.lujianfei.plugin1_4.widget.PopMenuLongClick
import java.util.ArrayList


class MainActivity : BasePluginActivity(), ChatContract.View {
    companion object {
        const val TAG = "MainActivity"
    }

    ...
    private var toolbar: View? = null

    private var recyclerview: RecyclerView? = null
    private var txtSend: TextView? = null
    private var editText: EditText? = null
    private val mAdapter by lazy { ChatAdapter() }
    private val mPresenter by lazy { ChatPresenter(this) }

    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
        editText = findViewById(R.id.editText)
        txtSend = findViewById(R.id.txtSend)
        ...
        recyclerview = findViewById(R.id.recyclerview)
        recyclerview?.apply {
            adapter = mAdapter
            val linearLayoutManager = LinearLayoutManager(that)
            linearLayoutManager.stackFromEnd = true // 发新消息能从最底部开始
            linearLayoutManager.reverseLayout = true // 可以让消息从上到下排列
            layoutManager = linearLayoutManager
        }
        txtTitle?.text = getPluginString(R.string.app_name)
        ...
    }

    private fun addToobar(): View? {
        return findViewById(R.id.toolbar)
    }

    override fun initData() {
        ...
        mPresenter.loadData(true)
    }

    override fun initEvent() {
       ...
        txtSend?.setOnClickListener {
            mPresenter.sendMessage()
        }
        mAdapter.onItemLongClickListener = { view ->
            val popupMenu = PopMenuLongClick(that, view)
            popupMenu.onMenuCallback = { id ->
                when (id) {
                    R.id.item_delete_all -> {
                        mPresenter.clearAllData()
                        toastMessage("已全部删除")
                        mPresenter.loadData(true)
                    }
                    R.id.item_cancel -> {

                    }
                }
            }
            popupMenu.show()
        }
    }

   ...

    override fun setData(datalist: ArrayList<AdapterItemBean>) {
        mAdapter.setData(datalist)
    }

    override fun getEditMessage(): String? {
        return editText?.text.toString()
    }

    override fun clearEditMessage() {
        editText?.setText("")
    }

    override fun scrollToBottom() {
        if (mAdapter.itemCount > 0) {
            recyclerview?.post {
                recyclerview?.layoutManager?.scrollToPosition(0)
            }
        }
    }

    override fun toastMessage(s: String) {
        Toast.makeText(that, s, Toast.LENGTH_SHORT).show()
    }

    override fun onBackPressed() {
        hideKeyboard()
        super.onBackPressed()
    }

    private fun hideKeyboard() {
        val imm = that?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(that?.currentFocus?.windowToken, 0); //强制隐藏键盘
    }
}

```

##### ChatContract.kt

```kotlin
package com.lujianfei.plugin1_4.contract

import com.lujianfei.plugin1_4.bean.AdapterItemBean
import com.lujianfei.plugin1_4.base.BasePresenter
import com.lujianfei.plugin1_4.base.BaseView
import java.util.ArrayList

interface ChatContract {
    interface View: BaseView {
        fun setData(datalist: ArrayList<AdapterItemBean>)
        fun getEditMessage(): String?
        fun clearEditMessage()
        fun scrollToBottom()
        fun toastMessage(s: String)

    }
    abstract class Presenter(view: View) : BasePresenter<View>(view) {
        abstract fun loadData(scrollToBottom: Boolean = false)
        abstract fun sendMessage()
        abstract fun clearAllData()

    }
}
```

##### ChatPresenter.kt

```kotlin
package com.lujianfei.plugin1_4.presenter

import com.lujianfei.plugin1_4.ChatAdapter
import com.lujianfei.plugin1_4.bean.AdapterItemBean
import com.lujianfei.plugin1_4.bean.ChatMessageBean
import com.lujianfei.plugin1_4.contract.ChatContract
import com.lujianfei.plugin1_4.database.ChatMessageDao
import java.text.SimpleDateFormat
import java.util.*

class ChatPresenter(view: ChatContract.View) : ChatContract.Presenter(view) {
    override fun loadData(scrollToBottom: Boolean) {
        val queryAll = ChatMessageDao.queryAll()
        val datalist = arrayListOf<AdapterItemBean>()
        queryAll?.sortedByDescending { it.date }?.forEach {
            if (it.name == ChatAdapter.toName) {
                datalist.add(AdapterItemBean(ChatAdapter.ITEM_TYPE_RECEIVE,value = it))
            } else {
                datalist.add(AdapterItemBean(ChatAdapter.ITEM_TYPE_SEND,value = it))
            }
        }
        mView?.setData(datalist)
        if (scrollToBottom) {
            mView?.scrollToBottom()
        }
    }

    override fun sendMessage() {
        val message = mView?.getEditMessage()
        if (message.isNullOrEmpty()){
            mView?.toastMessage("输入内容不能为空")
            return
        }
        message.let {
            ChatMessageDao.insert(ChatMessageBean(id = UUID.randomUUID().toString(),
                    icon = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1594543732758&di=9d9fdb5dc2f91ced5c4f2a9ad62383d7&imgtype=0&src=http%3A%2F%2Fsrc.onlinedown.net%2Fimages%2Fxcs%2F10%2F2017-06-23_594c7b91076e1.jpeg",
                    name = ChatAdapter.fromName,
                    message = message,
                    date = System.currentTimeMillis()))
        }
        receiveMessage()
        mView?.clearEditMessage()
        loadData(true)
    }

    override fun clearAllData() {
        ChatMessageDao.clearAll()
    }

    private fun receiveMessage() {
        ChatMessageDao.insert(ChatMessageBean(id = UUID.randomUUID().toString(),
                icon = "https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=1441048056,993952330&fm=11&gp=0.jpg",
                name = ChatAdapter.toName,
                message = "你好吖，亲爱的",
                date = System.currentTimeMillis()))
    }
}
```

##### ChatRecyclerView.kt (扩展 RecyclerView 的功能，加入最新的消息到列表的底部)

```kotlin
package com.lujianfei.plugin1_4.widget

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView

class ChatRecyclerView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr) {
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (oldh > h) { // 确保弹出软键盘的时候，最新消息到列表的底部
            layoutManager?.scrollToPosition(0)
        }
    }
}
```

##### SoftInputRelativeLayout.kt

```kotlin
package com.lujianfei.plugin1_4.widget

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.view.WindowInsets
import android.widget.RelativeLayout

/**
 * 根布局设置了android:fitsSystemWindows="true", 保证标题栏正常显示，加入以下容器处理
 */
class SoftInputRelativeLayout @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    override fun fitSystemWindows(insets: Rect?): Boolean {
        insets?.left = 0;
        insets?.top = 0;
        insets?.right = 0;
        return super.fitSystemWindows(insets);
    }

    override fun onApplyWindowInsets(insets: WindowInsets?): WindowInsets {
        return super.onApplyWindowInsets(insets?.replaceSystemWindowInsets(0, 0, 0, insets.systemWindowInsetBottom));
    }
}
```