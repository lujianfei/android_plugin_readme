# 可拖拽 RecyclerView 代码展示

以下只展示关键部分

#### gradle 依赖引用

```
dependencies {
   ...
    // RecyclerView
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
	...
}
```

##### DragRecyclerViewFragment.kt

```kotlin
package com.lujianfei.other.ui

import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.other.R
import com.lujianfei.other.adapter.MainAdapter
import com.lujianfei.other.bean.MainBean
import com.lujianfei.other.itemdecoration.RecycleGridDivider
import com.lujianfei.other.utils.MyItemTouchCallback

class DragRecyclerViewFragment: BaseFragment(delayInit = false) {
  
    private var recyclerview:RecyclerView ?= null
    private val mAdapter by lazy { MainAdapter() }

    override fun initView() {        
        recyclerview = view?.findViewById(R.id.recyclerview)
        recyclerview?.apply {
            layoutManager = GridLayoutManager(context, 2)
            addItemDecoration(RecycleGridDivider(color = 0xff333333.toInt(), space = 1))
            adapter = mAdapter
        }
    }

    override fun initData() {        
        val mData = arrayListOf<MainBean>()
        for (idx in 0 until 20) {
            mData.add(
                MainBean(title = "码农宝标题 $idx", summary = "长按拖拽 $idx")
            )
        }
        mAdapter.setData(mData)
    }

    override fun initEvent() {        
        val mItemTouchHelper = ItemTouchHelper(MyItemTouchCallback(mAdapter))
        mItemTouchHelper.attachToRecyclerView(recyclerview)
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }

    override fun resouceId() = R.layout.other_drag_recyclerview
}
```

##### other_drag_recyclerview.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">  

    <androidx.recyclerview.widget.RecyclerView
        android:id="@+id/recyclerview"
        android:layout_width="match_parent"
        android:layout_height="match_parent"/>
</LinearLayout>
```

##### MainAdapter.kt

```kotlin
package com.lujianfei.other.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.lujianfei.other.R
import com.lujianfei.other.bean.MainBean
import com.lujianfei.other.utils.MyItemTouchCallback
import java.util.*

class MainAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(),
    MyItemTouchCallback.OnItemPositionListener {

    var mData = arrayListOf<MainBean>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.other_adapter_main, parent, false))
    }

    override fun getItemCount(): Int = mData.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is MyViewHolder) {
            holder.setData(mData[position])
        }
    }

    fun setData(mData: ArrayList<MainBean>) {
        this.mData.clear()
        this.mData.addAll(mData)
        notifyItemRangeChanged(0,mData.size)
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView? = null
        var summary: TextView? = null

        init {
            title = itemView.findViewById(R.id.title)
            summary = itemView.findViewById(R.id.summary)
        }

        fun setData(mainBean: MainBean) {
            title?.text = mainBean.title
            summary?.text = mainBean.summary
        }
    }

    override fun onItemSwap(from: Int, target: Int) {
        //交换数据
        val s = mData[from]
        mData.removeAt(from)
        mData.add(target,s)
        notifyItemMoved(from, target);
    }

    override fun onItemMoved(position: Int) {
        mData.removeAt(position)
        notifyItemRemoved(position)
    }
}
```



##### other_adapter_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:orientation="vertical"
    android:padding="5dp"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="Title"
        android:textSize="20sp" />

    <TextView
        android:id="@+id/summary"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_marginTop="10dp"
        android:ellipsize="end"
        android:maxLines="2"
        android:text="summary"
        android:textSize="18sp" />
</LinearLayout>
```



##### MyItemTouchCallback.kt

```kotlin
package com.lujianfei.other.utils

import android.graphics.Color
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class MyItemTouchCallback(val mOnItemPositionListener:OnItemPositionListener): ItemTouchHelper.Callback() {

    override fun getMovementFlags(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder
    ): Int {
        val dragFlag: Int
        val swipeFlags: Int
        // 如果是表格布局，则可以上下左右的拖动，但是不能滑动
        if (recyclerView.layoutManager is GridLayoutManager) {
            dragFlag = ItemTouchHelper.UP or
            ItemTouchHelper.DOWN or
            ItemTouchHelper.LEFT or
            ItemTouchHelper.RIGHT
            swipeFlags = 0;
        }
        // 如果是线性布局，那么只能上下拖动，只能左右滑动
        else {
            dragFlag = ItemTouchHelper.UP or ItemTouchHelper.DOWN
            swipeFlags = ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT
        }

        // 通过makeMovementFlags生成最终结果
        return makeMovementFlags(dragFlag, swipeFlags);
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        // 被拖动的item位置
        val fromPosition = viewHolder.layoutPosition;
        // 他的目标位置
        val targetPosition = target.layoutPosition;
        // 为了降低耦合，使用接口让Adapter去实现交换功能
        mOnItemPositionListener.onItemSwap(fromPosition, targetPosition);
        return true;
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        // 为了降低耦合，使用接口让Adapter去实现交换功能
        mOnItemPositionListener.onItemMoved(viewHolder.layoutPosition);
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        super.onSelectedChanged(viewHolder, actionState)
        // 当开始拖拽的时候
        if (actionState != ItemTouchHelper.ACTION_STATE_IDLE) {
            viewHolder?.itemView?.setBackgroundColor(Color.LTGRAY)
        }
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        // 当手指松开的时候
        viewHolder.itemView.setBackgroundColor(Color.TRANSPARENT)
        super.clearView(recyclerView, viewHolder)
    }

    interface  OnItemPositionListener {
        // 交换
        fun onItemSwap(from: Int, target: Int)

        // 滑动
        fun onItemMoved(position: Int)
    }
}
```



##### MainBean.kt

```kotlin
package com.lujianfei.other.bean

data class MainBean(
        var title: String,
        var summary: String
)
```

