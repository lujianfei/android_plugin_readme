# 自定义条形进度图 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    ...    
    <LinearLayout
        android:id="@+id/container"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin6_2

import android.content.Intent
import android.net.Uri
import android.util.TypedValue
import android.view.View
import android.widget.*
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.DensityUtils


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

   ...
    
    private var progressBar1: PlayProgressBar? = null
    private var progressBar2: ProgressBar? = null
    private var progressBar3: ProgressBar? = null
    private var btClickHere: Button? = null
    private var container: LinearLayout? = null
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        container = findViewById(R.id.container)
        // ProgressBar 1
        addLabel("样式1")
        progressBar1 = PlayProgressBar(container?.context)
        progressBar1?.currentPosition = 15
        addProgressBar(progressBar1)

        // ProgressBar 2
        addLabel("样式2")
        progressBar2 = ProgressBar(container?.context, null, android.R.style.Widget_ProgressBar_Horizontal)
        progressBar2?.apply {
            progress = 15
            progressDrawable = that?.getDrawable(R.drawable.progress_drawable2)
            indeterminateDrawable = that?.getDrawable(android.R.drawable.progress_indeterminate_horizontal)
            minimumHeight = DensityUtils.dip2px(15f)
        }
        addProgressBar(progressBar2)
        
        // ProgressBar 3
        addLabel("样式3")
        progressBar3 = ProgressBar(container?.context, null, android.R.style.Widget_ProgressBar_Horizontal)
        progressBar3?.apply {
            progress = 15
            progressDrawable = that?.getDrawable(R.drawable.progress_drawable3)
            indeterminateDrawable = that?.getDrawable(android.R.drawable.progress_indeterminate_horizontal)
            minimumHeight = DensityUtils.dip2px(15f)
        }
        addProgressBar(progressBar3)
        
        // Button
        btClickHere = Button(container?.context)
        btClickHere?.apply { 
            text = "点击操作"
        }
        val layoutParamsButton = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParamsButton.topMargin = DensityUtils.dip2px(30f)
        container?.addView(btClickHere, layoutParamsButton)
        
        ...
    }

    private fun addProgressBar(progressBar:View?) {
        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, DensityUtils.dip2px(20f))
        layoutParams.topMargin = DensityUtils.dip2px(30f)
        layoutParams.leftMargin = DensityUtils.dip2px(10f)
        layoutParams.rightMargin = DensityUtils.dip2px(10f)
        container?.addView(progressBar, layoutParams)
    }

    private fun addLabel(s: String) {
        val textView = TextView(container?.context)
        textView.apply {
            text = s
            setTextSize(TypedValue.COMPLEX_UNIT_PX,DensityUtils.dip2px(20f).toFloat())
        }
        val layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutParams.leftMargin = DensityUtils.dip2px(15f)
        layoutParams.topMargin = DensityUtils.dip2px(15f)
        container?.addView(textView, layoutParams)
    }
    

    override fun initEvent() {        
        progressBar1?.setOnClickListener {
            clickProcess()
        }
        progressBar2?.setOnClickListener {
            clickProcess()
        }
        progressBar3?.setOnClickListener {
            clickProcess()
        }
        btClickHere?.setOnClickListener {
            clickProcess()
        }
    }

    private fun clickProcess() {
        progressBar1?.let { playProgressBar->
            playProgressBar.currentPosition = if (playProgressBar.currentPosition >= playProgressBar.max) {
                0
            } else {
                playProgressBar.currentPosition + 10
            }
            progressBar2?.progress = playProgressBar.currentPosition
            progressBar3?.progress = playProgressBar.currentPosition
        }
    }
    
}

```

#### 样式一

是用的派生 View 的方式实现，自定义程度比较高

##### PlayProgressBar.kt

```kotlin
package com.lujianfei.plugin6_2

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View

/**
 *@date     创建时间:2020/7/6
 *@name     作者:陆键霏
 *@describe 描述:
 */
class PlayProgressBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    
    companion object {
        const val TAG = "PlayProgressBar"
        const val BGColor = 0xffEAEAEA.toInt()
        const val ProgressBarColor = 0xff0000ff.toInt()
    }
    var max:Int = 100
    var currentPosition:Int = 0
    set(value) {
        field = value
        invalidate()
    }
    var paint = Paint()
    var progressColor:Int ?= 0
    init {
        paint.color = BGColor
        paint.strokeWidth = 4f
        progressColor = ProgressBarColor
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        if (progressColor == null) {
                progressColor = ProgressBarColor
        }
        canvas?.let {canvas->
            canvas.save()
                val percent = currentPosition.toFloat() / max.toFloat()
                val currentWidth = width.toFloat() * percent
                // 画底色
                paint.color = BGColor
                canvas.drawLine(currentWidth,0f, width.toFloat(),0f,paint)
                // 画进度色
                progressColor?.let {progressColor->
                    paint.color = progressColor
                }
                canvas.drawLine(0f,0f, currentWidth ,0f,paint)
            canvas.restore()
        }
    }
}
```

#### 样式二

是用的 progressDrawable ， indeterminateDrawable 分别来进行配置实现

#### progressDrawable 

progress_drawable2.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:id="@android:id/background">
        <shape>
            <corners android:radius="20dp" />
            <gradient
                android:angle="0"
                android:endColor="@color/progress_bg_color"
                android:startColor="@color/progress_bg_color" />
        </shape>
    </item>
    <item android:id="@android:id/progress">
        <clip>
            <scale
                android:drawable="@drawable/progressbar_shape"
                android:scaleWidth="100%" />
        </clip>
    </item>
</layer-list>

```

progressbar_shape.xml
```xml
<?xml version="1.0" encoding="utf-8"?>
<shape xmlns:android="http://schemas.android.com/apk/res/android">

    <corners android:radius="20dp" />
    <gradient
        android:angle="0"
        android:endColor="@color/progress_color"
        android:startColor="@color/progress_color" />

</shape>
```


#### indeterminateDrawable 

直接用系统预置的 android.R.drawable.progress_indeterminate_horizontal 即可



#### 样式三

也是用的 progressDrawable ， indeterminateDrawable 分别来进行配置实现，这是带边框的版本，且加了一点内间距

#### progressDrawable 

progress_drawable3.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<layer-list xmlns:android="http://schemas.android.com/apk/res/android">
    <item android:id="@android:id/background">
        <shape>
            <corners android:radius="20dp" />
            <stroke
                android:width="1dp"
                android:color="@color/progress_border_color" />
            <gradient
                android:angle="0"
                android:endColor="@color/transparent"
                android:startColor="@color/transparent" />
            <padding
                android:bottom="3dp"
                android:left="3dp"
                android:right="3dp"
                android:top="3dp" />
        </shape>
    </item>
    <item android:id="@android:id/progress">
        <clip>
            <scale
                android:drawable="@drawable/progressbar_shape"
                android:scaleWidth="100%" />
        </clip>
    </item>
</layer-list>

```

#### indeterminateDrawable 

直接用系统预置的 android.R.drawable.progress_indeterminate_horizontal 即可





#### 各种色值

colors.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="theme_color">#6200EE</color>
    <color name="text_color">#ffffff</color>
    <color name="transparent">#00000000</color>
    <color name="progress_bg_color">#dddddd</color>
    <color name="progress_color">@color/theme_color</color>
    <color name="progress_border_color">@color/theme_color</color>
</resources>
```

