# 自绘等级进度条(继承View实现) 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
      
    
    <TextView
        android:id="@+id/txt_current_progress"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:textSize="20sp"
        android:layout_gravity="center_horizontal"
        android:text="当前等级: 2"
        android:padding="5dp"
        />
    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:paddingTop="20dp"
        android:paddingBottom="20dp"
        android:paddingStart="20dp"
        android:paddingEnd="20dp"
        android:background="#1b000000"
        android:orientation="vertical"
        >
        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="horizontal"
            android:paddingStart="5dp"
            android:paddingEnd="5dp">
            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_weight="1"
                android:text="1"
                />
            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_weight="1"
                android:gravity="center"
                android:text="2"
                />
            <TextView
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_weight="1"
                android:gravity="end"
                android:text="3"
                />
        </LinearLayout>
        <com.lujianfei.plugin6_4.CustomLevelSeekBar
            android:id="@+id/customLevelSeekBar"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"/>
    </LinearLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin6_4

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.*
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }
   
    private var customLevelSeekBar: CustomLevelSeekBar?= null
    private var txt_current_progress:TextView ?= null
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        customLevelSeekBar = findViewById(R.id.customLevelSeekBar)
        txt_current_progress = findViewById(R.id.txt_current_progress)       
    }    

    override fun initEvent() {       
        customLevelSeekBar?.onSeekCallbackListener = { level->
            txt_current_progress?.text = "当前等级: ${level+1}"
        }
    }    
}

```



##### CustomSeekBar.kt (核心组件类)

```kotlin
package com.lujianfei.plugin6_4

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.os.Build
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.annotation.RequiresApi
import com.lujianfei.module_plugin_base.utils.DensityUtils

/**
 *@date     创建时间:2020/11/11
 *@name     作者:陆键霏
 *@describe 描述:
 */
class CustomLevelSeekBar @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        const val TAG = "CustomSeekBar"
        val radius = DensityUtils.dip2px(8f).toFloat() // 滑动条圆角
        val circleRadius = DensityUtils.dip2px(8f).toFloat() // 中心拖动按钮半径
        val barHeight = DensityUtils.dip2px(10f).toFloat() // 滑动条高度
        val barTotalHeight = DensityUtils.dip2px(25f) // 滑动条总高度（包括不可见区域）
        val paddingStartEnd = DensityUtils.dip2px(10f).toFloat() // 给拖动按钮左右两端预留空间
        val leftBarColor = 0xff0A79E6.toInt() // 拖动按钮左侧滑动条颜色
        val rightBarColor = 0xffBFBFBF.toInt() // 拖动按钮右侧滑动条颜色
    }

    private val paint = Paint()
    private var progress = 50
    private var max = 100
    private var levelSize = 3 // 分为3个等级
    private var levelRange = arrayListOf<SeekRange>() // 用于记录滑动按钮可回弹的区域
    var onSeekCallbackListener:((Int)->Unit) ?= null

    init {
        paint.isAntiAlias = true
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        // 设置滑动条的总高度, 否则将等于父布局的高度
        setMeasuredDimension(widthMeasureSpec, barTotalHeight)
    }
    
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        // 为了给拖动按钮预留左右两端的显示空间，将滑动条的有效果长度减少
        val afterWidth = width - 2 * paddingStartEnd 
        // 计算当前拖动按钮的移动比例
        val scale = progress.toFloat() / max.toFloat()
        // 计算滑动条顶部位置
        val barTop = height / 2 - barHeight / 2
        // 计算滑动条的垂直中心位置
        val barCenterY = (height / 2).toFloat()
        // 等级划分
        calRange()
        // 绘制拖动按钮的左边
        paint.color = leftBarColor
        val thumbCenterX = paddingStartEnd + afterWidth * scale
        canvas?.drawRoundRect(
            paddingStartEnd,
            barTop,
                thumbCenterX,
            barTop + barHeight,
            radius, radius,
            paint
        )
        // 绘制拖动按钮的右边
        paint.color = rightBarColor
        canvas?.drawRoundRect(
                thumbCenterX,
            barTop,
            paddingStartEnd + afterWidth,
            barTop + barHeight,
            radius,
            radius,
            paint
        )
        // 绘制拖动按钮
        paint.color = 0xffffffff.toInt()
        canvas?.drawCircle(thumbCenterX, barCenterY, circleRadius, paint)
        onSeekCallbackListener?.invoke(progressToLevel(progress))
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        val afterWidth = width - 2 * paddingStartEnd
        when (event?.action) {
            MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> {
                // 滑动时，通过改变 progress 来改变拖动按钮的位置
                progress = ((event.x - paddingStartEnd)* max.toFloat() / afterWidth).toInt()
                if (progress < 0) progress = 0
                if (progress > max) progress = max
            }
            MotionEvent.ACTION_UP -> {
                progress = progressToLevelProgress(progress)
            }
        }
       
        invalidate()
        return true
    }

    private fun calRange() {
        if (levelRange.isEmpty() && levelSize > 2) {
            val rangeSize = (max / (levelSize - 1))
            for (idx in 0 until levelSize) {
                when (idx) {
                    0 -> {
                        // 首个刻度
                        levelRange.add(
                                SeekRange(
                                        levelProgress = idx * rangeSize,
                                        minProgress = idx * rangeSize,
                                        maxProgress = idx * rangeSize + rangeSize / 2
                                )
                        )
                    }
                    levelSize - 1 -> {
                        // 结尾刻度
                        levelRange.add(
                                SeekRange(
                                        levelProgress = idx * rangeSize,
                                        minProgress = idx * rangeSize - rangeSize / 2,
                                        maxProgress = idx * rangeSize
                                )
                        )
                    }
                    else -> {
                        // 中间刻度
                        levelRange.add(
                                SeekRange(
                                        levelProgress = idx * rangeSize,
                                        minProgress = idx * rangeSize - rangeSize / 2,
                                        maxProgress = idx * rangeSize + rangeSize / 2
                                )
                        )
                    }
                }
            }
        }
    }

    fun setProgress(progress:Int) {
        this.progress = progress
        invalidate()
    }

    /**
     * 当前进度转换为等级进度
     */
    private fun progressToLevelProgress(progress:Int):Int {
        for (levelR in levelRange.withIndex()) {
            if (progress >= levelR.value.minProgress && progress <= levelR.value.maxProgress) {
                return levelR.value.levelProgress
            }
        }
        return 0
    }

    /**
     * 当前进度转等级
     */
    private fun progressToLevel(progress:Int):Int {
        for (levelR in levelRange.withIndex()) {
            if (progress >= levelR.value.minProgress && progress <= levelR.value.maxProgress) {
                return levelR.index
            }
        }
        return 0
    }

    data class SeekRange(
            var levelProgress:Int,
            var minProgress:Int,
            var maxProgress:Int
    )
}
```
