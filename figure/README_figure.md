# 圆形进度图 代码展示

以下只展示关键部分

##### activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context=".MainActivity">
    
    ...

    <RelativeLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        >
        <com.lujianfei.plugin6_1.RoundFigure
            android:id="@+id/roundFigure"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            />
    </RelativeLayout>
</LinearLayout>
```

##### MainActivity.kt

```kotlin
package com.lujianfei.plugin6_1

import android.content.Intent
import android.net.Uri
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.lujianfei.module_plugin_base.base.BasePluginActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean


class MainActivity : BasePluginActivity() {
    companion object {
        const val TAG = "MainActivity"
    }

    ...
    private var roundFigure: RoundFigure? = null
    override fun resouceId(): Int = R.layout.activity_main

    override fun initView() {
        roundFigure = findViewById(R.id.roundFigure)
        ...
    }

   ...

    override fun initEvent() {
       ...
        roundFigure?.setOnClickListener {
            clickProcess()
        }       
    }

    private fun clickProcess() {
        roundFigure?.let {
            roundFigure?.currentProgress?.let { currentProgress ->
                if (currentProgress + 10f >= 110) {
                    roundFigure?.currentProgress = 0
                } else {
                    roundFigure?.currentProgress = currentProgress + 10
                }
            }
        }
    }

...
}

```

##### RoundFigure.kt

```kotlin
package com.lujianfei.plugin6_1

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PointF
import android.graphics.RectF
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import com.lujianfei.module_plugin_base.utils.DensityUtils
import java.lang.Float.min
import java.lang.Math.abs


class RoundFigure @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    companion object {
        const val TAG = "RoundFigure"
        const val TEXT_COLOR = 0xff000000.toInt() // 文字颜色
        const val RING_COLOR = 0xff67D5CC.toInt() // 圆环颜色
        const val RING_BG_COLOR = 0xffdddddd.toInt() // 圆环背景颜色
    }
    // 圆环样式
    private var ringPaint  = Paint()
    // 圆环背景样式
    private var ringBgPaint  = Paint()
    // 圆环文字样式
    private var textPaint  = Paint()
    // 半径   
    private var radius = DensityUtils.dip2px(80f)
    // 总进度
    private var totalProgress = 100
    // 记录文字高度
    private var txtHeight = 0f
    // 记录圆环位置以及大小
    private val oval = RectF()
    // 当前进度
    var currentProgress = 0
        set(value) {
            field = value
            postInvalidate()
        }

    init {
        setBackgroundColor(0x00000000)
        initVariable()
    }

    private fun initVariable() {
        ringPaint.apply {
            isAntiAlias = true
            isDither = true
            color = RING_COLOR
            style = Paint.Style.STROKE
            strokeCap = Paint.Cap.ROUND
            strokeWidth = DensityUtils.dip2px(10f).toFloat()
        }
        ringBgPaint.apply {
            isAntiAlias = true
            isDither = true
            color = RING_BG_COLOR
            style = Paint.Style.STROKE
            strokeCap = Paint.Cap.ROUND
            strokeWidth = DensityUtils.dip2px(10f).toFloat()
        }
        textPaint.apply {
            isAntiAlias = true
            style = Paint.Style.FILL
            color = TEXT_COLOR
            textSize = DensityUtils.dip2px(20f).toFloat()
        }
        val fm = textPaint.fontMetrics
        txtHeight = fm.descent + kotlin.math.abs(fm.ascent)
        Log.d(TAG, "initVariable txtHeight = $txtHeight radius = $radius")
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        canvas?.let {
            if (currentProgress >= 0) {

                // 确定圆环的位置以及大小
                oval.set( (width / 2 - radius).toFloat(),
                        (height / 2 - radius).toFloat(),
                        (width / 2 + radius).toFloat(), (height / 2 + radius).toFloat())

                // 画背景
                canvas.drawArc(oval, 0f, 360f, false, ringBgPaint)

                // 画前景
                canvas.drawArc(
                        oval,
                        0f,
                        currentProgress.toFloat() / totalProgress * 360,
                        false,
                        ringPaint
                )
                // 显示百分比文字
                val showText = "$currentProgress%"
                val txtWidth = textPaint.measureText(showText, 0, showText.length)
                canvas.drawText(
                        showText,
                        width / 2 - txtWidth / 2,
                        height / 2 + txtHeight / 4,
                        textPaint
                )
            }
        }
    }
}
```

