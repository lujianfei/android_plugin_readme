# Compose Canvas 代码展示

引用的依赖

```groovy
implementation "androidx.compose.ui:ui:1.0.1"
implementation "androidx.compose.material:material:1.0.1"
implementation "androidx.compose.ui:ui-tooling-preview:1.0.1"
implementation 'androidx.lifecycle:lifecycle-runtime-ktx:2.3.1'
implementation "androidx.compose.runtime:runtime-livedata:1.0.1"
implementation "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
implementation 'androidx.activity:activity-compose:1.3.1'
implementation "com.google.accompanist:accompanist-coil:0.13.0"
```



以下只展示关键部分

##### MainActivity.kt

```kotlin
 override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Page(
                leftIcon = R.drawable.ic_back,
                onLeftClick = {
                    finish()
                },
                title = "Compose Canvas",
                onRightClick = {
                    openBrowser()
                },
                rightText = bean.codeUrl
            ) {
                Content()
            }
        }
 }

@ExperimentalAnimationApi
@ExperimentalTextApi
@ExperimentalUnitApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun Content() {
    CanvasPage()
}

```

##### Page.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable

@Composable
fun Page(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
    content: @Composable () -> Unit,
) {
    Column {
        AppBar(title = title, leftIcon, rightText, onLeftClick, onRightClick)
        content()
    }
}
```

##### AppBar.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.lujianfei.module_plugin_base.R

@Composable
fun AppBar(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
) {
    TopAppBar {
        Row(verticalAlignment = Alignment.CenterVertically) { // 垂直居中
            leftIcon?.let {
                Spacer(modifier = Modifier.width(5.dp))
                Icon(painter = painterResource(id = leftIcon), contentDescription = null, Modifier.clickable {
                    onLeftClick()
                })
            }
            Spacer(modifier = Modifier.width(20.dp))
            Text(text = title, modifier = Modifier.weight(1f), fontSize = 20.sp, textAlign = TextAlign.Start)
            if (rightText != null) {
                Text(text = rightText, textAlign = TextAlign.Center, modifier = Modifier.clickable {
                    onRightClick()
                })
                Spacer(modifier = Modifier.width(10.dp))
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun PreviewAppBar() {
    AppBar(title = "hello", leftIcon = R.drawable.ic_back)
}
```

##### CanvasDemo.kt

```kotlin
package com.lujianfei.compose.ui.demo

import android.graphics.BitmapFactory
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.CornerRadius
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.*
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.graphics.drawscope.drawIntoCanvas
import androidx.compose.ui.graphics.drawscope.rotate
import androidx.compose.ui.graphics.drawscope.withTransform
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import com.lujianfei.compose.R

@Composable
fun CanvasPage() {
    CanvasContent()
}

@Composable
private fun CanvasContent() {
    Box(modifier = Modifier.fillMaxSize()) {
        CanvasDemo()
    }
}

@Composable
private fun CanvasDemo() {
    Column(
        modifier = Modifier.verticalScroll(rememberScrollState())
    ) {
        val color = MaterialTheme.colors.onBackground
        CanvasLineDemo(color)
        CanvasDashLineDemo(color)
        CanvasRoundLineDemo(color)
        CanvasRectDemo(color)
        CanvasCirCleDemo(color)
        CanvasPathDemo(color)
        CanvasArcDemo(color)
        CanvasSectorDemo(color)
        CanvasOvalDemo(color)
        CanvasRoundRectDemo(color)
        CanvasCurveDemo(color)
        CanvasRotateDemo(color)
        CanvasRotateTransitionDemo(color)
        CanvasBitmapDemo(color)
        AndroidCanvasDemo(color)
        Spacer(modifier = Modifier.height(100.dp))
    }
}

@Composable
private fun CanvasLineDemo(color: Color) {
    Text("实线")
    Canvas(
        modifier = Modifier
            .fillMaxWidth()
            .padding(30.dp, 0.dp)
            .height(30.dp)
    ) {
        drawLine(color, Offset(0f, 20f), Offset(400f, 20f), strokeWidth = 5f)
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasDashLineDemo(color: Color) {
    Text("虚线")
    Canvas(
        modifier = Modifier
            .fillMaxWidth()
            .padding(30.dp, 0.dp)
            .height(30.dp)
    ) {
        drawLine(
            color, Offset(0f, 20f), Offset(400f, 20f), strokeWidth = 5f,
            pathEffect = PathEffect.dashPathEffect(floatArrayOf(10f, 20f), 5f)
        )
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasRoundLineDemo(color: Color) {
    Text("端点圆滑")
    Canvas(
        modifier = Modifier
            .fillMaxWidth()
            .height(30.dp)
            .padding(30.dp, 0.dp)
    ) {
        drawLine(
            color, Offset(0f, 20f), Offset(400f, 20f), strokeWidth = 15f,
            cap = StrokeCap.Round
        )
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasRectDemo(color: Color) {
    Text("矩形")
    Canvas(modifier = Modifier.size(100.dp)) {
        drawRect(color = color, size = size / 2f)
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasCirCleDemo(color: Color) {
    Text("圆")
    Canvas(
        modifier = Modifier
            .size(100.dp)
    ) {
        drawCircle(color = color)
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasPathDemo(color: Color) {
    Text("Path")
    Canvas(
        modifier = Modifier
            .height(100.dp)
            .fillMaxWidth()
    ) {
        val path = Path()
        path.lineTo(200f, 0f)
        path.lineTo(100f, 40f)
        path.lineTo(180f, 20f)
        path.lineTo(180f, 100f)
        drawPath(path, color, style = Stroke(5f))
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasArcDemo(color: Color) {
    Text("圆弧")
    Canvas(
        modifier = Modifier
            .size(100.dp)
    ) {
        drawArc(color, 0f, -135f, false, style = Stroke(5f))
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasSectorDemo(color: Color) {
    Text("扇形")
    Canvas(
        modifier = Modifier
            .size(100.dp)
    ) {
        drawArc(color, 0f, -135f, true, style = Stroke(5f))
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasOvalDemo(color: Color) {
    Text("椭圆")
    Canvas(modifier = Modifier.size(120.dp, 100.dp)) {
        drawOval(color)
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasRoundRectDemo(color: Color) {
    Text("圆角矩形")
    Canvas(
        modifier = Modifier
            .size(120.dp, 100.dp)
            .padding(10.dp)
    ) {
        drawRoundRect(color, cornerRadius = CornerRadius(10f))
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasCurveDemo(color: Color) {
    Text("贝塞尔曲线")
    Canvas(
        modifier = Modifier
            .height(100.dp)
            .fillMaxWidth()
    ) {
        val path = Path()
        path.cubicTo(0f, 100f, 100f, 0f, 200f, 100f)
        drawPath(path, color, style = Stroke(5f))
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasRotateDemo(color: Color) {
    Text("旋转")
    Canvas(
        modifier = Modifier
            .size(100.dp)
    ) {
        rotate(45f) {
            drawRect(
                color = color,
                topLeft = Offset(100f, 100f),
                size = size / 2f
            )
        }
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasRotateTransitionDemo(color: Color) {
    Text("旋转加位移")
    Canvas(
        modifier = Modifier
            .size(100.dp)
    ) {
        withTransform({
            translate(300f)
            rotate(45f)
        }) {
            drawRect(
                color = color,
                topLeft = Offset(0f, 100f),
                size = size / 2f
            )
        }
    }
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun CanvasBitmapDemo(color: Color) {
    Text("Bitmap")
    val context = LocalContext.current
    Canvas(
        modifier = Modifier
            .size(100.dp)
    ) {
        val bitmap = BitmapFactory.decodeResource(context.resources, R.mipmap.viewactivity_ic_launcher)
        drawImage(bitmap.asImageBitmap())
    }
    Spacer(modifier = Modifier.height(60.dp))
}

@Composable
private fun AndroidCanvasDemo(color: Color) {
    Text("Android Canvas")
    Canvas(
        modifier = Modifier
            .size(100.dp)
    ) {
        drawIntoCanvas { canvas ->
            val paint = Paint()
            paint.color = color
            canvas.drawOval(0f, 0f, 100f, 100f, paint)
        }
    }
    Spacer(modifier = Modifier.height(10.dp))
}

```



