# compose 底部Tab导航 代码展示

引用的依赖

```groovy
implementation "androidx.compose.ui:ui:1.0.1"
implementation "androidx.compose.material:material:1.0.1"
implementation "androidx.compose.ui:ui-tooling-preview:1.0.1"
implementation 'androidx.lifecycle:lifecycle-runtime-ktx:2.3.1'
implementation "androidx.compose.runtime:runtime-livedata:1.0.1"
implementation "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
implementation 'androidx.activity:activity-compose:1.3.1'
implementation "com.google.accompanist:accompanist-coil:0.13.0"

implementation "androidx.navigation:navigation-compose:2.4.0-alpha04" // 新增项
```



以下只展示关键部分

##### MainActivity.kt

```kotlin
 override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Page(
                leftIcon = R.drawable.ic_back,
                onLeftClick = {
                    finish()
                },
                title = "Compose 布局",
                onRightClick = {
                    openBrowser()
                },
                rightText = bean.codeUrl
            ) {
                Content()
            }
        }
 }

@ExperimentalAnimationApi
@ExperimentalTextApi
@ExperimentalUnitApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun Content() {
    BottomTabDemo()
}

```

##### Page.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable

@Composable
fun Page(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
    content: @Composable () -> Unit,
) {
    Column {
        AppBar(title = title, leftIcon, rightText, onLeftClick, onRightClick)
        content()
    }
}
```

##### AppBar.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.lujianfei.module_plugin_base.R

@Composable
fun AppBar(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
) {
    TopAppBar {
        Row(verticalAlignment = Alignment.CenterVertically) { // 垂直居中
            leftIcon?.let {
                Spacer(modifier = Modifier.width(5.dp))
                Icon(painter = painterResource(id = leftIcon), contentDescription = null, Modifier.clickable {
                    onLeftClick()
                })
            }
            Spacer(modifier = Modifier.width(20.dp))
            Text(text = title, modifier = Modifier.weight(1f), fontSize = 20.sp, textAlign = TextAlign.Start)
            if (rightText != null) {
                Text(text = rightText, textAlign = TextAlign.Center, modifier = Modifier.clickable {
                    onRightClick()
                })
                Spacer(modifier = Modifier.width(10.dp))
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun PreviewAppBar() {
    AppBar(title = "hello", leftIcon = R.drawable.ic_back)
}
```

##### BottomTabDemo.kt

```kotlin
package com.lujianfei.compose.ui.demo

import androidx.annotation.StringRes
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.lujianfei.compose.R
import com.lujianfei.compose.viewmodel.MainViewModel
import com.lujianfei.module_plugin_base.utils.ResUtils

const val KEY_ROUTE = "key_route"
@ExperimentalFoundationApi
@ExperimentalAnimationApi
@Composable
fun BottomTabPage() {
    Tab(MainViewModel())
}

@ExperimentalFoundationApi
@Composable
fun Tab(viewModel: MainViewModel){
    // tab标题
    val listItem = listOf("组织","发现","我的")
    // tab未选图标
    val icons = listOf(R.drawable.ic_home,R.drawable.ic_video,R.drawable.ic_profile)
    // tab选中图标
    val selectIcons = listOf(R.drawable.ic_home_fill,R.drawable.ic_video_fill,R.drawable.ic_profile_fill)
    // 记住选中tab位置
    val selectIndex = remember {
        mutableStateOf(0)
    }
    // 脚手架
    val navControllers = rememberNavController()
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        bottomBar = {
            // BottomNavigation的显示和隐藏先借助本地数据存储，再说
            BottomNavigation(
                backgroundColor = Color.White,
                elevation = 3.dp
            ) {
                val navBackStackEntry by navControllers.currentBackStackEntryAsState()
                val currentRoute = navBackStackEntry?.arguments?.getString(KEY_ROUTE)

                items.forEachIndexed { index, s ->
                    BottomNavigationItem(
                        selected = currentRoute == s.route,
                        onClick = {
                            selectIndex.value = index
                            navControllers.navigate(s.route) {
                                // Pop up to the start destination of the graph to
                                // avoid building up a large stack of destinations
                                // on the back stack as users select items
                                popUpTo(navControllers.graph.startDestinationId)
                                // Avoid multiple copies of the same destination when
                                // reselecting the same item
                                launchSingleTop = true
                            }
                        },
                        icon = {
                            when(index){
                                selectIndex.value -> {
                                    Image(painter = painterResource(id = selectIcons[index]), contentDescription = null)
                                }
                                else -> {
                                    Image(painter = painterResource(id = icons[index]), contentDescription = null)
                                }
                            }
                        },
                        label = {
                            Text(
                                text = listItem[index],
                                textAlign = TextAlign.Center,
                                color = if (index == selectIndex.value) Color(ResUtils.context?.resources?.getColor(R.color.purple_700)?:0) else Color(ResUtils.context?.resources?.getColor(R.color.unselect_tab_color)?:0)
                            )
                        }
                    )
                }
            }
        },
        content = {
            NavHost(navControllers, startDestination = Screen.Home.route) {
                // 首页
                composable(Screen.Home.route) { HomePage(viewModel = viewModel,navControllers = navControllers) }
                // 发现
                composable(Screen.Find.route) { FindPage(viewModel = viewModel,navControllers = navControllers) }
                // 我的
                composable(Screen.Profile.route) { ProfilePage(viewModel = viewModel,navControllers = navControllers) }
            }
        }
    )
}

@Composable
fun HomePage(viewModel: MainViewModel, navControllers:NavHostController) {
    Box(modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
        ) {
            Text(text = "Home Page")
        }
    }
}

@Composable
fun FindPage(viewModel: MainViewModel, navControllers:NavHostController) {
    Box(modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
        ) {
            Text(text = "Find Page")
        }
    }
}

@Composable
fun ProfilePage(viewModel: MainViewModel, navControllers:NavHostController) {
    Box(modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .verticalScroll(rememberScrollState())
        ) {
            Text(text = "Profile Page")
        }
    }
}


sealed class Screen(val route: String, @StringRes val resourceId: Int) {
    object Home : Screen("home", R.string.compose_tab_home)
    object Find : Screen("video", R.string.compose_tab_video)
    object Profile : Screen("profile", R.string.compose_tab_profile)

}

val items = listOf(
    Screen.Home,
    Screen.Find,
    Screen.Profile
)
```



