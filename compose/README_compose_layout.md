# compose 布局 代码展示

引用的依赖

```groovy
implementation "androidx.compose.ui:ui:1.0.1"
implementation "androidx.compose.material:material:1.0.1"
implementation "androidx.compose.ui:ui-tooling-preview:1.0.1"
implementation 'androidx.lifecycle:lifecycle-runtime-ktx:2.3.1'
implementation "androidx.compose.runtime:runtime-livedata:1.0.1"
implementation "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
implementation 'androidx.activity:activity-compose:1.3.1'
implementation "com.google.accompanist:accompanist-coil:0.13.0"
```



以下只展示关键部分

##### MainActivity.kt

```kotlin
 override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Page(
                leftIcon = R.drawable.ic_back,
                onLeftClick = {
                    finish()
                },
                title = "Compose 布局",
                onRightClick = {
                    openBrowser()
                },
                rightText = bean.codeUrl
            ) {
                Content()
            }
        }
 }

@ExperimentalAnimationApi
@ExperimentalTextApi
@ExperimentalUnitApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun Content() {
    LayoutPage()
}

```

##### Page.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable

@Composable
fun Page(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
    content: @Composable () -> Unit,
) {
    Column {
        AppBar(title = title, leftIcon, rightText, onLeftClick, onRightClick)
        content()
    }
}
```

##### AppBar.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.lujianfei.module_plugin_base.R

@Composable
fun AppBar(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
) {
    TopAppBar {
        Row(verticalAlignment = Alignment.CenterVertically) { // 垂直居中
            leftIcon?.let {
                Spacer(modifier = Modifier.width(5.dp))
                Icon(painter = painterResource(id = leftIcon), contentDescription = null, Modifier.clickable {
                    onLeftClick()
                })
            }
            Spacer(modifier = Modifier.width(20.dp))
            Text(text = title, modifier = Modifier.weight(1f), fontSize = 20.sp, textAlign = TextAlign.Start)
            if (rightText != null) {
                Text(text = rightText, textAlign = TextAlign.Center, modifier = Modifier.clickable {
                    onRightClick()
                })
                Spacer(modifier = Modifier.width(10.dp))
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun PreviewAppBar() {
    AppBar(title = "hello", leftIcon = R.drawable.ic_back)
}
```

##### LayoutDemo.kt

```kotlin
package com.lujianfei.compose.ui.demo

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.unit.dp

/**
 * 布局界面代码
 */
@ExperimentalUnitApi
@Composable
fun LayoutPage() {
    LayoutContent()
}


@ExperimentalUnitApi
@Composable
private fun LayoutContent() {
    var clickCount by remember { mutableStateOf(0) }
    Scaffold(
        modifier = Modifier.fillMaxSize(),
        /**
         * 抽屉菜单内容
         */
        drawerContent = {
            Text(text = "这是drawerContent")
        },
        /**
         * 悬浮按钮
         */
        floatingActionButton = {
            val context = LocalContext.current
            Box(
                modifier = Modifier
                    .size(70.dp)
                    .background(
                        MaterialTheme.colors.primary,
                        CircleShape
                    )
                    .clickable {
                        clickCount++
                    }, contentAlignment = Alignment.Center
            ) {
                Text(
                    text = "Action\nButton",
                    color = Color.White,
                    fontSize = TextUnit(10f, TextUnitType.Sp),
                    textAlign = TextAlign.Center
                )
            }
        },
        /**
         * 内容部分界面
         */
        content = {
            Column(modifier = Modifier.fillMaxSize()) {
                RowDemo() // 行布局
                ColumnDemo() // 列布局
                BoxDemo() // 盒式布局
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .height(100.dp)
                        .fillMaxWidth()
                ) {
                    Text(text = "试试向右滑>>>")
                }
                Text(
                    "第${clickCount}次点击",
                    Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
                Text(
                    text = "这个界面使用的是Scaffold来布局",
                    modifier = Modifier.fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
            }
        })
}

/**
 * 行布局
 */
@Composable
private fun RowDemo() {
    Box(modifier = Modifier.height(IntrinsicSize.Min)) {
        Row {
            Box(
                modifier = Modifier
                    .background(Color(0xFFCC3939)) //红色
                    .width(100.dp)
                    .height(50.dp)
            )
            Box(
                modifier = Modifier
                    .background(Color(0xFF3587EC)) // 蓝色
                    .width(100.dp)
                    .height(50.dp)
            )
            Box(
                modifier = Modifier
                    .background(Color(0xFFDD26AF)) // 粉色
                    .width(100.dp)
                    .height(50.dp)
            )
            Box(
                modifier = Modifier
                    .background(Color(0xFF1BC3E0)) // 浅蓝
                    .width(100.dp)
                    .height(50.dp)
            )
        }
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(
                text = "这是Row",
                modifier = Modifier
                    .padding(10.dp),
                color = Color.White
            )
        }
    }
}

/**
 * 列布局
 */
@Composable
fun ColumnDemo() {
    Box(
        modifier = Modifier
            .height(IntrinsicSize.Min)
            .padding(start = 0.dp, top = 30.dp, end = 0.dp, bottom = 0.dp)
    ) {
        Column() {
            Box(
                modifier = Modifier
                    .background(Color(0xFFDB4B4B)) // 红色
                    .fillMaxWidth()
                    .height(50.dp)
            )
            Box(
                modifier = Modifier
                    .background(Color(0xFF3587EC)) // 蓝色
                    .fillMaxWidth()
                    .height(50.dp)
            )
            Box(
                modifier = Modifier
                    .background(Color(0xFFDD26AF)) // 粉色
                    .fillMaxWidth()
                    .height(50.dp)
            )
            Box(
                modifier = Modifier
                    .background(Color(0xFF1BC3E0)) // 浅蓝
                    .fillMaxWidth()
                    .height(50.dp)
            )
        }
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
            Text(
                text = "这是Column",
                modifier = Modifier
                    .padding(10.dp),
                color = Color.White
            )
        }
    }
}

/**
 * 嵌套盒式布局
 */
@Composable
fun BoxDemo() {
    Box(modifier = Modifier.padding(start = 0.dp, top = 30.dp, end = 0.dp, bottom = 0.dp)) {
        Box(
            modifier = Modifier
                .size(200.dp)
                .background(Color(0xFFDD26AF)), // 粉色
            contentAlignment = Alignment.Center
        ) {
            Box(
                modifier = Modifier
                    .size(160.dp)
                    .background(Color(0xFF1BC3E0)), // 浅蓝
                contentAlignment = Alignment.Center
            ) {
                Box(
                    modifier = Modifier
                        .size(120.dp)
                        .background(Color(0xFFDB4B4B)), // 红色
                    contentAlignment = Alignment.Center
                ) {
                    Text(text = "这是Box", modifier = Modifier.padding(10.dp), color = Color.White)
                }
            }
        }
    }
}
```



