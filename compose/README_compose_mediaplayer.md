# Compose MediaPlayer 代码展示

引用的依赖

```groovy
implementation "androidx.compose.ui:ui:1.0.1"
implementation "androidx.compose.material:material:1.0.1"
implementation "androidx.compose.ui:ui-tooling-preview:1.0.1"
implementation 'androidx.lifecycle:lifecycle-runtime-ktx:2.3.1'
implementation "androidx.compose.runtime:runtime-livedata:1.0.1"
implementation "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
implementation 'androidx.activity:activity-compose:1.3.1'
implementation "com.google.accompanist:accompanist-coil:0.13.0"
```



以下只展示关键部分

##### MainActivity.kt

```kotlin
private var mMediaPlayerViewModel: MediaPlayerViewModel?= null

override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Page(
                leftIcon = R.drawable.ic_back,
                onLeftClick = {
                    finish()
                },
                title = "Compose 文本",
                onRightClick = {
                    openBrowser()
                },
                rightText = bean.codeUrl
            ) {
                Content()
            }
        }
 }

@ExperimentalAnimationApi
@ExperimentalTextApi
@ExperimentalUnitApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun Content() {
		 mMediaPlayerViewModel = ViewModelProvider(this).get(MediaPlayerViewModel::class.java)
        mMediaPlayerViewModel?.let {
          // 初始化播放器和定时器
          it.initMediaPlayerAndTimer(this@MainActivity)
          // 渲染界面
          MediaPlayerPage(it)
     }
}

override fun onDestroy() {
  mMediaPlayerViewModel?.release()
  super.onDestroy()
}

```

##### MediaPlayerDemo.kt

```kotlin
package com.lujianfei.compose.ui.demo

import android.R
import android.media.MediaPlayer
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.ExperimentalTextApi
import androidx.compose.ui.unit.ExperimentalUnitApi
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.viewinterop.AndroidView
import com.lujianfei.compose.viewmodel.MediaPlayerViewModel
import com.lujianfei.module_plugin_base.utils.DensityUtils
import java.util.*

/**
 * Author: mac
 * Date: 18.11.21 3:43 下午
 * Description: MediaPlayerDemo
 */
@ExperimentalUnitApi
@ExperimentalTextApi
@Composable
fun MediaPlayerPage(mediaPlayerViewModel: MediaPlayerViewModel) {
    Column {
        // 进度度
        ProgressBarView(mediaPlayerViewModel = mediaPlayerViewModel)
        // 播放时间显示
        PlayerTimeView(mediaPlayerViewModel = mediaPlayerViewModel)
        // 播放按钮
        PlayButton(mediaPlayerViewModel = mediaPlayerViewModel)
    }
}

/**
 * 进度条
 */
@Composable
private fun ProgressBarView(mediaPlayerViewModel: MediaPlayerViewModel) {
    AndroidView(factory = { context ->
        val progressBar = ProgressBar(context, null, R.attr.progressBarStyleHorizontal)
        progressBar.layoutParams = ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            DensityUtils.dip2px(50f)
        )
        progressBar
    }, update = { progressBar ->
        progressBar.progress = mediaPlayerViewModel.progress
        progressBar.max = mediaPlayerViewModel.max
    })
}

/**
 * 播放时间显示
 */
@ExperimentalUnitApi
@Composable
fun PlayerTimeView(mediaPlayerViewModel: MediaPlayerViewModel) {
    Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
        Text(text = "${mediaPlayerViewModel.currentTimeStr}/${mediaPlayerViewModel.totalTimeStr}",
            fontSize = TextUnit(20f, TextUnitType.Sp))
    }
}

/**
 * 播放按钮
 */
@Composable
private fun PlayButton(mediaPlayerViewModel: MediaPlayerViewModel) {
    Box(
        modifier = Modifier.fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        Image(
            painter = painterResource(id = mediaPlayerViewModel.playResId),
            contentDescription = null,
            modifier = Modifier.clickable(
                onClick = {
                    if (!mediaPlayerViewModel.isPlaying) {
                        mediaPlayerViewModel.play()
                    } else {
                        mediaPlayerViewModel.pause()
                    }
                })
        )
    }
}
```

##### MediaPlayerViewModel.kt

```kotlin
package com.lujianfei.compose.viewmodel

import android.content.Context
import android.media.MediaPlayer
import androidx.compose.runtime.*
import androidx.lifecycle.ViewModel
import java.util.*

/**
 * Author: mac
 * Date: 19.11.21 3:48 下午
 * Description: MediaPlayerViewModel
 */
class MediaPlayerViewModel: ViewModel() {

    // 记录当前播放进度
    var progress by mutableStateOf(0)

    // 记录音频总长度
    var max by mutableStateOf(100)

    // 记录播放状态
    var isPlaying by mutableStateOf(false)

    // 记录播放按钮素材
    var playResId  by mutableStateOf(com.lujianfei.compose.R.drawable.ic_compose_play_fill)

    // 当前播放时间
    var currentTimeStr by mutableStateOf("00:00")
    // 总播放时间
    var totalTimeStr by mutableStateOf("00:00")

    var mediaPlayer: MediaPlayer?= null
    var timer : Timer? = null

    /**
     * 初始化 MediaPlayer 和 定时器
     */
    fun initMediaPlayer(context: Context) {
        mediaPlayer = MediaPlayer.create(context, com.lujianfei.compose.R.raw.audio_seawave)
    }
    /**
    * 播放方法
    */
    fun play() {
        if (mediaPlayer?.isPlaying == false) {
            watchMediaPlayerPositionChange()
            mediaPlayer?.start()
            max = mediaPlayer?.duration?:0
            totalTimeStr = getTimeMMSS(max)
            playResId = com.lujianfei.compose.R.drawable.ic_compose_pause
            isPlaying = true
        }
    }

    /**
     * 暂停方法
     */
    fun pause() {
        mediaPlayer?.pause()
        timer?.cancel()
        isPlaying = false
        playResId = com.lujianfei.compose.R.drawable.ic_compose_play_fill
    }

    /**
     * 监听播放进度
     */
    private fun watchMediaPlayerPositionChange() {
        timer = Timer()
        timer?.schedule(object : TimerTask() {
            override fun run() {
                progress = mediaPlayer?.currentPosition ?: 0
                currentTimeStr = getTimeMMSS(progress)
            }
        }, 1000, 500)
    }


    /**
     * 时间戳 转 mm:ss
     */
    private fun getTimeMMSS(milliSec: Int): String {
        var sec = milliSec / 1000
        var min = 0
        if (sec >= 60) {
            min = sec / 60
            sec %= 60
        }
        return "${String.format("%02d", min)}:${String.format("%02d", sec)}"
    }

    fun release() {
        if (mediaPlayer != null && mediaPlayer?.isPlaying == true) {
            mediaPlayer?.stop()
            mediaPlayer?.release()
            mediaPlayer = null
        }
        if (timer != null) {
            timer?.cancel()
        }
    }
}
```



##### AppBar.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.lujianfei.module_plugin_base.R

@Composable
fun AppBar(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
) {
    TopAppBar {
        Row(verticalAlignment = Alignment.CenterVertically) { // 垂直居中
            leftIcon?.let {
                Spacer(modifier = Modifier.width(5.dp))
                Icon(painter = painterResource(id = leftIcon), contentDescription = null, Modifier.clickable {
                    onLeftClick()
                })
            }
            Spacer(modifier = Modifier.width(20.dp))
            Text(text = title, modifier = Modifier.weight(1f), fontSize = 20.sp, textAlign = TextAlign.Start)
            if (rightText != null) {
                Text(text = rightText, textAlign = TextAlign.Center, modifier = Modifier.clickable {
                    onRightClick()
                })
                Spacer(modifier = Modifier.width(10.dp))
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun PreviewAppBar() {
    AppBar(title = "hello", leftIcon = R.drawable.ic_back)
}
```





