# Compose 手势 代码展示

引用的依赖

```groovy
implementation "androidx.compose.ui:ui:1.0.1"
implementation "androidx.compose.material:material:1.0.1"
implementation "androidx.compose.ui:ui-tooling-preview:1.0.1"
implementation 'androidx.lifecycle:lifecycle-runtime-ktx:2.3.1'
implementation "androidx.compose.runtime:runtime-livedata:1.0.1"
implementation "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
implementation 'androidx.activity:activity-compose:1.3.1'
implementation "com.google.accompanist:accompanist-coil:0.13.0"
```



以下只展示关键部分

##### MainActivity.kt

```kotlin
 override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Page(
                leftIcon = R.drawable.ic_back,
                onLeftClick = {
                    finish()
                },
                title = "Compose 手势",
                onRightClick = {
                    openBrowser()
                },
                rightText = bean.codeUrl
            ) {
                Content()
            }
        }
 }

@ExperimentalAnimationApi
@ExperimentalTextApi
@ExperimentalUnitApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun Content() {
    TextPage()
}

```

##### Page.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable

@Composable
fun Page(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
    content: @Composable () -> Unit,
) {
    Column {
        AppBar(title = title, leftIcon, rightText, onLeftClick, onRightClick)
        content()
    }
}
```

##### AppBar.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.lujianfei.module_plugin_base.R

@Composable
fun AppBar(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
) {
    TopAppBar {
        Row(verticalAlignment = Alignment.CenterVertically) { // 垂直居中
            leftIcon?.let {
                Spacer(modifier = Modifier.width(5.dp))
                Icon(painter = painterResource(id = leftIcon), contentDescription = null, Modifier.clickable {
                    onLeftClick()
                })
            }
            Spacer(modifier = Modifier.width(20.dp))
            Text(text = title, modifier = Modifier.weight(1f), fontSize = 20.sp, textAlign = TextAlign.Start)
            if (rightText != null) {
                Text(text = rightText, textAlign = TextAlign.Center, modifier = Modifier.clickable {
                    onRightClick()
                })
                Spacer(modifier = Modifier.width(10.dp))
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun PreviewAppBar() {
    AppBar(title = "hello", leftIcon = R.drawable.ic_back)
}
```

##### GestureDemo.kt

```kotlin
package com.lujianfei.compose.ui.demo

import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.gestures.*
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.input.pointer.consumeAllChanges
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import kotlin.math.roundToInt

@ExperimentalMaterialApi
@Composable
fun GesturePage() {
    GestureContent()
}

@ExperimentalMaterialApi
@Composable
private fun GestureContent() {
    Box(modifier = Modifier.fillMaxSize()) {
        GestureDemo()
    }
}

@ExperimentalMaterialApi
@Composable
private fun GestureDemo() {
    Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
        ClickableDemo()
        DoubleClickableDemo()
        HorizontalDragDemo()
        VerticalDragDemo()
        DragDemo()
        TouchPositionDemo()
        SwipeableDemo()
        TransformableDemo()
    }
}

@Composable
private fun ClickableDemo() {
    Spacer(modifier = Modifier.height(10.dp))
    Text("点击事件监听")
    var clickCount by remember { mutableStateOf(0) }
    Button(onClick = { clickCount++ }) {
        Text("Clicked $clickCount")
    }
}

@Composable
private fun DoubleClickableDemo() {
    Spacer(modifier = Modifier.height(10.dp))
    Text("单击，双击，长按，按下事件监听")
    val context = LocalContext.current
    Box(
        modifier = Modifier
            .size(100.dp)
            .background(MaterialTheme.colors.primary)
            .pointerInput(Unit) {
                detectTapGestures(
                    onPress = {
                        Toast
                            .makeText(context, "按下", Toast.LENGTH_SHORT)
                            .show()
                    },
                    onDoubleTap = {
                        Toast
                            .makeText(context, "双击", Toast.LENGTH_SHORT)
                            .show()
                    },
                    onLongPress = {
                        Toast
                            .makeText(context, "长按", Toast.LENGTH_SHORT)
                            .show()
                    },
                    onTap = {
                        Toast
                            .makeText(context, "单击", Toast.LENGTH_SHORT)
                            .show()
                    }
                )
            }) {
        Text("Clicked me", color = MaterialTheme.colors.onSurface)
    }
}

@Composable
private fun TouchPositionDemo() {
    var touchedX by remember { mutableStateOf(0f) }
    var touchedY by remember { mutableStateOf(0f) }
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(300.dp)
            .pointerInput(Unit) {
                detectDragGestures { change, dragAmount ->
                    change.consumeAllChanges()

                    touchedX = change.position.x
                    touchedY = change.position.y
                }
            }, contentAlignment = Alignment.Center
    ) {
        Column {
            Text(text = "这是一个监听触摸位置的组件")
            Text(text = "touchedX=${touchedX.toInt()}   touchedY=${touchedY.toInt()}")
        }
    }
}

@Composable
private fun HorizontalDragDemo() {
    Text("水平拖动")
    var offsetX by remember { mutableStateOf(0f) }
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(100.dp),
        contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .size(100.dp)
                .offset { IntOffset(offsetX.roundToInt(), 0) }
                .background(MaterialTheme.colors.primary)
                .draggable(
                    orientation = Orientation.Horizontal,
                    state = rememberDraggableState { delta ->
                        offsetX += delta
                    })
        )
    }
}

@Composable
private fun VerticalDragDemo() {
    Text("垂直拖动")
    var offsetY by remember { mutableStateOf(0f) }
    Box(
        modifier = Modifier
            .width(100.dp)
            .height(500.dp),
        contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .size(100.dp)
                .offset { IntOffset(0, offsetY.roundToInt()) }
                .background(MaterialTheme.colors.primary)
                .draggable(
                    orientation = Orientation.Vertical,
                    state = rememberDraggableState { delta ->
                        offsetY += delta
                    })
        )
    }
}


@Composable
private fun DragDemo() {
    Text("任意拖动")
    var offsetX by remember { mutableStateOf(0f) }
    var offsetY by remember { mutableStateOf(0f) }
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(500.dp),
        contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .size(100.dp)
                .offset { IntOffset(offsetX.roundToInt(), offsetY.roundToInt()) }
                .background(MaterialTheme.colors.primary)
                .pointerInput(Unit) {
                    detectDragGestures { change, dragAmount ->
                        change.consumeAllChanges()
                        offsetX += dragAmount.x
                        offsetY += dragAmount.y
                    }
                }
        )
    }
}

@ExperimentalMaterialApi
@Composable
private fun SwipeableDemo() {
    Text("Swipeable ")
    val width = 96.dp
    val squareSize = 48.dp

    val swipeableState = rememberSwipeableState(0)
    val sizePx = with(LocalDensity.current) { squareSize.toPx() }
    val anchors = mapOf(0f to 0, sizePx to 1) // Maps anchor points (in px) to states

    Box(
        modifier = Modifier
            .width(width)
            .swipeable(
                state = swipeableState,
                anchors = anchors,
                thresholds = { _, _ -> FractionalThreshold(0.3f) },
                orientation = Orientation.Horizontal
            )
            .background(MaterialTheme.colors.secondary)
    ) {
        Box(
            Modifier
                .offset { IntOffset(swipeableState.offset.value.roundToInt(), 0) }
                .size(squareSize)
                .background(MaterialTheme.colors.onBackground)
        )
    }
}

@Composable
private fun TransformableDemo() {
    Text("缩放，平移，旋转 ")
    var scale by remember { mutableStateOf(1f) }
    var rotation by remember { mutableStateOf(0f) }
    var offset by remember { mutableStateOf(Offset.Zero) }
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(500.dp),
        contentAlignment = Alignment.Center
    ) {
        Box(
            Modifier
                .graphicsLayer(
                    scaleX = scale,
                    scaleY = scale,
                    rotationZ = rotation,
                    translationX = offset.x,
                    translationY = offset.y
                )
                .pointerInput(Unit) {
                    detectTransformGestures { _, pan, zoom, rotationChanged ->
                        scale *= zoom
                        rotation += rotationChanged
                        offset += pan
                    }
                    detectDragGestures { change, dragAmount ->
                        change.consumeAllChanges()
                        offset = Offset(dragAmount.x, dragAmount.y)
                    }
                }
                .background(MaterialTheme.colors.primary)
                .size(100.dp)
        )
    }
}
```



