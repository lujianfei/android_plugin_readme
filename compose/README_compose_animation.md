# Compose 动画 代码展示

引用的依赖

```groovy
implementation "androidx.compose.ui:ui:1.0.1"
implementation "androidx.compose.material:material:1.0.1"
implementation "androidx.compose.ui:ui-tooling-preview:1.0.1"
implementation 'androidx.lifecycle:lifecycle-runtime-ktx:2.3.1'
implementation "androidx.compose.runtime:runtime-livedata:1.0.1"
implementation "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
implementation 'androidx.activity:activity-compose:1.3.1'
implementation "com.google.accompanist:accompanist-coil:0.13.0"
```



以下只展示关键部分

##### MainActivity.kt

```kotlin
 override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Page(
                leftIcon = R.drawable.ic_back,
                onLeftClick = {
                    finish()
                },
                title = "Compose 动画",
                onRightClick = {
                    openBrowser()
                },
                rightText = bean.codeUrl
            ) {
                Content()
            }
        }
 }

@ExperimentalAnimationApi
@ExperimentalTextApi
@ExperimentalUnitApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun Content() {
    AnimationPage()
}

```

##### Page.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable

@Composable
fun Page(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
    content: @Composable () -> Unit,
) {
    Column {
        AppBar(title = title, leftIcon, rightText, onLeftClick, onRightClick)
        content()
    }
}
```

##### AppBar.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.lujianfei.module_plugin_base.R

@Composable
fun AppBar(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
) {
    TopAppBar {
        Row(verticalAlignment = Alignment.CenterVertically) { // 垂直居中
            leftIcon?.let {
                Spacer(modifier = Modifier.width(5.dp))
                Icon(painter = painterResource(id = leftIcon), contentDescription = null, Modifier.clickable {
                    onLeftClick()
                })
            }
            Spacer(modifier = Modifier.width(20.dp))
            Text(text = title, modifier = Modifier.weight(1f), fontSize = 20.sp, textAlign = TextAlign.Start)
            if (rightText != null) {
                Text(text = rightText, textAlign = TextAlign.Center, modifier = Modifier.clickable {
                    onRightClick()
                })
                Spacer(modifier = Modifier.width(10.dp))
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun PreviewAppBar() {
    AppBar(title = "hello", leftIcon = R.drawable.ic_back)
}
```

##### AnimationDemo.kt

```kotlin
package com.lujianfei.compose.ui.demo

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.lujianfei.compose.ui.demo.animation.*

@ExperimentalAnimationApi
@Composable
fun AnimationPage() {
    AnimationContent()
}

@ExperimentalAnimationApi
@Composable
private fun AnimationContent() {
    Box(modifier = Modifier.fillMaxSize()) {
        AnimationDemo()
    }
}

@ExperimentalAnimationApi
@Composable
private fun AnimationDemo() {
    Column(modifier = Modifier.verticalScroll(rememberScrollState())) {
        HighLevelAnimation()
        LowLevelAnimation()
    }
}

@ExperimentalAnimationApi
@Composable
private fun HighLevelAnimation() {
    Spacer(modifier = Modifier.height(20.dp))
    Row(horizontalArrangement = Arrangement.Center, modifier = Modifier.fillMaxWidth()) {
        Text("--------高级别动画API-------")
    }

    AnimatedVisibilityDemo()
    AnimateContentSizeDemo()
    CrossfadeDemo()
}

@Composable
private fun LowLevelAnimation() {
    Spacer(modifier = Modifier.height(20.dp))
    Row(horizontalArrangement = Arrangement.Center, modifier = Modifier.fillMaxWidth()) {
        Text("--------低级别动画API-------")
    }

    AnimateFloatAsStateDemo()
    AnimatableDemo()
    UpdateTransitionDemo()
    RememberInfiniteTransitionDemo()
    SpringDemo()
    TweenDemo()

    Spacer(modifier = Modifier.height(100.dp))
}
```



##### HighLevelAnimations.kt

```kotlin
package com.lujianfei.compose.ui.demo.animation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp


@ExperimentalAnimationApi
@Composable
fun AnimatedVisibilityDemo() {
    var visible by remember { mutableStateOf(false) }
    Spacer(modifier = Modifier.height(10.dp))
    Text("AnimatedVisibility:控制显示与隐藏")
    Row {
        Button(onClick = { visible = !visible }) {
            Text("AnimatedVisibility")
        }
        Text("isVisible=$visible")
    }
    androidx.compose.animation.AnimatedVisibility(
        visible = visible,
        modifier = Modifier.padding(10.dp)
    ) {
        Box(
            modifier = Modifier
                .background(MaterialTheme.colors.primary)
                .size(100.dp)
        )
    }
}

@Composable
fun AnimateContentSizeDemo() {
    var size by remember { mutableStateOf(100.dp) }
    Spacer(modifier = Modifier.height(10.dp))
    Text("AnimateContentSize:尺寸该变时添加动画过渡")
    Row {
        Button(onClick = { size = if (size == 100.dp) 150.dp else 100.dp }) {
            Text("AnimateContentSize")
        }
        Text("size=$size")
    }
    Spacer(modifier = Modifier.height(10.dp))
    Box(
        modifier = Modifier
            .background(MaterialTheme.colors.primary)
            .animateContentSize()
            .padding(10.dp)
            .size(size)
    )
}


@Composable
fun CrossfadeDemo() {
    var currentPage by remember { mutableStateOf("A") }
    Spacer(modifier = Modifier.height(10.dp))
    Text("Crossfade:多个页面切换时添加过渡效果")
    Row {
        Button(onClick = { currentPage = if (currentPage == "A") "B" else "A" }) {
            Text("Crossfade")
        }
        Text("currentPage=$currentPage")
    }
    Spacer(modifier = Modifier.height(10.dp))
    androidx.compose.animation.Crossfade(targetState = currentPage) { screen ->
        when (screen) {
            "A" -> {
                Box(
                    modifier = Modifier
                        .background(MaterialTheme.colors.primary)
                        .padding(10.dp)
                        .size(100.dp)
                )
            }
            "B" -> Box(
                modifier = Modifier
                    .background(MaterialTheme.colors.onBackground)
                    .padding(10.dp)
                    .size(100.dp)
            )
        }
    }
}
```



##### LowLevelAnimations.kt

```kotlin
package com.lujianfei.compose.ui.demo.animation

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.*
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp

@Composable
fun AnimateFloatAsStateDemo() {
    Spacer(modifier = Modifier.height(10.dp))
    Text("animateFloatAsState:给定初始值和结束值，展示动画")
    var enabled by remember { mutableStateOf(false) }
    val scale: Float by animateFloatAsState(if (enabled) 1f else 0.5f)
    Row {
        Button(onClick = { enabled = !enabled }) {
            Text("animateFloatAsState")
        }
        Text("scaleX=${scale}  scaleY=$scale")
    }
    Box(
        Modifier
            .size(100.dp)
            .graphicsLayer(scaleX = scale, scaleY = scale)
            .background(MaterialTheme.colors.primary)
    )
}

@Composable
fun AnimatableDemo() {
    Spacer(modifier = Modifier.height(10.dp))
    Text("Animatable:给定初始值，使用animateTo(targetValue)过渡到结束值")
    var enabledAnimatable by remember { mutableStateOf(false) }
    val translationX = remember { androidx.compose.animation.core.Animatable(0f) }
    LaunchedEffect(enabledAnimatable) {
        if (enabledAnimatable) translationX.animateTo(100f) else translationX.animateTo(0f)
    }
    Row() {
        Button(onClick = { enabledAnimatable = !enabledAnimatable }) {
            Text("Animatable")
        }
        Text("offset=${translationX.value}")
    }
    Spacer(modifier = Modifier.height(10.dp))
    Box(
        Modifier
            .size(100.dp)
            .offset(translationX.value.dp, 0.dp)
            .background(MaterialTheme.colors.primary)
    )
}


@Composable
fun UpdateTransitionDemo() {
    Spacer(modifier = Modifier.height(10.dp))
    Text("updateTransition:组合多种动画")
    var state by remember { mutableStateOf(false) }
    val transition = updateTransition(state, label = "")
    val color by transition.animateColor {
        if (it) Color.Red else MaterialTheme.colors.primary
    }
    val offset by transition.animateIntOffset {
        if (it) IntOffset(100, 100) else IntOffset(0, 0)
    }
    Row {
        Button(onClick = { state = !state }) {
            Text("updateTransition")
        }
        Text(
            "color=${
                color.value.toString(16).subSequence(0, 8)
            }   \noffsetX=${offset.x}  offsetY=${offset.y}"
        )
    }
    Box(
        Modifier
            .size(100.dp)
            .offset { offset }
            .background(color)
    )
}

@Composable
fun RememberInfiniteTransitionDemo() {
    Spacer(modifier = Modifier.height(10.dp))
    Text(text = "rememberInfiniteTransition用于一直执行动画")
    val infiniteTransition = rememberInfiniteTransition()
    val color by infiniteTransition.animateColor(
        initialValue = Color.Red,
        targetValue = Color.Green,
        animationSpec = infiniteRepeatable(
            animation = tween(1000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )
    val offset by infiniteTransition.animateFloat(
        initialValue = 0f,
        targetValue = 100f,
        animationSpec = infiniteRepeatable(
            animation = tween(1000, easing = LinearEasing),
            repeatMode = RepeatMode.Reverse
        )
    )

    Box(
        Modifier
            .size(100.dp)
            .offset(offset.dp, 0.dp)
            .background(color)
    )
}

@Composable
fun SpringDemo() {
    Spacer(modifier = Modifier.height(60.dp))
    var enabled by remember { mutableStateOf(false) }
    var dampingRatio by remember { mutableStateOf(Spring.DampingRatioNoBouncy) }
    var stiffness by remember { mutableStateOf(Spring.StiffnessVeryLow) }
    val offsetX: Int by animateIntAsState(
        if (enabled) 200 else 0, animationSpec = spring(dampingRatio, stiffness)
    )
    Row {
        Button(onClick = { enabled = !enabled }) {
            Text("Animate with spring")
        }
        Text("offsetX=${offsetX}")
    }
    Spacer(modifier = Modifier.height(20.dp))
    Box(
        Modifier
            .size(10.dp)
            .offset(offsetX.dp, 0.dp)
            .background(MaterialTheme.colors.primary)
    )
    Spacer(modifier = Modifier.height(20.dp))
    OutlinedTextField(
        value = dampingRatio.toString(),
        onValueChange = { dampingRatio = if (it.isNotEmpty()) it.toFloat() else 0.2f },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        label = { Text("dampingRatio 弹性系数") })
    OutlinedTextField(
        value = stiffness.toString(),
        onValueChange = { stiffness = if (it.isNotEmpty()) it.toFloat() else 0.2f },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        label = { Text("stiffness 移动速度") })
    Spacer(modifier = Modifier.height(10.dp))
}


@Composable
fun TweenDemo() {
    Spacer(modifier = Modifier.height(60.dp))
    var enabled by remember { mutableStateOf(false) }
    var duration by remember { mutableStateOf(300) }
    var easingType by remember { mutableStateOf(LinearEasing) }
    val offsetX: Int by animateIntAsState(
        if (enabled) 200 else 0,
        animationSpec = tween(durationMillis = duration, easing = easingType)
    )
    Row {
        Button(onClick = { enabled = !enabled }) {
            Text("Animate with tween")
        }
        Text("offsetX=${offsetX}")
    }
    Spacer(modifier = Modifier.height(20.dp))
    Box(
        Modifier
            .size(10.dp)
            .offset(offsetX.dp, 0.dp)
            .background(MaterialTheme.colors.primary)
    )
    Spacer(modifier = Modifier.height(20.dp))
    RadioButton(
        isSelected = easingType == LinearEasing,
        text = "LinearEasing"
    ) {
        easingType = LinearEasing
    }
    RadioButton(
        isSelected = easingType == FastOutSlowInEasing,
        text = "FastOutSlowInEasing"
    ) {
        easingType = FastOutSlowInEasing
    }
    RadioButton(
        isSelected = easingType == LinearOutSlowInEasing,
        text = "LinearOutSlowInEasing"
    ) {
        easingType = LinearOutSlowInEasing
    }
    RadioButton(
        isSelected = easingType == FastOutLinearInEasing,
        text = "FastOutLinearInEasing"
    ) {
        easingType = FastOutLinearInEasing
    }
    val cubicBezierEasing = CubicBezierEasing(0f, 0.2f, 0.8f, 1f)
    RadioButton(
        isSelected = easingType == cubicBezierEasing,
        text = "CubicBezierEasing(自定义）"
    ) {
        easingType = cubicBezierEasing
    }
    OutlinedTextField(
        value = duration.toString(),
        onValueChange = { duration = if (it.isNotEmpty()) it.toInt() else 300 },
        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number),
        label = { Text("duration 动画时间") })
    Spacer(modifier = Modifier.height(10.dp))
}

@Composable
private fun RadioButton(isSelected: Boolean, text: String, onClick: () -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .height(50.dp)
            .clickable { onClick() }, verticalAlignment = Alignment.CenterVertically
    ) {
        RadioButton(selected = isSelected, modifier = Modifier.size(30.dp), onClick = onClick)
        Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.CenterStart) {
            Text(text = text, color = MaterialTheme.colors.onSurface)
        }
    }
}

```

