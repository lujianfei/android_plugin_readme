# compose 列表 代码展示

引用的依赖

```groovy
implementation "androidx.compose.ui:ui:1.0.1"
implementation "androidx.compose.material:material:1.0.1"
implementation "androidx.compose.ui:ui-tooling-preview:1.0.1"
implementation 'androidx.lifecycle:lifecycle-runtime-ktx:2.3.1'
implementation "androidx.compose.runtime:runtime-livedata:1.0.1"
implementation "androidx.lifecycle:lifecycle-viewmodel-compose:1.0.0-alpha07"
implementation 'androidx.activity:activity-compose:1.3.1'
implementation "com.google.accompanist:accompanist-coil:0.13.0"
```



以下只展示关键部分

##### MainActivity.kt

```kotlin
 override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            Page(
                leftIcon = R.drawable.ic_back,
                onLeftClick = {
                    finish()
                },
                title = "Compose 列表",
                onRightClick = {
                    openBrowser()
                },
                rightText = bean.codeUrl
            ) {
                Content()
            }
        }
 }

@ExperimentalAnimationApi
@ExperimentalTextApi
@ExperimentalUnitApi
@ExperimentalFoundationApi
@ExperimentalMaterialApi
@Composable
fun Content() {
    val viewModel: MainViewModel = viewModel()
    FunctionList(functions = viewModel.functions, onClick = {
        Toast.makeText(this, "${it.name} clicked !", Toast.LENGTH_SHORT).show()
    })
}

/**
* 列表显示
*/
@ExperimentalMaterialApi
@Composable
fun FunctionList(functions: ArrayList<FuncInfo>, onClick: (FuncInfo) -> Unit) {
    LazyColumn(
        verticalArrangement = Arrangement.spacedBy(15.dp) // 垂直边距
    ) {
        item {
            Spacer(Modifier.size(0.dp))
        }
        items(functions) { func ->
            FuncItem(func = func, onClick = onClick)
        }
        item {
            Spacer(Modifier.size(0.dp))
        }
    }
}

/**
 * 选项卡
 */
@ExperimentalMaterialApi
@Composable
private fun FuncItem(func: FuncInfo,onClick: (FuncInfo) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth() // 最大宽度
            .height(200.dp) // 200 dp 卡片高度
            .padding(15.dp, 0.dp), // 水平边距
        backgroundColor = func.color,
        elevation = 2.dp,
        onClick = {
            onClick.invoke(func)
        }
    ) {
        Column(verticalArrangement = Arrangement.Center) { // 文本垂直居中
            Text(
                text = func.name,
                modifier = Modifier.padding(15.dp, 0.dp, 0.dp, 0.dp), // 文本加入左边距
                textAlign = TextAlign.Start,
                fontSize = MaterialTheme.typography.h5.fontSize,
                color = Color.White
            )
        }
    }
}
```

##### Page.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable

@Composable
fun Page(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
    content: @Composable () -> Unit,
) {
    Column {
        AppBar(title = title, leftIcon, rightText, onLeftClick, onRightClick)
        content()
    }
}
```

##### AppBar.kt

```kotlin
package com.lujianfei.compose.ui.public

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.width
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.lujianfei.module_plugin_base.R

@Composable
fun AppBar(
    title: String,
    leftIcon: Int? = null,
    rightText: String? = null,
    onLeftClick: () -> Unit = {},
    onRightClick: () -> Unit = {},
) {
    TopAppBar {
        Row(verticalAlignment = Alignment.CenterVertically) { // 垂直居中
            leftIcon?.let {
                Spacer(modifier = Modifier.width(5.dp))
                Icon(painter = painterResource(id = leftIcon), contentDescription = null, Modifier.clickable {
                    onLeftClick()
                })
            }
            Spacer(modifier = Modifier.width(20.dp))
            Text(text = title, modifier = Modifier.weight(1f), fontSize = 20.sp, textAlign = TextAlign.Start)
            if (rightText != null) {
                Text(text = rightText, textAlign = TextAlign.Center, modifier = Modifier.clickable {
                    onRightClick()
                })
                Spacer(modifier = Modifier.width(10.dp))
            }
        }
    }
}

@Preview(showSystemUi = true)
@Composable
fun PreviewAppBar() {
    AppBar(title = "hello", leftIcon = R.drawable.ic_back)
}
```



##### MainViewModel.kt

```kotlin
package com.lujianfei.compose.viewmodel

import androidx.compose.ui.graphics.Color
import androidx.lifecycle.ViewModel
import com.lujianfei.compose.entities.FuncInfo

class MainViewModel() : ViewModel() {
    val functions = arrayListOf<FuncInfo>().apply {
        add(FuncInfo("布局", Color(0xFF00A1FF)))
        add(FuncInfo("主题", Color(0xFF6200EE)))
        add(FuncInfo("列表", Color(0xFF00C29B)))
        add(FuncInfo("文字", Color(0xFF000000)))
        add(FuncInfo("图片", Color(0xFFFFBE3B)))
        add(FuncInfo("Canvas", Color(0xFF1D2E44)))
        add(FuncInfo("自定义布局", Color(0xFF3587EC)))
        add(FuncInfo("动画", Color(0xFFDD26AF)))
        add(FuncInfo("手势", Color(0xFF1BC3E0)))
    }
}
```

##### FuncInfo.kt

```kotlin
data class FuncInfo(
    val name: String,
    val color: Color
)
```

