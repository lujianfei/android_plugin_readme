# 简单样式日历 代码展示

以下只展示关键部分

先引用这个开源的日历库

```
https://gitee.com/huanghaibin_dev/CalendarView
```

然后就可以开始你的demo编写

##### fragment_simple_calendar.xml 

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">
    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintEnd_toEndOf="parent"/>

    <TextView
        android:id="@+id/tv_year_month"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:padding="15dp"
        app:layout_constraintTop_toBottomOf="@id/toolbar"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        tools:text="2022年03月"
        android:textColor="@color/black"
        android:textSize="18sp"
        />

    <com.haibin.calendarview.CalendarView
        android:id="@+id/calendarView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_constraintTop_toBottomOf="@id/tv_year_month"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        android:background="#fff"
        app:calendar_padding="10dp"
        app:month_view="com.lujianfei.other.ui.simple.SimpleMonthView"
        app:calendar_height="46dp"
        app:current_month_lunar_text_color="#CFCFCF"
        app:current_month_text_color="#333333"
        app:min_year="2010"
        app:other_month_text_color="#e1e1e1"
        app:scheme_text="假"
        app:scheme_text_color="#333"
        app:scheme_theme_color="#333"
        app:selected_text_color="#fff"
        app:selected_theme_color="#FF3700B3"
        app:week_background="#fff"
        app:week_text_color="#111"
        app:week_view="com.lujianfei.other.ui.simple.SimpleWeekView"
        app:year_view_day_text_color="#333333"
        app:year_view_day_text_size="9sp"
        app:year_view_month_text_color="#ff0000"
        app:year_view_month_text_size="20sp"
        app:year_view_scheme_color="#f17706"
        app:month_view_scrollable = "true"
        app:current_day_text_color = "#ff0000"
        />
</androidx.constraintlayout.widget.ConstraintLayout>
```

##### SimpleCalendarFragment

```kotlin
package com.lujianfei.other.ui

import android.view.View
import android.widget.TextView
import com.haibin.calendarview.Calendar
import com.haibin.calendarview.CalendarView
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.other.R
import java.text.SimpleDateFormat
import java.util.*

/**
 * 简单单选日历
 */
class SimpleCalendarFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    var toolbar: PluginToolBar?= null
    private var bean: PluginActivityBean?= null
    var tv_year_month:TextView ?= null
    var calendarView:com.haibin.calendarview.CalendarView ?= null

    override fun resouceId() = R.layout.fragment_simple_calendar

    override fun initView() {
        toolbar = view?.findViewById(R.id.toolbar)
        tv_year_month = view?.findViewById(R.id.tv_year_month)
        calendarView = view?.findViewById(R.id.calendarView)
    }

    override fun initData() {
        bean = arguments?.getParcelable("data")
        bean?.let {bean->
            toolbar?.setTitle(bean.itemName)
            toolbar?.getRightTopTextView()?.visibility = if (bean.codeUrl.isNullOrEmpty()) View.GONE else View.VISIBLE
        }
        updateCurrentYearMonthDay()
    }

    override fun initEvent() {
        toolbar?.onBackListener = {
            activity?.finish()
        }
        calendarView?.setOnCalendarSelectListener(object : CalendarView.OnCalendarSelectListener {
            override fun onCalendarOutOfRange(calendar: Calendar?) {
            }

            override fun onCalendarSelect(calendar: Calendar?, isClick: Boolean) {
                updateCurrentYearMonthDay()
            }
        })
    }

    /**
     * 更新当前日期
     */
    private fun updateCurrentYearMonthDay() {
        calendarView?.selectedCalendar?.timeInMillis?.let { timeInMillis->
            tv_year_month?.text = SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Date(timeInMillis))
        }
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }
}
```

##### SimpleMonthView
```java
package com.lujianfei.other.ui.simple;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.MonthView;

/**
 * 高仿魅族日历布局
 * Created by huanghaibin on 2017/11/15.
 */

public class SimpleMonthView extends MonthView {

    private int mRadius;

    public SimpleMonthView(Context context) {
        super(context);
    }

    @Override
    protected void onPreviewHook() {
        mRadius = Math.min(mItemWidth, mItemHeight) / 5 * 2;
        mSchemePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected void onLoopStart(int x, int y) {

    }

    @Override
    protected boolean onDrawSelected(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme) {
        int cx = x + mItemWidth / 2;
        int cy = y + mItemHeight / 2;
        canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
        return false;
    }

    @Override
    protected void onDrawScheme(Canvas canvas, Calendar calendar, int x, int y) {
        int cx = x + mItemWidth / 2;
        int cy = y + mItemHeight / 2;
        canvas.drawCircle(cx, cy, mRadius, mSchemePaint);
    }

    @Override
    protected void onDrawText(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme, boolean isSelected) {
        float baselineY = mTextBaseLine + y;
        int cx = x + mItemWidth / 2;

        if (isSelected) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    mSelectTextPaint);
        } else if (hasScheme) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() ? mSchemeTextPaint : mOtherMonthTextPaint);

        } else {
                canvas.drawText(String.valueOf(calendar.getDay()), cx, baselineY,
                        calendar.isCurrentDay() ? mCurDayTextPaint :
                                calendar.isCurrentMonth() ? mCurMonthTextPaint : mOtherMonthTextPaint);
        }
    }
}

```

##### SimpleWeekView

```java
package com.lujianfei.other.ui.simple;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.WeekView;

/**
 * 简单周视图
 * Created by huanghaibin on 2017/11/29.
 */

public class SimpleWeekView extends WeekView {
    private int mRadius;


    public SimpleWeekView(Context context) {
        super(context);
    }

    @Override
    protected void onPreviewHook() {
        mRadius = Math.min(mItemWidth, mItemHeight) / 5 * 2;
        mSchemePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected boolean onDrawSelected(Canvas canvas, Calendar calendar, int x, boolean hasScheme) {
        int cx = x + mItemWidth / 2;
        int cy = mItemHeight / 2;
        canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
        return false;
    }

    @Override
    protected void onDrawScheme(Canvas canvas, Calendar calendar, int x) {
        int cx = x + mItemWidth / 2;
        int cy = mItemHeight / 2;
        canvas.drawCircle(cx, cy, mRadius, mSchemePaint);
    }

    @Override
    protected void onDrawText(Canvas canvas, Calendar calendar, int x, boolean hasScheme, boolean isSelected) {
        float baselineY = mTextBaseLine;
        int cx = x + mItemWidth / 2;
        if (isSelected) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    mSelectTextPaint);
        } else if (hasScheme) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() ? mSchemeTextPaint : mSchemeTextPaint);

        } else {
            canvas.drawText(String.valueOf(calendar.getDay()), cx, baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() ? mCurMonthTextPaint : mCurMonthTextPaint);
        }
    }
}

```