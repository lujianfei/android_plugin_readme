# 多选日历 代码展示

以下只展示关键部分

先引用这个开源的日历库

```
https://gitee.com/huanghaibin_dev/CalendarView
```

然后就可以开始你的demo编写

##### fragment_multi_calendar.xml 

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">
    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintEnd_toEndOf="parent"/>

    <TextView
        android:id="@+id/tv_year_month"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:padding="15dp"
        app:layout_constraintTop_toBottomOf="@id/toolbar"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        tools:text="2022年03月"
        android:textColor="@color/black"
        android:textSize="18sp"
        />

    <com.haibin.calendarview.CalendarView
        android:id="@+id/calendarView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_constraintTop_toBottomOf="@id/tv_year_month"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        android:background="#fff"
        app:calendar_padding="10dp"
        app:current_month_lunar_text_color="#CFCFCF"
        app:current_month_text_color="#333333"
        app:max_year="2020"
        app:min_year="2004"
        app:month_view="com.lujianfei.other.ui.multi.CustomMultiMonthView"
        app:month_view_show_mode="mode_only_current"
        app:other_month_lunar_text_color="#e1e1e1"
        app:other_month_text_color="#e1e1e1"
        app:scheme_text="假"
        app:scheme_text_color="#333"
        app:scheme_theme_color="#128c4b"
        app:selected_lunar_text_color="#CFCFCF"
        app:selected_text_color="#ffffff"
        app:selected_theme_color="#f17706"
        app:week_background="#fff"
        app:week_start_with="sun"
        app:week_text_color="#111111"
        app:select_mode="multi_mode"
        app:max_multi_select_size="5"
        app:week_view="com.lujianfei.other.ui.multi.CustomMultiWeekView"
        app:week_view_scrollable="true"
        app:year_view_day_text_color="#333333"
        app:year_view_day_text_size="9sp"
        app:year_view_month_text_color="#ff0000"
        app:year_view_month_text_size="20sp"
        app:year_view_scheme_color="#f17706"
        />

    <TextView
        android:id="@+id/tv_multi_select"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:padding="15dp"
        app:layout_constraintTop_toBottomOf="@id/calendarView"
        app:layout_constraintStart_toStartOf="parent"
        tools:text="2022年03月"
        android:textColor="@color/black"
        android:textSize="18sp"
        />
</androidx.constraintlayout.widget.ConstraintLayout>
```

##### RangeCalendarFragment

```kotlin
package com.lujianfei.other.ui

import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.haibin.calendarview.Calendar
import com.haibin.calendarview.CalendarView
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.other.R
import java.text.SimpleDateFormat
import java.util.*

/**
 * 简单多选日历
 */
class MultiCalendarFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    var toolbar: PluginToolBar?= null
    private var bean: PluginActivityBean?= null
    var tv_year_month:TextView ?= null
    var tv_multi_select:TextView ?= null
    var calendarView:com.haibin.calendarview.CalendarView ?= null

    override fun resouceId() = R.layout.fragment_multi_calendar

    override fun initView() {
        toolbar = view?.findViewById(R.id.toolbar)
        tv_year_month = view?.findViewById(R.id.tv_year_month)
        tv_multi_select = view?.findViewById(R.id.tv_multi_select)
        calendarView = view?.findViewById(R.id.calendarView)
    }

    override fun initData() {
        bean = arguments?.getParcelable("data")
        bean?.let {bean->
            toolbar?.setTitle(bean.itemName)
            toolbar?.getRightTopTextView()?.visibility = if (bean.codeUrl.isNullOrEmpty()) View.GONE else View.VISIBLE
        }

        updateCurrentYearMonthDay(calendarView?.curYear, calendarView?.curMonth)
    }

    override fun initEvent() {
        toolbar?.onBackListener = {
            activity?.finish()
        }
        calendarView?.setOnMonthChangeListener { year, month ->
            updateCurrentYearMonthDay(year, month)
        }
        calendarView?.setOnCalendarMultiSelectListener(object :
            CalendarView.OnCalendarMultiSelectListener {
            override fun onCalendarMultiSelectOutOfRange(calendar: Calendar?) {
                LogUtils.d("MultiCalendarFragment", "onCalendarMultiSelectOutOfRange")
            }

            override fun onMultiSelectOutOfSize(calendar: Calendar?, maxSize: Int) {
                LogUtils.d("MultiCalendarFragment", "onMultiSelectOutOfSize")
                Toast.makeText(context, "onMultiSelectOutOfSize", Toast.LENGTH_SHORT).show()
            }

            override fun onCalendarMultiSelect(calendar: Calendar?, curSize: Int, maxSize: Int) {
                LogUtils.d("MultiCalendarFragment", "onCalendarMultiSelect")
                updateMultiSelectInfo()
            }

        })
    }

    private fun updateMultiSelectInfo() {
        val sb = StringBuilder()
        calendarView?.multiSelectCalendars?.forEach {
            sb.append(SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Date(it.timeInMillis))).append("\r\n")
        }
        tv_multi_select?.text = sb.toString()
    }

    /**
     * 更新当前日期
     */
    private fun updateCurrentYearMonthDay(year: Int?, month: Int?) {
            tv_year_month?.text = "$year-$month"
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }
}
```

##### CustomMultiMonthView
```java
package com.lujianfei.other.ui.multi;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.MultiMonthView;

/**
 * 高仿魅族日历布局
 * Created by huanghaibin on 2017/11/15.
 */

public class CustomMultiMonthView extends MultiMonthView {

    private int mRadius;

    public CustomMultiMonthView(Context context) {
        super(context);
    }


    @Override
    protected void onPreviewHook() {
        mRadius = Math.min(mItemWidth, mItemHeight) / 5 * 2;
        mSchemePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected boolean onDrawSelected(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme,
                                     boolean isSelectedPre, boolean isSelectedNext) {
        int cx = x + mItemWidth / 2;
        int cy = y + mItemHeight / 2;
        if (isSelectedPre) {
            if (isSelectedNext) {
                canvas.drawRect(x, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            } else {//最后一个，the last
                canvas.drawRect(x, cy - mRadius, cx, cy + mRadius, mSelectedPaint);
                canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
            }
        } else {
            if(isSelectedNext){
                canvas.drawRect(cx, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            }
            canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
            //
        }

        return false;
    }

    @Override
    protected void onDrawScheme(Canvas canvas, Calendar calendar, int x, int y, boolean isSelected) {
        int cx = x + mItemWidth / 2;
        int cy = y + mItemHeight / 2;
        canvas.drawCircle(cx, cy, mRadius, mSchemePaint);
    }

    @Override
    protected void onDrawText(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme, boolean isSelected) {
        float baselineY = mTextBaseLine + y;
        int cx = x + mItemWidth / 2;

        boolean isInRange = isInRange(calendar);
        boolean isEnable = !onCalendarIntercept(calendar);

        if (isSelected) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    mSelectTextPaint);
        } else if (hasScheme) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mSchemeTextPaint : mOtherMonthTextPaint);

        } else {
            canvas.drawText(String.valueOf(calendar.getDay()), cx, baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mCurMonthTextPaint : mOtherMonthTextPaint);
        }
    }
}

```

##### CustomMultiWeekView

```java
package com.lujianfei.other.ui.multi;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.MultiWeekView;

/**
 * 魅族周视图
 * Created by huanghaibin on 2017/11/29.
 */

public class CustomMultiWeekView extends MultiWeekView {
    private int mRadius;

    public CustomMultiWeekView(Context context) {
        super(context);
    }

    @Override
    protected void onPreviewHook() {
        mRadius = Math.min(mItemWidth, mItemHeight) / 5 * 2;
        mSchemePaint.setStyle(Paint.Style.STROKE);
    }


    @Override
    protected boolean onDrawSelected(Canvas canvas, Calendar calendar, int x, boolean hasScheme,
                                     boolean isSelectedPre, boolean isSelectedNext) {
        int cx = x + mItemWidth / 2;
        int cy = mItemHeight / 2;

        if (isSelectedPre) {
            if (isSelectedNext) {
                canvas.drawRect(x, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            } else {//最后一个，the last
                canvas.drawRect(x, cy - mRadius, cx, cy + mRadius, mSelectedPaint);
                canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
            }
        } else {
            if (isSelectedNext) {
                canvas.drawRect(cx, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            }
            canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);

        }
        return false;
    }

    @Override
    protected void onDrawScheme(Canvas canvas, Calendar calendar, int x, boolean isSelected) {
        int cx = x + mItemWidth / 2;
        int cy = mItemHeight / 2;
        canvas.drawCircle(cx, cy, mRadius, mSchemePaint);
    }

    @Override
    protected void onDrawText(Canvas canvas, Calendar calendar, int x, boolean hasScheme, boolean isSelected) {
        float baselineY = mTextBaseLine;
        int cx = x + mItemWidth / 2;
        boolean isInRange = isInRange(calendar);
        boolean isEnable = !onCalendarIntercept(calendar);
        if (isSelected) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    mSelectTextPaint);
        } else if (hasScheme) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mSchemeTextPaint : mOtherMonthTextPaint);

        } else {
            canvas.drawText(String.valueOf(calendar.getDay()), cx, baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mCurMonthTextPaint : mOtherMonthTextPaint);
        }
    }

}

```