# 范围选择日历 代码展示

以下只展示关键部分

先引用这个开源的日历库

```
https://gitee.com/huanghaibin_dev/CalendarView
```

然后就可以开始你的demo编写

##### fragment_range_calendar.xml 

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">
    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolbar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintEnd_toEndOf="parent"/>

    <TextView
        android:id="@+id/tv_year_month"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:padding="15dp"
        app:layout_constraintTop_toBottomOf="@id/toolbar"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        tools:text="2022年03月"
        android:textColor="@color/black"
        android:textSize="18sp"
        />

    <com.haibin.calendarview.CalendarView
        android:id="@+id/calendarView"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:layout_constraintTop_toBottomOf="@id/tv_year_month"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        android:background="#fff"
        app:calendar_height="46dp"
        app:calendar_padding="0dp"
        app:current_month_lunar_text_color="#CFCFCF"
        app:current_month_text_color="#4f4f4f"
        app:day_text_size="18sp"
        app:max_select_range="-1"
        app:min_select_range="-1"
        app:min_year="2010"
        app:month_view="com.lujianfei.other.ui.range.CustomRangeMonthView"
        app:month_view_show_mode="mode_only_current"
        app:other_month_text_color="#e1e1e1"
        app:scheme_text="假"
        app:scheme_text_color="#333"
        app:scheme_theme_color="#333"
        app:select_mode="range_mode"
        app:selected_text_color="#fff"
        app:selected_theme_color="#EE7621"
        app:week_background="#fff"
        app:week_text_color="#111"
        app:week_view="com.lujianfei.other.ui.range.CustomRangeWeekView"
        app:year_view_day_text_color="#333333"
        app:year_view_day_text_size="9sp"
        app:year_view_month_text_color="#ff0000"
        app:year_view_month_text_size="20sp"
        app:year_view_scheme_color="#f17706"
        />

    <TextView
        android:id="@+id/tv_start_year_month_lbl"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_marginStart="15dp"
        android:layout_marginTop="10dp"
        app:layout_constraintTop_toBottomOf="@id/calendarView"
        app:layout_constraintStart_toStartOf="parent"
        android:text="开始日期"
        android:textColor="@color/black"
        android:textSize="18sp"
        />

    <TextView
        android:id="@+id/tv_end_year_month_lbl"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintTop_toBottomOf="@id/calendarView"
        app:layout_constraintEnd_toEndOf="parent"
        android:layout_marginEnd="15dp"
        android:layout_marginTop="10dp"
        android:text="结束日期"
        android:textColor="@color/black"
        android:textSize="18sp"
        />

    <TextView
        android:id="@+id/tv_start_year_month"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintTop_toBottomOf="@id/tv_start_year_month_lbl"
        app:layout_constraintStart_toStartOf="@id/tv_start_year_month_lbl"
        tools:text="2022年03月"
        android:textColor="@color/black"
        android:textSize="18sp"
        />

    <TextView
        android:id="@+id/tv_end_year_month"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        app:layout_constraintTop_toBottomOf="@id/tv_end_year_month_lbl"
        app:layout_constraintEnd_toEndOf="@id/tv_end_year_month_lbl"
        tools:text="2022年03月"
        android:textColor="@color/black"
        android:textSize="18sp"
        />

</androidx.constraintlayout.widget.ConstraintLayout>
```

##### RangeCalendarFragment

```kotlin
package com.lujianfei.other.ui

import android.view.View
import android.widget.TextView
import com.haibin.calendarview.Calendar
import com.haibin.calendarview.CalendarView
import com.lujianfei.module_plugin_base.base.BaseFragment
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.other.R
import java.text.SimpleDateFormat
import java.util.*

/**
 * 简单范围选择日历
 */
class RangeCalendarFragment(delayInit: Boolean = false) : BaseFragment(delayInit) {

    var toolbar: PluginToolBar?= null
    private var bean: PluginActivityBean?= null
    var tv_year_month:TextView ?= null
    var tv_start_year_month:TextView ?= null
    var tv_end_year_month:TextView ?= null
    var calendarView:com.haibin.calendarview.CalendarView ?= null

    override fun resouceId() = R.layout.fragment_range_calendar

    override fun initView() {
        toolbar = view?.findViewById(R.id.toolbar)
        tv_year_month = view?.findViewById(R.id.tv_year_month)
        tv_start_year_month = view?.findViewById(R.id.tv_start_year_month)
        tv_end_year_month = view?.findViewById(R.id.tv_end_year_month)

        calendarView = view?.findViewById(R.id.calendarView)
    }

    override fun initData() {
        bean = arguments?.getParcelable("data")
        bean?.let {bean->
            toolbar?.setTitle(bean.itemName)
            toolbar?.getRightTopTextView()?.visibility = if (bean.codeUrl.isNullOrEmpty()) View.GONE else View.VISIBLE
        }
        val calendar = java.util.Calendar.getInstance()
        updateCurrentYearMonthDay(year = calendar.get(java.util.Calendar.YEAR), month = calendar.get(java.util.Calendar.MONTH) + 1)
    }

    override fun initEvent() {
        toolbar?.onBackListener = {
            activity?.finish()
        }
        calendarView?.setOnMonthChangeListener { year, month ->
            updateCurrentYearMonthDay(year, month)
        }
        calendarView?.setOnCalendarRangeSelectListener(object :
            CalendarView.OnCalendarRangeSelectListener {
            override fun onCalendarSelectOutOfRange(calendar: Calendar?) {
            }

            override fun onSelectOutOfRange(calendar: Calendar?, isOutOfMinRange: Boolean) {
            }

            override fun onCalendarRangeSelect(calendar: Calendar?, isEnd: Boolean) {
                updateRangeDate()
            }
        })
    }

    /**
     * 更新当前日期
     */
    private fun updateCurrentYearMonthDay(year: Int, month: Int) {
            tv_year_month?.text = "$year-$month"
    }

    /**
     * 更新当前日期
     */
    private fun updateRangeDate() {
        calendarView?.selectCalendarRange?.let { calendars->
            if (calendars.isNotEmpty()) {
                tv_start_year_month?.text =
                        SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Date(calendars[0].timeInMillis))
                tv_end_year_month?.text =
                        SimpleDateFormat("yyyy-MM-dd", Locale.US).format(Date(calendars[calendars.size-1].timeInMillis))
            }
        }
    }

    override fun updateTitle() {
    }

    override fun onSearchListener(key: String?) {
    }
}
```

##### CustomRangeMonthView
```java
package com.lujianfei.other.ui.range;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.RangeMonthView;

/**
 * 范围选择月视图
 * Created by huanghaibin on 2018/9/13.
 */

public class CustomRangeMonthView extends RangeMonthView {

    private int mRadius;

    public CustomRangeMonthView(Context context) {
        super(context);
    }


    @Override
    protected void onPreviewHook() {
        mRadius = Math.min(mItemWidth, mItemHeight) / 5 * 2;
        mSchemePaint.setStyle(Paint.Style.STROKE);
    }

    @Override
    protected boolean onDrawSelected(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme,
                                     boolean isSelectedPre, boolean isSelectedNext) {
        int cx = x + mItemWidth / 2;
        int cy = y + mItemHeight / 2;
        if (isSelectedPre) {
            if (isSelectedNext) {
                canvas.drawRect(x, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            } else {//最后一个，the last
                canvas.drawRect(x, cy - mRadius, cx, cy + mRadius, mSelectedPaint);
                canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
            }
        } else {
            if(isSelectedNext){
                canvas.drawRect(cx, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            }
            canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
            //
        }

        return false;
    }

    @Override
    protected void onDrawScheme(Canvas canvas, Calendar calendar, int x, int y, boolean isSelected) {
        int cx = x + mItemWidth / 2;
        int cy = y + mItemHeight / 2;
        canvas.drawCircle(cx, cy, mRadius, mSchemePaint);
    }

    @Override
    protected void onDrawText(Canvas canvas, Calendar calendar, int x, int y, boolean hasScheme, boolean isSelected) {
        float baselineY = mTextBaseLine + y;
        int cx = x + mItemWidth / 2;

        boolean isInRange = isInRange(calendar);
        boolean isEnable = !onCalendarIntercept(calendar);

        if (isSelected) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    mSelectTextPaint);
        } else if (hasScheme) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mSchemeTextPaint : mOtherMonthTextPaint);

        } else {
            canvas.drawText(String.valueOf(calendar.getDay()), cx, baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mCurMonthTextPaint : mOtherMonthTextPaint);
        }
    }
}

```

##### CustomRangeWeekView

```java
package com.lujianfei.other.ui.range;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.haibin.calendarview.Calendar;
import com.haibin.calendarview.RangeWeekView;

/**
 * 范围选择周视图
 * Created by huanghaibin on 2018/9/13.
 */

public class CustomRangeWeekView extends RangeWeekView {

    private int mRadius;

    public CustomRangeWeekView(Context context) {
        super(context);
    }

    @Override
    protected void onPreviewHook() {
        mRadius = Math.min(mItemWidth, mItemHeight) / 5 * 2;
        mSchemePaint.setStyle(Paint.Style.STROKE);
    }


    @Override
    protected boolean onDrawSelected(Canvas canvas, Calendar calendar, int x, boolean hasScheme,
                                     boolean isSelectedPre, boolean isSelectedNext) {
        int cx = x + mItemWidth / 2;
        int cy = mItemHeight / 2;

        if (isSelectedPre) {
            if (isSelectedNext) {
                canvas.drawRect(x, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            } else {//最后一个，the last
                canvas.drawRect(x, cy - mRadius, cx, cy + mRadius, mSelectedPaint);
                canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);
            }
        } else {
            if (isSelectedNext) {
                canvas.drawRect(cx, cy - mRadius, x + mItemWidth, cy + mRadius, mSelectedPaint);
            }
            canvas.drawCircle(cx, cy, mRadius, mSelectedPaint);

        }
        return false;
    }

    @Override
    protected void onDrawScheme(Canvas canvas, Calendar calendar, int x, boolean isSelected) {
        int cx = x + mItemWidth / 2;
        int cy = mItemHeight / 2;
        canvas.drawCircle(cx, cy, mRadius, mSchemePaint);
    }

    @Override
    protected void onDrawText(Canvas canvas, Calendar calendar, int x, boolean hasScheme, boolean isSelected) {
        float baselineY = mTextBaseLine;
        int cx = x + mItemWidth / 2;
        boolean isInRange = isInRange(calendar);
        boolean isEnable = !onCalendarIntercept(calendar);
        if (isSelected) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    mSelectTextPaint);
        } else if (hasScheme) {
            canvas.drawText(String.valueOf(calendar.getDay()),
                    cx,
                    baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mSchemeTextPaint : mOtherMonthTextPaint);

        } else {
            canvas.drawText(String.valueOf(calendar.getDay()), cx, baselineY,
                    calendar.isCurrentDay() ? mCurDayTextPaint :
                            calendar.isCurrentMonth() && isInRange && isEnable? mCurMonthTextPaint : mOtherMonthTextPaint);
        }
    }
}

```