# 腾讯开屏广告 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
    // 腾讯广告
    implementation 'com.qq.e.union:union:4.430.1300'
}
```

##### thirdparty_splash_ad.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">
    <RelativeLayout
        android:id="@+id/splash_main"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        >

        <TextView
            android:id="@+id/logo"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:gravity="center"
            android:layout_centerHorizontal="true"
            android:textColor="@color/global_textcolor"
            android:textSize="20sp"
            android:layout_marginTop="200dp"
            android:text="码 农 宝"
            />
        <ProgressBar
            android:id="@+id/progressBar"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:layout_below="@id/logo"
            android:layout_marginTop="30dp"
            android:layout_centerHorizontal="true"
            style="?android:attr/progressBarStyleInverse"
            android:indeterminateDrawable="@drawable/progress_small"
            android:visibility="visible"
            />

        <!-- 3.开屏广告容器区域：-->
        <!-- 注意：该区域高度不得小于400dp。在本示例中没有写死splash_container的高度值，是因为第1部分的app_logo区域是一个高度很小的图片。 -->
        <FrameLayout
            android:id="@+id/splash_container"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            />
    </RelativeLayout>
</FrameLayout>
```

##### ThirdPartySplashActivity.kt

```kotlin
package com.lujianfei.thirdparty.ui

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.lujianfei.module_plugin_base.Constant
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.*
import com.lujianfei.module_plugin_base.utils.splash.SplashZoomOutManager
import com.lujianfei.thirdparty.R
import com.qq.e.ads.splash.SplashAD
import com.qq.e.ads.splash.SplashADListener
import com.qq.e.ads.splash.SplashADZoomOutListener
import com.qq.e.comm.util.AdError
import java.util.*

class ThirdPartySplashActivity: AppCompatActivity(), SplashADZoomOutListener {
    companion object {
        const val TAG = "ThirdPartySplashActivity"
    }

    private var splashAD: SplashAD? = null
    private var container: ViewGroup? = null

    var canJump = false
    private var needStartDemoList = true

    private var loadAdOnly = false
    private var showingAd = false

    /**
     * 为防止无广告时造成视觉上类似于"闪退"的情况，设定无广告时页面跳转根据需要延迟一定时间，demo
     * 给出的延时逻辑是从拉取广告开始算开屏最少持续多久，仅供参考，开发者可自定义延时逻辑，如果开发者采用demo
     * 中给出的延时逻辑，也建议开发者考虑自定义minSplashTimeWhenNoAD的值（单位ms）
     */
    private var minSplashTimeWhenNoAD = 2000

    /**
     * 记录拉取广告的时间
     */
    private var fetchSplashADTime: Long = 0
    private var handler = Handler(Looper.getMainLooper())

    private var isZoomOut = false
    private var isSupportZoomOut = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.apply {
            addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
            addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        }
        setContentView(R.layout.thirdparty_splash_ad)
        initView()
    }

    fun initView() {
        container = findViewById(R.id.splash_container)
        fetchSplashAD(this, adContainer = container, posId = getPosId(), adListener = this, fetchDelay = 0)
    }

    /**
     * 拉取开屏广告，开屏广告的构造方法有3种，详细说明请参考开发者文档。
     *
     * @param activity        展示广告的activity
     * @param adContainer     展示广告的大容器
     * @param skipContainer   自定义的跳过按钮：传入该view给SDK后，SDK会自动给它绑定点击跳过事件。SkipView的样式可以由开发者自由定制，其尺寸限制请参考activity_splash.xml或者接入文档中的说明。
     * @param posId           广告位ID
     * @param adListener      广告状态监听器
     * @param fetchDelay      拉取广告的超时时长：取值范围[3000, 5000]，设为0表示使用广点通SDK默认的超时时长。
     */
    private fun fetchSplashAD(activity: Activity, adContainer: ViewGroup?,
                              posId: String?="", adListener: SplashADListener, fetchDelay: Int) {
        fetchSplashADTime = System.currentTimeMillis()
        splashAD = SplashAD(activity, posId, adListener, fetchDelay)
        if (loadAdOnly) {
            splashAD?.fetchAdOnly()
        } else {
            splashAD?.fetchAndShowIn(adContainer)
        }
    }

    /**
     * 设置一个变量来控制当前开屏页面是否可以跳转，当开屏广告为普链类广告时，点击会打开一个广告落地页，此时开发者还不能打开自己的App主页。当从广告落地页返回以后，
     * 才可以跳转到开发者自己的App主页；当开屏广告是App类广告时只会下载App。
     */
    private fun next() {
        if (canJump) {
            if (needStartDemoList) {
                LogUtils.d(TAG, "zoomOut isZoomOut:$isZoomOut")
                if (isZoomOut) {
                    val zoomOutManager = SplashZoomOutManager.getInstance()
                    zoomOutManager.setSplashInfo(splashAD, container!!.getChildAt(0),
                        window.decorView)
                }
                startMainActivity()
            }
            finish()
            if (isZoomOut) {
                overridePendingTransition(0, 0)
            }
        } else {
            canJump = true
        }
    }

    private fun getPosId() = "4041059253941391"

    override fun isSupportZoomOut() = isSupportZoomOut

    override fun onADPresent() {
        LogUtils.d(TAG, "onADPresent")
    }

    override fun onADLoaded(expireTimestamp: Long) {
        LogUtils.d(TAG, "onADLoaded expireTimestamp = $expireTimestamp")
    }

    override fun onZoomOut() {
        LogUtils.d(TAG, "onZoomOut")
        isZoomOut = true
        next()
    }

    override fun onADExposure() {
        LogUtils.d(TAG, "onADExposure")
    }

    override fun onADDismissed() {
        LogUtils.d(TAG, "onADDismissed")
        next()
    }

    override fun onPause() {
        super.onPause()
        canJump = false
    }

    override fun onResume() {
        super.onResume()
        if (canJump) {
            next();
        }
        canJump = true;
    }

    /**
     * 开屏页一定要禁止用户对返回按钮的控制，否则将可能导致用户手动退出了App而广告无法正常曝光和计费
     */
    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return if (keyCode == KeyEvent.KEYCODE_BACK || keyCode == KeyEvent.KEYCODE_HOME) {
            if (keyCode == KeyEvent.KEYCODE_BACK) {
                super.onKeyDown(keyCode, event)
            } else true
        } else super.onKeyDown(keyCode, event)
    }

    override fun onNoAD(error: AdError?) {
        val text = "onNoAD code:${error?.errorCode}, errorMsg:${error?.errorMsg}"
        if (loadAdOnly && !showingAd) {
            return  //只拉取广告时，不终止activity
        }
        /**
         * 为防止无广告时造成视觉上类似于"闪退"的情况，设定无广告时页面跳转根据需要延迟一定时间，demo
         * 给出的延时逻辑是从拉取广告开始算开屏最少持续多久，仅供参考，开发者可自定义延时逻辑，如果开发者采用demo
         * 中给出的延时逻辑，也建议开发者考虑自定义minSplashTimeWhenNoAD的值
         */
        val alreadyDelayMills = System.currentTimeMillis() - fetchSplashADTime //从拉广告开始到onNoAD已经消耗了多少时间

        val shouldDelayMills = if (alreadyDelayMills > minSplashTimeWhenNoAD) 0 else minSplashTimeWhenNoAD
        - alreadyDelayMills //为防止加载广告失败后立刻跳离开屏可能造成的视觉上类似于"闪退"的情况，根据设置的minSplashTimeWhenNoAD

        // 计算出还需要延时多久
        handler.postDelayed({
            if (needStartDemoList) {
                startMainActivity()
            }
            finish()
        }, shouldDelayMills.toLong())
    }

    override fun onADClicked() {
        LogUtils.d(TAG, "onADClicked")
        EventHelper.eventUpload(this, EventHelper.EventIds.SplashAdClick)
    }

    override fun onADTick(millisUntilFinished: Long) {
        LogUtils.d(TAG, "onADTick $millisUntilFinished")
    }

    override fun onZoomOutPlayFinish() {
        LogUtils.d(TAG, "onZoomOutPlayFinish")
    }

    private fun startMainActivity() {
        //
    }

    override fun onDestroy() {
        super.onDestroy()
        handler.removeCallbacksAndMessages(null);
    }
}
```
