# 腾讯信息流/详情页插入/视频贴片 广告 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
    // 腾讯广告
    implementation 'com.qq.e.union:union:4.430.1300'
}
```

##### thirdparty_native_express_ad.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">
    <com.lujianfei.module_plugin_base.widget.PluginToolBar
        android:id="@+id/toolBar"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>


    <FrameLayout
        android:id="@+id/bannerContainer"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>


    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/loadAdButton"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="加载广告"
        android:layout_marginTop="10dp"
        android:layout_marginStart="10dp"
        android:layout_marginEnd="10dp"
        />
</LinearLayout>
```

##### ThirdPartyNativeExpressAdActivity.kt
package com.lujianfei.thirdparty.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import com.lujianfei.module_plugin_base.Constant
import com.lujianfei.module_plugin_base.base.BaseActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.utils.LogUtils
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.thirdparty.R
import com.qq.e.ads.cfg.VideoOption
import com.qq.e.ads.nativ.ADSize
import com.qq.e.ads.nativ.NativeExpressAD
import com.qq.e.ads.nativ.NativeExpressAD.NativeExpressADListener
import com.qq.e.ads.nativ.NativeExpressADView
import com.qq.e.comm.util.AdError


/**
 * Author: mac
 * Date: 24.11.21 11:55 上午
 * Description: 腾讯信息流/详情页插入/视频贴片 广告
 */
class ThirdPartyNativeExpressAdActivity: BaseActivity(),
    NativeExpressADListener {

    companion object {
        const val TAG = "ThirdPartyNativeExpressAdActivity="
        fun start(context: Context?, bean: PluginActivityBean ?= null) {
            context?.startActivity(Intent(context, ThirdPartyNativeExpressAdActivity::class.java).putExtra("data",bean))
        }
    }

    var bannerContainer: ViewGroup?= null
    var mNativeExpressAdView: NativeExpressADView?= null
    var nativeExpressAD: NativeExpressAD?= null

    var toolBar:PluginToolBar ?= null
    var loadAdButton:Button ?= null
    var bean: PluginActivityBean? = null

    override fun resouceId() = R.layout.thirdparty_native_express_ad

    override fun initView(savedInstanceState: Bundle?) {
        toolBar = findViewById(R.id.toolBar)
        bannerContainer = findViewById(R.id.bannerContainer)
        loadAdButton = findViewById(R.id.loadAdButton)
    }

    override fun initData() {
        bean = intent?.getParcelableExtra("data")
        bean?.let { bean->
            toolBar?.setTitle(bean.itemName)
            toolBar?.getRightTopTextView()?.visibility = if (bean.codeUrl.isNullOrEmpty()) View.GONE else View.VISIBLE
        }
    }

    override fun initEvent() {
        toolBar?.onBackListener = {
            finish()
        }
        toolBar?.onRightTopListener = {
            openBrowser()
        }
        loadAdButton?.setOnClickListener {
            refreshAd()
        }
    }

    private fun closeAd() {
        bannerContainer?.removeAllViews()
        mNativeExpressAdView?.destroy()
        mNativeExpressAdView = null
    }

    private fun openBrowser() {
        bean?.let {bean->
            val uri = Uri.parse(bean.codeUrl)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            startActivity(intent)
        }
    }

    private fun getPosID() = Constant.ConfigInfo.NativeExpressPosID

    // 仅展示部分代码，完整代码请参考 GDTUnionDemo 工程
    // 1.加载广告，先设置加载上下文环境和条件
    private fun refreshAd() {
        LogUtils.d(TAG, "refreshAd")
        if (nativeExpressAD == null) {
            nativeExpressAD = NativeExpressAD(
                this,
                ADSize(340, ADSize.AUTO_HEIGHT),
                getPosID(),
                this
            ) // 传入Activity
            // 注意：如果您在平台上新建平台模板广告位时，选择了支持视频，那么可以进行个性化设置（可选）
            nativeExpressAD?.setVideoOption(
                VideoOption.Builder()
                    .setAutoPlayPolicy(VideoOption.AutoPlayPolicy.WIFI) // WIFI 环境下可以自动播放视频
                    .setAutoPlayMuted(true) // 自动播放时为静音
                    .build()
            ) //
        }
        nativeExpressAD?.loadAD(1)
    }

    override fun onNoAD(error: AdError?) {
        LogUtils.d(TAG, "onNoAD ${error?.errorCode} ${error?.errorMsg}")
    }

    override fun onADLoaded(adList: MutableList<NativeExpressADView>?) {
        LogUtils.d(TAG, "onADLoaded $adList")
        if (adList.isNullOrEmpty()) {
            return
        }
        mNativeExpressAdView?.destroy()
        if (mNativeExpressAdView != null) {
            bannerContainer?.removeAllViews()
        }
        if (mNativeExpressAdView == null) {
            mNativeExpressAdView = adList.firstOrNull()
            mNativeExpressAdView?.render()
        }
        bannerContainer?.addView(mNativeExpressAdView)
    }

    override fun onRenderFail(adView: NativeExpressADView?) {
        LogUtils.d(TAG, "onRenderFail adView:$adView")
    }

    override fun onRenderSuccess(adView: NativeExpressADView?) {
        LogUtils.d(TAG, "onRenderSuccess adView:$adView")
    }

    override fun onADExposure(adView: NativeExpressADView?) {
        LogUtils.d(TAG, "onADExposure adView:$adView")
    }

    override fun onADClicked(adView: NativeExpressADView?) {
        LogUtils.d(TAG, "onADClicked adView:$adView")
    }

    override fun onADClosed(adView: NativeExpressADView?) {
        LogUtils.d(TAG, "onADClosed adView:$adView")
    }

    override fun onADLeftApplication(adView: NativeExpressADView?) {
        LogUtils.d(TAG, "onADLeftApplication adView:$adView")
    }
}
```
