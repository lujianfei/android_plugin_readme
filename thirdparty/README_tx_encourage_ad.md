# 腾讯激励广告 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
    // 腾讯广告
    implementation 'com.qq.e.union:union:4.430.1300'
}
```

##### thirdparty_rewardvideo_ad.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">
    

    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/loadAdButton"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="加载激励广告"
        android:layout_marginTop="10dp"
        android:layout_marginStart="10dp"
        android:layout_marginEnd="10dp"
        />

    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/showAdButton"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="显示激励广告"
        android:layout_marginTop="10dp"
        android:layout_marginStart="10dp"
        android:layout_marginEnd="10dp"
        />
</LinearLayout>
```

##### ThirdPartyEncourageAdActivity.kt

```kotlin
package com.lujianfei.thirdparty.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.lujianfei.module_plugin_base.Constant
import com.lujianfei.module_plugin_base.base.BaseActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.thirdparty.R
import com.qq.e.ads.rewardvideo.RewardVideoAD
import com.qq.e.ads.rewardvideo.RewardVideoADListener
import com.qq.e.ads.rewardvideo.ServerSideVerificationOptions
import com.qq.e.comm.util.AdError
import java.util.*

/**
 * Author: mac
 * Date: 24.11.21 11:55 上午
 * Description: 激励广告
 */
class ThirdPartyEncourageAdActivity: BaseActivity(), RewardVideoADListener {

    companion object {
        const val TAG = "EncourageAdActivity="       
    }

    var mRewardVideoAD: RewardVideoAD?= null
    var adLoaded:Boolean = false
    var videoCached:Boolean = false

    var loadAdButton:Button ?= null
    var showAdButton:Button ?= null
  
    override fun resouceId() = R.layout.thirdparty_rewardvideo_ad

    override fun initView(savedInstanceState: Bundle?) {
        toolBar = findViewById(R.id.toolBar)
        loadAdButton = findViewById(R.id.loadAdButton)
        showAdButton = findViewById(R.id.showAdButton)
        // 仅展示部分代码，完整代码请参考 Demo 工程
        // 1.加载广告，先设置加载上下文环境和条件
        // 如果想静音播放，请使用5个参数的构造函数，且volumeOn传false即可
        mRewardVideoAD = RewardVideoAD(this, getPosID(), this, false) // 无声播放
    }

    override fun initData() {
       
    }

    override fun initEvent() {      
        loadAdButton?.setOnClickListener {
            mRewardVideoAD?.loadAD()
        }
        showAdButton?.setOnClickListener {
            mRewardVideoAD?.showAD(this@ThirdPartyEncourageAdActivity)
        }
    }  

    private fun getPosID() = "2022464324599593"

    override fun onADLoad() {
        adLoaded = true
        mRewardVideoAD?.expireTimestamp?.let { getExpireTimestamp->
            val msg = "加载广告成功 ! 过期时间: " + Date(getExpireTimestamp)
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
        }
    }

    /**
     * 视频素材缓存成功，可在此回调后进行广告展示
     */
    override fun onVideoCached() {
        videoCached = true;
        Log.i(TAG, "onVideoCached");
    }

    /**
     * 激励视频广告页面展示
     */
    override fun onADShow() {
        Log.i(TAG, "onADShow")
    }

    /**
     * 激励视频广告曝光
     */
    override fun onADExpose() {
        Log.i(TAG, "onADExpose")
    }

    /**
     * 激励视频触发激励（观看视频大于一定时长或者视频播放完毕）
     *
     * @param map 若选择了服务端验证，可以通过 ServerSideVerificationOptions#TRANS_ID 键从 map 中获取此次交易的 id；若未选择服务端验证，则不需关注 map 参数。
     */
    override fun onReward(map: MutableMap<String, Any>?) {
        Log.i(TAG, "onReward " + map?.get(ServerSideVerificationOptions.TRANS_ID))  // 获取服务端验证的唯一 ID
    }

    /**
     * 激励视频广告被点击
     */
    override fun onADClick() {
        Log.i(TAG, "onADClick")
    }

    /**
     * 激励视频播放完毕
     */
    override fun onVideoComplete() {
        Log.i(TAG, "onVideoComplete")
    }

    /**
     * 激励视频广告被关闭
     */
    override fun onADClose() {
        Log.i(TAG, "onADClose")
    }

    /**
     * 广告流程出错
     */
    override fun onError(adError: AdError?) {
        val msg = String.format(Locale.getDefault(), "onError, error code: %d, error msg: %s",
        adError?.errorCode, adError?.errorMsg
        )
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }
}
```
