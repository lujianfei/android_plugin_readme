# 腾讯插屏广告 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
    // 腾讯广告
    implementation 'com.qq.e.union:union:4.430.1300'
}
```

##### thirdparty_rewardvideo_ad.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">
    

    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/loadAdButton"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="加载激励广告"
        android:layout_marginTop="10dp"
        android:layout_marginStart="10dp"
        android:layout_marginEnd="10dp"
        />

    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/showAdButton"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="显示激励广告"
        android:layout_marginTop="10dp"
        android:layout_marginStart="10dp"
        android:layout_marginEnd="10dp"
        />
</LinearLayout>
```

##### ThirdPartyInterstitialAdActivity.kt

```kotlin
package com.lujianfei.thirdparty.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import com.lujianfei.module_plugin_base.Constant
import com.lujianfei.module_plugin_base.base.BaseActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.thirdparty.R
import com.qq.e.ads.interstitial2.UnifiedInterstitialAD
import com.qq.e.ads.interstitial2.UnifiedInterstitialADListener
import com.qq.e.ads.interstitial2.UnifiedInterstitialMediaListener
import com.qq.e.comm.util.AdError

/**
 * Author: mac
 * Date: 24.11.21 11:55 上午
 * Description: 插屏广告
 */
class ThirdPartyInterstitialAdActivity: BaseActivity(), UnifiedInterstitialADListener,
    UnifiedInterstitialMediaListener {

    companion object {
        const val TAG = "InterstitialAdActivity="        
    }

    var mUnifiedInterstitialAD: UnifiedInterstitialAD?= null
    var isRenderFail = false

    var toolBar:PluginToolBar ?= null
    var loadAdButton:Button ?= null
    var showAdButton:Button ?= null
    var bean: PluginActivityBean? = null

    override fun resouceId() = R.layout.thirdparty_interstitial_ad

    override fun initView(savedInstanceState: Bundle?) {
        toolBar = findViewById(R.id.toolBar)
        loadAdButton = findViewById(R.id.loadAdButton)
        showAdButton = findViewById(R.id.showAdButton)
        // 仅展示部分代码，完整代码请参考 Demo 工程
        // 1.加载广告，先设置加载上下文环境和条件
        // 如果想静音播放，请使用5个参数的构造函数，且volumeOn传false即可
        mUnifiedInterstitialAD = UnifiedInterstitialAD(this, getPosID(), this)
    }
    

    override fun initEvent() {       
        loadAdButton?.setOnClickListener {
            loadAd()
        }
        showAdButton?.setOnClickListener {
            showAd()
        }
    }

    private fun loadAd() {
        if (mUnifiedInterstitialAD != null) {
            mUnifiedInterstitialAD?.close()
            mUnifiedInterstitialAD?.destroy()
        }
        mUnifiedInterstitialAD?.setMediaListener(this)
        mUnifiedInterstitialAD?.loadAD()
    }

    private fun showAd() {
        if (mUnifiedInterstitialAD != null && mUnifiedInterstitialAD?.isValid == true && !isRenderFail) {
            mUnifiedInterstitialAD?.show()
        } else {
            Toast.makeText(this, "请加载广告并渲染成功后再进行展示 ！ ", Toast.LENGTH_LONG).show()
        }
    }

   
    private fun getPosID() = "4002460421659646"


    ////==================== UnifiedInterstitialADListener ====================////////
    override fun onADReceive() {
        Log.i(TAG, "onADReceive")
    }

    override fun onVideoCached() {
        Log.i(TAG, "onVideoCached")
    }

    override fun onNoAD(p0: AdError?) {
        Log.i(TAG, "onNoAD")
    }

    override fun onADOpened() {
        Log.i(TAG, "onADOpened")
    }

    override fun onADExposure() {
        Log.i(TAG, "onADExposure")
    }

    override fun onADClicked() {
        Log.i(TAG, "onADClicked")
    }

    override fun onADLeftApplication() {
        Log.i(TAG, "onADLeftApplication")
    }

    override fun onADClosed() {
        Log.i(TAG, "onADClosed")
    }

    override fun onRenderSuccess() {
	      Log.i(TAG, "onRenderSuccess")
				Toast.makeText(this, "加载完成", Toast.LENGTH_SHORT).show()
    }

    override fun onRenderFail() {
        Log.i(TAG, "onRenderFail")
        isRenderFail = true
    }
    ////==================== UnifiedInterstitialADListener end ====================////////


    ////==================== UnifiedInterstitialMediaListener ====================////////
    override fun onVideoInit() {
        Log.i(TAG, "onVideoInit")
    }

    override fun onVideoLoading() {
        Log.i(TAG, "onVideoLoading")
    }

    override fun onVideoReady(p0: Long) {
        Log.i(TAG, "onVideoReady")
    }

    override fun onVideoStart() {
        Log.i(TAG, "onVideoStart")
    }

    override fun onVideoPause() {
        Log.i(TAG, "onVideoPause")
    }

    override fun onVideoComplete() {
        Log.i(TAG, "onVideoComplete")
    }

    override fun onVideoError(p0: AdError?) {
        Log.i(TAG, "onVideoError")
    }

    override fun onVideoPageOpen() {
        Log.i(TAG, "onVideoPageOpen")
    }

    override fun onVideoPageClose() {
        Log.i(TAG, "onVideoPageClose")
    }

    ////==================== UnifiedInterstitialMediaListener end ====================////////
}
```
