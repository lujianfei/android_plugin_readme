# 腾讯 Banner 广告 代码展示

以下只展示关键部分

#### gradle 依赖引用

```javascript
dependencies {
    // 腾讯广告
    implementation 'com.qq.e.union:union:4.430.1300'
}
```

##### thirdparty_rewardvideo_ad.xml

```xml
<?xml version="1.0" encoding="utf-8
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical">
    
    <FrameLayout
        android:id="@+id/bannerContainer"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"/>


    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/loadAdButton"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="加载广告"
        android:layout_marginTop="10dp"
        android:layout_marginStart="10dp"
        android:layout_marginEnd="10dp"
        />

    <com.lujianfei.module_plugin_base.widget.PluginButton
        android:id="@+id/closeAdButton"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:text="关闭广告"
        android:layout_marginTop="10dp"
        android:layout_marginStart="10dp"
        android:layout_marginEnd="10dp"
        />
</LinearLayout>
```

##### ThirdPartyBannerAdActivity.kt

```kotlin
package com.lujianfei.thirdparty.ui

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import com.lujianfei.module_plugin_base.Constant
import com.lujianfei.module_plugin_base.base.BaseActivity
import com.lujianfei.module_plugin_base.beans.PluginActivityBean
import com.lujianfei.module_plugin_base.widget.PluginToolBar
import com.lujianfei.thirdparty.R
import com.qq.e.ads.banner2.UnifiedBannerADListener
import com.qq.e.ads.banner2.UnifiedBannerView
import com.qq.e.comm.util.AdError

/**
 * Author: mac
 * Date: 24.11.21 11:55 上午
 * Description: Banner 广告
 */
class ThirdPartyBannerAdActivity: BaseActivity(),
    UnifiedBannerADListener {

    companion object {
        const val TAG = "BannerAdActivity="
    }

    var bannerContainer: ViewGroup?= null
    var mUnifiedBannerView: UnifiedBannerView?= null

    var loadAdButton:Button ?= null
    var closeAdButton:Button ?= null

    override fun resouceId() = R.layout.thirdparty_banner_ad

    override fun initView(savedInstanceState: Bundle?) {       
        bannerContainer = findViewById(R.id.bannerContainer)
        loadAdButton = findViewById(R.id.loadAdButton)
        closeAdButton = findViewById(R.id.closeAdButton)
    }

    override fun initEvent() {       
        loadAdButton?.setOnClickListener {
            loadAd()
        }
        closeAdButton?.setOnClickListener {
            closeAd()
        }
    }

    private fun loadAd() {
        if (mUnifiedBannerView == null) {
            mUnifiedBannerView = UnifiedBannerView(this, getPosID(), this)
        }
        if (mUnifiedBannerView != null) {
            bannerContainer?.removeView(mUnifiedBannerView)
            mUnifiedBannerView?.destroy()
        }
        bannerContainer?.addView(mUnifiedBannerView)
        mUnifiedBannerView?.loadAD()
    }

    private fun closeAd() {
        bannerContainer?.removeAllViews()
        mUnifiedBannerView?.destroy()
        mUnifiedBannerView = null
    }   

    private fun getPosID() = "5012060401555617"


    ////==================== UnifiedBannerADListener ====================////////
    override fun onNoAD(p0: AdError?) {
        Log.i(TAG, "onNoAD")
    }

    override fun onADReceive() {
        Log.i(TAG, "onADReceive")
    }

    override fun onADExposure() {
        Log.i(TAG, "onADExposure")
    }

    override fun onADClosed() {
        Log.i(TAG, "onADClosed")
    }

    override fun onADClicked() {
        Log.i(TAG, "onADClicked")
    }

    override fun onADLeftApplication() {
        Log.i(TAG, "onADLeftApplication")
    }

    override fun onADOpenOverlay() {
        Log.i(TAG, "onADOpenOverlay")
    }

    override fun onADCloseOverlay() {
        Log.i(TAG, "onADCloseOverlay")
    }
    ////==================== UnifiedBannerADListener end ====================////////

    override fun onDestroy() {
        closeAd()
        super.onDestroy()
    }
}
```
